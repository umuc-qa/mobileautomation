/**
 * 
 */
package com.project.ClassicObjectRepository;

import org.openqa.selenium.By;

/**
 * @author E001880
 *
 */
public class CalendarObjects {

	//public static String CreateEvent = "//*[@id='CreateEventButtonId' and @text='Create Event']";
	public static String EventTitle = "//*[@placeholder='Enter Event Title' or @id='Title' or @nodeName=INPUT]";
	public static String AttendeeId = "//*[@id='AttendeeId']";
	public static String Select_AttendeeId = "//*[@text='Everybody in the Course Offering']";

	//public static String AllDay = "//*[@nodeName='INPUT' and @id='EventsData$DurationTypeId' and @type='checkbox']";

	public static String TestStartDate = "//*[@id='EventsData$StartDateTimeId']";
	public static String TestEndDate = "//*[@id='EventsData$EndDateTimeId']";
	public static String Location = "//*[@id='EventsData$LocationString']";
	//public static String CreateButton = "//*[@nodeName='BUTTON' and @text='Create' and @type='submit']";
	//public static String SearchEvents = "//*[@nodeName='INPUT' and @placeholder='Search Events' and @type='search' and @class='vui-input' and @onScreen='true']";
	//public static String SearchButton = "(//*[@nodeName='INPUT' and @type='button' and @value='Search' and @class='vui-input-search-button' and @onScreen='true'])[1]";

	//public static String ClickEvent = "//*[@nodeName='LI' and @class='d2l-datalist-item d2l-datalist-item-actionable d2l-datalist-checkboxitem d2l-datalist-lastchild' and @onScreen='true']";
	public static String EditEvent = "//*[@nodeName='A' and ./*[@text='Edit Event']]";
	//public static String SaveButton = "//*[@text='Save']";
	//public static String DescriptionText = "//*[@id='Description$html_iframecontainer' or @nodeName='P' and @iframeId='Description$html_ifr' and @onScreen='true' and @visible='true']";
//	public static String YesButton = "//*[@nodeName='BUTTON' and @text='Yes' and @onScreen='true' and @visible='true']";

	//new paths
	public static String CreateEvent = "//*[text()='Create Event']";
	public static String DescriptionText = "//body[@id='tinymce']";
	public static String frameDescription = "Description$html_ifr";
	public static String AllDay = "//input[@id='EventsData$DurationTypeId']";
	public static String CreateButton = "//*[text()='Create']";
	public static String msgCreatedSuccessfully = "//*[text()='Created Successfully']";
	public static String SearchEvents = "//input[@aria-label='Search']";
	public static String SearchButton = "//button[@title='Search']";
	public static String ClickEvent = "//div[text()='ssssample']";
	public static String SaveButton = "//*[text()='Save']";
	public static String ddDelete = "//span[text()='Delete Event']";
	public static String YesButton = "//button[text()='Yes']";
	public static String msgDeletedSuccessfully = "//*[text()='Deleted Successfully']";
	
	public static By d2LmenuItemLinktextName(String text)
    {
    	By lnkName=By.xpath("//d2l-menu-item[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))]");
    	return lnkName;
    }
	
}
