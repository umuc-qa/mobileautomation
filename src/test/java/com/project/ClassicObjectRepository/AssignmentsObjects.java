/**
 * 
 */
package com.project.ClassicObjectRepository;

import org.openqa.selenium.By;

/***
 * 
 * @author E001115
 *
 */
public class AssignmentsObjects {

	public static String Select_A_Course_Icon = "//*[@nodeName='D2L-ICON' and ./parent::*[@nodeName='BUTTON'] and ./following-sibling::*[@text='Select a course...']]";
	
	
	public static String Search_Box = "//input[@placeholder='Search for a course']";
	//public static String Search_Icon = "//*[@value='Search']";
	// Mahender
	//public static String Search_Icon = "//*[@value='Search']";
	public static String Search_Icon = "//d2l-icon[@icon='d2l-tier1:search']";
	public static String List_Item = "//*[contains(@text,'1950')][1]";
	//public static String Menu_Bar_Icon = "//*[@nodeName='D2L-ICON' and ./parent::*[@nodeName='BUTTON'] and ./following-sibling::*[@text='menu']]";
	
	//public static String Menu_Bar_Icon = "//div[@class='d2l-navigation-s-mobile-menu-course-selector']//button[@title='Select a course...']";
	public static String Menu_Bar_Icon = "(//d2l-navigation-button-notification-icon[@text='Select a course...'])[last()]";
	
	//public static String Menu_Bar_Icon1 = "//button[@class='d2l-navigation-s-button-highlight']//d2l-icon[@icon='d2l-tier3:menu-hamburger']";
	//public static String Menu_Bar_Icon1 = "//button[@class='d2l-focusable style-scope d2l-navigation-button']//d2l-icon[@icon='d2l-tier3:menu-hamburger']";
	
	
	//public static String Menu_Bar_Icon1 = "//div[contains(@class,'d2l-navigation-s-header-open-button-wrapper')]//d2l-navigation-button-notification-icon[@icon='d2l-tier3:menu-hamburger']";
	
	public static String Menu_Bar_Icon1 = "//div[contains(@class,'d2l-navigation-s-header-open-button-wrapper')]";
	
	public static String MobileSideMenuNavigation = "//div[contains(@class,'d2l-navigation-s-mobile-menu-nav')]//d2l-menu[@active='' and @shown ]";
	
	public static String Assignments_Option = "//*[@text='Assignments']";
//	public static String New_Submission_Folder = "//*[@text='New Submission Folder']";
	public static String New_Submission_Folder = "//*[text()='New Assignment']";
	public static String New_Assignment = "//*[text()='New Assignment']";
	public static String Assignment_Name_TextBox = "//*[@id='nameId']";
	//public static String New_Category_Link = "//*[@text='[New Category]']";
	public static String New_Category_Link = "//*[text()='[New Category]']";
	
	
	public static String Category_Name_TextBox = "//*[@id='z_d' and @name='name']";
	//public static String Category_Save_Button = "//*[@text='Save' and ./parent::*[@nodeName='TD']]";
	public static String Category_Save_Button = "//table[@class='d2l-dialog-buttons']//button[text()='Save']";
	
	
	//public static String New_Grade_Link = "//*[@text='[New Grade Item]']";
	public static String New_Grade_Link = "//a[text()='[New Grade Item]']";
	public static String btnMoreActions = "//span[text()='More Actions']";
	//public static String txtDelete = "//*[text()='Delete']";
	
	public static String txtDelete = "//d2l-menu-item[@text='Delete']";
	
	public static String Grade_Name_TextBox = "//*[@id='z_c' and @type='text']";
	//public static String Grade_Save_Button = "//*[text()='Save']";	
	public static String Grade_Save_Button = "//table[@class='d2l-dialog-buttons']//*[text()='Save']";
	
	public static String Out_Of_Box_Score = "//*[@id='z_w']";
	
	//public static String txtOut_Of_Box_Score = "//input[@id='z_bv']";
	public static String txtOut_Of_Box_Score = "//span[contains(text(),'Out Of')]/following::input[@type='text'][1]";
	//span[contains(text(),'Out Of')]/following::input[@type='text'][1]
	//public static String Add_Rubrics = "//*[@nodeName='BUTTON' and @text='Add Rubric']";
	
	//public static String Selecting_Rubric = "//*[@value='i2_29906']";
	public static String Instructions_TextBox = "//*[@id='instructions$html_ifr']";
	public static String txtInstructionsTextBox = "//body[@id='tinymce']";
	//public static String Save_And_Close_Button = "//*[@id='z_a' and @text='Save and Close']";
	public static String Save_And_Close_Button = "//*[text()='Save and Close']";
	
	public static String Created_SuccessFully_Text = "//*[@text='Created successfully']";
	//public static String Saved_SuccessFully_Text = "//*[@text='Saved successfully']";
	public static String msgsavedSuccesfullyText = "//*[text()='Saved successfully']";
	//public static String My_Tools_Option = "//*[@text='My Tools']";
	public static String My_Tools_Option = "//div[@class='d2l-navigation-s-mobile-menu-nav']//span[@class='style-scope d2l-menu-item'and text()='My Tools']";
	public static String Grades_Option = "//*[@text='Grades']";
	public static String Manage_Grades_Button = "//a[text()='Manage Grades']";

	public static String ConditionType_DropDown = "//*[@name='z_b']";
	public static String GradeItem_DropDown = "//*[@name='GradeObjects']";
	public static String Grade_score = "//*[@id='z_bc']";
	//public static String Create_Button = "//*[@nodeName='BUTTON' and @text='Create']";
	//public static String Delete_Button = "//*[@nodeName='BUTTON' and @text='Delete']";
	public static String Delete_Button = "//button[text()='Delete']";
	public static String btnConfirmDelete = "//table//button[text()='Delete']";
	
	//public static String My_Computers = "//*[@nodeName='SPAN' and @text='My Computer' and @class='d2l-textblock']";
	//public static String Upload_Button = "//*[@nodeName='BUTTON' and @text='Upload']";
	//public static String Documents_Option = "//*[@text='Documents' and @onScreen='true']";
	public static String File_image = "//*[contains(@text,'.jpg') or contains(@text,'.JPG')]";
	public static String Done_Button = "//*[text()='Done']";
	public static String Alternative_Text = "//*[@text='Alternative Text: ']/..//*[@class='d_edt vui-input']";

	public static String Edit_Box_Score = "//*[@id='z_by']";
	public static String FeedBack_Box = "//*[@id='REDT_comments$html_iframecontainer']";
	public static String Publish_Button = "//*[@nodeName='BUTTON' and @text='Publish']";
	public static String Published_Text = "//*[@text='Published:']";
	public static String Uploaded_File_Checkbox = "//*[@name='gridFiles_cb' and @type='checkbox']";
	public static String Download_Link = "//*[@text='Download']";
	public static String Download_Close_Button = "//*[@contentDescription='Close']";

	//public static String HasStartDate_checkbox = "//*[@text='Has Start Date']/..//input";
    public static String HasDueDate_checkbox = "//*[@text='Has Due Date']/..//input";
   // public static String HasEndDate_checkbox = "//*[@text='Has End Date']/..//input";
	//public static String EndDate = "//*[@nodeName='INPUT' and @id='z_z' and @type='text']";
	//public static String Now_Button = "(//*[@nodeName='BUTTON' and @text='Now'])[1]";

	// Choose file button for uploading video
	public static String ChooseFile_Button = "//*[@id='ctl_2']";
	public static String Iphone_CamaraRoll = "//*[text()='Camera Roll']";
	// public static String Documents_Option =
	// "//*[@text='Documents' and @onScreen='true']";
	public static String PhotoLibrary_Iphone_Option = "//*[text()='Photo Library']";
	public static String File_Iphone_image = "//*[contains(@text,'Screenshot,')]";
	public static String File_Video = "//*[contains(@text,'.mp4')]";
	public static String File_Iphone_Video = "//*[contains(@text,'Video,')]";
	public static String Iphone_ChooseVideo_Use = "//*[@text='Use' or @text='Choose']";
	public static String Link_Text = "//*[@id='z_h']//input";
	public static String Iphone_Alternative_Text = "//*[@name='z_m']";

	//public static String Restrictions_Tab = "//*[@text='Restrictions']";
	public static String Restrictions_Tab = "//*[text()='Restrictions']";
	
	
	// Learner Submits assignment
	// ========================================================================
	public static String AddFile_button = "//*[text()='Add a File']";
	public static String MyComputer_Link = "//*[@nodeName='SPAN' and @text='My Computer']";
	public static String Add_Button = "//*[text()='Add']";

	// public static String Documents_Option =
	// By.xpath("//*[@id='icon' and ./parent::*[./following-sibling::*[./*[@text='Documents']]]]");

	// public static String File_image =
	// By.xpath("//*[@id='icon_mime' and @class='android.widget.ImageView']");

	// public static String File_image = "//*[contains(@text,'.jpg')]";
	//public static String radioHiddenUsers = "//*[@name='z_k' and  @type='checkbox']";
	public static String chkHiddenUsers = "//label[text()='Hidden from users']/preceding-sibling::input";
	public static String chkHiddenFromUsers="//label[text()='Hide from Users']/preceding-sibling::input";
	public static String Submit_button = "//*[text()='Submit']";
	public static String Select_Category = "//*[@name='categoryId']";

	public static String Select_Grade = "//*[@name='gradeItemId']";
	
	public static String InvertedTriangle_Beside_Assignment = "//*[@text='oldString']/../../../*[@class='dcm bsi-button-menu dcm_handle']";
	public static String SubmissionLog_Title = "//*[@text='Submission Log' and @onScreen='true']";
	public static String Reorder_Category = "//*[@text='oldString']/ancestor::tr//select";
	
	public static String Uploaded_File_Link = "//*[contains(@text,'.jpg') or contains(@text,'.PNG') or contains(@text,'.png') or contains(@text,'.JPG')]";
	public static String Rubric_Name= "//*[@id='z_f' and @class='d_edt vui-input']";
	public static String Rubric_Status = "//*[@class='vui-input d2l-select' and @onScreen='true']";
	public static String Rubric_checkBox = "//*[@text='oldString']/../..//*[@class='vui-input d_chb' and @onScreen='true']";
	
	public static String FeedbackView = "//*[@text='oldString']/ancestor::tr//*[@text='View']";
	public static String File_image_Android = "//*[@id='icon_thumb' and @class='android.widget.ImageView']";
	
	public static By lnkDeleteAssignmentInActions(String assignmentName, String action)
    {
    	By lnkDeleteAssignmentInAction=By.xpath("//a[contains(@title,'Actions for "+assignmentName+"')]/following::span[text()='"+action+"']");
    	return lnkDeleteAssignmentInAction;
    }
	
	 
	public static String ReplaceString(String xpath, String oldString,String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
	
	//New xpaths
	public static String Add_Rubrics = "//*[text()='Add Rubric']";
	public static String Selecting_Rubric = "//input[@title='Select TestRubric']";
	public static String btnAddSelected = "//button[text()='Add Selected']";
	public static String HasStartDate_checkbox = "//label[text()='Has Start Date']/preceding-sibling::input[@type='checkbox']";
	public static String HasEndDate_checkbox = "//label[text()='Has End Date']/preceding-sibling::input[@type='checkbox']";
	public static String EndDate = "(//input[contains(@title,'Arrow down to access mini-calendar')])[3]";
	public static String Now_Button = "//div[@class='dco js_startDateEdit']//button";
	public static String Saved_SuccessFully_Text = "//*[text()='Saved successfully']";
	public static String Create_Button = "	//*[text()='Create']";
	public static String iframeAddAFile = "//iframe[contains(@title,'Add a File')]";
	public static String My_Computers = "//img[contains(@src,'pixel.gif')]/following-sibling::span[text()='My Computer']";
	public static String Upload_Button = "//button[text()='Upload']";
	public static String Documents_Option = "//*[text()='Documents']";
	public static String emailIconInMessageAlerts="(//d2l-button-subtle[@text='Email'])[1]";
	public static By selectDropDownTextwithContainsID(String ID) {
		By LiItem = By.xpath("//select[contains(@id,'"+ID+"')]");
		return LiItem;
	}
	
	
	public static By selectDropDownTextwithContainsName(String Name) {
		By LiItem = By.xpath("//select[contains(@name,'"+Name+"')]");
		return LiItem;
	}
	
	public static By LinkTextNameInMenu(String text)
    {
    	By lnkName=By.xpath("//d2l-menu-item-link[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))]");
    	return lnkName;
    }
	
	
	public static By d2LmenuItemtextName(String text)
    {
    	By lnkName=By.xpath("//d2l-menu-item[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))]");
    	return lnkName;
    }
	
	public static By LinkTextNameInMyTool(String text)
    {
    	By lnkName=By.xpath("(//d2l-menu-item-link[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))])[last()]");
    	return lnkName;
    }
	
	public static By selectDropDownTextwithID(String ID) {
		By LiItem = By.xpath("//select[@id='"+ID+"']");
		return LiItem;
	}
	public static String linkNumeric ="//a[text()='Numeric']";
}
