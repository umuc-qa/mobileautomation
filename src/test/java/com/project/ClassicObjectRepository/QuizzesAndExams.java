package com.project.ClassicObjectRepository;

public class QuizzesAndExams {
	
	//New Quiz
	//public static String New_Quiz = "//*[@nodeName='BUTTON' and @text='New Quiz']";
	public static String New_Quiz = "//li[@class='d2l-action-buttons-item']//button[text()='New Quiz']";
	
	public static String Quiz_Name = "//*[@id='z_j']";
	//public static String Add_Category = "//*[@text='[add category]']";
	public static String Add_Category = "//*[text()='[add category]']";
	
	public static String Add_Category_Name = "//*[@id='z_c' and @name='name']";
	//public static String Category_Save_Button = "//*[@text='Save' and ./parent::*[@nodeName='TD']]";
	public static String Category_Save_Button = "//*[text()='Save' and not(@role='presentation')]";
	
	public static String Add_Or_Edit_Questions = "//*[@id='z_p']";
	public static String Editing_Quiz_Name = "//*[@id='z_j']";
	public static String Editing_Category_Name = "//*[@id='z_c' and @name='name']";
	public static String Editing_Add_Or_Edit_Questions = "//*[@id='z_x']";
	
	public static String New_Button = "//*[@contentDescription='New View Actions']";
	public static String Question_Title = "//*[@class='android.widget.EditText' and @text='Title' and @enabled='true']";
	public static String Question_Text = "//*[@contentDescription='\n' and ./parent::*[@contentDescription='\n']]";
	public static String True_Weight = "xpath=((//*[@class='android.view.View' and ./parent::*[@class='android.view.View' and ./preceding-sibling::*[@class='android.view.View'] and ./parent::*[@class='android.view.View'"
			+ " and ./parent::*[@class='android.view.View' and ./parent::*[@class='android.view.View'] and ./following-sibling::*[@class='android.view.View' and ./*[./*[@contentDescription='Save']]]]]]]/*/*[@class='android.view.View' and ./parent::*[@class='android.view.View']])[9]/*/*[@text and @height>0 and ./parent::*[@class='android.view.View']])[1]";
	public static String False_Weight = "xpath=((//*[@class='android.view.View' and ./parent::*[@class='android.view.View' and ./preceding-sibling::*[@class='android.view.View'] and ./parent::*[@class='android.view.View' "
			+ "and ./parent::*[@class='android.view.View' and ./parent::*[@class='android.view.View'] and ./following-sibling::*[@class='android.view.View' and ./*[./*[@contentDescription='Save']]]]]]]/*/*[@class='android.view.View' and ./parent::*[@class='android.view.View']])[9]/*/*[@text and @height>0 and ./parent::*[@class='android.view.View']])[2]";
	public static String True_FeedBack = "//*[@contentDescription='\n' and ./parent::*[@contentDescription='\n' and ./parent::*[./parent::*[./parent::*[./preceding-sibling::*[@contentDescription='Edit Entry 1 Feedback']]]]]]";
	public static String False_FeedBack = "//*[@contentDescription='\n' and ./parent::*[@contentDescription='\n' and ./parent::*[./parent::*[./parent::*[./preceding-sibling::*[@contentDescription='Edit Entry 2 Feedback']]]]]]";
	public static String Save_Button = "//*[@contentDescription='Save']";
	public static String Done_Editing_Questions = "//*[@contentDescription='Done Editing Questions']";
	public static String Quiz_Status = "//*[@id='z_j']";
	public static String Save_And_Close = "//*[@id='z_a']";
	public static String Quiz_Name_Select = "(//*[contains(@text,'Quiz#')])[2]";
	//public static String Quiz_Name_Validating = "//*[contains(@text,'Quiz#')]";
	public static String Quiz_Name_Validating = "//a[text()='sss']";
	public static String Import_Source_Collection = "//*[@contentDescription='-- Select Source Collection --']";
	public static String Choose_Section_To_Import = "//*[@contentDescription='-- Choose a section from which to import --']";
	public static String Import_Source = "//*[@contentDescription='From an Existing Collection']";
	//public static String Import_Button = "//*[@contentDescription='Import']";
	public static String Import_Button ="//span[text()='Import']";
	public static String Choose_File = "//*[@contentDescription='Source File' and @class='android.widget.Button']";
	public static String CSV_File = "//*[@text='Sample_Question_Import .csv']";
	public static String Quiz_Save_Button = "//*[@contentDescription='Save']";
	public static String Saved_Successfully = "//*[@text='Saved successfully']";
	
	//Content
	//public static String Add_A_Module = "//*[@text='Add a module...' and @nodeName='DIV']";
	public static String  Add_A_Module ="(//div[@class='d2l-editable-padding']//*[text()='Add a module...'])[1]";
	public static String txtAdd_A_Module = "//input[@id='UnitName']";
	public static String Upload_Or_Create = "//*[@text='Upload / Create']";
	public static String Start_Quiz_Button = "//*[@id='z_a']";
	public static String Ok_Button = "//*[@nodeName='BUTTON' and @text='OK']";
	public static String Answer_Description_1 = "//*[@contentDescription='True']";
	public static String Answer_Description_2 = "(//*[@contentDescription='\n' and ./parent::*[@contentDescription='\n']])[2]";
	public static String Answer_Description_3 = "(//*[@contentDescription='\n' and ./parent::*[@contentDescription='\n']])[3]";
	public static String Go_To_Submit_Quiz = "//*[@contentDescription='Go to Submit Quiz']";
	public static String Submit_Quiz = "//*[@contentDescription='Submit Quiz']";
	public static String Yes_Submit_Quiz = "//*[@text='Yes, submit quiz']";
	public static String Quiz_Done_Button = "//*[@id='z_c']";
	
	public static String btnExistingActivities = "//span[text()='Existing Activities']";
	public static String optnQuizzes = "//span[text()='Quizzes']";
	public static String tpcQuizzes ="//div[@class=' d2l-quicklinkselector-item']"; 
			//"//div[text()='sample quiz']";
	public static String quizValidation = "//a[text()='sample quiz']";
	
	//Restrictions
	public static String HasStartDate_checkbox = "//*[@nodeName='INPUT' and @id='z_r' and @type='checkbox']";
	public static String Now_Button = "//*[@nodeName='BUTTON' and @text='Now']";
	public static String HasEndDate_checkbox = "//*[@nodeName='INPUT' and @id='z_u' and @type='checkbox']";
	public static String Has_End_Date = "//*[@nodeName='INPUT' and @id='z_v' and @type='text']";
	public static String Calender_CheckBox = "//*[@nodeName='INPUT' and @name='z_y' and @type='checkbox']";
	
	//Assessment
	public static String Allow_Attempt = "//*[@nodeName='INPUT' and @id='z_i' and @type='checkbox']";
	public static String Add_Grade_Item = "//*[@text='[add grade item]']";
	public static String Grade_Name = "//*[@nodeName='INPUT' and @id='z_c']";
	public static String Grade_Save_Button = "//*[@text='Save' and ./parent::*[@nodeName='TD']]";
	public static String Create_New_Rubric = "//*[@text='[Create Rubric in New Window]']";
	public static String Rubric_Name = "//*[@class='android.widget.EditText' and ./preceding-sibling::*[@contentDescription='General'] and ./parent::*[@class='android.view.View']]";
	public static String Rubric_Status = "//*[@id='z_j']";
	public static String Rubric_Description = "//*[@nodeName='P' and @iframeId='RE_description$html_ifr']";
	public static String Add_Rubric = "//*[@id='z_bi']";
	public static String Add_Selected = "//*[@text='Add Selected']";
	

}
