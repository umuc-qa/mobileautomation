package com.project.ClassicObjectRepository;

public class SelfAssessementObjects {
	
	//Self Assessement Create Objects
	public static String SelfAssessement="//*[text()='Self Assessments']";
	public static String NewSelfAssessement ="//*[text()='New Self Assessment']";
	public static String NewSelfAssessement_Name ="//*[@name='name']";
	public static String NewSelfAssessement_SaveandCloseButton="//*[text()='Save and Close']";
	public static String SelfAssesementList="//div/table/tbody/tr/td[2]";
	public static String SelfAssesementEditButton="//*[text()='Add/Edit Questions']";
	//public static String SelfAssesementNew_Button="//*[@contentDescription='New View Actions']";
	//public static String SelfAssesementNew_Button="//*[(@contentDescription='New View Actions') or (@name='New')]";
	public static String FrameElement="//*[@name='listFrame']";
	//public static String NewMultiSelectedvalue="//*[@contentDescription='Written Response Question (WR)' or @name='Written Response Question (WR)']";
	//public static String NewMultiSelectedvalue="//*[@contentDescription='Written Response Question (WR)']";
	public static String CreatedSucessfullyvalidation="//*[@text='Created successfully']";
	public static String Title="//*[@text='Title']";
	//public static String Title="//*[(@text='Title' or @placeholder='')]/*[@placeholder='' and @width>0][59]";
	public static String QuestionText="//*[@id='tinymce' and @height>0]";
	public static String QuestionText1="//*[@contentDescription='\n' and ./parent::*[@contentDescription='\n' and ./parent::*[./parent::*[./preceding-sibling::*[@contentDescription='Skip Toolbars for Question Text.']]]]]";
	//public static String SaveButton="//*[@contentDescription='Save']";
	//public static String SaveButton="//*[@contentDescription='Save' or @name='Save']";
	//public static String DoneEditingQuestion="//*[@contentDescription='Done Editing Questions']";
	//public static String DoneEditingQuestion="//*[@contentDescription='Done Editing Questions' or @name='Done Editing Questions']";
	public static String SaveAndClose="//*[text()='Save and Close']";
	public static String DoneButton="//*[text()='Done']";
	public static String SucessMessage="//*[text()='Saved successfully']";	
	public static String SelfAssesementNew_Button="//*[text()='New']";
	public static String NewMultiSelectedvalue="//*[span='Written Response Question (WR)']";
//new xpaths
	public static String textQstn = "//div[@id='qed-body']";
	public static String DoneEditingQuestion="//*[text()='Done Editing Questions']";
	public static String SaveButton="//button[text()='Save']";
	public static String lnkManageSelfAssessments = "//a[text()='Manage Self Assessments']";
	public static String iframe = "//iframe[@class='dif d2l-navbar-margin']";
	public static String listFrame = "//frame[@name='listFrame']";
	
	
	
	
}