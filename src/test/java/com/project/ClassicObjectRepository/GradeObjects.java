package com.project.ClassicObjectRepository;

import org.openqa.selenium.By;

public class GradeObjects {

	public static String CheckBox_Beside_User = "//*[@text='oldString']/../..//*[@name='gs_cb' and @checked='false' and @onScreen='true']";
	public static String ManageGrades="//*[@nodeName='A' and @text='Manage Grades' and @onScreen='true']";
	public static String EnterGrades="//*[@nodeName='A' and @text='Enter Grades' and @onScreen='true']";
	//public static String MoreActions="//*[@nodeName='SPAN' and @text='More Actions' and @onScreen='true']";
	//public static String EventLogs="//*[@nodeName='SPAN' and @text='Event Log' and @onScreen='true']";

	public static String SwitchToStandardView="//*[@nodeName='BUTTON' and @text='Switch to Standard View' and @onScreen='true']";
	//public static String SaveButton="//*[@nodeName='BUTTON' and @text='Save' and @onScreen='true']";
	//public static String YesButton ="//*[@nodeName='BUTTON' and @text='Yes' and @type='submit' and @onScreen='true']";
	
	public static String Searchbox ="//*[@nodeName='INPUT' and @placeholder='Search For:â€¦' and @type='text' and @onScreen='true' and @visible='true']";
	//public static String SearchIcon="//*[@nodeName='INPUT' and @value='Search' and @onScreen='true' and @visible='true']";
	public static String ActionStatus="//*[@text='Created' and @nodeName='TD']";

	public static String Score = "//*[@text='Score']/ancestor::tbody//input";
	public static String Feedback = "//*[@iframeId='feedback$html_ifr']";
	
	public static String ReplaceString(String xpath, String oldString,String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
	
	//new xpaths
	
	public static String txtGradeItemValue(String gradeItem) {
		String txtGradeItemValue = "(//input[contains(@title,'"+gradeItem+" value for')])[1]";
		return txtGradeItemValue;
	}
	public static String txtAdjustFinalGradeNumerator="(//input[contains(@title,'Final Adjusted Grade numerator value for')])[1]";
	public static String txtAdjustFinalGradeDenominator="(//input[contains(@title,'Final Adjusted Grade denominator value for')])[1]";

	public static String YesButton="//button[text()='Yes']";
	public static String SaveButton="//button[text()='Save']";
	public static String MoreActions="//a[@title='View Actions']";
	public static String EventLogs="//span[text()='Event Log']";
//	public static String txtSearch="//input[@placeholder='Search For…' or @placeholder='Search For:…']";
	public static String txtSearch="//d2l-input-search[@class='d2l-search-simple-wc-input']";
	public static String SearchIcon = "//button[@title='Search']";
	public static String lblCreated(String gradeItem) {
		String lblCreated = "//a[contains(text(),'"+gradeItem+"')]/../preceding-sibling::td[text()='Created']";
		return lblCreated;
	}
	
	//10.8.9
	
	public static String btnMoreActions = "//span[text()='More Actions']";
		
	public static By d2LmenuItemLinktextName(String text)
    {
    	By lnkName=By.xpath("//d2l-menu-item[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))]");
    	return lnkName;
    }
}
