package com.project.ClassicObjectRepository;

import org.openqa.selenium.By;

public class DiscussionsObjects {

	//settings objects
	//public static String Three_Dot_Icon = "//*[@nodeName='D2L-ICON' and ./parent::*[@nodeName='BUTTON' and ./parent::*[@nodeName='D2L-DROPDOWN' and ./parent::*[@text]]]]";
	//Mahender
	//public static String Three_Dot_Icon = "//button[@title='Utilities']";
	public static String Three_Dot_Icon = "//d2l-button-icon[@text='Utilities']";
	
	//public static String Settings_Button = "//*[@text='Settings']";
	//public static String Save_Button = "//*[@text='Save']";
	// Mahender
	public static String Save_Button = "//button[text()='Save']";
	
	//public static String Reding_View_Radio_Uncheck = "//*[@text='Reading View']/..//*[@name='radMessageListStyle' and @checked='false']";
	public static String Reding_View_Radio_Uncheck = "//*[@name='radMessageListStyle' and @checked='checked']";
	public static String Subscription_Settings_Uncheck = "//*[@text='When creating a new thread, subscribe to the thread by default']/..//*[@name='subscribeByDefault' and @checked='false']";
	public static String Menu_Bar = "//*[@class='menu-icon page-action-group x-scope d2l-icon-0']";
	
	//New forum creation
	public static String New_Button = "//*[@id='NewDiscussionsButtonMenu']";
	
	//public static String New_Forum = "//*[@nodeName='A'  and ./*[@text='New Forum']]";
	// Mahender
	//public static String New_Forum = "//*[@text='New Forum' and @nodeName='SPAN']";
	public static String New_Forum = "//span[text()='New Forum']";
	
	
	
	public static String Forum_Title_TextBox = "//*[@name='forumTitle']";
	
	//public static String Description_TextBox = "//*[@id='forumDescription$html_ifr']";
	 public static String Description_TextBox = "//iframe[@title='Description']";//"//iframe[@id='forumDescription$html_ifr']";
	 
	
	
	//public static String Save_Add_Topic = "//*[@text='Save and Add Topic']";
	// Mahender
	public static String Save_Add_Topic = "//button[text()='Save and Add Topic']";
	
	
	//New Topic creation
	public static String Topic_Title_Textbox = "//*[@name='topicTitle']";
	//public static String Topic_Description_TextBox = "//*[@id='topicDescription$html_ifr']";
	public static String Topic_Description_TextBox = "//iframe[@id='topicDescription$html_ifr']";
	//public static String Topic_Rate_Posts = "//*[@id='z_z']";
	//public static String Save_Add_Close = "//*[@text='Save and Close']";
	//public static String Save_Add_Close = "//div[@class='d2l-floating-buttons-container style-scope d2l-floating-buttons']//button[text()='Save and Close']";
	public static String Save_Add_Close = "//button[text()='Save and Close']";
	//public static String Save_Add_Close = "//button[@class='d2l-button d2l_1_1_217']";
//	public static String Yes_Button = "//*[@text='Yes']";
	public static String Yes_Button = "//button[@class='d2l-button' and text()='Yes']";
	
	//public static String Restore_Topic_Forum = "//*[@text='Restore Topic and Forum']";
	//public static String Restore_Topic_Forum = "//*[text()='Restore Forums and Topics']";
	
	public static String Restore_Topic_Forum_Button = "//td[@class='d2l-grid-cell d2l-table-cell-last']//button[@class='d2l-button'][1]";
	
	//Thread Objects
	//public static String Start_A_New_Thread = "//*[@id='createThreadButton']";
	//public static String Thread_Title_Textbox = "//*[@id='newThread$threadData$subject']";
	public static String Thread_Description_TextBox = "//*[@id='newThread$threadData$message$html_ifr']";
	//public static String Post_Button = "//*[@id='newThreadBtn']";
//	public static String Thread_Created_SuccessFully = "//*[@text='	Thread created successfully']";
//public static String Reply_To_Thread = "//*[@id='replyThreadBottomButton']";
//	public static String Reply_Description_Box = "//*[contains(@id,'$postData$message$html_ifr')]";
	//public static String Reply_Post_Button = "//*[contains(@id,'postButtonBottom')]";
	//public static String Replied_SuccessFully = "//*[@text='	Replied successfully']";
	
	//Groups
	public static String List_Of_Groups = "//*[@class='d2l-grid-wrapper d_gl']";
//	public static String List_Of_Groups = "//*[@class='d_ggl2']";

	//public static String Group_Link = "//*[@class='d2l-link d2l-link-inline']";
	public static String Group_Link = "(//*[@class='d2l-link d2l-link-inline'])[1]";
	//public static String Members_Enrolled_In_Group = "//*[@class='d2l-grid-container']//*[@nodeName='D2L-TABLE-WRAPPER']//div/table/tbody/tr";
	public static String Members_Enrolled_In_Group = "//span[text()='Number Of Users']//following::tr[@id='z_m']//label";
//	public static String Members_Enrolled_In_Group = "//*[@class='d2l-table d2l-grid d_gl' and not(@summary)]//tr//td";

	//Subscribe topic
	public static String Topic_Created_Successfully = "//*[contains(@text,'Topic subscribed successfully')]";
	
	//public static String Inverted_Triangle_Beside_Topic = "//*[@text='oldString']/../..//*[starts-with(@id,'TopicContextMenu')]";
	public static String Inverted_Triangle_Beside_Topic = "//a[@title='Actions for Topic Subscribe']";
	
	/*public static By Inverted_Triangle_Beside_Topics(String topicName)
	    {
	    	By lnkTopicName=By.xpath("//*[@title='Actions for "+topicName+"']");
	    	return lnkTopicName;
	   
	    }*/
	

	public static By Inverted_Triangle_Beside_Topics(String topicName)
	    {
	    	By lnkTopicName=By.xpath("//d2l-button-icon[@text='Actions for "+topicName+"']");
	    	return lnkTopicName;
	   
	    }
	
	public static By Inverted_Triangle_Beside_Forum(String forumName)
    {
    	By forumname=By.xpath("//d2l-button-icon[@text='Actions for "+forumName+"']");
    	return forumname;
   
    }
	
	 public static By actionsForTopic(String topicName,String actionName)
	    {
	    	By actionsForTopic=By.xpath("//*[@aria-label='Actions for "+topicName+"']//span[text()='"+actionName+"']");//("//a[@title='Actions for "+topicName+"']//following-sibling::div[1]//span[text()='"+actionName+"']");
	    	return actionsForTopic;
	    }
	
	
	//Add Topic,Edit Forum,Delete
		 public static By actionsForForum(String forumName,String actionName)
		    {
		    	//10.8.9  By actionsForForum=By.xpath("//a[@title='Actions for "+forumName+"']//following-sibling::div[1]//span[text()='"+actionName+"']");//a[@title='Actions for "+forumName+"']/following-sibling::div[contains(@aria-labelledby,'ForumContextMenu')]//span[text()='"+actionName+"']");
		    	By actionsForForum = By.xpath("//*[@aria-label='Actions for "+forumName+"']//span[text()='"+actionName+"']");
		    	return actionsForForum;
		    }
		 
	public static String subscribeFromInvertedTriangle = "//li[@class='d2l-contextmenu-item d2l-last-visible-item']//span[text()='Subscribe']";
    public static String unSubscribeFromInvertedTriangle = "//li[@class='d2l-contextmenu-item d2l-last-visible-item']//span[text()='Unsubscribe']";
	public static String radioButtonForSubscribe ="//label[text()='Show notifications in minibar only']//preceding-sibling::input";
	public static String btnSubscribe = "//div[@class='d2l-dialog-button-group']/button[1]";
	public static String topicSubscribedSucessfully = "//*[text()='Topic subscribed successfully']";
	public static String topicUnSubscribedSucessfully = "//*[text()='Topic unsubscribed successfully']";
	public static String txtDescription = "//body[@class='mce-content-body ']/p";//"//body[@id='tinymce']";
	public static String btn_Restore_Topic_Forum = "//*[text()='Restore Topic and Forum']";
	
	public static String ReplaceString(String xpath, String oldString,String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
	//new xpaths
	public static String Thread_Created_SuccessFully = "//*[text()='Thread created successfully']";
	public static String Reply_Post_Button = "//button[text()='Post']";
	public static String Replied_SuccessFully = "//*[text()='Replied successfully']";
	public static String txtMsgArea = "//body[@id='tinymce']";
	public static String Topic_Rate_Posts ="//a[contains(@title,' Rate Posts')]/./preceding-sibling::div//select";
	

	//10.8.9 
	
	public static String Settings_Button = "//span[@class='style-scope d2l-menu-item' and text()='Settings']";
	public static String radioReadingView = "//input[@name='radMessageListStyle' and @value='2']";
	public static String Restore_Topic_Forum = "//*[text()='Restore Topic and Forum']";
	public static String Start_A_New_Thread = "//*[@id='BTN_CreateThread']";
	public static String Thread_Title_Textbox ="//*[@id='EDT_subject']";
	public static String Post_Button = "//button[text()='Post']";
	public static String Reply_To_Thread = "//*[@id='BTN_Reply']";
	public static String Reply_Description_Box = "//iframe[@id='message$html_ifr']";
	
	public static By d2LmenuItemLinktextName(String text)
    {
    	By lnkName=By.xpath("(//d2l-menu-item[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))])[last()]");
    	return lnkName;
    }
	
	public static By d2LmenuItemLinktextNameandIDName(String text)
    {
    	By lnkName=By.xpath("(//d2l-menu-item[@text='"+text+"'  or contains(@id,'EditTopic') and not(contains(@class,'d2l-navigation-s-menu-item-root'))])[1]");
    	return lnkName;
    }
	
	
}