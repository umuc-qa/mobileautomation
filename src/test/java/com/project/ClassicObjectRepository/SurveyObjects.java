/**
 * 
 */
package com.project.ClassicObjectRepository;

import org.openqa.selenium.By;

/***
 * 
 * @author E001880
 *
 */
public class SurveyObjects {
	
	public static String MySurvey = "//*[text() ='My Surveys']";
	//public static String NewSurvey="//*[@nodeName='BUTTON' and @text='New Survey']";
	//public static String Surveyname="//*[@nodeName='INPUT' and @id='z_i' and @name='name']";
	//public static String AddOrEditQuestions ="//*[@nodeName='BUTTON' and @text='Add/Edit Questions']";
	
	//elements in ipad Non instrument mode
	/*public static String NewButton="//*[@name='New' and @label='New' and @class='UIATextField']";
	public static String TrueorFalseQuestion="//*[@name='True or False Question (T/F)' and @label='True or False Question (T/F)' and @class='UIATextField']";
	public static String QuestionTitle="(//*[@placeholder='']/*[@placeholder='' and @width>0])[59]";
	public static String QuestionText="(//*[@name='Question Text' and @label='Question Text' and @class='UIATextField'])[2]";
	public static String DoneEditingQuestions="//*[@name='Done Editing Questions' and @class='UIATextField']";
	public static String Save_And_Close_Button ="//*[@nodeName='BUTTON' and @id='z_a' and @text='Save and Close']";
	public static String SelectStatus ="//*[@nodeName='SELECT' and @id='z_i' and @name='isActive']";
	public static String Active ="//*[@text='Active' and @class='UIAView']";
	*/
	//elements in Samsung s7  Non instrument mode
	
	//public static String NewButton="//*[@contentDescription='New View Actions' and @content-desc='New View Actions']";
	//public static String TrueorFalseQuestion="//*[@content-desc='True or False Question (T/F)' and @enabled='true' and @onScreen='true']";
	//public static String QuestionTitle="//*[@text='Title' and @enabled='true' and @onScreen='true']";
	//public static String QuestionText="//*[@resource-id='tinymce' and @enabled='true' and @onScreen='true']";
	public static String QuestionText1="//*[@contentDescription='\n' and @height>0 and ./parent::*[@contentDescription='\n']]";
	//public static String SaveButton="//*[@contentDescription='Save' and @enabled='true' and @onScreen='true']";
	public static String DoneEditingQuestions="//*[@content-desc='Done Editing Questions' and @enabled='true' and @onScreen='true']";
	//public static String Save_And_Close_Button ="//*[@nodeName='BUTTON' and @id='z_a' and @text='Save and Close']";
	//public static String SelectStatus ="//*[@nodeName='SELECT' and @id='z_i' and @name='isActive']";
	public static String Active ="//*[@text='Active']";
	
	public static String SelectTrue ="//*[@contentDescription='True' and @enabled='true' and @onScreen='true']";
	//public static String SubmitButton ="//*[@contentDescription='Submit' and @enabled='true' and @onScreen='true']";
//	public static String YesButton ="//*[@nodeName='BUTTON' and @text='Yes' and @type='submit' and @onScreen='true']";
	public static String ReturnToSurveyList="//*[text()='Return to Survey List']";
	public static String SuccessMessgae="//*[@text='You have successfully submitted the survey.']"; 
	
	
	//new xpaths
	
	public static String NewSurvey = "//*[text()='New Survey']";
	public static String Surveyname="//*[@id='z_i' and @name='name']";
	public static String AddOrEditQuestions ="//*[text()='Add/Edit Questions']";
	public static String NewButton = "//*[text()='New']";
	public static String TrueorFalseQuestion = "//span[text()='True or False Question (T/F)']";
	public static String QuestionTitle= "//input[@type='text']";
	public static String QuestionText="//div[@id='qed-body']";		
	public static String  SaveButton="//button[text()='Save']";
	public static String radioTrue = "(//input[@type='radio'])[1]";
	public static String SubmitButton = "//button[text()='Submit']";
	public static String  YesButton  = "//button[text()='Yes']";
	public static String iframe ="//iframe[@class='dif d2l-navbar-margin']";
	public static String pageFrame ="//frame[@name='pageFrame']";
	public static String SelectStatus = "//select[@name='isActive']";
	public static String Save_And_Close_Button ="//button[text()='Save and Close']";
	public static String startDate = "//*[text()='Has Start Date']/..//*[@type='checkbox']";
	public static String endDate = "//*[text()='Has End Date']/..//*[@type='checkbox']";
	public static String btnNowStartDate = "(//button[text()='Now'])[1]";
	public static String btnNowEndDate = "(//button[text()='Now'])[2]";
//	public static String txtEndDateTime = "//*[@id='dateR_dts_ed$time']";
	public static String txtEndDateTime = "(//*[text()='Has End Date']//ancestor::div[contains(@class,'endDateEdit')]//*[@type='text'])[2]";
	public static String btnDoneEditing = "//button[text()='Done Editing Questions']";
	public static String endDateFuture = "//li[contains(@class,'selected')]/following-sibling::li[2]";
	
	public static By d2LmenuItemLinktextName(String text)
    {
    	By lnkName=By.xpath("//d2l-menu-item[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))]");
		return lnkName;
    	
    }
	
	
	
}
