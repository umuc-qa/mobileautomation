package com.project.ClassicObjectRepository;

public class EmailObjects {
	// Email via Message alert box [Native non Instrumented objects]
	//public static String MessageAlertBox_Email_To = "//*[@resource-id='ToAddresses' and @enabled='true' and @onScreen='true']";
	//public static String MessageAlertBox_Email_Subject = "//*[@resource-id='Subject' and @enabled='true' and @onScreen='true']";
	//public static String MessageAlertBox_Email_Body = "//*[@resource-id='tinymce' and @enabled='true' and @onScreen='true']";
	//public static String MessageAlertBox_Email_Send = "//*[@content-desc='Send' and @enabled='true' and @onScreen='true']";
	
	// Class list instant message Web Objects
	public static String ClassList_CheckBox = "//*[@class='d2l-grid d_gl x-scope d2l-table-0']//*[@text='oldString']/../..//*[@name='gridUsers_cb' and @checked='false' and @onScreen='true']";
	public static String Instant_Message = "//*[@text='Instant Message']";
	public static String Message_Dialog_Box = "//*[@id='z_f']"; 
	public static String Auto_Filled_BCC = "//*[@class='d2l-multiselect-choice']";
	public static String ClassList_Email_Subject = "//*[@name='Subject' and @onScreen='true']";
	public static String ClassList_Email_Body ="//*[@nodeName='P' and @iframeId='BodyHtml$html_ifr' and @onScreen='true']";
	
	public static String Email_Confirmation_Yes = "//*[text()='Yes']";
	public static String Continue_Button = "//*[text()='Continue']";
	
	public static String ReplaceString(String xpath, String oldString,String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
	
	//New xpaths
	public static String MessageAlertBox_Email_To ="//input[@id='ToAddresses']"; 
	public static String MessageAlertBox_Email_Subject = "//input[@name='Subject']";
	public static String MessageAlertBox_Email_Body = "//body[@id='tinymce']";
	public static String MessageAlertBox_Email_Send = "//button[text()='Send']";
	public static String msgEmailSuccess = "(//*[text()='Sent successfully'])[2]";
	public static String  iframeOfContent= "//iframe[@name='content']";
    public static String  frameOfEmailDetails= "//frame[@title='Email Details']";
	public static String  frameOfComposeMail= "//frame[@title='Compose Messages']";
	public static String lnkEmail = "//d2l-button-subtle[@text='Email']";
	public static String lnkInstantMsg = "//d2l-button-subtle[@text='Instant Message']";
	public static String headingMessage = "//h2[text()='Send Message']";
	public static String txtTextArea="//textarea"; 
	
	

}
