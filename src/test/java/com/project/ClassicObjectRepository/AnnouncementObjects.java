/**
 * 
 */
package com.project.ClassicObjectRepository;

/**
 * @author E001115
 *
 */
public class AnnouncementObjects {

	public static String AnnouncementsValue = null;

	public static String Select_A_Course_Icon = "//*[@nodeName='D2L-ICON' and ./parent::*[@nodeName='BUTTON'] and ./following-sibling::*[@text='Select a course...']]";
	public static String Search_Box = "//*[@placeholder='Search for a course' and @type='search']";
	public static String Search_Icon = "//*[@value='Search']";	
	public static String Announcements_Inverted_Triangle = "//*[@class='d2l-widget' and @onScreen='true']//*[@text='Announcements']/../../..//div[2]//a";
	public static String Announcements_Inverted_Triangle_Value = "//*[@class=' vui-dropdown-menu-item-link']/*[@text='New Announcement']";
	public static String HeadLine_Text = "//*[@id='headlineId' and @name='headline']";	
	//public static String Content_Text = "//*[@nodeName='P']";
	public static String Insert_Sample_Video = "//*[@class='d2l-htmleditor-group d2l-htmleditor-hide1']//span[1]//img";
	public static String Insert_Sample_Image = "//*[@class='d2l-htmleditor-group d2l-htmleditor-hide1']//span[2]//img";
	public static String MyComputer = "//*[@text='My Computer' and @onScreen='true']";
	public static String ChooseFile = "//*[@id='ctl_2']";
	public static String Gallery = "//*[@text='Gallery']";
	public static String List_Of_Annotations = "//*[@text='replaceData']";
	
	public static String ReplaceString(String xpath, String oldString, String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
	
	public static String StartDateNow = "//*[@text='Start Date']//parent::*//*[@text='Now']";
    public static String Start_Time = "//*[@id='startDate$time']";
    public static String Has_End_Date = "//*[@id='hasEndDateId']";
    public static String EndDateNow = "//*[@text='End Date']//parent::*//*[@text='Now']";
    public static String End_Time = "//*[@id='endDate$time']";
    
    //=====================================================================================================================//
                                                 /*ELM Objects*/
    
    public static String Edit_Button = "//*[text()='Edit']";
    public static String Delete_Button = "(//*[@text='Delete'])[2]";
   // public static String Yes_Button = "//*[@text='Yes']";
    
    //new xpaths
   // public static String lnkActionsForAnnouncement="//a[@title='Actions for Announcements']";
    public static String lnkActionsForAnnouncement = "//h2[text()='Announcements']";
    public static String btnNewAnnouncement = "//button[text()='New Announcement']";
    public static String Content_Text="//*[@id='tinymce']";
    public static String chkShowStartDate = "//input[@name='showDate']";
    public static String btnNow = "//label[text()='Start Date']/following-sibling::div//button[@title='Set date to current date']";
    public static String btnPublish = "//button[text()='Publish']";
    public static String ddDelete = "//a//span[text()='Delete']";
    public static String Yes_Button = "//button[text()='Yes']";
    public static String btnUpdate = "//button[text()='Update']";
}
