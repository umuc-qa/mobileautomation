package com.project.ClassicObjectRepository;

import com.project.BusinessFunctions.BusinessFunctions;

/**
 * Created by e001237 on 08/05/17.
 */
public class Amazon extends BusinessFunctions {


    public static String Btn_Accpet = "//*[@text='Accept & continue' or @text='ACCEPT & CONTINUE']";
    public static String Btn_Next = "//*[@text='Next' or @text='NEXT']";
    public static String Btn_NoThanks = "//*[@text='No Thanks' or @text='NO THANKS']";
    public static String Btn_Search = "//android.widget.Button[contains(@content-desc,'Go')]";
    public static String Txt_SearchProducts = "//android.widget.EditText[contains(@resource-id,'nav-search-keywords') " +
            "and @text='Type search keywords']";
    public static String Item_Product = "//android.view.View[contains(@content-desc,'%s')]";

}
