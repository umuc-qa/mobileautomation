package com.project.ClassicObjectRepository;

import org.openqa.selenium.By;

public class CommonObjects {

	/**
	 * Login Locators
	 */
	
	public static String Login_UserName = "//*[@name='username']";
	public static String Login_Password = "//*[@name='password']";
	public static String Login_Button = "//*[@value='LOG IN' or @name='submit']";
	public static String Do_It_Later = "//*[@text='Do It Later' or @content-desc='Do It Later']";
	public static String Logout_IOS = "(//*[@nodeName='D2L-ICON' and @css='D2L-ICON.x-scope.d2l-icon-0' and @onScreen='true' and @visible='true'])[6]";
	
	public static String Logout_Android = "(//*[@nodeName='D2L-ICON' and @class='x-scope d2l-icon-0' and @onScreen='true' and @visible='true'])[10]"; 
	//public static String profileIcon = "//span[@class='d2l-navigation-s-personal-menu-wrapper']";
	
	public static String profileIcon = "//div[@class='d2l-navigation-s-personal-menu']";
	public static String logoutButton = "//a[text()='Log Out']";
	
	public static String Calender = "//*[@id='d2l_1_7_694' or @text='Calendar']";
	//public static String Select_A_Course_Icon = "//*[@nodeName='BUTTON' and ./*[@nodeName='D2L-ICON'] and ./*[@text='Select a course...']]";
	// Mahender
	//public static String Select_A_Course_Icon = "//button[@class='d2l-navigation-s-button-highlight']//d2l-icon[@icon='d2l-tier3:menu-hamburger']";
	//public static String Select_A_Course_Icon = "//button[@class='d2l-focusable style-scope d2l-navigation-button']//d2l-icon[@icon='d2l-tier3:menu-hamburger']";
	public static String Select_A_Course_Icon = "//div[@class='d2l-navigation-header-left']//d2l-navigation-button-notification-icon[@type='button']";
	public static String Login_Continue = "//*[@class='errors']//*[@nodeName='A' and @text='continue']";

	//public static String Menu_Bar_Icon = "//*[@nodeName='D2L-ICON' and ./*[./*[./*[@nodeName='path']]] and ./parent::*[@nodeName='BUTTON'] and ./following-sibling::*[@text='menu']]";
	// Mahender
	public static String Menu_Bar_Icon = "//div[@class='d2l-navigation-s-mobile-menu-course-selector']//button[@title='Select a course...']";
//div[@class='d2l-navigation-s-mobile-menu-course-selector']//button[@title='Select a course...']
	public static String MyRole_InvertedTriangle = "//*[@name='z_c']";
	//public static String MyRole_PrimaryInstructor = "//*[@id='z_c' and @onScreen='true']//*[contains(@text,'My Role (PrimaryInstructor)')]";
	public static String MyRole_PrimaryInstructor = "//select[contains(@id,'z_b')]//option[@selected='selected' and contains(text(),'PrimaryInstructor')]";
	public static String MyRole_Learner = "//select[contains(@id,'z_b')]//option[@selected='selected' and contains(text(),'Learner')]";

	public static String Announcement_Published = "//*[@text='oldString']/../../..//*[@text='Published']";
	public static String Announcement_Scheduled = "//*[@text='oldString']/../../..//*[@text='Scheduled']";
	public static String Announcement_Expired = "//*[@text='oldString']/../../..//*[@text='Expired']";


	/** Login Locators */

	public static String Logout = "//*[@text='Log Out']";
	public static String userIcon = "//*[@nodeName='D2L-ICON' and @width>0 and ./*[./*[./*[@nodeName='path']]] and ./parent::*[@nodeName='SPAN']]";
	public static String DoItLater = "//*[@text='Do It Later']";
	//public static String Calendar = "//*[@class='d2l-heading vui-heading-2' and @text='Calendar']";
	public static String Select_a_Course = "//*[@nodeName='BUTTON' and @width>0 and ./*[@nodeName='D2L-ICON'] and ./*[@text='Select a course...']]";
	public static String Search_Box = "//*[@placeholder='Search for a course' and @nodeName='INPUT']";
	public static String Search = "(//*[@nodeName='INPUT' and @type='button' and @value='Search' and @class='vui-input-search-button'])[1]";
	public static String SearchResult = "//*[@nodeName='A' and  @class='d2l-link d2l-datalist-item-actioncontrol' and @onScreen='true' and @visible='true']";
	public static String Btn_Accpet = "//*[@text='Accept & continue' or @text='ACCEPT & CONTINUE']";
	public static String Btn_Next = "//*[@text='Next' or @text='NEXT']";
	public static String Btn_NoThanks = "//*[@text='No Thanks' or @text='NO THANKS']";
	public static String Btn_Never = "//*[@text='Never' or @text='NEVER']";
	//public static String Message_Alerts = "//*[@nodeName='D2L-ICON' and ./parent::*[@nodeName='BUTTON'] and ./following-sibling::*[@text='Message alerts']]";
	
	public static String ReplaceString(String xpath, String oldString, String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
	
	// Non-Instrumented object
    public static String Done_Button = "//*[@text='Done' and @enabled='true' and @onScreen='true']";
    public static String Add_Button = "//*[@text='Add' and @enabled='true' and @onScreen='true']";
    
    // Clear browser Locators - IOS
    public static String ShowBookmarks ="//*[@text='Show Bookmarks']";
    public static String History_Clear ="//*[@text='Clear']";
    public static String History_All_Time ="//*[@text='All time']";
    public static String History_Done ="//*[@text='Done']";
    public static String History_Pages ="//*[@text='Pages']";
    public static String CloseButtonTab ="//*[@accessibilityLabel='closeTabButton']";
    
    // Clear browser Locators - Android
    public static String History_Menu_Button ="//*[@id='menu_button']";
    public static String History_Button ="//*[@text='History']";
    public static String History_Clear_Browsing_Data ="//*[@id='clear_browsing_data_button' and @enabled='true' and @onScreen='true']";
    //Xpath Changed in Vivo
    //public static String History_clear = "//*[@text='Clear data']";
    public static String History_clear = "//*[text()='CLEAR DATA']";
    public static String History_StorageClear = "//*[text()='CLEAR']";
    
    public static String History_Close_Menu_Id ="//*[@id='close_menu_id']";
    public static String Android_Pages = "//*[@id='tab_switcher_button' and @onScreen='true']";
    public static String Close_All_Tabs = "//*[@text='Close all tabs']";
    public static String Plus_Icon = "//*[@id='new_tab_button' and @class='android.widget.Button' and @clickable='true']";
    public static String type_WebAddress_Searchbox = "//*[@id='search_box_text']";

    //NEW PATHS
  
    public static String Calendar = "//*[text()='Calendar']";
    public static String Message_Alerts = "//d2l-navigation-button-notification-icon[@text='Message alerts']";
    
    public static String linkContinue="//a[text()='continue']";
    public static String lnkDoItLater="//a[text()='Do It Later']";
    
    //public static String processingInPsft=By.id("processing");
    public static By processingInPsft= By.id("processing");
    
  /*  public static By processingInPsft()
    {
    	By lnkName=By.id("processing");
    	return lnkName;
    }*/
	
    
  
}
