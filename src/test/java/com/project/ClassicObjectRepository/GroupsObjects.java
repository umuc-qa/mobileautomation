/**
 * 
 */
package com.project.ClassicObjectRepository;

import org.openqa.selenium.By;

/***
 * 
 * @author E001115
 *
 */
public class GroupsObjects {
	
	//Group Creation Objects
	public static String MyTools = "//*[@nodeName='D2L-MENU-ITEM' and @width>0 and ./*[@text='My Tools']]";
	public static String Groups="//*[@text='Groups']";
//	public static String NewCategory="//*[@nodeName='BUTTON' and @text='New Category']";
//	public static String CategoryName="//*[@nodeName='INPUT' and @id='z_e' and @name='name']";
//	public static String DescriptionText="//*[@nodeName='P' and @iframeId='description$html_ifr' and @onScreen='true']";
	//public static String EnrollmentType="//*[@nodeName='SELECT' and @name='enrollmentType']";
	public static String GroupsOfSelfEnrollment="//*[@text='Groups of # - Self Enrollment']";
	//public static String NumberOfGroups="//*[@id='z_p']";
	
	// Enroll Group with user objects
	//public static String ViewCategoryDropDown = "//*[@name='category' and @onScreen='true']";
	public static String SelectCategoryWithGroup(String GroupName){
		String CategoryWithGroup = "//*[@text='"+GroupName+"']";
		return CategoryWithGroup;
	}
	public static String selectInvertedTriangleBesideGroup(String GroupName){
		String InvertedTriangleBesideGroup = "//*[@text='"+GroupName+"' and @onScreen='true']/..//*[@id='z_e']";
		return InvertedTriangleBesideGroup;
	}
	/*public static String selectUserCheckBox(String UserToEnroll){
		String userCheckBox = "//*[@text='"+UserToEnroll+"']/..//*[@id='z_o' and @checked='false']";
		return userCheckBox;
	}*/
	
	public static String selectUserCheckBox(String UserToEnroll){
		String userCheckBox = "//input[contains(@title,'"+UserToEnroll+"') and @type='checkbox']";
		return userCheckBox;
	}
	public static String NewCategory = "//button[text()='New Category']";
	public static String CategoryName = "//input[@id='z_f']";
	public static String DescriptionText = "//body[@id='tinymce']";
	public static String EnrollmentType="//select[@name='enrollmentType']";
	public static String NumberOfGroups = "//*[@id='z_r']";
	public static String btnSave = "//button[text()='Save']";
	
	//new xpaths
	public static String ViewCategoryDropDown = "//button[text()='New Category']";
	
	public static By Inverted_Triangle_Beside_Topics(String topicName)
    {
    	By lnkTopicName=By.xpath("//d2l-button-icon[@text='Actions for "+topicName+"']");
		return lnkTopicName;
   
    }

	public static By anchorTextfollowingInverted_Triangle_Beside_Topics(String topicName)
    {
    	By lnkTopicName=By.xpath("//a[text()='"+topicName+"']//following::d2l-dropdown[contains(@class,'d2l-contextmenu-ph d2l-contextmenu-ph-dropdown')]");
		return lnkTopicName;
   
    }
	
	public static By selectDropDownTextwithContainsName(String Name) {
		By LiItem = By.xpath("//select[contains(@name,'"+Name+"')]");
		return LiItem;
	}
	
	public static By LinkTextNameInMenu(String text)
    {
    	By lnkName=By.xpath("//d2l-menu-item-link[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))]");
    	return lnkName;
    }
	
	
	public static By d2LmenuItemLinktextName(String text)
    {
    	By lnkName=By.xpath("//d2l-menu-item[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))]");
		return lnkName;
    	
    }
	
	public static By LinkTextNameInMyTool(String text)
    {
    	By lnkName=By.xpath("(//d2l-menu-item-link[@text='"+text+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))])[last()]");
    	return lnkName;
    }
	
	public static By selectDropDownTextwithID(String ID) {
		By LiItem = By.xpath("//select[@id='"+ID+"']");
		return LiItem;
	}

	
}
