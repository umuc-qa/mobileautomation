package com.project.setup;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileBrowserType;
import io.appium.java_client.remote.MobileCapabilityType;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.annotations.*;

import qa.test.util.CommandExecutor;
import qa.test.util.TestNGParallelClassesTestBase;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.experitest.appium.SeeTestAndroidDriver;
import com.experitest.appium.SeeTestAndroidElement;
import com.experitest.appium.SeeTestIOSDriver;
import com.experitest.appium.SeeTestIOSElement;
import com.project.report.Support.HtmlReportSupport;

import java.text.DateFormat;
//import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.project.report.Support.*;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;


public class TestSetup extends org.testng.TestNG {

	private static final Logger logger = LogManager.getLogger(TestSetup.class
			.getName());

	// DesiredCapabilities capabilities = new DesiredCapabilities();
	// public static AndroidDriver<MobileElement> driver;
	// public static SeeTestIOSDriver<SeeTestIOSElement> driver;
	// public static SeeTestAndroidDriver<SeeTestAndroidElement> driver = null;

	public static ConfiguratorSupport configProps = new ConfiguratorSupport(
			"config.properties");

	public static ConfiguratorSupport counterProp = new ConfiguratorSupport(
			configProps.getProperty("counterPath"));

	public static HashMap<String, String> tstData = null;

	public static HtmlReportSupport htmlRepSupport = new HtmlReportSupport();
	public static String rpSumDt = null;
	public static String rStartDate = null;
	public static String browser1;
	public static String suiteName = "";
	public static String folderName = null;
	public static ITestContext itc;
	public boolean flag = false;
	public static String chartName = null;

	public static int stepNum = 0;
	public static int PassNum = 0;
	public static int FailNum = 0;
	public String testName = "";
	public static String testCaseDescription = "";
	public static Map<String, String> testDescription = new LinkedHashMap<String, String>();
	public static Map<String, String> testResults = new LinkedHashMap<String, String>();

	public static int failCounter = 0;
	public static int skipCounter = 0;
	public static int passCounter = 0;

	public static int stepHeaderNum = 0;

	public static File file = null;

	public static String application = null;

	// DesiredCapabilities capabilities = new DesiredCapabilities();
	//public static AndroidDriver<MobileElement> driver;
	public static AndroidDriver<MobileElement> androidDriver1;
	public static SeeTestAndroidDriver<SeeTestAndroidElement> androidDriver;
	public static SeeTestIOSDriver<SeeTestIOSElement>  iosDriver;
//	public static AndroidDriver<AndroidElement> androidDriver1;
	//public static SeeTestIOSDriver<SeeTestIOSElement>  driver;
	
//	public testSetup() {
//		// TODO Auto-generated constructor stub
//		ConfiguratorSupport configProps1 = new ConfiguratorSupport(
//				"config.properties");
//		if(configProps1.getProperty("deviceType") == "iOS"){
//			SeeTestIOSDriver<SeeTestIOSElement>  driver;
//		}
//		else
//		{
//			SeeTestAndroidDriver<SeeTestAndroidElement> driver;
//		}
//	}
	
	@BeforeSuite(alwaysRun = true)
	public void beforeSuite(ITestContext context) throws Throwable {

		//CommandExecutor.executeCommand("/usr/bin/open -a Terminal /Users/e001237/Projects/General/Appium_Base_Framework-master/environment/Mac/RUNGRID");
		//CommandExecutor.executeCommand("/usr/bin/open -a Terminal /Users/e001237/Projects/General/Appium_Base_Framework-master/environment/Mac/Run_Node_1");
		//commandWrapper.elementHardSleep(60000);

		try {
			SimpleDateFormat rpSumDtSDT = new SimpleDateFormat("hhmmss");
			rpSumDt = rpSumDtSDT.format(new Date());
			rStartDate = rpSumDtSDT.format(new Date());
			ReportStampSupport.calculateSuiteStartTime();
			Map<String, String> suiteParameters = context.getCurrentXmlTest()
					.getSuite().getParameters();
			// Map<String, String> testParameters =
			// context.getCurrentXmlTest().getSuite().getTests().toString();
			String testParameters = context.getCurrentXmlTest().getSuite()
					.getTests().toString();
			System.out.println(testParameters);
			
			System.out.println("~~~~~~~~Before Suite~~~~~~~~");
			// System.out.println("Before Test: " +stepHeaderNum);
			// System.out.println(suiteParameters.get("CurrentApplication"));
			Configuration.PutProperty("CurrentApplication",
					suiteParameters.get("CurrentApplication"));
			Configuration.PutProperty("OS", suiteParameters.get("OS"));
			browser1 = suiteParameters.get("OS");
			// application=suiteParameters.get("CurrentApplication");
			suiteName = context.getCurrentXmlTest().getSuite().getName()
					.replace(" ", "_").trim();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Before Class
	@Parameters({ "OS" })
	@BeforeClass(alwaysRun = true)
	public void first(ITestContext ctx, String OS) throws Throwable {
		System.out.println("~~~~~~~~Before Class~~~~~~~~");

	}

	public void calculateTestCaseStartTime() {
		htmlRepSupport.iStartTime = System.currentTimeMillis();
	}

	public void reportCreater() throws Throwable {
		int intReporterType = Integer.parseInt(configProps
				.getProperty("reportsType"));

		switch (intReporterType) {
		case 1:

			break;
		case 2:

			//htmlRepSupport.htmlCreateReport();
			// HtmlReportSupport.createDetailedReport();

			break;
		default:

			htmlRepSupport.htmlCreateReport();
			break;
		}
	}

	@BeforeTest(alwaysRun = true)
	@Parameters({ "CurrentApplication", "port", "device" })
	public void setupCapabilities(final ITestContext ctx,
			String CurrentApplication, String port, String device)
			throws MalformedURLException, InterruptedException, Throwable {

		System.out.println("~~~~~~~~Before Test~~~~~~~~");

		//driver = new SeeTestAndroidDriver<SeeTestAndroidElement>(new URL("http://localhost:"
			//	+ port), createDesiredCapabilities(device));
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver = new SeeTestIOSDriver<SeeTestIOSElement>(new URL("http://localhost:"+ port), createDesiredCapabilities(device));
			//logger.info("URL is defined: " + "http://127.0.0.1:" + port);
			//System.out.println("Session ID is...." + iosDriver.getSessionId());
			iosDriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		}
		else
		{
			/*driver = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));*/
			androidDriver1 = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));
      		/*androidDriver = new SeeTestAndroidDriver<SeeTestAndroidElement>(new URL("http://localhost:"+ port), createDesiredCapabilities(device));*/
			//logger.info("URL is defined: " + "http://127.0.0.1:" + port);
			//System.out.println("Session ID is...." + iosDriver.getSessionId());
			androidDriver1.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("Driver Launched");
		}
		 

		HtmlReportSupport.iTestStartTime = System.currentTimeMillis();
		application = ctx.getCurrentXmlTest()
				.getParameter("CurrentApplication").trim();
		chartName = configProps.getProperty("ReleaseVersion")
				+ "ExecutionStatus";
		failCounter = 0;
		passCounter = 0;
		HtmlReportSupport.serialNo = 0;
		folderName = ctx.getName().trim();
		System.out.println(folderName); // it prints "Check name test"

	}

	@BeforeMethod(alwaysRun = true)
	@Parameters({ "OS" })
	public void reportHeader(Method method, ITestContext ctx, String OS)
			throws Throwable {
		
		System.out.println("~~~~~~~~Before Method~~~~~~~~");
		
		try {
			reportCreater();
		} catch (Exception e1) {
			System.out.println(e1);
		}

		itc = ctx;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");
		sdf.format(date);
		calculateTestCaseStartTime();

		flag = false;

		HtmlReportSupport.tc_name = method.getName().toString() + "-" + OS;
		String[] ts_Name = this.getClass().getName().toString().split("\\.");
		HtmlReportSupport.packageName = ts_Name[0] + "." + ts_Name[1] + "." + ts_Name[2] + "." + ts_Name[3];
		//htmlRepSupport.packageName = ts_Name[0];
		htmlRepSupport.testHeader(HtmlReportSupport.tc_name.replace("-" + OS, ""));
		folderName = ctx.getName().trim();
		stepNum = 0;
		PassNum = 0;
		FailNum = 0;
		testName = method.getName();

		String[] tmp = testName.split("_");
		String desc = "";
		if ((testCaseDescription != null)
				&& (!testCaseDescription.isEmpty())) {
			desc = testCaseDescription;
		} else {
			for (int i = 0; i < tmp.length; i++) {
				desc = desc + " " + tmp[i];
			}
		}
		testDescription.put(testName + "-" + OS, desc);
	}

	protected DesiredCapabilities createDesiredCapabilities(String device) {
		DesiredCapabilities capabilities = new DesiredCapabilities();

//		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,
//				"ANDROID");
//		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device);
//		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "CHROME");
		//driver = new SeeTestAndroidDriver<SeeTestAndroidElement>(new URL("http://localhost:"+ "8889"), capabilities);
		if(configProps.getProperty("deviceType").equals("iOS")){
			capabilities.setBrowserName(MobileBrowserType.SAFARI);
	  	  	capabilities.setCapability(MobileCapabilityType.AUTO_WEBVIEW, true); 
		}
		else
		{
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device);
			capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "CHROME");
		}
		
     // driver = new SeeTestIOSDriver<SeeTestIOSElement>(new URL("http://localhost:"+ "8889"), capabilities);

		return capabilities;
	}

	/**
	 * Write results to Browser specific path
	 * 
	 * @Revision History
	 * 
	 */
	// @Parameters({"browserType"})
	public static String filePath() {
		String strDirectoy = "";

		// strDirectoy =
		// TestEngine.folderName+"_"+configProps.getProperty("ReleaseVersion")+"_"+rStartDate;

		strDirectoy = folderName.trim() + "_" + rStartDate;
		if (strDirectoy != "") {
			new File(configProps.getProperty("screenShotPath") + strDirectoy)
					.mkdirs();
		}

		File results = new File(configProps.getProperty("screenShotPath")
				+ strDirectoy + "/" + "Screenshots");
		if (!results.exists()) {
			results.mkdir();
			HtmlReportSupport.copyLogos();
		}

		return configProps.getProperty("screenShotPath") + strDirectoy;

	}

	/***
	 * This method is supposed to be used in the @AfterMethod to calculate the
	 * total test case execution time to show in Reports by taking the start
	 * time from the calculateTestCaseStartTime method.
	 */
	public void calculateTestCaseExecutionTime() {
		htmlRepSupport.iEndTime = System.currentTimeMillis();
		htmlRepSupport.iExecutionTime = (htmlRepSupport.iEndTime - htmlRepSupport.iStartTime);
		TimeUnit.MILLISECONDS.toSeconds(htmlRepSupport.iExecutionTime);
		HtmlReportSupport.executionTime.put(HtmlReportSupport.tc_name, String
				.valueOf(TimeUnit.MILLISECONDS
						.toSeconds(htmlRepSupport.iExecutionTime)));
		// System.out.println(tc_name+";Time
		// :"+String.valueOf(TimeUnit.MILLISECONDS.toSeconds(iExecutionTime)));
	}

	/***
	 * 
	 */
	public void closeStep() {

		if (stepHeaderNum > 0) {

			try {
				File file = new File(filePath() + "/"
						+ HtmlReportSupport.strTestName.split("-")[0] + "_"
						+ HtmlReportSupport.rpTime + ".html");// "SummaryReport.html"
				Writer writer = null;
				writer = new FileWriter(file, true);
				writer.write("</tbody>");
				writer.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/***
	 * 
	 * @param OS
	 */
	public void closeDetailedReport(String OS) {

		try {
			closeStep();

			File file = new File(filePath() + "/"
					+ HtmlReportSupport.strTestName.split("-")[0] + "_"
					+ HtmlReportSupport.rpTime + ".html");// "SummaryReport.html"
			Writer writer = null;
			writer = new FileWriter(file, true);
			writer.write("</table>");
			writer.write("<table id='footer'>");
			writer.write("<colgroup>");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("</colgroup>");
			writer.write("<tfoot>");
			writer.write("<tr class='heading'> ");
			writer.write("<th colspan='4'>Execution Time In Seconds (Includes Report Creation Time) : "
					+ HtmlReportSupport.executionTime.get(HtmlReportSupport.tc_name)
					+ "&nbsp;</th> ");
			writer.write("</tr> ");
			writer.write("<tr class='content'>");
			writer.write("<td class='pass'>&nbsp;Steps Passed&nbsp;:</td>");
			writer.write("<td class='pass'> " + PassNum + "</td>");
			writer.write("<td class='fail'>&nbsp;Steps Failed&nbsp;: </td>");
			writer.write("<td class='fail'>" + FailNum + "</td>");
			writer.write("</tr>");
			writer.close();
		} catch (Exception e) {

		}
	}

	/***
	 * 
	 * @param OS
	 * @throws Exception
	 */
	@Parameters({ "OS" })
	@AfterMethod(alwaysRun = true)
	public void tearDown(String OS) throws Exception {
		System.out.println("~~~~~~~~After Method~~~~~~~~");
		calculateTestCaseExecutionTime();
		closeDetailedReport(OS);

		try {

			if (FailNum != 0) {

				testResults.put(HtmlReportSupport.tc_name, "FAIL");
				failCounter = failCounter + 1;
			} else {
				testResults.put(HtmlReportSupport.tc_name, "PASS");
				passCounter = passCounter + 1;
			}

			/*
			 * if (FailNum != 0 && tstData!=null ) {
			 * 
			 * testResults.put(tc_name, "FAIL"); failCounter = failCounter + 1;
			 * } else if(FailNum == 0 && tstData!=null){
			 * testResults.put(tc_name, "PASS"); passCounter = passCounter + 1;
			 * }else {//
			 * if(tstData.get("ExecuteStatus").trim().equalsIgnoreCase("No")){
			 * testResults.put(tc_name, "SKIP"); skipCounter=skipCounter+1; }
			 */
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println(HtmlReportSupport.tc_name);
		HtmlReportSupport.createHtmlSummaryReport(OS);
		/*
		 * try { driver.switchTo().alert(); //return true; } // try catch
		 * (NoAlertPresentException Ex) { //return false; DateFormat dateFormat
		 * = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss"); Date date = new
		 * Date(); File scrFile = ((TakesScreenshot) driver)
		 * .getScreenshotAs(OutputType.FILE); FileUtils.copyFile(scrFile, new
		 * File(testSetup.filePath() + "/Screenshots/"
		 * +testName+dateFormat.format(date) + ".png")); }
		 */
		// driver.close();
		/*
		 * if(driver!=null) driver.quit();
		 */
		try {
			//driver.switchTo().alert();
			//return true;
		} // try
		catch (NoAlertPresentException Ex) {
			//return false;
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
			Date date = new Date();
			if(configProps.getProperty("deviceType").equals("iOS")){
				File scrFile = ((TakesScreenshot) iosDriver)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(TestSetup.filePath() + "/Screenshots/"
						+testName+dateFormat.format(date) + ".png"));
			}
			else
			{
				//File scrFile = ((TakesScreenshot) androidDriver).getScreenshotAs(OutputType.FILE);
				File scrFile = ((TakesScreenshot) androidDriver1)
						.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(TestSetup.filePath() + "/Screenshots/"
						+testName+dateFormat.format(date) + ".png"));
			}
		}
		//androidDriver1.quit();
		//androidDriver1.close();
		//androidDriver1.quit();
	}

	/***
	 * 
	 * @param strStepName
	 * @param strStepDes
	 */
//	public void onSuccess(String strStepName, String strStepDes) {
//		onSuccess(strStepName, strStepDes, "");
//	}

	/***
	 * 
	 * @param strStepName
	 * @param strStepDes
	 * @param stepTime
	 */
	public void onSuccess(String strStepName, String strStepDes, String stepTime) {

		file = new File(TestSetup.filePath() + "/"
				+ HtmlReportSupport.strTestName.split("-")[0] + "_"
				+ HtmlReportSupport.rpTime + ".html");// "SummaryReport.html"
		Writer writer = null;
		stepNum = stepNum + 1;

		try {
			// testdescrption.put(TestTitleDetails.x.toString(),
			// TestEngine.testDescription.get(TestTitleDetails.x));
			if (!HtmlReportSupport.map.get(
					HtmlReportSupport.packageName + ":" + HtmlReportSupport.tc_name)
					.equals("FAIL")) {
				HtmlReportSupport.map.put(HtmlReportSupport.packageName + ":"
						+ HtmlReportSupport.tc_name, "PASS");
				// map.put(TestTitleDetails.x.toString(),
				// TestEngine.testDescription.get(TestTitleDetails.x.toString()));
			}
			writer = new FileWriter(file, true);
			writer.write("<tr class='content2' >");
			writer.write("<td>" + stepNum + "</td> ");
			writer.write("<td class='justified'>" + strStepName + "</td>");
			writer.write("<td class='justified'>"
					+ strStepDes.replace("[", "<b>").replace("]", "</b>")
					+ "</td> ");
			writer.write("<td class='Pass' align='center'><font size='2' color='green'><B>Pass</B></font><img  src='./Screenshots/passed.ico' width='18' height='18'/></td> ");
			PassNum = PassNum + 1;
			String strPassTime = ReportStampSupport.getTime();
			writer.write("<td><small>" + strPassTime + "</small></td> ");
			writer.write("</tr> ");
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/***
	 * 
	 * @param strStepDes
	 */
	public void addStep(String strStepDes) {
		closeStep();
		File file = new File(TestSetup.filePath() + "/"
				+ HtmlReportSupport.strTestName.split("-")[0] + "_"
				+ HtmlReportSupport.rpTime + ".html");// "SummaryReport.html"
		Writer writer = null;

		stepHeaderNum = stepNum + 1;
		String sheaderId = "Header_" + stepHeaderNum;
		try {

			writer = new FileWriter(file, true);
			writer.write("<tbody><tr class='section'>");
			writer.write("<td colspan='5' onclick=toggleMenu('" + sheaderId
					+ "')>+ ");
			writer.write(strStepDes.replace("[", "<b>").replace("]", "</b>")
					+ "</td></tr></tbody>");
			writer.write("<tbody id='" + sheaderId
					+ "' style='display:table-row-group'>");

			/*
			 * writer.write("<tr class='content2' >"); writer.write("<td>" +
			 * stepNum + "</td> "); writer.write("<td class='justified'>" +
			 * strStepName + "</td>");
			 * writer.write("<td class='justified'><span style='text-align: left;'>"
			 * + strStepDes.replace("[", "<b>").replace("]", "</b>") +
			 * "</span></td> "); writer.write(
			 * "<td class='Pass' align='center'><img  src='./Screenshots/passed.ico' width='18' height='18'/></td> "
			 * ); PassNum = PassNum + 1; String strPassTime =
			 * ReportStampSupport.getTime(); writer.write("<td><small>" +
			 * strPassTime + "</small></td> "); writer.write("</tr> ");
			 */
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/***
	 * 
	 * @param strStepName
	 * @param strStepDes
	 */
	public void onWarning(String strStepName, String strStepDes) {
		onWarning(strStepName, strStepDes, "");
	}

	/***
	 * 
	 * @param strStepName
	 * @param strStepDes
	 * @param stepTime
	 */
	public void onWarning(String strStepName, String strStepDes, String stepTime) {

		Writer writer = null;
		try {
			File file = new File(TestSetup.filePath() + "/"
					+ HtmlReportSupport.strTestName.split("-")[0] + "_"
					+ HtmlReportSupport.rpTime + ".html");// "SummaryReport.html"

			writer = new FileWriter(file, true);
			stepNum = stepNum + 1;

			writer.write("<tr class='content2' >");
			writer.write("<td>" + stepNum + "</td> ");
			writer.write("<td class='justified'>" + strStepName + "</td>");
			writer.write("<td class='justified'>" + strStepDes + "</td> ");
			FailNum = FailNum + 1;

			writer.write("<td class='Fail'  align='center'><a  href='"
					+ "./Screenshots"
					+ "/"
					+ strStepDes.replace(" ", "_").replace("[", "")
							.replace("]", "")
					+ stepTime
					+ ".jpeg'"
					+ " alt= Screenshot  width= 15 height=15 style='text-decoration:none;'><img src='./Screenshots/warning.ico' width='18' height='18'/></a></td>");

			String strFailTime = ReportStampSupport.getTime();
			writer.write("<td><small>" + strFailTime + "</small></td> ");
			writer.write("</tr> ");
			writer.close();

		} catch (Exception e) {

		}

	}

	/***
	 * 
	 * @param strStepName
	 * @param strStepDes
	 * @param stepTime
	 */
	public void onFailure(String strStepName, String strStepDes, String stepTime) {
		Writer writer = null;
		try {
			File file = new File(TestSetup.filePath() + "/"
					+ HtmlReportSupport.strTestName.split("-")[0] + "_"
					+ HtmlReportSupport.rpTime + ".html");// "SummaryReport.html"

			writer = new FileWriter(file, true);
			stepNum = stepNum + 1;

			writer.write("<tr class='content2' >");
			writer.write("<td>" + stepNum + "</td> ");
			writer.write("<td class='justified'>" + strStepName + "</td>");

			writer.write("<td class='justified'>"
					+ strStepDes.replace("[", "<b>").replace("]", "</b>")
					+ "</td> ");

			FailNum = FailNum + 1;

			writer.write("<td class='Fail' align='center'><a  href='"
					+ "./Screenshots"
					+ "/"
					+ stepTime
					+ ".jpeg'"
					+ " alt= Screenshot  width= 15 height=15 style='text-decoration:none;'><font size='2' color='red'><B>Fail</B></font><img  src='./Screenshots/failed.ico' width='18' height='18'/></a></td>");

			String strFailTime = ReportStampSupport.getTime();
			writer.write("<td><small>" + strFailTime + "</small></td> ");
			writer.write("</tr> ");
			writer.close();
			if (!HtmlReportSupport.map.get(
					HtmlReportSupport.packageName + ":" + HtmlReportSupport.tc_name)
					.equals("PASS")) {
				HtmlReportSupport.map.put(HtmlReportSupport.packageName + ":"
						+ HtmlReportSupport.tc_name + ":", "FAIL");
				// map.put(TestTitleDetails.x.toString(),
				// TestEngine.testDescription.get(TestTitleDetails.x.toString()));
			}
		} catch (Exception e) {

		}

	}

	/***
	 * 
	 * @param OS
	 */
	public void closeSummaryReport(String OS) {
		file = new File(TestSetup.filePath() + "/" + "Summary_Results_"
				+ TestSetup.rpSumDt + ".html");// "SummaryReport.html"
		Writer writer = null;
		try {
			writer = new FileWriter(file, true);

			writer.write("<table id='footer'>");
			writer.write("<colgroup>");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' /> ");
			writer.write("</colgroup> ");
			writer.write("<tfoot>");
			writer.write("<tr class='heading'>");
			writer.write("<th colspan='4'>Total Duration  In Seconds (Including Report Creation) : "
					+ ((int) HtmlReportSupport.iTestExecutionTime) + "</th>");
			writer.write("</tr>");
			writer.write("<tr class='content'>");
			writer.write("<td class='pass'>&nbsp;Tests Passed&nbsp;:</td>");
			writer.write("<td class='pass'> " + passCounter + "</td> ");
			writer.write("<td class='fail'>&nbsp;Tests Failed&nbsp;:</td>");
			writer.write("<td class='fail'> " + failCounter + "</td> ");
			writer.write("</tr>");
			writer.write("</tfoot>");
			writer.write("</table> ");

			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***
	 * 
	 * @param StepDesc
	 */
	public void reportStep(String StepDesc) {
		StepDesc = StepDesc.replaceAll(" ", "_");
		File file = new File(TestSetup.filePath() + "/"
				+ HtmlReportSupport.strTestName.split("-")[0] + "_"
				+ HtmlReportSupport.rpTime + ".html");// "SummaryReport.html"
		Writer writer = null;

		try {
			writer = new FileWriter(file, true);
			if (HtmlReportSupport.BFunctionNo > 0) {
				writer.write("</tbody>");
			}
			writer.write("<tbody>");
			writer.write("<tr class='section'> ");
			writer.write("<td colspan='5' onclick=toggleMenu('" + StepDesc
					+ stepNum + "')>+ " + StepDesc + "</td>");
			writer.write("</tr> ");
			writer.write("</tbody>");
			writer.write("<tbody id='" + StepDesc + stepNum
					+ "' style='display:table-row-group'>");
			writer.close();
			HtmlReportSupport.BFunctionNo = HtmlReportSupport.BFunctionNo + 1;
		} catch (Exception e) {

		}
	}

	/*
	 * @BeforeTest public void startTest(final ITestContext testContext) {
	 * 
	 * htmlRepSupport.iTestStartTime = System.currentTimeMillis();
	 * application=testContext
	 * .getCurrentXmlTest().getParameter("CurrentApplication").trim();
	 * chartName=configProps.getProperty("ReleaseVersion")+"ExecutionStatus";
	 * failCounter=0; passCounter=0; htmlRepSupport.serialNo=0;
	 * folderName=testContext.getName().trim(); System.out.println(folderName);
	 * // it prints "Check name test" }
	 */

	/***
	 * 
	 * @throws IOException
	 */
	@AfterTest
	public void closeTest() throws IOException {

		ReportStampSupport.calculateTestExecutionTime();

		if (configProps.getProperty("ChartType").trim().equalsIgnoreCase("Pie")
				|| configProps.getProperty("ChartType").trim().contains("Pie")
				|| configProps.getProperty("ChartType").trim().contains("pie"))
			createPieChartForReport(chartName, passCounter, failCounter,
					skipCounter);
		else
			System.out.println("Currently Supports only Pie Graph");
		// createBarChartForReport(chartName, passCounter, failCounter, 0);

		closeSummaryReport(HtmlReportSupport.browser);
		HtmlReportSupport.writer.close();
	}

	/**
	 * Used to Create Pie Chart for Report
	 * 
	 * @param suiteName
	 * @param passCount
	 * @param failCount
	 * @param skipCount
	 */
	public void createPieChartForReport(String chartName, int passCounter,
			int failCounter, int skipCounter) throws IOException {

		DefaultPieDataset dataset = new DefaultPieDataset();

		// Data Set Values
		dataset.setValue("Pass " + passCounter, new Double(passCounter));
		dataset.setValue("Fail " + failCounter, new Double(failCounter));
		dataset.setValue("Skip " + skipCounter, new Double(skipCounter));

		JFreeChart chart = ChartFactory.createPieChart(chartName, dataset,
				true, true, true);

		// LegendTitle legend = chart.getLegend();
		// legend.setFrame(new BlockBorder(Color.white));

		PiePlot ColorConfigurator = (PiePlot) chart.getPlot();
		ColorConfigurator.setSimpleLabels(true);
		ColorConfigurator.setLabelLinksVisible(true);
		ColorConfigurator.setSectionOutlinesVisible(false);
		ColorConfigurator.setShadowPaint(null);
		ColorConfigurator.setLabelGenerator(null);

		// Percentage Values
		ColorConfigurator
				.setLabelGenerator(new StandardPieSectionLabelGenerator("{2}"));

		// Customized Colors
		Color pass_color = new Color(200, 57, 35);
		Color fail_color = new Color(231, 222, 21);
		Color skip_color = new Color(83, 180, 29);

		ColorConfigurator.setSectionPaint("Skip " + skipCounter, fail_color);
		ColorConfigurator.setSectionPaint("Fail " + failCounter, pass_color);
		ColorConfigurator.setSectionPaint("Pass " + passCounter, skip_color);

		int width = 400;
		int height = 400;
		try {
			ChartUtilities.saveChartAsJPEG(new File(TestSetup.filePath()
					+ "\\Screenshots\\" + chartName + ".jpg"), chart, width,
					height);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/***
	 * 
	 * @param ctx
	 * @param OS
	 */
	@Parameters({ "OS" })
	@AfterSuite(alwaysRun = true)
	public void afterSuite(ITestContext ctx, String OS) {

		System.out.println("~~~~~~~~After Suite~~~~~~~~");
		ReportStampSupport.calculateSuiteExecutionTime();
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.closeApp();
			iosDriver.quit();
		}
		else
		{
			//androidDriver.closeApp();
			//androidDriver.quit();
			//androidDriver1.closeApp();
			//androidDriver1.quit();
			
		}
		//CommandExecutor.executeCommand("kill all Terminal");
	}

	// ~~~~~~~~After Class~~~~~~~~
	/***
	 * 
	 * @param browser
	 */
	@Parameters({ "OS" })
	@AfterClass(alwaysRun = true)
	public void close(String browser) {
		
			try {
				System.out.println("...After Class...");

			} catch (Exception e) {
				System.out.println("...Failed in After Class...");
				androidDriver1.quit();
				e.printStackTrace();
			}
		
		
		
		/*if (androidDriver1 != null)
			androidDriver1.quit();*/
		/* driver.close(); */
		/*
		 * if(driver!=null) driver.quit();
		 * 
		 * driver.close(); if(driver!=null) driver.quit(); if (FailNum != 0) {
		 * 
		 * testResults.put(tc_name, "FAIL"); failCounter = failCounter + 1; }
		 * else { testResults.put(tc_name, "PASS"); passCounter = passCounter +
		 * 1; }
		 */
		
		/*
			try {
				System.out.println("...After Class...");

			} catch (Exception e) {
				System.out.println("...Failed in After Class...");
				androidDriver1.close();
				e.printStackTrace();
			}*/
		}
	
	
	public void SuccessReport(String strStepName, String strStepDes) throws Throwable {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");
		String stepTime = sdf.format(date);
		int intReporterType = Integer.parseInt(configProps.getProperty("reportsType"));
		switch (intReporterType) {
		case 1:

			break;
		case 2:
			if (configProps.getProperty("OnSuccessScreenshot").equalsIgnoreCase("True")) {
				screenShot(filePath() + strStepDes.replace(" ", "_") + ".jpeg");
			}
			onSuccess(strStepName, strStepDes, stepTime);

			break;

		default:
			if (configProps.getProperty("OnSuccessScreenshot").equalsIgnoreCase("True")) {
				screenShot(filePath() + strStepDes.replace(" ", "_") + ".jpeg");
			}
			onSuccess(strStepName, strStepDes, stepTime);
			break;
		}
	}

	//
	public void failureReport(String strStepName, String strStepDes) throws Throwable {

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");
		String stepTime = sdf.format(date);

		int intReporterType = Integer.parseInt(configProps.getProperty("reportsType"));
		switch (intReporterType) {
		case 1:
			flag = true;
			break;
		case 2:
//			screenShot(testSetup.filePath() + "/" + "Screenshots" + "/" + strStepDes.replace(" ", "_") + stepTime
//					+ ".jpeg");
			screenShot(filePath() + "/" + "Screenshots" + "/" + stepTime
					+ ".jpeg");
			flag = true;
			onFailure(strStepName, strStepName, stepTime);
			break;
         
		default:
			flag = true;
			screenShot(filePath() + "/" + "Screenshots" + "/" + strStepDes.replace(" ", "_") + stepTime
					+ ".jpeg");
			onFailure(strStepName, strStepDes, stepTime);
			break;
		}

	}
	
	/**
	 * Capture Screenshot
	 * 
	 * @param fileName
	 *            : FileName screenshot save in local directory
	 * 
	 */
	public void screenShot(String fileName) {
		File scrFile;
		if(configProps.getProperty("deviceType").equals("iOS")){
			scrFile = ((TakesScreenshot) iosDriver).getScreenshotAs(OutputType.FILE);
		}
		else
		{
			//scrFile = ((TakesScreenshot) androidDriver).getScreenshotAs(OutputType.FILE);
			scrFile = ((TakesScreenshot) androidDriver1).getScreenshotAs(OutputType.FILE);
		}
		try {
			// Now you can do whatever you need to do with it, for example copy
			// somewhere
			FileUtils.copyFile(scrFile, new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
