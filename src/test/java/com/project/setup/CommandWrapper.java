package com.project.setup;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import io.appium.java_client.SwipeElementDirection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;

import com.google.common.base.Function;
import com.project.ClassicObjectRepository.CommonObjects;
import com.project.ClassicObjectRepository.DiscussionsObjects;
import com.project.report.Accelerators.ActionEngine;


/**
 * Created by e001237 on 07/05/17.
 */

public class CommandWrapper extends TestSetup {

	boolean b = true; 
	private static final Logger logger = LogManager.getLogger(CommandWrapper.class.getName());
	public static boolean DEBUG = false;
	ActionEngine actionEngine = new ActionEngine();

	/**
	 * 
	 * @param url
	 *            - application URL
	 * @return true or false
	 * @throws Throwable
	 */
	public boolean launchUrl(String url) throws Throwable {
		boolean flag = false;
		try{
			if(configProps.getProperty("deviceType").equals("iOS")){
				iosDriver.get(url);
			}
			else
			{
				//androidDriver.get(url);
				//driver.get(url);
				String s=androidDriver1.getContext();
				System.out.println(s);
				androidDriver1.get(url);
				Thread.sleep(2000);
				//androidDriver1.manage().deleteAllCookies();
				
				
				//driver.close();
				System.out.println("Launched URL in Chrome");
				Thread.sleep(2000);
			}

			flag = true;			
			return true;
		} catch (Exception e) {
			// logger.info(url + " could not be launched");
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				failureReport("Launch URL", "URL: " + url
						+ " could not be launched");
				// throw new ElementNotFoundException("", "", "");
		
			} else {
				SuccessReport("Launch URL", "URL: " +  url
						+ " launched successfully");
			
			}			
	
		}
		
	}
	
	
	/**
	 * 
	 * @param url
	 *            - application URL
	 * @return true or false
	 * @throws Throwable
	 */
	public boolean launchNewUrl(String url) throws Throwable {
		boolean flag = false;
		try{
			if(configProps.getProperty("deviceType").equals("iOS")){
				iosDriver.get(url);
			}
			else
			{
				//androidDriver.get(url);
				//driver.get(url);
				String s=androidDriver1.getContext();
				System.out.println(s);
				deleteCookies();
				androidDriver1.get(url);
				//androidDriver1.manage().deleteAllCookies();
				
				
				//driver.close();
				System.out.println("Launched URL in Chrome");
			}

			flag = true;			
			return true;
		} catch (Exception e) {
			// logger.info(url + " could not be launched");
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				failureReport("Launch URL", "URL: " + url
						+ " could not be launched");
				// throw new ElementNotFoundException("", "", "");
		
			} else {
				SuccessReport("Launch URL", "URL: " +  url
						+ " launched successfully");
			
			}			
	
		}
		
	}
	
	
	
	public boolean deleteCookies(){
		boolean status=true;
		try{
			androidDriver1.manage().deleteAllCookies();
			Thread.sleep(3000);
			androidDriver1.navigate().refresh();
			Thread.sleep(3000);
			androidDriver1.manage().deleteAllCookies(); 
			androidDriver1.navigate().refresh();
			Thread.sleep(1000);
			androidDriver1.manage().deleteAllCookies(); 
			Thread.sleep(3000);
			androidDriver1.navigate().refresh();
			androidDriver1.manage().deleteAllCookies(); 
			androidDriver1.navigate().refresh();
			Thread.sleep(1000);
			androidDriver1.manage().deleteAllCookies(); 
			Thread.sleep(3000);
			androidDriver1.navigate().refresh();
		 
		}catch(Exception e){
			status=false;
			e.printStackTrace();
		}
		return status;
	}
	
	// Mahender as Browser Search is in Mid of the chrome browser .. Changing to pass by xpath
	
	
	
	// Not Required
	
	public boolean launchUrl1(String url) throws Throwable {
		boolean flag = false;
		try{
			if(configProps.getProperty("deviceType").equals("iOS")){
				iosDriver.get(url);
			}
			else
			{
				//androidDriver.get(url);
				//driver.get(url);
				
				/*String s=androidDriver1.getContext();
				System.out.println(s);*/
				
				//androidDriver1.get(url);
				
				androidDriver1.findElement(By.xpath("//*[@id='search_box_text']")).sendKeys(url);
				
				
				//driver.close();
				System.out.println("RE-Launched URL in Chrome");
			}

			flag = true;			
			return true;
		} catch (Exception e) {
			// logger.info(url + " could not be launched");
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				failureReport("Launch URL", "URL: " + url
						+ " could not be launched");
				// throw new ElementNotFoundException("", "", "");
		
			} else {
				SuccessReport("Launch URL", "URL: " +  url
						+ " launched successfully");
			
			}			
	
		}
		
	}

	

	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementClick(By by, String eleDesc) throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, by);
		}
		else
		{
			//explicitWait(androidDriver, by);
			explicitWait(androidDriver1, by);
		}
		boolean flag = false;
		if (elementExists(by, eleDesc)) {
			try {
				if(configProps.getProperty("deviceType").equals("iOS")){
					iosDriver.findElement(by).click();
				}
				else
				{
					//androidDriver.findElement(by).click();
					androidDriver1.findElement(by).click();
				}

				// logger.info(btnText + " clicked successfully");
				flag = true;
				return true;
			} catch (Exception e) {
				// logger.info(btnText + " could not be clicked");
				return false;
			} finally {
				if (!flag) {
					failureReport("Click element",
							" Failed to click element:" + eleDesc);
					// throw new ElementNotFoundException("", "", "");

				} else {
					SuccessReport("Click element",
							"Successfully clicked on element:" + eleDesc);
				}
			}
		} else {
			// logger.info(btnText + " could not be clicked");
			failureReport("Element exists",
					" Failed to identify element:" + eleDesc);
			return false;
		}
	}
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementClickNegative(By by, String eleDesc) throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, by);
		}
		else
		{
			//explicitWait(androidDriver, by);
			explicitWait(androidDriver1, by);
		}
		boolean flag = false;
		if (elementExists(by, eleDesc)) {
			try {
				if(configProps.getProperty("deviceType").equals("iOS")){
					iosDriver.findElement(by).click();
				}
				else
				{
					//androidDriver.findElement(by).click();
					androidDriver1.findElement(by).click();
				}

				// logger.info(btnText + " clicked successfully");
				flag = true;
				return true;
			} catch (Exception e) {
				// logger.info(btnText + " could not be clicked");
				return false;
			} finally {
				if (!flag) {
					//failureReport("Click element", " Failed to click element:" + eleDesc);
					// throw new ElementNotFoundException("", "", "");

				} else {
					//SuccessReport("Click element", "Successfully clicked on element:" + eleDesc);
				}
			}
		} else {
			// logger.info(btnText + " could not be clicked");
			failureReport("Element exists",
					" Failed to identify element:" + eleDesc);
			return false;
		}
	}
	/**
	 * This method verify check box is checked or not
	 * 
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * 
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:sign in Checkbox etc..)
	 * 
	 * @return: boolean value(True: if it is checked, False: if not checked)
	 * 
	 */
	public boolean isCheckedNegative(By locator, String locatorName) throws Throwable {
		boolean bvalue = false;
		try {
			System.out.println("..");
			if (androidDriver1.findElement(locator).isSelected()) {
				flag = true;
				bvalue = true;
			}

		} catch (NoSuchElementException e) {

			bvalue = false;
		}
		return bvalue;
	}

	/**
	 * This method switch the focus to selected frame using frame index
	 * 
	 * @param index
	 *            : Index of frame wish to switch
	 * 
	 */
	public boolean switchToFrameByIndex(int index) throws Throwable {
		boolean flag = false;
		try {
			androidDriver1.switchTo().frame(index);
			Thread.sleep(200);
			flag = true;
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} 
	}
	
	/**
	 * This method returns check existence of element
	 * 
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:Textbox, checkbox etc)
	 * @return: Boolean value(True or False)
	 * @throws NoSuchElementException
	 */
	public boolean isElementPresentNegative(By by, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			androidDriver1.findElement(by);
			flag = true;
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
			return flag;
		
		}
	}
	
	/**
	 * This method wait driver until Invisibility of Elements on WebPage.
	 * 
	 * @param by
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * 
	 * @param by
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * 
	 */
	public boolean waitForInVisibilityOfElement(By by, String locator) throws Throwable {
		boolean flag = false;
		WebDriverWait wait = new WebDriverWait(androidDriver1, 120000);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
			flag = true;
			return flag;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	/**
	 * This method wait selenium until visibility of Elements on WebPage.
	 * 
	 * @param by
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @throws Throwable
	 * 
	 */

	public boolean waitForVisibilityOfElement(By by, String locator) throws Throwable {
		boolean flag = false;
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(androidDriver1, 30);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (!flag) {
				failureReport("WaitForVisibilityOfElement ", " Element " + locator + " is not visible");
			} else if (b && flag) {
				SuccessReport("WaitForVisibilityOfElement ", " Element " + locator + "  is visible");
			}
		}
	}
	
	public boolean isElementPresent(By by, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			androidDriver1.findElement(by);
			flag = true;
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if (!flag) {
				failureReport("Element Present ", locatorName + " is not present on the page");
			} else if (b && flag) {
				SuccessReport("Element Present ", locatorName+" present on the page");
			}

		}
	}
	
	
	public boolean isElementDisplayed(By by, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			androidDriver1.findElement(by).isDisplayed();
			flag = true;
			return flag;
		} catch (Exception e) {
			//e.printStackTrace();
			return flag;
		
		}
	}
	/**
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:Login Button, SignIn Link
	 *            etc..)
	 * @return --boolean (true or false)
	 * @throws Throwable
	 */

	public boolean clickNoImage(By locator) throws Throwable {
		try {
			androidDriver1.findElement(locator).click();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	
	
	/**
	 * Used to Move Focus to the Latest Browser Window
	 * 
	 * @throws Throwable
	 */
	public void windowHandles() throws Throwable {
		try {
			Set<String> windows = androidDriver1.getWindowHandles();
			for (String w : windows) {
				androidDriver1.switchTo().window(w);
			}
		} catch (Exception e) {
			failureReport("Window Handle", "Failed");
			e.printStackTrace();
		}

	}
	
	/**
	 * To Switch Driver focus to Main Window
	 * 
	 * @throws Throwable
	 */
	public void backToMainWindow() throws Throwable {

		try {
			Set<String> windows = androidDriver1.getWindowHandles();
			for (String w : windows) {
				androidDriver1.switchTo().window(w);
				break;
			}
		} catch (Exception e) {
			failureReport("Back to Main Window", "Failed");
			e.printStackTrace();
		}
	}
	

	/**
	 * This method switch the to Default Frame.
	 * 
	 * @throws Throwable
	 */
	public boolean switchToDefaultFrame() throws Throwable {
		boolean flag = false;
		try {
			androidDriver1.switchTo().defaultContent();
			//androidDriver1.switchTo().parentFrame();
			
			flag = true;
			Thread.sleep(3000);
			return flag;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} 
		
	}
	
	/**
	 * This method switch the to frame using frame Name.
	 * 
	 * @param nameValue
	 *            : Frame Name wish to switch
	 * 
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:SignIn Link, login button
	 *            etc..)
	 * 
	 * 
	 */
	public boolean switchToFrameByLocator(By lacator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			androidDriver1.switchTo().frame(androidDriver1.findElement(lacator));
			flag = true;
			return flag;
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		} finally {}
	}

	public int getElementsSize(By locator, String locatorName) throws Throwable {
		int text = 0;
		try {
			if (androidDriver1.findElement(locator).isDisplayed()) {
				text = androidDriver1.findElements(locator).size();
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}
		return text;
	}
	

	
	/***
	 * 
	 * @param by
	 * @param sendText
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementSendText(By by, String sendText, String eleDesc) throws Throwable {
		boolean flag = false;
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, by);
		}
		else
		{
			//explicitWait(androidDriver, by);
			explicitWait(androidDriver1, by);
		}
		if (elementExists(by, eleDesc)) {
			try {
				 if(configProps.getProperty("deviceType").equals("iOS")){
					iosDriver.findElement(by).sendKeys(sendText);
				 }
				 else
				 {
					//androidDriver.findElement(by).sendKeys(sendText);
					 androidDriver1.findElement(by).sendKeys(sendText);
				 }
				 
				// logger.info(sendText + " inserted successfully");
				flag = true;
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			} finally {
				if (!flag) {
					failureReport("Send text",
							" Failed to send text" + sendText + " to element:"
									+ eleDesc);
					// throw new ElementNotFoundException("", "", "");

				} else {
					SuccessReport("Send text", "Send text "
							+ sendText + " to element:" + eleDesc);
				}
			}
		} else {
			// logger.info(sendText + " could not be inserted");
			failureReport("Element exists",
					" Failed to identify element:" + eleDesc);
			return false;
		}
	}
	
	public void ExplicitWaitOnElementToBeClickable(By locator) {
		try {
			WebDriverWait wait = new WebDriverWait(androidDriver1, 20);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Throwable e) {
		}
	}

	/**
	 * select value from DropDown by using selectByVisibleText
	 * 
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * 
	 * @param visibletext
	 *            : VisibleText wish to select from dropdown list.
	 * 
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:Year Dropdown, items
	 *            Listbox etc..)
	 */

	public boolean selectByVisibleText(By locator, String visibletext, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(androidDriver1.findElement(locator));
			Thread.sleep(2000);
			s.selectByVisibleText(visibletext);
			flag = true;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				failureReport("Select ", visibletext + " is Not Select from the DropDown " + locatorName);

			} else if (b && flag) {
				SuccessReport("Select ", visibletext + "  is Selected from the DropDown " + locatorName);
			}
		}
	}

	public boolean selectByValue(By locator, String value, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			Select s = new Select(androidDriver1.findElement(locator));
			Thread.sleep(500);
			s.selectByValue(value);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (!flag) {
				failureReport("Select",
						"Option with value attribute " + value + " is Not Select from the DropDown " + locatorName);

			} else if (b && flag) {
				SuccessReport("Select ",
						"Option with value attribute " + value + " is  Selected from the DropDown " + locatorName);
			}
		}
	}
	
	
	
	
	
	/***
	 * 
	 * @param by
	 * @param sendText
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public String elementGetText(By by,String eleDesc) throws Throwable {             
        String text = "";
        boolean flag = false;
        if (elementExists(by, eleDesc)) {
               try {
            	    if(configProps.getProperty("deviceType").equals("iOS")){
       				   text = iosDriver.findElement(by).getText();
       			   }
       			   else
       			   {
       				   //text = androidDriver.findElement(by).getText();
       				   text = androidDriver1.findElement(by).getText();
       			   }
                     //
                     flag = true;  
               } catch (Exception e) {
                  e.printStackTrace();
               } finally {
                     if (!flag) {
                            failureReport("Get text"," Failed to get text from element:"+ eleDesc);
                     } else {
                            SuccessReport("Get text", "Get text from element:" + eleDesc);
                     }
               }
        } else {
               failureReport("Element exists"," Failed to identify element:" + eleDesc);
        }
        return text;
     }
	
	public String getText(By locator, String locatorName) throws Throwable {
		ExplicitWaitOnElementToBeClickable(locator);
		String text = "";
		//boolean flag = false;
		try {
			if (isElementPresentNegative(locator, locatorName)) {
				text = androidDriver1.findElement(locator).getText();
			//	flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			
		}
		return text;
	}

	
	public String getValue(By locator, String locatorName) throws Throwable {
		String text = "";
		boolean flag = false;
		try {
			if (androidDriver1.findElement(locator).isDisplayed()) {
				text = androidDriver1.findElement(locator).getAttribute("value");
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!flag) {
				failureReport("GetValue ", " Unable to get Text from " + locatorName);
			} else if (b && flag) {
				SuccessReport("GetValue ", " Able to get Text from " + locatorName);
			}
		}
		return text;
	}

	
	
	
	/***
	 * 
	 * @param by
	 * @return
	 * @throws Throwable
	 */
	public boolean elementExists(By by) throws Throwable {
	 try {
		  if(configProps.getProperty("deviceType").equals("iOS")){
				iosDriver.findElement(by);
			}
			else
			{
				//androidDriver.findElement(by);
				androidDriver1.findElement(by);
			} 
			 return true;
		} catch (NoSuchElementException e) {
			//e.printStackTrace();
			return false;
		} 
	}
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	@SuppressWarnings("finally")
	public boolean elementExists(By by, String eleDesc) throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, by);
		}
		else
		{
			//explicitWait(androidDriver, by);
			explicitWait(androidDriver1, by);
		}
		boolean flag = false;
		try {
			if(configProps.getProperty("deviceType").equals("iOS")){
				iosDriver.findElement(by);
			}
			else
			{
				//androidDriver.findElement(by);
				androidDriver1.findElement(by);
			}			 
			
			flag = true;
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				
				failureReport("Element exists ","Element "  + eleDesc  +   "   exists");
				return false;
			} else {
				SuccessReport("Element exists ", "Element " + eleDesc  +  "   exists");
				return true;
			}
		}
	}

	
	
	public boolean elementExistsNoReport(By by, String eleDesc) throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, by);
		}
		else
		{
			//explicitWait(androidDriver, by);
			explicitWait(androidDriver1, by);
		}
		boolean flag = false;
		try {
			if(configProps.getProperty("deviceType").equals("iOS")){
				iosDriver.findElement(by);
			}
			else
			{
				//androidDriver.findElement(by);
				androidDriver1.findElement(by);
			}			 
			
			flag = true;
			return true;
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (!flag) {
				SuccessReport("Element exists", "Element " + eleDesc + "exists");
				return false;
			} else {
				failureReport("Element exists","Element " + eleDesc + "exists");
				return true;
			}
		}
	}
	/***
	 * 
	 * @param by
	 * @param timeOutInSec
	 * @return
	 * @throws Throwable
	 */
	public boolean elementWait(By by, int timeOutInSec) throws Throwable {
		boolean flag = false;
		WebDriverWait wait; 
		if(configProps.getProperty("deviceType").equals("iOS")){
			wait = new WebDriverWait(iosDriver, timeOutInSec);
		}
		else
		{
			//wait = new WebDriverWait(androidDriver, timeOutInSec);
			wait = new WebDriverWait(androidDriver1, timeOutInSec);
		}
		try {
			if(configProps.getProperty("deviceType").equals("iOS")){
				wait.until(ExpectedConditions.visibilityOf(iosDriver.findElement(by)));
			}
			else
			{
				//wait.until(ExpectedConditions.visibilityOf(androidDriver.findElement(by)));
				wait.until(ExpectedConditions.visibilityOf(androidDriver1.findElement(by)));
			}
			 
			flag = true;
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if (!flag) {
				failureReport("Element wait", "Element " + by
						+ " does not exists or visbile");
			} else {
				SuccessReport("Element wait", "Element " + by
						+ " visible");
			}
		}
	}
	
	/***
	 * 
	 * @param by
	 * @param timeOutInSec
	 * @return
	 * @throws Throwable
	 */
	public boolean elementWait(By by, int timeOutInSec, String eleDesc) throws Throwable {
		boolean flag = false;
		WebDriverWait wait;
		if(configProps.getProperty("deviceType").equals("iOS")){
			wait = new WebDriverWait(iosDriver, timeOutInSec);
		}
		else
		{
			//wait = new WebDriverWait(androidDriver, timeOutInSec);
			 wait = new WebDriverWait(androidDriver1, timeOutInSec);
		}
			try{
			if(configProps.getProperty("deviceType").equals("iOS")){
				wait.until(ExpectedConditions.visibilityOf(iosDriver.findElement(by)));
			}
			else
			{
				//wait.until(ExpectedConditions.visibilityOf(androidDriver.findElement(by)));
				wait.until(ExpectedConditions.visibilityOf(androidDriver1.findElement(by)));
			}
			
			flag = true;
			return true;
		} catch (Exception e) {
			
			return false;
		} finally {
			if (!flag) {
				failureReport("Element wait", "Element " + eleDesc
						+ " does not exists or visbile");
			} else {
				SuccessReport("Element wait", "Element " + eleDesc
						+ " visible");
			}
		}
	}

	/***
	 * 
	 * @param milliseconds
	 */
	public static void elementHardSleep(int milliseconds) {
		try {
			if (DEBUG) {
				logger.info("Sleep " + milliseconds + " milliseconds");
			}
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
		}

	}

	/***
	 * 
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean JSClick(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement element = androidDriver1.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) androidDriver1;
			Actions actions = new Actions(androidDriver1);
			actions.moveToElement(element);
			actions.perform();
			executor.executeScript("arguments[0].click();", element);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	/***
	 * 
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean JSMousehoverDoubleClick(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement element = androidDriver1.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) androidDriver1;
			Actions actions = new Actions(androidDriver1);
			//actions.moveToElement(element);
			//actions.perform();
			executor.executeScript("arguments[0].scrollIntoView();", element);
			actions.doubleClick(element).build().perform();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	
	/***
	 * 
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean JSscroll(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement element = androidDriver1.findElement(locator);
			JavascriptExecutor executor = (JavascriptExecutor) androidDriver1;
			executor.executeScript("arguments[0].scrollIntoView();", element);
			element.getLocation();
			element.getSize();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	
	
	

	/***
	 * 
	 * @author E001115
	 *
	 */
	enum deviceType {
		ANDROID, IOS
	}
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementSwipe(SwipeElementDirection direction,int offset,int duration) throws Throwable {
		boolean flag = false;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				 iosDriver.swipe(direction, offset, duration);
				
			 }
			 else
			 {
				//androidDriver.swipe(direction, offset, duration);
				/* androidDriver1.swipe(direction, offset, duration);
				 androidDriver1.swipe(startx, starty, endx, endy, duration);*/
				 //androidDriver1.pinch(el);
				//androidDriver1.pinch(offset, duration);
				
				 
				
				 
			 }
			  
			  flag = true;
			  return true;
			} catch (Exception e) {
				return flag;
		} 
	}
	
	/*public boolean AndroidElementSwipe() {
		TouchActions action = new TouchActions(androidDriver1);
		action.scroll(element, 10, 100);
		action.build();
		action.perform();
		return flag;
		
	}
	*/
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementSwipeWhileNotFound(By by,int offset,SwipeElementDirection direction,int duration,int delay,int maxSwipeRounds,boolean doClick) throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, by);
		}
		else
		{
			//explicitWait(androidDriver, by);
			explicitWait(androidDriver1, by);
		}
		boolean flag = false;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				 iosDriver.swipeWhileNotFound(by, offset, direction, duration, delay, maxSwipeRounds, doClick);
			 }
			 else
			 {
				 androidDriver.swipeWhileNotFound(by, offset, direction, duration, delay, maxSwipeRounds, doClick);
				//androidDriver1.swipeWhileNotFound(by, offset, direction, duration, delay, maxSwipeRounds, doClick);	
				
				/* WebElement element = androidDriver1.findElement(by);

		        ((JavascriptExecutor) androidDriver1).executeScript("arguments[0].scrollIntoView(true);", element);
			 */
			 }
			  		  
			  flag = true;
			  return true;
			} catch (Exception e) {
				return flag;
		} 
	}
	
	/**
	 * Swipe from bottum to top
	 * @return 
	 * @throws Throwable 
	 */
	public boolean swipeBottumToTop(By locator, String locatorName) throws Throwable{
		boolean flag = false;
		try {
			System.out.println("Ready to swipe");
			Dimension dimensions = androidDriver1.manage().window().getSize();
			boolean elFound=isNElementPresentNegative(locator, locatorName);
			while (!elFound) {
				Double screenHeightStart = dimensions.getHeight() * 0.5;
				int scrollStart = screenHeightStart.intValue();
				Double screenHeightEnd = dimensions.getHeight() * 0.2;
				int scrollEnd = screenHeightEnd.intValue();
				androidDriver1.swipe(0, scrollStart, 0, scrollEnd, 2000);
				elFound=isNElementPresentNegative(locator, locatorName);
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if (!flag) {
				failureReport("Swipe", "Swipe to locate an element and not found " + locatorName);
				return false;
			} else if (b && flag) {
				SuccessReport("Swipe", "Swipe to locate an element and found " + locatorName);
				return true;
			}
		}
		return flag;
		
	}
	
	public boolean isNElementPresentNegative(By by, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			System.out.println("Finding mobile element............");
			androidDriver1.findElement(by);
			flag = true;
			return flag;
		} catch (Exception e) {
			//e.printStackTrace();
			return flag;
		
		}
	}
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementContext(String context) throws Throwable {
		boolean flag = false;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				 iosDriver.context(context);
			 }
			 else
			 {
				 //androidDriver.context(context);
				 androidDriver1.context(context);
				 System.out.println(context);
			 }
			 
			  flag = true;
			  return true;
			} catch (Exception e) {
			  return flag;
		} 
	}
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementSendKeys(String text) throws Throwable {
		boolean flag = false;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				 iosDriver.sendKeys(text);
			 }
			 else
			 {
				//androidDriver.sendKeys(text);
				//androidDriver1.sendKeys(text);
			 }
			  
			  flag = true;
			  return true;
			} catch (Exception e) {
				e.printStackTrace();
			  return flag;
		} 
	}
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementCloseKeyBoard() throws Throwable {
		boolean flag = false;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				 iosDriver.closeKeyboard();
			 }
			 else
			 {
				// androidDriver.closeKeyboard();
				 //androidDriver1.closeKeyboard();
				androidDriver1.hideKeyboard();
				 
			 }
			  
			  flag = true;
			  return true;
			} catch (Exception e) {
				e.printStackTrace();
			  return flag;
		} 
	}
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean elementClear(By by) throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, by);
		}
		else
		{
			//explicitWait(androidDriver, by);
			explicitWait(androidDriver1, by);
		}
		boolean flag = false;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				 iosDriver.findElement(by).clear();
			 }
			 else
			 {
				 //androidDriver.findElement(by).clear();
				 androidDriver1.findElement(by).clear();
			 }
			 
			  flag = true;
			  return true;
			} catch (Exception e) {
				e.printStackTrace();
			  return flag;
		} 
	}
	
	/***
	 * 
	 * @param by
	 * @param eleDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean pageRefresh() throws Throwable {
		boolean flag = false;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				 iosDriver.navigate().refresh();
			 }
			 else
			 {
				 //androidDriver.navigate().refresh();
				 androidDriver1.navigate().refresh();
			 }
			  //driver.navigate().refresh();
			  flag = true;
			  return true;
			} catch (Exception e) {
			  return flag;
		} 
	}
	
	
	
	public void scrollingToElementofAPage(By locator) {
		try{ 
		WebElement element = androidDriver1.findElement(locator);
		((JavascriptExecutor) androidDriver1).executeScript("arguments[0].scrollIntoView();", element);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static WebElement explicitWait(WebDriver driver,By by) { 
		WebElement element = null;
    try{
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver);
        
        ((FluentWait<WebDriver>) wait).withTimeout(100, TimeUnit.SECONDS)  
                 .pollingEvery(1, TimeUnit.SECONDS)  
                 .ignoring(NoSuchElementException.class); 
        
      Function<WebDriver,WebElement> f =  new Function<WebDriver, WebElement>() {  
               public WebElement apply(WebDriver driver) {  
                 return driver.findElement(by);  
                }  
          };
          element  = wait.until(f);  
    }catch(Exception e){
      e.printStackTrace();
    }
            return element;
      
    }

	
}
