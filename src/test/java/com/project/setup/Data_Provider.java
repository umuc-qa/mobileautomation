package com.project.setup;

import java.io.File; 
import java.util.HashMap;
import jxl.LabelCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Data_Provider {
	
	public static ConfiguratorSupport configProps = new ConfiguratorSupport("config.properties");
	
	public static String testDataFolderPath = configProps.getProperty("TestData");
	/**
	 * Read data from excel sheet using data provider
	 * @param sheetName
	 * 
	 * @throws Exception
	 * @Date  19/02/2013
	 * @Revision History
	 * 
	 */

	public static String[][] getTableArray(String sheetName) throws Exception{
		try{
			Workbook workbook = Workbook.getWorkbook(new File(testDataFolderPath));
			Sheet sheet = workbook.getSheet(sheetName);
			int rows = sheet.getRows();
			int cols = sheet.getColumns();
			String[][] tabArray=new String[rows-1][cols];
			

			for (int i=1;i<rows;i++){
				for (int j=0;j<cols;j++){
					tabArray[i-1][j]=sheet.getCell(j,i).getContents();
				}
			}

			workbook.close();
			return(tabArray);
		}
		catch (Exception e) {
			System.out.println(e+Thread.currentThread().getStackTrace()[1].getClassName()+" dataprovider");
			return null;
		}

	}
	
	/***
	 * 
	 * @param sheetName
	 * @param uname
	 * @param text
	 * @param offset
	 * @throws Exception
	 */
	public static void setTableData(String sheetName,String uname,String text,int offset) throws Exception
	{
		Workbook work = Workbook.getWorkbook(new File("Data\\TestData.xlsx"));
		WritableWorkbook writer = Workbook.createWorkbook(new File("TestData\\TestData.xlsx"),work);
		//Workbook workbook = Workbook.getWorkbook(new File("TestData\\TestData.xls"));
		WritableSheet sheet = writer.getSheet(sheetName);
		LabelCell cell = sheet.findLabelCell(uname);
		//Cell cell = sheet.findCell(uname, 0, 0, 1000, 1000, false);
		
		sheet.addCell(new jxl.write.Label(cell.getColumn()+offset,cell.getRow() ,text));
		
		writer.write();
		writer.close();
		work.close();
	}


	public static HashMap<String,String> getTestData(String sheetName, String executeTCName) throws Exception{//, String executeTCName, String executeStatus
        try{
        		System.out.println("application name :"+TestSetup.application);
               System.out.println(TestSetup.application);
               HashMap<String,String> data=new HashMap<String,String>();
               String filePath=System.getProperty("user.dir")+ "\\Data\\"+TestSetup.application+"\\TestData.xls";
//               String filePath=System.getProperty("user.dir")+ "\\Data\\Classic\\TestData.xls";

               Workbook workbook = Workbook.getWorkbook(new File(filePath));
               String[]  tdata=null;
               Sheet sheet = workbook.getSheet(sheetName);
               int rows = sheet.getRows();
               int cols = sheet.getColumns(); 
               String[]  tabHeader=new String[cols];
               for (int i=0;i<cols;i++){
                       tabHeader[i]=sheet.getCell(i,0).getContents();
               }
               
               for (int i=1;i<rows;i++){ 
                     if(sheet.getCell(0,i).getContents().equalsIgnoreCase("Yes") && sheet.getCell(1,i).getContents().equalsIgnoreCase(executeTCName)){ //matching key;
                            tdata=new String[cols];
                            
                          for (int j=0;j<cols;j++){
                             
                                   if(sheet.getCell(j,i).getContents().equalsIgnoreCase("n/a")){
                                          tdata[j]="";
                                   }else{
                                          tdata[j]=sheet.getCell(j,i).getContents();
                                   }
                            
                            }
                          
                         break; 
                            }
                     }
               for(int i=0;i<tabHeader.length;i++){
                     data.put(tabHeader[i], tdata[i]);
                 }

               workbook.close();
               return data;
               
        }
        catch (Exception e) {
               System.out.println(e+Thread.currentThread().getStackTrace()[1].getClassName()+" dataprovider");
               return null;
        }

 }

}
