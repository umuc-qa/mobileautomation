package com.project.ELMTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

/**
 * Description : Creating Assignment, adding a file, evaluated by giving score and verifying if program chair 
 * is able to view the last score and comment for each project and competency submitted by Learner in their program.
 */

public class DM_74_VerifyIfProgramChairViewLastScoreCommentProjectCompetencySubmittedLearnerProgram extends BusinessFunctions {
	
	@Test
	public void DM_74_VerifyProgramChairViewLastScoreCommentsForProject() throws Throwable {
		
    tstData = Data_Provider.getTestData("Grades", "DM_74_VerifyIfProgramChairViewLastScore_CommentProjectCompetencySubmittedLearnerProgram");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(configProps.getProperty("Admin_App_URL"));		
		clearBrowserHistory();	
		getCommand.launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools","Assignments");
		assignmentCreation(assignmentName, categoryName, gradeName, score, instructionsDesc);
        //Logout();        
		
        clearBrowserHistory();
        getCommand.launchUrl(configProps.getProperty("ELM_App_URL"));
        classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));
        courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
        Thread.sleep(20000);
        selectMenuItem("My Tools");
		selectingOption("My Tools","Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}		
		selectingOption("Submit Files","Submit");
        selectingOption("Done Button", "Done");
	    validatingAssignmentSubmission(assignmentName);
        //Logout();
             
        clearBrowserHistory();
        getCommand.launchUrl(configProps.getProperty("ELM_App_URL"));
        classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
        courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools","Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		clickOnUploadedFileAndVerifyAssignmentPresent();
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		enterScoreFeedback("89", "FeedBack");
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		validatingAssignmentPublished();	
		//Logout();
		
		clearBrowserHistory();
        getCommand.launchUrl(configProps.getProperty("Admin_App_URL"));
        elmLogIn(configProps.getProperty("ELMAreaManager"), configProps.getProperty("AdminPassword"));
        courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools","Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		clickOnAssignmentPublished(assignmentName);
		validatingLatestScoreForEvaluatedAssignment("89");
	}
}