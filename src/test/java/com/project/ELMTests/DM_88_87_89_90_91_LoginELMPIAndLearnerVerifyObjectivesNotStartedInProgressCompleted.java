package com.project.ELMTests;

import io.appium.java_client.SwipeElementDirection;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ELMObjectRepository.ELMCommonObjects;
import com.project.ELMObjectRepository.ELMProjects;
import com.project.setup.Data_Provider;

/**
* Description : Creating Assignment, adding a file to assignment, Evaluating the assignment.
* Verify if Primary Faculty In progress , Not started and Completed and Verify Learner is able to view the project(s) that are In progress and Completed
*/

public class DM_88_87_89_90_91_LoginELMPIAndLearnerVerifyObjectivesNotStartedInProgressCompleted extends BusinessFunctions{
	@Test
	public void DM_88_87_89_90_91_LoginELMPIAndLearner_VerifyObjectivesNotStartedInProgressCompleted() throws Throwable {
		
		tstData = Data_Provider.getTestData("Projects", "DM_88_87_89_90_91_LoginELMPIAndLearnerVerifyObjectivesNotStartedInProgressCompleted");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName");
		String score = tstData.get("score");
		String rubricName = tstData.get("rubricName");
		String instructionsDesc = tstData.get("instructionsDesc");
		String competencyName = tstData.get("competencyName");
		String childCompetencyName = tstData.get("childCompetencyName");
		String learningObjectiveName = tstData.get("learningObjectiveName");	
		String assignmentNameTwo = tstData.get("assignmentNameTwo")+getRandomNumberDate();
		String competencyNameTwo = tstData.get("competencyNameTwo");
		String childCompetencyNameTwo = tstData.get("childCompetencyNameTwo");
		String learningObjectiveNameTwo = tstData.get("learningObjectiveNameTwo");
		String learner = tstData.get("learner");
		
		launchUrl(configProps.getProperty("Admin_App_URL"));
		clearBrowserHistory();
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		creatingAssignmentWithLearnerObjectivesELM(assignmentName, categoryName, gradeName, score, rubricName, instructionsDesc, competencyName, childCompetencyName, learningObjectiveName);
		creatingAssignmentWithLearnerObjectivesELM(assignmentNameTwo, categoryName, gradeName, score, rubricName, instructionsDesc, competencyNameTwo, childCompetencyNameTwo, learningObjectiveNameTwo);
		
//		Logout();
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Class Progress", "Class Progress");
		elementExists(By.xpath(ELMProjects.Class_Progress_Heading));
        elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		selectingOption(learner, learner);
		elementExists(By.xpath(ELMProjects.Progress_Summary));
		selectingOption("Objectives", "Objectives");
		verifyClassProgressObjectives("Not Started", "ELM Primary Faculty");

//		Logout();
		clearBrowserHistory();		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));		
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		clickOnCreatedAssignment(assignmentName);
        elementSwipe(SwipeElementDirection.DOWN, 500, 10000);
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}
		
		selectingOption("Submit Files","Submit");
        selectingOption("Done Button", "Done");
//		Logout();
        
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		
		clickOnCreatedAssignment(assignmentName);
		clickOnUploadedFileAndVerifyAssignmentPresent();
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		selectingRadioButtonForCompetency(rubricName ,"100", "FeedBack");
		selectingOption("publish", "Publish");
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
        elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
        selectMenuItem("My Tools");
		selectingOption("Class Progress", "Class Progress");
        elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		selectingOption(learner, learner);
		selectingOption("Objectives", "Objectives");
        elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		verifyClassProgressObjectives("In progress", "ELM Primary Faculty");
		
//		Logout();
		clearBrowserHistory();		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Class Progress", "Class Progress");
        elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		selectingOption(learner, learner);
		selectingOption("Objectives", "Objectives");
		elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		verifyClassProgressObjectives("In progress", "Learner");		
		selectingOption("Course Home", "Course Home");
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		clickOnCreatedAssignment(assignmentNameTwo);
		elementSwipe(SwipeElementDirection.DOWN, 500, 10000);
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}
		
		selectingOption("Submit Files","Submit");
        selectingOption("Done Button", "Done");
		
//		Logout();
		clearBrowserHistory();		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");		
		clickOnCreatedAssignment(assignmentNameTwo);
		clickOnUploadedFileAndVerifyAssignmentPresent();
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		selectingRadioButtonForCompetency(rubricName ,"100", "FeedBack");
		selectingOption("Publish", "Publish");
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
        elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
        selectMenuItem("My Tools");
		selectingOption("Class Progress", "Class Progress");
		elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		selectingOption(learner, learner);
		selectingOption("Objectives", "Objectives");
		elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		verifyClassProgressObjectives("Completed" , "ELM Primary Faculty");
		
	//	Logout();
		clearBrowserHistory();		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Class Progress", "Class Progress");
		elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		selectingOption(learner, learner);
		selectingOption("Objectives", "Objectives");
		elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		verifyClassProgressObjectives("Completed" , "ELM Primary Faculty");		
			
//		Logout();
//		Deleting Created Assignment to clear memory
		clearBrowserHistory();
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		deletingAssignment(assignmentName);
		elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
		deletingAssignment(assignmentNameTwo);
	}
}