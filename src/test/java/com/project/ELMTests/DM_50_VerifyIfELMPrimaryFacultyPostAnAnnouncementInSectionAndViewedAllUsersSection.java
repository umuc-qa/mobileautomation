package com.project.ELMTests;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AnnouncementObjects;
import com.project.setup.Data_Provider;

/**
 * Description : Creating and verify if ELM Primary Faculty is able to post an announcement in his section and can be 
 * viewed by all users in that section
 */

public class DM_50_VerifyIfELMPrimaryFacultyPostAnAnnouncementInSectionAndViewedAllUsersSection extends BusinessFunctions {
	
	@Test
	public void DM_50_VerifyIfELMPrimaryFaculty_PostAnAnnouncementInSectionAndViewedAllUsersSection() throws Throwable {

	tstData = Data_Provider.getTestData("Announcements", "DM_50_VerifyIfELMPrimaryFacultyPostAnAnnouncementInSectionAndViewedAllUsersSection");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String announcementName = tstData.get("announcementName")+getRandomNumberDate();
		String contentDesc = tstData.get("contentDesc");
	/*	
		launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
			explicitWait(androidDriver1, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
		}
		elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcements Link");
		creatingAnnouncementInELM(announcementName, contentDesc, false, 0 , false, 0);
		verifyingAnnouncementCreated(announcementName);
		logout();
		Thread.sleep(2000);
		launchNewUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMLearner2"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		//selectingOption("Menu", "Announcements");
		explicitWait(androidDriver1, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
		elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcements Link");
		verifyingAnnouncementCreated(announcementName);
		logout();
        Thread.sleep(3000);
		//Deleting Created Announcement
		launchNewUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		//selectingOption("Menu", "Announcements");
		explicitWait(androidDriver1, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
		elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcements Link");
		deletingAnnouncementInELM(announcementName);	
        logout();
	}
}
