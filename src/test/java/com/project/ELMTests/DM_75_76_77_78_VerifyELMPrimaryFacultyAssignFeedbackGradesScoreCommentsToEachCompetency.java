package com.project.ELMTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

/**
 * Description : Creating Assignment, adding a file, evaluated by giving score and Verify if ELM Primary Faculty 
 * is able to send the Assignments back to Learner with feedback, grades and comments
 */

public class DM_75_76_77_78_VerifyELMPrimaryFacultyAssignFeedbackGradesScoreCommentsToEachCompetency extends BusinessFunctions {
	
	@Test
	public void DM_75_76_77_78_VerifyPrimaryFacultyAssignFeedbackGradesScoreCommentsToEachCompetency() throws Throwable {
		
    tstData = Data_Provider.getTestData("Grades", "DM_75_VerifyELMPrimaryFacultySendAssignmentToLearnerFeedbackGradesComments");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String rubricName = tstData.get("rubricName");
		String instructionsDesc = tstData.get("instructionsDesc");
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(configProps.getProperty("Admin_App_URL"));		
		clearBrowserHistory();	
		getCommand.launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools","Assignments");
		createAssignmentWithNameAndSelectedRubric(assignmentName, gradeName, score, instructionsDesc, rubricName);
        //Logout();        
		
        clearBrowserHistory();
        getCommand.launchUrl(configProps.getProperty("ELM_App_URL"));
        classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));
        courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
        Thread.sleep(20000);
        selectMenuItem("My Tools");
		selectingOption("My Tools","Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}
		selectingOption("Submit Files","Submit");
        selectingOption("Done Button", "Done");
	    validatingAssignmentSubmission(assignmentName);
        //Logout();
             
        clearBrowserHistory();
        getCommand.launchUrl(configProps.getProperty("ELM_App_URL"));
        classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
        courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools","Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		clickOnUploadedFileAndVerifyAssignmentPresent();
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		selectingRadioButtonForCompetency(rubricName, "89", "Feedback");
		selectingOption("publish", "Publish");
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		validatingAssignmentPublished();	
		//Logout();
	}
}