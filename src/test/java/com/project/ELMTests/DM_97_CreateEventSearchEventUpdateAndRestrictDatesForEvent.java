package com.project.ELMTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
 * Description : Creates Event with Make available to everyone and Makes available all day Go to Calendar and Search for the created Event. 
 * Update and restrict dates for event.
 */

public class DM_97_CreateEventSearchEventUpdateAndRestrictDatesForEvent extends BusinessFunctions {
	
	@Test
	public void DM_97_CreateEvent_SearchEventUpdateandrestrictdatesforevent() throws Throwable {

	tstData = Data_Provider.getTestData("Calendar","DM_97_CreateEventSearchEventUpdateandrestrictdatesforevent");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String EventTitle=tstData.get("EventTitle")+getRandomNumberDate();
		String Description=tstData.get("Description");
	    String Location=tstData.get("Location");
		
	  /*  launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		clickCalendar();
		createEvent(courseSearch,EventTitle, Description, "", "", Location);
		//createEvent(EventTitle,Description,Location);
		//Delete newly created event
		Thread.sleep(4000);
		deleteEventELM(EventTitle);
		logout();
	}
}
