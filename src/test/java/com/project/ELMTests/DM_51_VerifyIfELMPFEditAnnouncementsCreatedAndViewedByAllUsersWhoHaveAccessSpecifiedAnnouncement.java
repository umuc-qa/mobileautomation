package com.project.ELMTests;

import io.appium.java_client.android.AndroidDriver;

import java.net.URL;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AnnouncementObjects;
import com.project.setup.Data_Provider;

/**
 * Description : Creating and verify if ELM Primary Faculty is able to edit the announcements created by him and same can be viewed 
 * by all users who have access to the specified announcement
 */

public class DM_51_VerifyIfELMPFEditAnnouncementsCreatedAndViewedByAllUsersWhoHaveAccessSpecifiedAnnouncement extends BusinessFunctions {
	
	@Test
	public void DM_51_VerifyELMPFEditAnnouncements_ViewedByAllUsers() throws Throwable {

	tstData = Data_Provider.getTestData("Announcements", "DM_51_VerifyIfELMPFEditAnnouncementsCreatedAndViewedByAllUsersWhoHaveAccessSpecifiedAnnouncement");
		
	
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String announcementName = tstData.get("announcementName")+getRandomNumberDate();
		String contentDesc = tstData.get("contentDesc");
		String editAnnouncementName = tstData.get("editAnnouncementName")+getRandomNumberDate();
		String editContentDesc = tstData.get("editContentDesc");
		String port = "4723";
		String device = "02157df28cd34e02";
		/*launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	//	elementSwipe(SwipeElementDirection.DOWN, 100, 10000);

		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
			explicitWait(androidDriver1, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
		}
		elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcements text");
		Thread.sleep(3000);
		creatingAnnouncementInELM(announcementName, contentDesc, false, 0 , false, 0);
		verifyingAnnouncementCreated(announcementName);
		editingAnnouncementInELM(announcementName, editAnnouncementName, editContentDesc);
		verifyingAnnouncementCreated(editAnnouncementName);
		logout();
		androidDriver1.close();
		System.out.println("safasdfa");
		androidDriver1 = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMLearner2"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	//	elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
		//selectingOption("Menu", "Announcements");
		elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcement link");
		verifyingAnnouncementCreated(editAnnouncementName);				
		logout();
	}
}