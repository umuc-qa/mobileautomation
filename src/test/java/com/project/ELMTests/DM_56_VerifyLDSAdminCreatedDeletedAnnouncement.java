package com.project.ELMTests;

import io.appium.java_client.SwipeElementDirection;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AnnouncementObjects;
import com.project.setup.Data_Provider;

/**
 * Description : Creating and Verify if LD&S Admin is able to delete the announcements created by them and 
 * deleted announcement is no longer visible.
 */

public class DM_56_VerifyLDSAdminCreatedDeletedAnnouncement extends BusinessFunctions {
	
	@Test
	public void DM_56_VerifyLDSAdminCreated_DeletedAnnouncement() throws Throwable {

	tstData = Data_Provider.getTestData("Announcements", "DM_56_VerifyIfLDSAdminDeleteAnnouncementsCreatedDeletedAnnouncementNoLongerVisible");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String announcementName = tstData.get("announcementName")+getRandomNumberDate();
		String contentDesc = tstData.get("contentDesc");
			
		/*launchUrl(configProps.getProperty("Admin_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		//elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
			explicitWait(androidDriver1, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
		}
		elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcements Inverted Triangle");
		creatingAnnouncementInELM(announcementName, contentDesc, false, 0 , false, 0);
		verifyingAnnouncementCreated(announcementName);
		Thread.sleep(10000);
		deletingAnnouncementInELM(announcementName);
		verifyingAnnouncementDeleted(announcementName);
        logout();		
	}
}