package com.project.ELMTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if Student is able to select and view the syllabus of the course
*/

public class DM_95_VerifyIfStudentSelectViewSyllabusCourse extends BusinessFunctions {
	
	@Test
	public void DM_95_VerifyIfStudent_SelectViewSyllabusCourse() throws Throwable {

	tstData = Data_Provider.getTestData("Syllabus", "DM_93_94_VerifyIfELMPrimaryFacultyViewSyllabusContactInformationSyllabus");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");	
		String syllabusCourseTitle = tstData.get("syllabusCourse");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		/*launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMLearner"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
		scrollingToElementofAPage(By.xpath(AssignmentsObjects.Menu_Bar_Icon1));
		//elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
		JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar1");
		if(!(isElementPresentNegative(AssignmentsObjects.LinkTextNameInMenu("Syllabus"), "Syllabus"))) {
   		 Thread.sleep(1500);
            elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
   	 	}
		
		JSMousehoverDoubleClick(AssignmentsObjects.LinkTextNameInMenu("Syllabus"), "Syllabus");
		Thread.sleep(3000);
		verifyingSyllabusDetailsInSyllabusPage(syllabusCourseTitle);
	//	verifyingFacultyInformationInSyllabusPage();
		logout();
		closeBrowserAndLaunch(port, device);
	}
}