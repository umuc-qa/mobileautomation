package com.project.ELMTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
* Description : Verify if Learner is able to see the list of all projects associated with the specific course
*/

public class DM_85_VerifyLearnerSeeListAllProjectsAssociatedSpecificCourse extends BusinessFunctions{
	@Test
	public void DM_85_VerifyLearnerSeeList_AllProjectsAssociatedSpecificCourse() throws Throwable {
		
		tstData = Data_Provider.getTestData("Projects", "DM_85_VerifyLearnerSeeListAllProjectsAssociatedSpecificCourse");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		/*launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMLearner"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	//	elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
		Thread.sleep(2000);
		verifyCourseIntroductionPresent();
		verifyProjectsPresent();
        logout();
        closeBrowserAndLaunch(port, device);
	}
}