package com.project.ELMTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if ELM Area Manager is able to see the list of all projects associated with the specific course
*/

public class DM_92_VerifyELMAreaManagerIsAbleToSeeTheListOfAllProjectsAssociatedWithSpecificCourse extends BusinessFunctions{

	@Test
	public void DM_092_VerifyELMAreaManagerIsAbleToSeeTheListOfAllProjectsAssociatedWithSpecificCourse() throws Throwable {
		
		tstData = Data_Provider.getTestData("Projects", "DM_92_VerifyELMAreaManagerIsAbleToSeeTheListOfAllProjectsAssociatedWithSpecificCourse");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");

		/*launchUrl(configProps.getProperty("Admin_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("ELMAreaManager"), configProps.getProperty("AdminPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		//elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
        Thread.sleep(2000);
		verifyProjectsPresent();
        verifyCourseIntroductionPresent();		
        logout();
        closeBrowserAndLaunch(port, device);
	}
}
