package com.project.ELMTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
 * Description : Verify if ELM Primary Faculty is able to access the instructions on how to 
 * complete the step and can see an option that links to the corresponding class discussion with in a step
 */

public class DM_64_VerifyELMPrimaryFacultyAccessProjectDiscussionLink extends BusinessFunctions {
	
	@Test
	public void DM_64_Verify_ELMPrimaryFacultyAccessProjectDiscussionLink() throws Throwable {

	tstData = Data_Provider.getTestData("Discussions","DM_64_VerifyELMPrimaryFacultyAccessProjectDiscussionLink");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		
		String ProjectName=tstData.get("ProjectName");
		String StepNumber=tstData.get("StepNumber");
		String TopicName=tstData.get("TopicName");
		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectProject(true,ProjectName,false,StepNumber,false,TopicName); 
		verifyDiscussionlink(TopicName);
	}
}
