package com.project.ELMTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
 * Description : Creates new chat and verify if Learner is able to chat online or send synchronous messages with 
 * ELM Primary Faculty who is teaching the section.
 */

public class DM_67_VerifyIfLearnerChatOnlineToELMPrimaryFacultyTeachingSection extends BusinessFunctions {

	@Test
	public void DM_67_VerifyIfLearnerChatOnline_ToELMPrimaryFacultyTeachingSection() throws Throwable {

	tstData = Data_Provider.getTestData("Chats", "DM_67_VerifyIfLearnerChatOnlineToELMPrimaryFacultyTeachingSection");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String chatTitle = tstData.get("chatTitle")+getRandomNumberDate();
		String chatDescription = tstData.get("chatDescription");
		String textMessage1 = tstData.get("textMessage1");
		String textMessage2 = tstData.get("textMessage2");
		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Chat");
		newChatCreation(chatTitle, chatDescription);
		//Logout();
		
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Chat");
		clickingOnChat(chatTitle);
		sendingMessageDetails(textMessage1);
		validatingMessageInChat(textMessage1);
		
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Chat");
		clickingOnChat(chatTitle);
		validatingMessageInChat(textMessage1);
		sendingMessageDetails(textMessage2);
		validatingMessageInChat(textMessage2);
		
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Chat");
		clickingOnChat(chatTitle);
		validatingMessageInChat(textMessage2);				
	}
}