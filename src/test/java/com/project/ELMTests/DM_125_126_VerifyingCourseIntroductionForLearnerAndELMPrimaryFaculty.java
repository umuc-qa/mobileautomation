package com.project.ELMTests;


import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
* Description : Verifying the Course Introduction for Learner and ELM Primary Faculty
*/

public class DM_125_126_VerifyingCourseIntroductionForLearnerAndELMPrimaryFaculty extends BusinessFunctions {
	
	@Test
	public void DM_125_126_VerifyingcourseIntroductionForLearnerAndELMPrimaryFaculty() throws Throwable {
		
    tstData = Data_Provider.getTestData("Syllabus", "DM_125_126_VerifyingCourseIntroductionForLearnerAndELMPrimaryFaculty");
		
	String subjectArea = tstData.get("subjectArea");
	String catalog_no = tstData.get("catalog_no");
	String classSection = tstData.get("classSection");
	String term = tstData.get("term");
	String courseSearch = tstData.get("courseSearch");
	String port = configProps.getProperty("port");
	String device = configProps.getProperty("device");
	
	launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
    verifyCourseIntroductionPresent();	
    logout();
   
    closeBrowserAndLaunch(port, device);
	
    launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMLearner"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
    verifyCourseIntroductionPresent();		
    logout();
    
    closeBrowserAndLaunch(port, device);
	}
}
