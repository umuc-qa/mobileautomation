package com.project.ELMTests;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if Learner is able to view Navigation bar options.
* Verifying My Tools, Resources, Help options are present
*/

public class DM_121_122_VerifyingTheCourseHomeNavigationForLearnerAndELMPrimaryFaculty extends BusinessFunctions {
	
	@Test
	public void DM_121_122VerifyingTheCourseHomeNavigationForLearnerAndELMPrimaryFaculty() throws Throwable {
		
    tstData = Data_Provider.getTestData("Syllabus", "DM_121_122_VerifyingTheCourseHomeNavigationForLearnerAndELMPrimaryFaculty");
		
	String subjectArea = tstData.get("subjectArea");
	String catalog_no = tstData.get("catalog_no");
	String classSection = tstData.get("classSection");
	String term = tstData.get("term");
	String courseSearch = tstData.get("courseSearch");
	String port = configProps.getProperty("port");
	String device = configProps.getProperty("device");
	
	launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	scrollingToElementofAPage(By.xpath(AssignmentsObjects.Menu_Bar_Icon1));
	JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	verifyingMenuOptionsInTheNavigationBar("ELMPrimaryFaculty","");
	logout();
	
    closeBrowserAndLaunch(port, device);
	
    launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMLearner"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	scrollingToElementofAPage(By.xpath(AssignmentsObjects.Menu_Bar_Icon1));
	JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	verifyingMenuOptionsInTheNavigationBar("","ELMLearner");
    logout();
    
    closeBrowserAndLaunch(port, device);
	}
}
