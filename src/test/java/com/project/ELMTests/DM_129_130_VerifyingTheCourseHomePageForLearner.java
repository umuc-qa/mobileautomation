package com.project.ELMTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if Learner is able to view Navigation bar options.
* Verifying My Tools, Resources, Help options are present
*/

public class DM_129_130_VerifyingTheCourseHomePageForLearner extends BusinessFunctions {
	
	@Test
	public void DM_129_130_verifyingTheCourseHomePageForLearner() throws Throwable {
		
    tstData = Data_Provider.getTestData("Syllabus", "DM_129_130_verifyingTheCourseHomePageForLearner");
		
	String subjectArea = tstData.get("subjectArea");
	String catalog_no = tstData.get("catalog_no");
	String classSection = tstData.get("classSection");
	String term = tstData.get("term");
	String courseSearch = tstData.get("courseSearch");
	String port = configProps.getProperty("port");
	String device = configProps.getProperty("device");
	
    launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMLearner"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	Thread.sleep(3000);
	verifyingCourseTitle_Banner_TitleOnBanner(courseSearch);
	logout();
	closeBrowserAndLaunch(port, device);
	}
}
