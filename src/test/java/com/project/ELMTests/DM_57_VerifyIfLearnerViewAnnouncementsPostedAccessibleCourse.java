package com.project.ELMTests;

import io.appium.java_client.android.AndroidDriver;

import java.net.URL;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AnnouncementObjects;
import com.project.setup.Data_Provider;

/**
* Description : Creating and verify if Learner is able to view announcements posted in accessible course.
*/

public class DM_57_VerifyIfLearnerViewAnnouncementsPostedAccessibleCourse extends BusinessFunctions {

@Test
public void DM_57_VerifyIfLearnerViewAnnouncements_PostedAccessibleCourse() throws Throwable {

tstData = Data_Provider.getTestData("Announcements", "DM_57_VerifyIfLearnerViewAnnouncementsPostedAccessibleCourse");
	
	String subjectArea = tstData.get("subjectArea");
	String catalog_no = tstData.get("catalog_no");
	String classSection = tstData.get("classSection");
	String term = tstData.get("term");
	String courseSearch = tstData.get("courseSearch");
	String announcementName = tstData.get("announcementName")+getRandomNumberDate();
	String contentDesc = tstData.get("contentDesc");
	String port = "4723";
	String device = "02157df28cd34e02";
	
	/*launchUrl(configProps.getProperty("ELM_App_URL"));
	clearBrowserHistory();*/
	launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	//elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
		explicitWait(androidDriver1, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
	}
	elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcements link");
	Thread.sleep(2000);
	creatingAnnouncementInELM(announcementName, contentDesc, false, 0 , false, 0);
	verifyingAnnouncementCreated(announcementName);
    logout();
    androidDriver1.close();
	System.out.println("test");
	androidDriver1 = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));
    
	launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMLearner2"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
//	elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
	elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcement link");
	Thread.sleep(2000);
	verifyingAnnouncementCreated(announcementName);	
	logout();
	androidDriver1.close();
	System.out.println("test");
	androidDriver1 = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));

	//Deleting Created Announcement
	//clearBrowserHistory();
	launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
//	elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
	elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcement link");
	Thread.sleep(2000);
	//selectingOption("Menu", "Announcements");
	deletingAnnouncementInELM(announcementName);
	verifyingAnnouncementDeleted(announcementName);
	logout();
  }
}