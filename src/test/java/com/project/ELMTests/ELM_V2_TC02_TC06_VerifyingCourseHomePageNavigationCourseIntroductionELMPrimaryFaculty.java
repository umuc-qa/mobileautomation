package com.project.ELMTests;

import io.appium.java_client.SwipeElementDirection;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

/**
* Description : Verifying if Primary faculty is able to view Navigation bar options.
* Verifying My Tools, Resources, Help options are present
*/

public class ELM_V2_TC02_TC06_VerifyingCourseHomePageNavigationCourseIntroductionELMPrimaryFaculty extends BusinessFunctions {
		
	@Test
	public void ELM_V2_TC02_TC06_VerifyingCourseHomePageNavigation_CourseIntroductionELMPrimaryFaculty() throws Throwable {
		
    tstData = Data_Provider.getTestData("Assignments", "DM_006_007_PublishFeedBack");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();
        getCommand.launchUrl(configProps.getProperty("ELM_App_URL"));
        classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
        courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
        Thread.sleep(20000);
        clickingOnMenu();
		validatingMenuItem("Course Home");
		validatingMenuItem("Syllabus");
		validatingMenuItem("Projects");
		validatingMenuItem("My Tools");
		validatingMenuItem("Resources");
		validatingMenuItem("Help");
		validatingMenuItem("Instructor Tools");
		validatingMenuItem("Admin Tools");
		
		clickingOnMenuItem(courseSearch);
		elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
		verifyCourseIntroductionDefaultMode();
		verifyProjectsPresent();
		
		selectMenuItem("My Tools");
		validatingSubmenuOption("My Tools","Discussions");
		validatingSubmenuOption("My Tools","Assignments");
		validatingSubmenuOption("My Tools","Groups");
		validatingSubmenuOption("My Tools","Grades");
		validatingSubmenuOption("My Tools","Chat");
		validatingSubmenuOption("My Tools","Locker");
		validatingSubmenuOption("My Tools","Class Progress");
		validatingSubmenuOption("My Tools","Competencies");
		validatingSubmenuOption("My Tools","Classlist");
		validatingSubmenuOption("My Tools","Course Evaluation - ELM");
		
		elementSwipe(SwipeElementDirection.UP, 500, 1000);
		clickingOnMenuItem(courseSearch);
		Thread.sleep(20000);
		selectMenuItem("Resources");
		validatingSubmenuOption("Resources","Turnitin");
		validatingSubmenuOption("Resources","Library");
		validatingSubmenuOption("Resources","Effective Writing Center");
		
		clickingOnMenuItem(courseSearch);
		Thread.sleep(20000);
		selectMenuItem("Help");
		validatingSubmenuOption("Help","Learning Resources");
		validatingSubmenuOption("Help","Help Center");
		validatingSubmenuOption("Help","System Check");
		clickingOnMenuItem(courseSearch);
	}
}