package com.project.ELMTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ELMObjectRepository.ELMAlertsObjects;
import com.project.ELMObjectRepository.ELMCommonObjects;
import com.project.setup.Data_Provider;

/**
 * Description : Creates an assignment and Verify if ELM Primary Faculty is able to see an alert when a project assignment has been submitted 
 * by Learner in the course section.
 */

public class DM_82_VerifyIfELMPFAbleToSeeAnAlertWhenAProjectAssignmentHasSubmittedByLearner extends BusinessFunctions {

	@Test
	public void DM_082_VerifyIfELMPFAbleToSeeAnAlertWhenAProjectAssignmentHasSubmittedByLearner() throws Throwable {
		
		tstData = Data_Provider.getTestData("Alerts", "DM_82_VerifyIfELMPFAbleToSeeAnAlertWhenAProjectAssignmentHasSubmittedByLearner");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();		
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		
		launchUrl(configProps.getProperty("Admin_App_URL"));
		clearBrowserHistory();
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		assignmentCreation(assignmentName, categoryName, gradeName, score, instructionsDesc);
		
//		Logout();
		clearBrowserHistory();		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));		
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		clickOnCreatedAssignment(assignmentName);		
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}
		
		selectingOption("Submit Files","Submit");
        selectingOption("Done Button", "Done");
//		Logout();
        
		clearBrowserHistory();		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		verifyAlertELM("New Assignment Submissions");		
		elementClick(By.xpath(ELMAlertsObjects.AlertMessageNewAssignmentSubmissionsLink), "Alert link");		
		validatingCreatedAssignmentOrGrade(assignmentName);
		
//		Logout();
//		Deleting Created Assignment to clear memory
		clearBrowserHistory();
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");		
		selectingOption("Assignments", "Assignments");
		deletingAssignment(assignmentName);
	}
}
