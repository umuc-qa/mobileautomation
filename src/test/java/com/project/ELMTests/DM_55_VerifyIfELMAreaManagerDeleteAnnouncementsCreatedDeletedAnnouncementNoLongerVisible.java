package com.project.ELMTests;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AnnouncementObjects;
import com.project.setup.Data_Provider;

/**
 * Description : Creating and Verify if ELM Area Manager is able to delete the announcements created by them and 
 * deleted announcement is no longer visible.
 */

public class DM_55_VerifyIfELMAreaManagerDeleteAnnouncementsCreatedDeletedAnnouncementNoLongerVisible extends BusinessFunctions {
	
	@Test
	public void DM_55_VerifyIfELMAreaManagerDeleteAnnouncementsCreated_DeletedAnnouncementNoLongerVisible() throws Throwable {

	tstData = Data_Provider.getTestData("Announcements", "DM_55_VerifyIfELMAreaManagerDeleteAnnouncementsCreatedDeletedAnnouncementNoLongerVisible");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String announcementName = tstData.get("announcementName")+getRandomNumberDate();
		String contentDesc = tstData.get("contentDesc");
		
		/*launchUrl(configProps.getProperty("Admin_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("ELMAreaManager"), configProps.getProperty("AdminPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		//elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
			explicitWait(androidDriver1, By.xpath(AnnouncementObjects.lnkActionsForAnnouncement));
		}
		elementClick(By.xpath(AnnouncementObjects.lnkActionsForAnnouncement), "Announcements Inverted Triangle");
		creatingAnnouncementInELM(announcementName, contentDesc, false, 0 , false, 0);
		verifyingAnnouncementCreated(announcementName);
		//elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
		deletingAnnouncementInELM(announcementName);
		verifyingAnnouncementDeleted(announcementName);
        logout();
	}
}