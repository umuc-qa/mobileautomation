package com.project.ELMTests;


import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.setup.Data_Provider;

/**
* Description : Verifying Syllabus link is present for Elm Primary Faculty and Learner
* 
*/

public class DM_123_124_VerifyingSyllabusLinkAndSyllabusForLearnerAndElmPrimaryFaculty extends BusinessFunctions {
	
	@Test
	public void DM_123_124_VerifyingsyllabusLinkAndSyllabusForLearnerAndElmPrimaryFaculty() throws Throwable {
		
    tstData = Data_Provider.getTestData("Syllabus", "DM_125_126_VerifyingCourseIntroductionForLearnerAndELMPrimaryFaculty");
		
	String subjectArea = tstData.get("subjectArea");
	String catalog_no = tstData.get("catalog_no");
	String classSection = tstData.get("classSection");
	String term = tstData.get("term");
	String courseSearch = tstData.get("courseSearch");
	String syllabusSearch = tstData.get("syllabusCourse");
	String port = configProps.getProperty("port");
	String device = configProps.getProperty("device");
	
	launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	scrollingToElementofAPage(By.xpath(AssignmentsObjects.Menu_Bar_Icon1));
	JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	 if(!(isElementPresentNegative(AssignmentsObjects.LinkTextNameInMenu("Syllabus"), "Syllabus"))) {
		 Thread.sleep(1500);
         elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	 }
	 
	 JSMousehoverDoubleClick(AssignmentsObjects.LinkTextNameInMenu("Syllabus"), "Syllabus");
	Thread.sleep(3000);
	SuccessReport("syllabus link", "Syllabus link is present for Elm Primary Faculty");
	
	/*boolean flag =	selectMenuItemDoubleClick("Syllabus");
	if(flag == true)
	SuccessReport("syllabus link", "Syllabus link is present for Elm Primary Faculty");
	Thread.sleep(15000);*/
	verifyingSyllabusDetailsInSyllabusPage(syllabusSearch);
	logout();
   
    closeBrowserAndLaunch(port, device);
	
    launchUrl(configProps.getProperty("ELM_App_URL"));
	classicLogIn(configProps.getProperty("ELMLearner"), configProps.getProperty("FacultyLeanerPassword"));
	courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
	waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	scrollingToElementofAPage(By.xpath(AssignmentsObjects.Menu_Bar_Icon1));
	JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	 if(!(isElementPresentNegative(AssignmentsObjects.LinkTextNameInMenu("Syllabus"), "Syllabus"))) {
		 Thread.sleep(1500);
         elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
	 }
	 
	JSMousehoverDoubleClick(AssignmentsObjects.LinkTextNameInMenu("Syllabus"), "Syllabus");
	Thread.sleep(3000);
	SuccessReport("syllabus link", "Syllabus link is present for Elm Primary Faculty");
	/*
	Thread.sleep(3000);
	boolean flags =	selectMenuItemDoubleClick("Syllabus");
	if(flags == true)
	SuccessReport("syllabus link", "Syllabus link is present for learner");
	Thread.sleep(15000);*/
	verifyingSyllabusDetailsInSyllabusPage(syllabusSearch);	
    logout();
    
    closeBrowserAndLaunch(port, device);
	}
}
