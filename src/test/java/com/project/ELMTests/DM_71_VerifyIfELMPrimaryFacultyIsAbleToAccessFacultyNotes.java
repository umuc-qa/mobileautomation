package com.project.ELMTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.ELMObjectRepository.ELMFacultyNotesObjects;
import com.project.setup.Data_Provider;

/**
 * Description : Verifying if ELM Primary Faculty is able to view the Faculty Notes within the class 
 */

public class DM_71_VerifyIfELMPrimaryFacultyIsAbleToAccessFacultyNotes extends BusinessFunctions {
	
	@Test
	public void DM_71_VerifyIfELMPrimaryFaculty_AbleToAccessFacultyNotes() throws Throwable {

	tstData = Data_Provider.getTestData("FacultyNotes", "DM_71_VerifyIfELMPrimaryFacultyIsAbleToAccessFacultyNotes");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		/*launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		/*if(elementExists(By.xpath(ELMFacultyNotesObjects.More)))
		{
			selectingOption("Menu","More");
			selectingOption("Instructor Tools","Instructor Tools");
			selectingOption("Faculty Notes","Faculty Notes");
		}
		else
		{
			selectMenuItem("Instructor Tools");
			selectingOption("Faculty Notes","Faculty Notes");
		}*/
		//elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
		//elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
		scrollingToElementofAPage(By.xpath(AssignmentsObjects.Menu_Bar_Icon1));
		JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
		selectMenuIteminELM("Instructor Tools");
		selectingOption("Faculty Notes","Faculty Notes");
		//elementClick(By.xpath(ELMFacultyNotesObjects.lnkFacultyNotes), "Faculty notes");
		//elementClick(AssignmentsObjects.LinkTextNameInMyTool("Faculty notes"), "Faculty notes");
		Thread.sleep(1000);
		verifyFacultyNotes();
		logout();
		closeBrowserAndLaunch(port, device);
	}
}