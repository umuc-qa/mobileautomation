package com.project.ELMTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if Learner is able to view the Course introduction, projects on the project carousel
*/

public class DM_69_70_VerifyLearnerViewCourseIntroductionProjectsList extends BusinessFunctions {
	@Test
	public void DM_69_70_VerifyLearnerViewCourseIntroductionProjectList() throws Throwable {
		
    tstData = Data_Provider.getTestData("ELMHomePage", "DM_69_70_VerifyLearnerViewCourseIntroductionProjectsList");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
        launchUrl(configProps.getProperty("ELM_App_URL"));
        classicLogIn(configProps.getProperty("ELMLearner"), configProps.getProperty("FacultyLeanerPassword"));
        courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
       // waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
        Thread.sleep(3000);
        verifyCourseIntroductionPresent();
        Thread.sleep(2000);
		verifyProjectsPresent();
		logout();
		closeBrowserAndLaunch(port, device);
	}
}