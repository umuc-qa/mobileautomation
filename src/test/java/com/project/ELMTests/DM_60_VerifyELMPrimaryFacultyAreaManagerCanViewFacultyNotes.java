package com.project.ELMTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.ELMObjectRepository.ELMFacultyNotesObjects;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if Faculty notes in each course can only be viewed by ELM Primary Faculty or the ELM Area Manager
*/

public class DM_60_VerifyELMPrimaryFacultyAreaManagerCanViewFacultyNotes extends BusinessFunctions {
	
	@Test
	public void DM_60_VerifyIfELMPrimaryFaculty_FacultyNotes() throws Throwable {

	tstData = Data_Provider.getTestData("Content","DM_60_VerifyELMPrimaryFacultyAreaManagerCanViewFacultyNotes");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		launchUrl(url);
		//launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		
		scrollingToElementofAPage(By.xpath(AssignmentsObjects.Menu_Bar_Icon1));
		//elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
		JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
		Thread.sleep(2000);
		selectMenuIteminELM("Instructor Tools");
		selectingOption("Faculty Notes","Faculty Notes");
		
		verifyFacultyNotes();
		logout();
		closeBrowserAndLaunch(port, device);
		//clearBrowserHistory();
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("ELMAreaManager"), configProps.getProperty("AdminPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);	
		scrollingToElementofAPage(By.xpath(AssignmentsObjects.Menu_Bar_Icon1));
		//elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
		JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar1");
		selectMenuIteminELM("Instructor Tools");
		selectingOption("Faculty Notes","Faculty Notes");
		waitForVisibilityOfElement(By.xpath("//iframe[contains(@class,'d2l-iframe')]"), "Wait until Frame loaded");
		Thread.sleep(4000);
		verifyFacultyNotes();		
		logout();
		closeBrowserAndLaunch(port, device);
	}
}