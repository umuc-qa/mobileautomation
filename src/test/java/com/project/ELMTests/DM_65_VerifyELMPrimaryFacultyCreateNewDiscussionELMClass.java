package com.project.ELMTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.DiscussionsObjects;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if ELM Primary Faculty can create new discussion within a class enrolled
*/

public class DM_65_VerifyELMPrimaryFacultyCreateNewDiscussionELMClass extends BusinessFunctions {
	
	@Test
	public void DM_65_Verify_ELMPrimaryFacultyCreateNewDiscussionELMClass() throws Throwable {

	tstData = Data_Provider.getTestData("Discussions","DM_65_VerifyELMPrimaryFacultyCreateNewDiscussionELMClass");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String discussionName = tstData.get("discussionName")+getRandomNumberDate();
		String contentDesc = tstData.get("contentDesc");
		
		/*launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(2000);
		/*selectMenuItem("My Tools");
		selectingOption("Discussions","Discussions");*/
		selectingMyTools("Discussions");
		elementClick(By.xpath(DiscussionsObjects.New_Button), "New Button");
		Thread.sleep(1000);
		elementClick(By.xpath(DiscussionsObjects.New_Forum), "New Forum");
		Thread.sleep(3000);
	    editingForum(discussionName,contentDesc);
	    validatingCreatedTopicOrForum(discussionName);
	    logout();
	}	
}
