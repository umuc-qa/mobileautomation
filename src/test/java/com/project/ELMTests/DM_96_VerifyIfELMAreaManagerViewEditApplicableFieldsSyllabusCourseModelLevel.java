package com.project.ELMTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if ELM Area Manager is able to view and edit applicable fields syllabus for a course at model level
*/

public class DM_96_VerifyIfELMAreaManagerViewEditApplicableFieldsSyllabusCourseModelLevel extends BusinessFunctions {
	
	@Test
	public void DM_96_VerifyELMAreaManagerEdit_FieldsSyllabusCourseModelLevel() throws Throwable {

	tstData = Data_Provider.getTestData("Syllabus", "DM_93_94_VerifyIfELMPrimaryFacultyViewSyllabusContactInformationSyllabus");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String syllabusCourseTitle = tstData.get("syllabusCourse");
		
		launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMAreaManager2"), configProps.getProperty("FacultyLeanerPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("Syllabus");
		Thread.sleep(20000);
		verifyingSyllabusDetailsInSyllabusPage(syllabusCourseTitle);
		verifyingFacultyInformationInSyllabusPage();
	}
}