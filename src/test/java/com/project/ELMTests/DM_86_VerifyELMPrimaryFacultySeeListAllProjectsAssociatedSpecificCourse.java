package com.project.ELMTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

/**
* Description : Verifying if ELM Primary Faculty is able to see the list of all projects associated with the specific course
*/

public class DM_86_VerifyELMPrimaryFacultySeeListAllProjectsAssociatedSpecificCourse extends BusinessFunctions{
	@Test
	public void DM_86_VerifyELMPrimaryFacultySeeList_AllProjectsAssociatedSpecificCourse() throws Throwable {
		
		tstData = Data_Provider.getTestData("Projects", "DM_86_VerifyELMPrimaryFacultySeeListAllProjectsAssociatedSpecificCourse");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");

/*		launchUrl(configProps.getProperty("ELM_App_URL"));
		clearBrowserHistory();*/
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));	
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		//elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
        Thread.sleep(2000);
		verifyProjectsPresent();
        verifyCourseIntroductionPresent();		
        logout();
        closeBrowserAndLaunch(port, device);
        
	}
}