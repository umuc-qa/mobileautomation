package com.project.ELMTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ELMObjectRepository.ELMAlertsObjects;
import com.project.ELMObjectRepository.ELMCommonObjects;
import com.project.setup.Data_Provider;

/**
 * Description : Create an assignment and Verify if Learner is able to see an alert when feedback has been posted on the 
 * submitted project assignment
 */

public class DM_83_84_VerifyLearnerSeeAlertFeedbackOnPostedAssignment extends BusinessFunctions{
	@Test
	public void DM_83_84_VerifyLearnerSeeAlertFeedback_OnPostedAssignment() throws Throwable {
		
		tstData = Data_Provider.getTestData("Alerts", "DM_83_84_VerifyLearnerSeeAlertFeedbackOnPostedAssignment");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		
		launchUrl(configProps.getProperty("Admin_App_URL"));
		clearBrowserHistory();
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		assignmentCreation(assignmentName, categoryName, gradeName, score, instructionsDesc);
		
//		Logout();
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));		
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		clickOnCreatedAssignment(assignmentName);
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}
		
		selectingOption("Submit Files","Submit");
        selectingOption("Done Button", "Done");
		
//		Logout();
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("ELMPrimaryFaculty"), configProps.getProperty("FacultyLeanerPassword"));	
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		clickOnCreatedAssignment(assignmentName);
		clickOnUploadedFileAndVerifyAssignmentPresent();
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		enterScoreFeedback("100", "FeedBack");
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		
//		Logout();
		clearBrowserHistory();
		launchUrl(configProps.getProperty("ELM_App_URL"));
		classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("FacultyLeanerPassword"));		
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		verifyAlertELM("Assignment Submission Folders with Unread Feedback");
		elementClick(By.xpath(ELMAlertsObjects.AlertMessageAssignmentSFWithUnreadFeedback), "Alert link");
		verifyFeedBackIsPresentForAssignment(assignmentName);
		
//		Logout();
//		Deleting Created Assignment to clear memory
		clearBrowserHistory();
		launchUrl(configProps.getProperty("Admin_App_URL"));
		elmLogIn(configProps.getProperty("LDSAdmin"), configProps.getProperty("AdminPassword"));
		courseSearchForELM(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		elementExists(By.xpath(ELMCommonObjects.Course_Banner));
		selectMenuItem("My Tools");
		selectingOption("Assignments", "Assignments");
		deletingAssignment(assignmentName);
	}
}