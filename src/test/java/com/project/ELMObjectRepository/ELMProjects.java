package com.project.ELMObjectRepository;

public class ELMProjects {
	public static String ProjectsInCourseHomePage = "//*[@text='Projects']/ancestor::div[3]//iframe";
	public static String CourseIntroductionInformation = "//*[@text='Course Introduction']/../../..//*[@id='introduction']";
	//public static String InvertedTriangleBesideCourseIntroduction = "//*[@text='Course Introduction']/../..//a";
	public static String InvertedTriangleBesideProjects = "//*[@text='Projects']/../..//a";
	//public static String  ExpandThisWidget = "//*[@text='Expand this widget']";
	public static String  CollapseThisWidget = "//*[@text='Collapse this widget']";
	
	public static String Grade_Item_Dropdown = "//*[@nodeName='SELECT' and @id='z_s']";
	public static String Objectives_Not_Started = "//*[contains(@text,'Not Started')]";
	public static String Objectives_Completed_Value = "//*[@text='Completed: ']//*[@nodeName='STRONG']";
	public static String Class_Progress_Heading = "//*[@nodeName='H1' and @text='Class Progress']";
	public static String Progress_Summary = "//*[@nodeName='H1' and @text='Progress Summary']";

	
	//new xpaths
	
	public static String lblProjects = "//div/h2[text()='Projects']";
	public static String  iframeCourseProjects="//div[@class='d2l-htmlblock']//following::iframe[@scrolling='no']";
	public static String  iframeProjectsHomePage= "//iframe[@title='Content Adaptor']";
	public static String  lnkListOfAllProjects= "//body[@class='course-homepage']//h3";
	
	 public static String projectName(int editText)
	    {
	    String projectName="//body[@class='course-homepage']//h3/a[@id='"+editText+"']";
	    return projectName;
	    }
	// public static String ddProjectExpandWidget = "//li[@class='d2l-contextmenu-item d2l-first-visible-item d2l-last-visible-item']//span[text()='Expand this widget']";//a[@title='Actions for Projects']//parent::div//div[@role='menu']//span[text()='Expand this widget']";
	 public static String ddProjectCollapseWidget = "(//div[contains(@class,'d2l-floating-container vui-dropdown')]//span[text()='Collapse this widget'])[2]";
	// public static String InvertedTriangleBesideCourseIntroduction = "//button[@title='Actions for Course Introduction']";
	 public static String InvertedTriangleBesideCourseIntroduction = "//d2l-button-icon[@text='Actions for Course Introduction']";
	 public static String courseIntroductionText = "//h2[text()='Course Introduction']";
	 // public static String ddCourseIntroductionExpandWidget = "//li[@class='d2l-contextmenu-item d2l-first-visible-item d2l-last-visible-item']//span[text()='Expand this widget']";//a[@title='Actions for Course Introduction']//parent::div//div[@role='menu']//span[text()='Expand this widget']";
	 public static String ddCourseIntroductionCollapseWidget = "//li[@class='d2l-contextmenu-item d2l-first-visible-item d2l-last-visible-item']//span[text()='Collapse this widget']";
	// public static String infoCourseIntroduction = "//div[@class='container-fluid']/p";
	 public static String bannerImg = "//d2l-image-banner-overlay[contains(@class,'d2l-image-banner-overlay')]";
	// public static String titleOnBanner = "//div[@id='overlayContent']/div/div/h1";
	 
	 public static String titleOnBanner(String BannerCourseTitle)
	    {
	    String bannerTitle="//d2l-image-banner-overlay[contains(@banner-title,'DMG  850 1995 Producing Original Management Ideas That Influence:  Publishing and Conferencing (2188)')]";
	    return bannerTitle;
	    }
	 
	 //xpath release 10.8.8
	 public static String infoCourseIntroduction= "(//title[text()='Introduction']//following::div/p)[1]";
	// public static String ddCourseIntroductionExpandWidget= "//*[@class='d2l-contextmenu-item d2l-menu-item-first d2l-menu-item-last']//span[text()='Expand this widget']";
	 public static String ddCourseIntroductionExpandWidget= "//d2l-menu-item[@text='Expand this widget' and contains(@class,'d2l-contextmenu-item d2l-menu-item-first d2l-menu-item-last')]";
	 
	 //public static String ddProjectExpandWidget = "//*[@class='d2l-contextmenu-item d2l-menu-item-first d2l-menu-item-last']//span[text()='Expand this widget']";
	 public static String ddProjectExpandWidget = "//d2l-menu-item[@text='Expand this widget' and contains(@class,'d2l-contextmenu-item d2l-menu-item-first d2l-menu-item-last')]";
	 public static String iframeCourseIntroduction = "(//div[@class='d2l-htmlblock']//iframe[@scrolling='no'])[1]";
	// public static String invertedTriangleBesideProject = "//button[@title='Actions for Projects']";
	 public static String invertedTriangleBesideProject = "//d2l-button-icon[@text='Actions for Projects']";
	 public static String projectsTextwithArrow = "//h2[text()='Projects']";

}
