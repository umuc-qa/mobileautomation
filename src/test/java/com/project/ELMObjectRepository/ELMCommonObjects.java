package com.project.ELMObjectRepository;

public class ELMCommonObjects {
	
	public static String Login_UserName = "//*[@id='Username']";
	public static String Login_Password = "//*[@id='Password']";
	public static String Login_Button = "//*[@name='Login']";
	public static String Course_Banner = "//*[@nodeName='D2L-IMAGE-BANNER-OVERLAY']";
	public static String Course_Title = "//*[@id='courseName']";
	
//	Competency Creation elements
	public static String Competency_Name = "//*[@name='name']";
	public static String Competency_Description = "//*[@nodeName='P' and @iframeId='description$html_ifr' and @onScreen='true']";
	public static String Competency_Status_Android = "//*[@text='Draft']/ancestor::select";

	public static String Competency_Status = "//*[@name='SL_status']";
	public static String Competency_Status_Options = "//*[@class='UIAPicker']";
	public static String MakeCompetencyAndChildrenVisibleToUsers_CheckBox = "//*[@text='Make competency and its children visible to users']/..//*[@checked='false' and @type='checkbox']";

//	Child Competency Creation elements
	public static String Child_Competency_Structure = "//*[@text='Structure']";
	public static String Child_Competency_Name = "//*[@text='Name']";
	public static String Child_Competency_Status_Draft = "//*[@contentDescription='Draft']";
	public static String Child_MakeCompetencyAndChildrenVisibleToUsers_CheckBox = "//*[@class='android.view.View' and ./*[@contentDescription='Make competency and its children visible to users']]";
	public static String Child_Competency_Add = "//*[@content-desc='Add' or @contentDescription='Add']";

//	Learning Objective Creation elements	
	public static String Ready_For_Evaluation_Yes = "(//*[@content-desc='Yes' and @checked='false'])[2]";
	public static String CheckBox_Beside_Learner = "//*[@text='oldString']/../../../..//input";
	public static String Inverted_Triangle_Beside_LearnerObjective = "//*[@text='oldString']/ancestor::td/following-sibling::td[1]";
	public static String Activity_Required_To_Complete_Learning_Objective ="//*[@text='The activity is required to complete learning objective']/..//*[@nodeName='INPUT' and @checked='false']";
	
//	Deleting Competencies Elements
	public static String Plus_Sign_Beside_Competency = "//*[contains(@alt,'oldString')]";
	public static String Child_Competency_Status_Approved = "//*[@text='Status']/ancestor::tr/following-sibling::tr[1]//select";
	
//  Creating Rubric elements
	public static String Rubric_Radio_Button = "//*[@text='oldString']/../..//input";
	public static String Navigation_Admin_Tools_One = "(//*[@text='Admin Tools'])[1]"; 
	public static String Rubric_Name = "//*[@id='z_f' and @type='text']";
	public static String Rubric_Status = "//*[@nodeName='SELECT' and @id='z_j']";
	public static String Rubric_Status_Radio_Publiched = "//*[@text='Published' and @checkable='true']";

	public static String Rubric_Type = "(//*[@text='Rubric Type']/ancestor::tr/following-sibling::tr[1]//select)[1]";
	public static String Rubric_Initial_of_Levels = "//*[@id='z_bh' and @type='text']";
	public static String Rubric_Initial_of_Criteria = "//*[@id='z_bn' and @type='text']";
	public static String Rubric_Scoring_Method = "//*[@nodeName='SELECT' and @id='z_bs']";
	public static String Rubric_Scoring_Method_Text_Only = "//*[@text='Text Only' and @checkable='true']";

	public static String Rubric_Allow_New_Associations_In_CheckBox = "//*[@text='Competencies']/..//*[@nodeName='INPUT' and @checked='false']"; 
	public static String Rubric_Dropdown_Options = "//*[@class='UIAPicker']";
	
//	Creating Rubric Levels of criteria Elements
	public static String Inverted_Triangle_Beside_Criteria = "//*[@nodeName='A' and @id='z_g']";
	public static String Criteria_Group_Name = "//*[@nodeName='INPUT' and @id='z_e']";
	public static String Level_Name_Three = "//*[@nodeName='INPUT' and @id='z_j' or @value='Level 3']";
	public static String Level_Name_Two = "//*[@nodeName='INPUT' and @id='z_m' or @value='Level 2']";
	public static String Level_Name_One = "//*[@nodeName='INPUT' and @id='z_p' or @value='Level 1']";
	public static String Save = "//*[@text='Save']";
	
	public static String Score_Level_Three = "//*[@nodeName='A' and @id='z_w']";
	
	public static String Level_Name = "//*[@nodeName='INPUT' and @id='z_h' and @type='text']";
	


//	Creating Grade elements
	public static String Grading_Maximum_Points ="//*[@id='z_v']";
	public static String Grading_Scheme ="//*[@id='z_bf']";
	public static String ELMGrading ="//*[@text='-- Default Scheme -- (ELM Grading Scheme)']";

	public static String ReplaceString(String xpath, String oldString, String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
	//new xpaths
	
	public static String txtAdminUserName = "//*[@id='userName']";
	public static String txtAdminPassword = "//*[@id='password']";
	public static String btnAdminLogin = "//button[text()='Log In']";
	
	

}