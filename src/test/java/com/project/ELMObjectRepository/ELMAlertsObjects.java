package com.project.ELMObjectRepository;

public class ELMAlertsObjects {
	public static String ExpandedAlert = "//*[@text='Alerts']/../../..//div[2]//iframe";
	public static String AlertsExpand = "//*[@text='Alerts']/../..//a";
	public static String AlertMessageNewAssignmentSubmissions = "//*[contains(@text,'oldString')]";
	public static String AlertMessageNewAssignmentSubmissionsLink = "//*[@text='New Assignment Submissions']//*[@nodeName='STRONG']";
	public static String AlertMessageAssignmentSFWithUnreadFeedback = "//*[contains(@text,'Assignment Submission Folders with Unread Feedback')]//*[@nodeName='STRONG']";
	public static String FeedbackLinkBesideAssignment = "//*[@text='oldString']/ancestor::tr//td//*[@text='View']";
	
	public static String ReplaceString(String xpath, String oldString, String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
}
