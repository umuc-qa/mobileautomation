package com.project.ELMObjectRepository;

public class ELMChatObjects {
	
	//public static String New_Chat_Button = "//*[text()='New Chat' and @type='submit']";
	public static String Chat_Title = "//*[@id='z_e']";
	public static String General_Chat_Radio_Button = "//*[@id='z_i']";
	//public static String Chat_Description = "//*[@nodeName='P']";
	//public static String Chat_Create_Button = "//*[@nodeName='BUTTON' and @id='z_a' and @type='submit']";
	public static String Text_Message_Area = "//*[@name='msg']";
//	public static String Send_Button = "//*[@id='z_d']";
	public static String Text_Message = "//*[contains(@text,'Message send by')]";
	
	
	//new xpaths
	
	public static String New_Chat_Button = "//*[text()='New Chat']";
	public static String Chat_Description = "//body[@id='tinymce']";
	public static String Chat_Create_Button = "//button[text()='Create']";
	public static String Send_Button = "//*[@id='z_e']";
	
	
}
