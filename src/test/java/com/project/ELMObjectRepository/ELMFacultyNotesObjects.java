package com.project.ELMObjectRepository;

public class ELMFacultyNotesObjects {
	
	public static String Faculty_Notes ="//*[@contentDescription='Faculty Notes' and @content-desc='Faculty Notes' and @onScreen='true' or @text='Faculty Notes']";
	public static String More ="//*[@text='More']";
	
	//new xpaths
	
	//public static String lnkFacultyNotes ="(//*[@arialabel='Returns to previous menu.  You are viewing Instructor Tools.'])[2]/following-sibling::d2l-menu-item-link/a";
	public static String lnkFacultyNotes ="(//d2l-menu-item-link[@text='Faculty Notes' and not(contains(@class,'d2l-navigation-s-menu-item-root'))])[last()]";
			//"(//*[text()='Faculty Notes'])[3]";
	public static String txtFacultyNotes = "//h1[text()='Faculty Notes']";
	
	
}

