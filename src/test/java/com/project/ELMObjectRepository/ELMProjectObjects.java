package com.project.ELMObjectRepository;

public class ELMProjectObjects {
	
	
	
	public static String Project_Name="//*[@contentDescription='oldString' and @top='true' and @onScreen='true']";
	public static String Step_Number="//*[@contentDescription='oldString']";
	public static String Topic_Name="//*[@contentDescription='oldString']";
	public static String Project_Menu="//*[@class='android.view.View' and @height>0 and ./*[@contentDescription='Expand side panel Collapse side panel']]";
	
	
	public static String ReplaceString(String xpath, String oldString, String newString) {
		String updated_Xpath = xpath.replace(oldString, newString);
		return updated_Xpath;
	}
	
	
}

