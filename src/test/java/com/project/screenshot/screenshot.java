package com.project.screenshot;

import com.project.setup.TestSetup;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * 
 */
public class screenshot extends TestSetup{

//    public static String screenShotPath = System.getProperty("user.dir") +"/test-output/reports/screenshots/";
	public static String screenShotPath =  "C:\\Project\\UMUC\\Appium_Base_Framework-master\\Appium_Base_Framework-master\\test-output\\reports\\screeshots\\";

    public static String getScreenshot( ) throws IOException {
        File file = new File(screenShotPath);
        genResDir(file);
        String imgPath = screenShotPath + UUID.randomUUID();
        System.out.println("Capturing the snapshot of the page ");
        File srcFiler;
        if(configProps.getProperty("deviceType").equals("iOS")){
        	srcFiler=((TakesScreenshot)iosDriver).getScreenshotAs(OutputType.FILE);
		}
		else
		{
			//srcFiler=((TakesScreenshot)androidDriver).getScreenshotAs(OutputType.FILE);
			srcFiler=((TakesScreenshot)androidDriver1).getScreenshotAs(OutputType.FILE);
		}
        FileUtils.copyFile(srcFiler, new File(imgPath));
        return imgPath;
    }

    private static void genResDir(File theDir){
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            boolean result = false;

            try{
                theDir.mkdir();
                result = true;
            }
            catch(SecurityException se){
                //handle it
            }
            if(result) {
                System.out.println("DIR created");
            }
        }
    }
}
