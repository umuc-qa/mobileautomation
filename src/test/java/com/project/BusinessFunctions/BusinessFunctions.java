package com.project.BusinessFunctions;

import io.appium.java_client.MobileElement;
import io.appium.java_client.SwipeElementDirection;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.*;
import java.util.*;

import org.apache.commons.collections.functors.SwitchTransformer;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.SwitchToFrame;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.experitest.appium.SeeTestIOSDriver;
import com.experitest.appium.SeeTestIOSElement;
import com.project.ClassicObjectRepository.*;
import com.project.ELMObjectRepository.*;
import com.project.setup.CommandWrapper;
import com.project.report.Accelerators.ActionEngine;

/**
 * @author E001115
 *
 */
public class BusinessFunctions extends CommandWrapper {

	public static String url = configProps.getProperty("App_URL");	//"https://learnqa.umuc.edu";
	public SimpleDateFormat formatter;
	public String formattedStartDate;

	CommandWrapper getCommand = new CommandWrapper();
	ActionEngine actionEngine = new ActionEngine();

	/**
	 * Method to add days to current date
	 * @return 
	 * @param date
	 * @param noOfDays
	 */	
	private Date addDaystoClassAccess(int noOfDays, Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, noOfDays);
		return cal.getTime();
	}

	/**
	 * Method to get Random number with date
	 * @return 
	 * @throws Throwable
	 */	
	public String getRandomNumberDate() throws Throwable {
		Date todayDate = new Date();
		Date startDate = addDaystoClassAccess(0, todayDate);
		formatter = new SimpleDateFormat("MMddyymmss");
		formattedStartDate = formatter.format(startDate);
		return formattedStartDate;
	}

	/**
	 * Method to add days to current date
	 * @return 
	 * @throws Throwable
	 * @param noOfDays
	 */		 
	private String addDaysToCurrentDate(int noOfDays) throws Throwable {
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, noOfDays);
		Date currentDatePlusOne = cal.getTime();
		System.out.println(dateFormat.format(currentDatePlusOne));
		return dateFormat.format(currentDatePlusOne);
	}

	/**
	 * Method to clear browser history
	 * @return 
	 * @throws Throwable
	 */	
	public void clearBrowserHistory() throws Throwable {
       if(!configProps.getProperty("deviceType").equalsIgnoreCase("iOS")) {
           elementContext("NATIVE_APP");
           
           elementClick(By.xpath(CommonObjects.History_Menu_Button), "Browser Menu button");
           elementClick(By.xpath(CommonObjects.History_Button), "History Button");
           if(elementExists(By.xpath(CommonObjects.History_Clear_Browsing_Data))) {
              elementClick(By.xpath(CommonObjects.History_Clear_Browsing_Data), "Clear Browsing data");
           }
           elementClick(By.xpath(CommonObjects.History_clear), "Clear data");
           // From Mahender
           if(elementExists(By.xpath(CommonObjects.History_StorageClear))){
               elementClick(By.xpath(CommonObjects.History_StorageClear), "Clear Storage Popup");
           }
           
           if(elementExists(By.xpath(CommonObjects.History_Close_Menu_Id))){
              elementClick(By.xpath(CommonObjects.History_Close_Menu_Id), "Close Icon");
           }
           
          /* if(elementExists(By.xpath(CommonObjects.Android_Pages))) {
              elementClick(By.xpath(CommonObjects.Android_Pages), "Pages");
              elementClick(By.xpath(CommonObjects.History_Menu_Button), "Browser Menu button");
              elementClick(By.xpath(CommonObjects.Close_All_Tabs), "Close all tabs");
              if(elementExists(By.xpath(CommonObjects.Plus_Icon))){
            	  elementClick(By.xpath(CommonObjects.Plus_Icon), "Plus Icon");
              }
           }*/
           elementContext("WEBVIEW_1");               
        } else{
           elementContext("NATIVE_APP");
           elementClick(By.xpath(CommonObjects.ShowBookmarks), "Show BookMarks");
           elementClick(By.xpath(CommonObjects.History_Clear), "Clear");
           elementClick(By.xpath(CommonObjects.History_All_Time), "All time");
           if(elementExists(By.xpath(CommonObjects.History_Done)))
           {
        	   elementClick(By.xpath(CommonObjects.History_Done), "Done");
           }
           elementClick(By.xpath(CommonObjects.History_Pages), "Pages");
           if(elementExists(By.xpath(CommonObjects.CloseButtonTab))){
               for (int i = 0; i < 5; i++) {
            	   	 Thread.sleep(2000);
                     if(elementExists(By.xpath(CommonObjects.CloseButtonTab))){
                            elementClick(By.xpath(CommonObjects.CloseButtonTab), "closeButton");
                     } else {
                            break;
                     }
               }
           }
           elementContext("WEBVIEW_1");
        }
  }

	/**
	 * Method to clear browser history
	 * @return 
	 * @throws Throwable
	 * @param browser
	 */	
	public void clearBrowserHistory(String browser) throws Throwable {
		if (browser.equals("chrome")) {
			elementClick(By.xpath("//*[@resource-id='com.android.chrome:id/menu_button']"), "Menu Button");
			elementClick(By.xpath("//*[@text='History']"), "History Button");
			elementClick(By.xpath("//*[@resource-id='clear-browsing-data']"), "Clear Browsing data");
			elementClick(By.xpath("//*[@text='CLEAR DATA']"), "Clear Data");
		} else {
			System.out.println("Mozilla");
			elementContext("NATIVE_APP");
			elementClick(By.xpath("//*[@id='menu_button' and @content-desc='More options' and @index='0']"), "More Options");
			elementClick(By.xpath("//*[@text='History']"), "History Button");
			elementContext("WEBVIEW_1");
			elementClick(By.xpath("//*[@text='Clear browsing data...']"), "Clear Browsing Data");
			elementContext("NATIVE_APP");
			elementClick(By.xpath("//*[@text='Clear data']"), "Clear Data");
			elementContext("WEBVIEW_1");
		}
	}

	/**
	 * Method to login into D2l Classic application
	 * @return boolean
	 * @param userName - PI or Learner user name
	 * @param pwd - PI or Learner password
	 * @throws Throwable exception
	 */
	public boolean classicLogIn(String userName, String password) throws Throwable {
        boolean flag = true;
        try {  	
        	// elementClick(By.xpath(CommonObjects.Login_UserName), "Login Button");
              ExplicitWaitOnElementToBeClickable(By.xpath(CommonObjects.Login_UserName));
        	elementSendText(By.xpath(CommonObjects.Login_UserName), userName, "Username Textbox");
              elementSendText(By.xpath(CommonObjects.Login_Password), password, "Password textbox");
              waitForVisibilityOfElement(By.xpath(CommonObjects.Login_Button), "Login Button");
              scrollingToElementofAPage(By.xpath(CommonObjects.Login_Button));
              elementClick(By.xpath(CommonObjects.Login_Button), "Login Button");
              Thread.sleep(5000);
              explicitWait(androidDriver1, By.xpath(CommonObjects.Select_A_Course_Icon));
            /*  if (isElementPresentNegative(By.xpath(CommonObjects.linkContinue), "Continue"))
            	  elementClick(By.xpath(CommonObjects.linkContinue), "Continue");
              Thread.sleep(3000);
              if (isElementPresentNegative(By.xpath(CommonObjects.lnkDoItLater), "Do It Later"))
					clickNoImage(By.xpath(CommonObjects.lnkDoItLater));*/
        } catch (Exception e) {
               flag = false;
               e.printStackTrace();
        }
        finally{
               if(flag){
                     SuccessReport("Login", "Login successfull");
               } else {
                     failureReport("Login", "Failed to login");
               }
        }
        return flag;
  }

	/**
	 * Method to logout from the application
	 * @return 
	 * @throws Throwable
	 */
	public boolean logout() throws Throwable {
		boolean flag = true;
		try {
			switchToDefaultFrame();
			 if(configProps.getProperty("browserType").equalsIgnoreCase("Safari")) {
			     if(elementExists(By.xpath(CommonObjects.Logout_IOS), "Click on LogOut Icon")){
				    JSClick(By.xpath(CommonObjects.Logout_IOS), "Click on LogOut Icon");
			     } else {
			    	// elementSwipe(SwipeElementDirection.UP, 500, 1000);
			    	 JSClick(By.xpath(CommonObjects.Logout_IOS), "Click on LogOut Icon");
			     }
	    	 } else{

	    		 if(elementExists(By.xpath(CommonObjects.profileIcon), "Profile Icon")){
	    			 scrollingToElementofAPage(By.xpath(CommonObjects.profileIcon));
	    			 Thread.sleep(1000);
	    			 elementClick(By.xpath(CommonObjects.profileIcon), "Click on Profile ICon");
			        Thread.sleep(3000);
			        
	    		 } else {
			    	// elementSwipe(SwipeElementDirection.UP, 500, 1000);
			    	//elementClick(By.xpath(CommonObjects.Logout_Android), "Click on LogOut Icon");
			     }
	    	 }
			 selectingOptionForLogout("Log Out", "Log Out");
			 Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		finally{
			if(flag){
				SuccessReport("Logout", "Logout successfull");
			} else {
				failureReport("Logout", "Failed to Logout");
			}
		}
		return flag;
	}

	/***
	 * Method to create an assignment
	 * @param assignmentName
	 * @param categoryName
	 * @param gradeName
	 * @param outOfBoxScore
	 * @param instructionsDetails
	 * @return
	 * @throws Throwable
	 */
	public boolean assignmentCreation(String assignmentName, String categoryName, String gradeName, String outOfBoxScore, String instructionsDetails) throws Throwable {
		boolean flag = true;
		try {
			
			waitForVisibilityOfElement(By.xpath(AssignmentsObjects.New_Assignment), "New Assignment");
			 elementClick(By.xpath(AssignmentsObjects.New_Assignment),"New Assignment");
			 Thread.sleep(2000);
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), "Assignment Textbox");
			 elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment Name");
			 //textSearchRubricDoubleShadowDOM(assignmentName, tagName);
			 
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.New_Category_Link));
			 JSClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
			 Thread.sleep(2000);
			 switchToDefaultFrame();
			 switchToFrameByLocator(By.xpath("//iframe[@title='New Assignment Submission Category']"), "Frame New category");
			 Thread.sleep(1500);
			/* String text1 = elementGetText(By.xpath("//*[@text='Name']"), "Category Name"); 
			 System.out.println(text1);
			JSClick(By.xpath(AssignmentsObjects.Category_Name_TextBox),"Category Name TextBox");
			 elementSendKeys(categoryName);
			 */	
			
			 elementSendText(By.xpath(AssignmentsObjects.Category_Name_TextBox), categoryName, "Category name");
			 
			 if(!(isElementPresent(By.xpath(AssignmentsObjects.Category_Name_TextBox),""))) {
				 switchToDefaultFrame();
				 scrollingToElementofAPage(By.xpath(AssignmentsObjects.New_Category_Link));
				 JSClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
				 Thread.sleep(2000);
				 switchToDefaultFrame();
				 switchToFrameByLocator(By.xpath("//iframe[@title='New Assignment Submission Category']"), "Frame New category");
				Thread.sleep(1500);
				waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Category_Name_TextBox), "Category name");
				 elementSendText(By.xpath(AssignmentsObjects.Category_Name_TextBox), categoryName, "Category name");
				}
			 
			/* if(!(elementCloseKeyBoard()==true)) {
				 elementCloseKeyBoard(); 
			 }*/
			 
			 switchToDefaultFrame();
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Category_Save_Button));
			 JSClick(By.xpath(AssignmentsObjects.Category_Save_Button),"Category Save Button");
			 Thread.sleep(2000);
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");
			 JSClick(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");
			/*String text2 = elementGetText(By.xpath("//*[@text='Name']"), "Grade Name"); 
			 System.out.println(text2);
			 switchToFrameByIndex(0);
			 */		
			 Thread.sleep(2000);
			 switchToFrameByLocator(By.xpath("//iframe[@title='New Grade Item']"), "New grade frame");
			 /* elementClick(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"Grade Name TextBox");
			 elementSendKeys(gradeName);*/
			 if(isElementPresentNegative(By.xpath(AssignmentsObjects.linkNumeric),"")) {
					scrollingToElementofAPage(By.xpath(AssignmentsObjects.linkNumeric));
					JSClick(By.xpath(AssignmentsObjects.linkNumeric), "Numeric Link");
					Thread.sleep(3000);
				}
			 Thread.sleep(1500);
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"gradeName");
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Grade_Name_TextBox));
			 elementSendText(By.xpath(AssignmentsObjects.Grade_Name_TextBox), gradeName, "Grade");
			// elementCloseKeyBoard();
			 switchToDefaultFrame();
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Grade_Save_Button));
			 JSClick(By.xpath(AssignmentsObjects.Grade_Save_Button),"Grade Save Button");
			 Thread.sleep(3000);
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.txtOut_Of_Box_Score),"Out of Box Score");
             elementSendText(By.xpath(AssignmentsObjects.txtOut_Of_Box_Score),outOfBoxScore, "Out of Box Score");
             Thread.sleep(200);
             if(elementCloseKeyBoard()==false) {
				 elementCloseKeyBoard(); 
			 }
             switchToFrameByLocator(By.xpath(AssignmentsObjects.Instructions_TextBox), "Instructor field description");
             Thread.sleep(2000);
            // elementClick(By.xpath(AssignmentsObjects.txtInstructionsTextBox), "Instruction details");
             scrollingToElementofAPage(By.xpath(AssignmentsObjects.txtInstructionsTextBox));
             elementSendText(By.xpath(AssignmentsObjects.txtInstructionsTextBox), instructionsDetails, "Instruction details");
             // elementClick(By.xpath(AssignmentsObjects.Instructions_TextBox),"Instructions TextBox");	 
             //elementSendKeys(instructionsDetails);
             if(elementCloseKeyBoard()==false) {
				 elementCloseKeyBoard(); 
			 }
			 switchToDefaultFrame();
			 Thread.sleep(2000);
			 scrollingToElementofAPage(By.xpath("//*[text()='Restrictions']"));
			 JSClick(By.xpath("//*[text()='Restrictions']"),"Restrictions tab");
			 // selectingOption("Restrictions", "Restrictions");
			// elementClick(By.xpath("//*[@name='z_n' and @checked='checked' and @type='checkbox']"), "Hidden from users");
			 Thread.sleep(4000);
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.chkHiddenFromUsers),"chkHiddenUsers");
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.chkHiddenFromUsers));
			 JSClick(By.xpath(AssignmentsObjects.chkHiddenFromUsers), "checkbox Hidden from users");
			 Thread.sleep(3000);
			 /*if(elementExists(By.xpath(ELMAssignmentObjects.Created_Successfully_Close))){
                 elementClick(By.xpath(ELMAssignmentObjects.Created_Successfully_Close), "Created successfully close");
             }*/
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Save_And_Close_Button));
			 JSClick(By.xpath(AssignmentsObjects.Save_And_Close_Button),"Save and Close Button");	
			 elementExists(By.xpath(AssignmentsObjects.msgsavedSuccesfullyText), "Saved succesfully message");
			 Thread.sleep(5000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	
	public boolean assignmentCreationWithoutRestrictions(String assignmentName, String categoryName, String gradeName, String outOfBoxScore, String instructionsDetails) throws Throwable {
		boolean flag = true;
		try {
			System.out.println("Tet");
			 elementClick(By.xpath(AssignmentsObjects.New_Submission_Folder),"New Submission Folder");
			 elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment Name");
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.New_Category_Link));
			 elementClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
			 Thread.sleep(2000);
			 switchToDefaultFrame();
			 switchToFrameByLocator(By.xpath("//iframe[@title='New Assignment Submission Category']"), "Frame New category");
			Thread.sleep(1500);
			/* String text1 = elementGetText(By.xpath("//*[@text='Name']"), "Category Name"); 
			 System.out.println(text1);
			JSClick(By.xpath(AssignmentsObjects.Category_Name_TextBox),"Category Name TextBox");
			 elementSendKeys(categoryName);
			 */			
			 elementSendText(By.xpath(AssignmentsObjects.Category_Name_TextBox), categoryName, "Category name");
			 elementCloseKeyBoard();
			 switchToDefaultFrame();
			 JSClick(By.xpath(AssignmentsObjects.Category_Save_Button),"Category Save Button");
			 Thread.sleep(2000);
			 JSClick(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");
			/*String text2 = elementGetText(By.xpath("//*[@text='Name']"), "Grade Name"); 
			 System.out.println(text2);
			 switchToFrameByIndex(0);
			 */		
			 Thread.sleep(2000);
			 switchToFrameByLocator(By.xpath("//iframe[@title='New Grade Item']"), "New grade frame");
			 /* elementClick(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"Grade Name TextBox");
			 elementSendKeys(gradeName);*/
			 Thread.sleep(1500);
			 elementSendText(By.xpath(AssignmentsObjects.Grade_Name_TextBox), gradeName, "Grade");
			// elementCloseKeyBoard();
			 switchToDefaultFrame();
			 JSClick(By.xpath(AssignmentsObjects.Grade_Save_Button),"Grade Save Button");
			 Thread.sleep(3000);
             elementSendText(By.xpath(AssignmentsObjects.txtOut_Of_Box_Score),outOfBoxScore, "Out of Box Score");
             Thread.sleep(200);
             elementCloseKeyBoard();
             switchToFrameByLocator(By.xpath(AssignmentsObjects.Instructions_TextBox), "Instructor field description");
             Thread.sleep(2000);
            // elementClick(By.xpath(AssignmentsObjects.txtInstructionsTextBox), "Instruction details");
             scrollingToElementofAPage(By.xpath(AssignmentsObjects.txtInstructionsTextBox));
             elementSendText(By.xpath(AssignmentsObjects.txtInstructionsTextBox), instructionsDetails, "Instruction details");
             // elementClick(By.xpath(AssignmentsObjects.Instructions_TextBox),"Instructions TextBox");	 
             //elementSendKeys(instructionsDetails);
			// elementCloseKeyBoard();
			 switchToDefaultFrame();
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Save_And_Close_Button));
			 JSClick(By.xpath(AssignmentsObjects.Save_And_Close_Button),"Save and Close Button");
			 Thread.sleep(2000);
		 /*	// scrollingToElementofAPage(By.xpath("//*[text()='Restrictions']"));
			 JSClick(By.xpath("//*[text()='Restrictions']"),"Restrictions tab");
			 // selectingOption("Restrictions", "Restrictions");
			// elementClick(By.xpath("//*[@name='z_n' and @checked='checked' and @type='checkbox']"), "Hidden from users");
			 Thread.sleep(3000);
			 elementClick(By.xpath(AssignmentsObjects.chkHiddenUsers), "checkbox Hidden from users");	 
			 if(elementExists(By.xpath(ELMAssignmentObjects.Created_Successfully_Close))){
                 elementClick(By.xpath(ELMAssignmentObjects.Created_Successfully_Close), "Created successfully close");
             }
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Save_And_Close_Button));
			 JSClick(By.xpath(AssignmentsObjects.Save_And_Close_Button),"Save and Close Button");	
			 elementExists(By.xpath(AssignmentsObjects.msgsavedSuccesfullyText), "Saved succesfully message");*/
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	
	/***
	 * Method to create an assignment with Rubric
	 * @param assignmentName
	 * @param categoryName
	 * @param gradeName
	 * @param outOfBoxScore
	 * @param instructionsDetails
	 * @return
	 * @throws Throwable
	 */
	public boolean assignmentCreationWithRubric(String assignmentName, String categoryName, String gradeName, String outOfBoxScore,	String instructionsDetails) throws Throwable {
		boolean flag = true;
		try {
			/* elementClick(By.xpath(AssignmentsObjects.New_Submission_Folder), "New Submission Folder");
			 elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment Name");
			 elementClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
			 String text1 = elementGetText(By.xpath("//*[@text='Name']"), "Category Name"); 
			 System.out.println(text1);
			 elementClick(By.xpath(AssignmentsObjects.Category_Name_TextBox),"Category Name TextBox");
			 elementSendKeys(categoryName);
			 elementCloseKeyBoard();
			 elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
			 elementClick(By.xpath(AssignmentsObjects.Category_Save_Button),"Category Save Button");			
			 elementClick(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");		
			 String text2 = elementGetText(By.xpath("//*[@text='Name']"), "Grade Name"); 
			 System.out.println(text2);
			 elementClick(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"Grade Name TextBox");
			 elementSendKeys(gradeName);
			 elementCloseKeyBoard();
			 elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
			 elementClick(By.xpath(AssignmentsObjects.Grade_Save_Button),"Grade Save Button");			
			 elementSwipe(SwipeElementDirection.UP, 500, 1000);
			 elementSendText(By.xpath(AssignmentsObjects.Out_Of_Box_Score),outOfBoxScore, "Out of Box Score");
			 */
			
			
			 elementClick(By.xpath(AssignmentsObjects.New_Submission_Folder),"New Submission Folder");
			 elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment Name");
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.New_Category_Link));
			 JSClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
			 Thread.sleep(4000);
			 switchToDefaultFrame();
			 switchToFrameByLocator(By.xpath("//iframe[@title='New Assignment Submission Category']"), "Frame New category");
			 Thread.sleep(1500);
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Category_Name_TextBox),"Category_Name_TextBox");
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Category_Name_TextBox));
			 elementSendText(By.xpath(AssignmentsObjects.Category_Name_TextBox), categoryName, "Category name");
			 elementCloseKeyBoard();
			 switchToDefaultFrame();
			 Thread.sleep(3000);
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Category_Save_Button),"Category_Save_Button");
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Category_Save_Button));
			 JSClick(By.xpath(AssignmentsObjects.Category_Save_Button),"Category Save Button");
			 Thread.sleep(4000);
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.New_Grade_Link));
			 JSClick(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");	
			 Thread.sleep(4000);
			 switchToFrameByLocator(By.xpath("//iframe[@title='New Grade Item']"), "New grade frame");
			 if(isElementPresentNegative(By.xpath(AssignmentsObjects.linkNumeric),"")) {
					scrollingToElementofAPage(By.xpath(AssignmentsObjects.linkNumeric));
					JSClick(By.xpath(AssignmentsObjects.linkNumeric), "Numeric Link");
					Thread.sleep(3000);
				}
			 elementSendText(By.xpath(AssignmentsObjects.Grade_Name_TextBox), gradeName, "Grade");
			// elementCloseKeyBoard();
			 switchToDefaultFrame();
			 JSClick(By.xpath(AssignmentsObjects.Grade_Save_Button),"Grade Save Button");
			 Thread.sleep(3000);
             elementSendText(By.xpath(AssignmentsObjects.txtOut_Of_Box_Score),outOfBoxScore, "Out of Box Score");
             Thread.sleep(2000);
             elementCloseKeyBoard();
			 Thread.sleep(2000);
			 JSClick(By.xpath(AssignmentsObjects.Add_Rubrics),"Add Rubrics");
			 switchToFrameByLocator(By.xpath("//iframe[@title='Select Rubric']"),"Rubric Frame");
			 Thread.sleep(2000);
			 JSClick(By.xpath("//input[@title='Select TestRubric']"), "Select a Rubric");
			// elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
			 switchToDefaultFrame();
			 Thread.sleep(1500);
			 JSClick(By.xpath(AssignmentsObjects.btnAddSelected), "Add selected button");
			 switchToFrameByLocator(By.xpath(AssignmentsObjects.Instructions_TextBox), "Instructor field description");
             Thread.sleep(2000);
             //JSClick(By.xpath(AssignmentsObjects.txtInstructionsTextBox), "Instruction details");
            // elementSendText(By.xpath(AssignmentsObjects.txtInstructionsTextBox), instructionsDetails, "Instruction details");
			  /*selectingOption("select Rubric", "Add Selected");			
			 elementClick(By.xpath(AssignmentsObjects.Instructions_TextBox), "Instructions Textbox");
			 elementSendKeys(instructionsDetails);*/
			// elementCloseKeyBoard();
			// selectingOption("Restrictions", "Restrictions");
			 switchToDefaultFrame();
			 Thread.sleep(2000);
			 JSClick(By.linkText("Restrictions"),"Restrictions tab" );
			// elementClick(By.xpath("//*[@id='z_n' and @checked='checked' and @type='checkbox']"), "Hidden from users");
			 Thread.sleep(3000);
			 elementClick(By.xpath(AssignmentsObjects.chkHiddenUsers), "checkbox Hidden from users");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to enter feedback details in assignment submission
	 * @param feedBackDesc
	 * @return
	 * @throws Throwable
	 */
	public boolean enterFeedBackDetailsInAssignmentSubmission(String feedBackDesc) throws Throwable {
		boolean flag = true;
		try {	
			 elementSwipe(SwipeElementDirection.DOWN, 200,2000);
			 elementClick(By.xpath(AssignmentsObjects.FeedBack_Box), "FeedBack Box");
			 elementSendKeys(feedBackDesc);
			 elementCloseKeyBoard();
			 selectingOption("Submit Files","Submit");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating downloaded assignment
	 * @param assignmentName
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingDownloadedAssignment(String assignmentName) throws Throwable {
		boolean flag = true;
		try {	
			 elementExists(By.xpath("//*[contains(@text,'"+assignmentName+" Download')]"));
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating assignment submission
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingAssignmentSubmission(String name) throws Throwable {
		boolean flag = true;
		try {	
			 elementExists(By.xpath("//*[@nodeName='TD' and ./*[@text='1'] and ./preceding-sibling::*[./*[./*[./*[@text='"+name+"']]]]]"),"Assignment Submission Status");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating created assignment or grade
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingCreatedAssignmentOrGrade(String name) throws Throwable {
		boolean flag = true;
		try {	
			// elementSwipeWhileNotFound(By.xpath("//*[@text='"+name+"']"), 100, SwipeElementDirection.DOWN, 20000, 1000, 30, false);
			//swipeBottumToTop(By.xpath("//*text()='"+name+"']"), "Grade Item");
			waitForVisibilityOfElement(By.xpath("//*[text()='"+name+"']"),"");
			if(elementExists(By.xpath("//*[text()='"+name+"']"))){
				 SuccessReport("Created Assignment or Grade", "Created Assignment or Grade is seen");
			} else {
                failureReport("Created Assignment or Grade", "Created Assignment or Grade is not seen");
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to enter restrictions for assignment
	 * @param assignmentName
	 * @return
	 * @throws Throwable
	 */
	public boolean enteringToRestrictionsFromAssignment(String assignmentName) throws Throwable {
		boolean flag = true;
		try {	
			// elementClick(By.xpath("//*[text()='"+assignmentName+"']/../../../*[@class='dcm bsi-button-menu dcm_handle']"),"Assignments Submission arrow");
			waitForVisibilityOfElement(By.xpath("//*[text()='"+assignmentName+"']"),"Assignment Name");
			scrollingToElementofAPage(By.xpath("//*[text()='"+assignmentName+"']"));
			//elementClick(By.xpath("//*[text()='"+assignmentName+"']"), "Assignment Name");
			JSClick(By.xpath("//*[text()='"+assignmentName+"']"), "Assignment Name");
			Thread.sleep(3000);
			//waitForVisibilityOfElement(By.xpath("//button[text()='Edit Assignment']"),"Edit Assigment Name");
			//elementClick(By.xpath("//button[text()='Edit Assignment']"), "Edit assignment folder");
			JSClick(By.xpath("//button[text()='Edit Assignment']"), "Edit assignment folder");
			waitForVisibilityOfElement(By.xpath("//*[text()='Restrictions']"),"Restrictions");
			//elementClick(By.xpath("//a[text()='Restrictions']"), "Restrictions");
			JSClick(By.xpath("//*[text()='Restrictions']"), "Restrictions");
			Thread.sleep(3000);
			//selectingOption("Assignment", "Edit Submission Folder");
			 //selectingOption("Edit Submission Folder","Restrictions");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating created assignment with restrictions
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingCreatedAssignmentWithRestrictions() throws Throwable {
		boolean flag = true;
		try {	
			 elementClick(By.xpath(AssignmentsObjects.Save_And_Close_Button),"Save And Close Button");
			// elementExists(By.xpath(AssignmentsObjects.Saved_SuccessFully_Text),"Saved Successfully Text");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating assignment or grade is deleted
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingCreatedAssignmentOrGradeIsDeleted(String name) throws Throwable {
		boolean flag = true;
		try {	
			 //pageRefresh();
			 Thread.sleep(3000);
			 if(!(isElementDisplayed(By.xpath("//input[@title='Select "+name+"']"), name))) {
				 SuccessReport("Verify Deleted Assignment", "Successfully verified Assignment/Grade is deleted"); 
			 }
			/* if(!(isElementPresent(By.xpath("//input[@title='Select "+name+"']"), "Deleted assignment"))) {
				 SuccessReport("Verify Deleted Assignment", "Successfully verified Assignment/Grade is deleted");
			 } */
			 
			 else {
				 failureReport("Verify Deleted Assignment", "Failed that Assignment is not deleted");
			 }
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	
	/***
	 * Method to click on created assignment
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public boolean clickOnCreatedAssignment(String name) throws Throwable {
		boolean flag = true;
		try {	
			 //elementSwipeWhileNotFound(By.xpath("//*[@text='"+name+"']"), 100, SwipeElementDirection.DOWN, 2000, 1000, 5, false);
			scrollingToElementofAPage(By.xpath("//*[text()='"+name+"']")); 
			elementClick(By.xpath("//*[text()='"+name+"']"), "Created Topic");
			Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to create new release condition
	 * @param conditionTypeItem
	 * @param gradeItem
	 * @param gradeScore
	 * @return
	 * @throws Throwable
	 */
	public boolean createNewReleaseCondition(String conditionTypeItem, String gradeItem, String gradeScore) throws Throwable {
		boolean flag = true;
		try {
			switchToFrameByIndex(0);
			Thread.sleep(2000);
			//(By.xpath(AssignmentsObjects.ConditionType_DropDown), "Condition Type Dropdown");
			// elementContext("NATIVE_APP");
			 //elementClick(By.xpath("//option[@value='6']"), "Value from dropdown");
			waitForVisibilityOfElement(AssignmentsObjects.selectDropDownTextwithID("z_b"),"Condition Type Dropdown");
			selectByValue(AssignmentsObjects.selectDropDownTextwithID("z_b"),"6","Condition Type Dropdown");
			 
			 Thread.sleep(3000);
			 if(configProps.getProperty("deviceType").equals("iOS")){
				 iosDriver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -1000);
				 iosDriver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -4000);
				 iosDriver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -300);
			 }
			 else
			 {
				/* androidDriver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -1000);
				 androidDriver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -4000);
				 androidDriver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -300);*/
			 }
			 /*driver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -1000);
			 driver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -4000);
			 driver.drag(By.xpath("//*[@XCElementType='XCUIElementTypePicker' and @class='UIAPicker']"), 0, -300);*/
			// elementClick(By.xpath("//*[@text='Done']"), "Done");
			 //elementContext("WEBVIEW_1");
			 //elementClick(By.xpath(AssignmentsObjects.GradeItem_DropDown), "Grade Item Dropdown");
		//	 elementContext("NATIVE_APP");
			//elementClick(By.xpath("//option[text()='ssss']"), "GradeItem dropdown");
			 waitForVisibilityOfElement(AssignmentsObjects.selectDropDownTextwithContainsName("GradeObjects"),"Condition Type Dropdown");
			 selectByVisibleText(AssignmentsObjects.selectDropDownTextwithContainsName("GradeObjects"),gradeItem,"Grade Item");
			 // elementClick(By.xpath("//*[@text='Done']"), "Done");
			// elementContext("WEBVIEW_1");
			 Thread.sleep(2000);
			 //elementSendKeys(gradeItem);
			 //elementCloseKeyBoard();
			 elementSendText(By.xpath(AssignmentsObjects.Grade_score), gradeScore, "Grade Score");
			 switchToDefaultFrame();
			 
			 elementClick(By.xpath(AssignmentsObjects.Create_Button), "Create Button");	
			 Thread.sleep(1500);
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Save_And_Close_Button));
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Save_And_Close_Button));
			 JSClick(By.xpath(AssignmentsObjects.Save_And_Close_Button), "Save and Close Button");
			 Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to deleting assignment
	 * @param type
	 * @return
	 * @throws Throwable
	 */
	public boolean deletingAssignment(String type) throws Throwable {
		boolean flag = true;
		try {
			// elementSwipeWhileNotFound(By.xpath("//*[@text='"+type+ "']/../../../../../.. //*[@name='gridFolders_cb' and @type='checkbox']"), 100, SwipeElementDirection.DOWN, 20000, 1000, 30, false);
			// elementClick(By.xpath("//*[@text='"+type+ "']/../../../../../.. //*[@name='gridFolders_cb' and @type='checkbox']"),"Assignment Checkbox");
			waitForVisibilityOfElement(By.xpath("//*[@title='Select "+type+"']"), "");
			scrollingToElementofAPage(By.xpath("//*[@title='Select "+type+"']"));  
			elementClick(By.xpath("//*[@title='Select "+type+"']"), "Assignment name");
			Thread.sleep(2000);
			elementClick(By.xpath(AssignmentsObjects.btnMoreActions),"More Actions");
			Thread.sleep(2000);
			scrollingToElementofAPage(By.xpath(AssignmentsObjects.txtDelete));
			JSClick(By.xpath(AssignmentsObjects.txtDelete), "Delete");
			//JSClick(AssignmentsObjects.txtDelete, "Delete");
			 //elementClick(By.xpath(AssignmentsObjects.txtDelete), "Delete");
			// selectingOption("Assignment", "More Actions");
			 //selectingOption("More Actions", "Delete");
			 Thread.sleep(3000);
			 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Delete_Button), "");
			 JSClick(By.xpath(AssignmentsObjects.Delete_Button), "Delete Button");
			 Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to deleting grade
	 * @param type
	 * @return
	 * @throws Throwable
	 */
	public boolean deletingGrade(String type) throws Throwable {
		boolean flag = true;
		try {
			 //elementSwipeWhileNotFound(By.xpath("//*[@text='"+ type + "']/../..//*[@name='GradesList_cb' and @type='checkbox']"), 100, SwipeElementDirection.DOWN, 20000, 1000, 30, false);
			 // elementClick(By.xpath("//*[@text='"+ type + "']/../..//*[@name='GradesList_cb' and @type='checkbox']"), "Grade Checkbox");
			//Thread.sleep(4000);
			waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Manage_Grades_Button),"Manage Grades");
			JSClick(By.xpath(AssignmentsObjects.Manage_Grades_Button),"Manage grades");
			// elementClick(By.xpath("//input[@title='Select "+type+"']"), "Check box" );
			Thread.sleep(2000);
			waitForVisibilityOfElement(By.xpath(AssignmentsObjects.btnMoreActions),"More Actions");
			 elementClick(By.xpath(AssignmentsObjects.btnMoreActions), "More Actions"); 
			/*selectingOption("Assignment", "More Actions");
			 selectingOption("More Actions", "Delete");*/
			 Thread.sleep(1500);
			 elementClick(By.xpath(AssignmentsObjects.txtDelete), "Delete option");
			 //elementSwipeWhileNotFound(By.xpath("//*[@text='"+ type+ "']/../..//*[@checked='false' and @type='checkbox']"), 100, SwipeElementDirection.DOWN, 20000, 1000, 30, false);
			 //elementClick(By.xpath("//*[@text='"+ type+ "']/../..//*[@checked='false' and @type='checkbox']"),"Grade Checkbox");
			 scrollingToElementofAPage(By.xpath("//input[@title='Select "+type+"']"));
			 Thread.sleep(3000);
			 JSClick(By.xpath("//input[@title='Select "+type+"']"), "Check box" );
			 Thread.sleep(3000);
			 elementClick(By.xpath(AssignmentsObjects.Delete_Button), "Delete Button");
			 if(isElementDisplayed(By.xpath(AssignmentsObjects.btnConfirmDelete), "Delete button")) {
				 elementClick(By.xpath(AssignmentsObjects.btnConfirmDelete), "Confirmation Delete button");
			 }
			 explicitWait(androidDriver1, By.xpath("//*[text()='Deleted successfully']"));
			 elementExists(By.xpath("//*[text()='Deleted successfully']"), "Deleted successfully");
			 Thread.sleep(2000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
     * Method to add a file
     * @return
     * @throws Throwable
     */
     public boolean addAFile(String fileType) throws Throwable {
         boolean flag = true;
     try {
    	 
    	 
    	// me.sendKeys("C:\\Users\\E004248\\Desktop\\mobileAutomation\\umuc-qa-mobile_automation-f49d33c0fc3f\\Data\\assign.jpg");
    	 switchToFrameByLocator(By.xpath(AssignmentsObjects.iframeAddAFile), "Frame");
          if(elementExists(By.xpath(AssignmentsObjects.My_Computers))) {
                         elementClick(By.xpath(AssignmentsObjects.My_Computers), "My Computers");                            
          }   Thread.sleep(3000);
         /* MobileElement me = androidDriver1.findElement(By.xpath(AssignmentsObjects.Upload_Button));
          me.sendKeys("C:\\Users\\E004248\\Desktop\\mobileAutomation\\umuc-qa-mobile_automation-f49d33c0fc3f\\Data\\Images\\assign.jpg");*/
          //Thread.sleep(3000);
       elementClick(By.xpath(AssignmentsObjects.Upload_Button), "Upload button");
          Thread.sleep(3000);
	      if(!configProps.getProperty("deviceType").equalsIgnoreCase("iOS")) {
             elementContext("NATIVE_APP");
             elementClick(By.xpath(AssignmentsObjects.Documents_Option), "Documents option");
             if(elementExists(By.xpath(AssignmentsObjects.File_image))){
                elementClick(By.xpath(AssignmentsObjects.File_image), "File image");
             } else {
                elementClick(By.xpath(AssignmentsObjects.File_image_Android), "File Image");
             }
             for(int i = 1; i < 5; i++) {
                 if(elementExists(By.xpath("xpath=//*[contains(@text,'Please wait')]"))){
                 Thread.sleep(5000);
             } else {
                 break;
             }
           }
          } else {
             elementContext("NATIVE_APP");
             elementClick(By.xpath(AssignmentsObjects.Documents_Option), "Photo library Option");
             elementClick(By.xpath(AssignmentsObjects.Iphone_CamaraRoll), "Camera Roll");
             //elementClick(By.xpath(AssignmentsObjects.File_Iphone_image), "File Image");
             if(elementExists(By.xpath(AssignmentsObjects.File_image_Android), "File Image")){
                 elementClick(By.xpath(AssignmentsObjects.File_image_Android), "File Image");
             } else if(elementExists(By.xpath("(//*[@text='PhotosGridView']/*[contains(@text,'Live Photo')])[1]"), "File Image")){
                             elementClick(By.xpath("(//*[@text='PhotosGridView']/*[contains(@text,'Live Photo')])[1]"), "File Image");
             }
             elementClick(By.xpath(AssignmentsObjects.Iphone_ChooseVideo_Use), "Iphone choose video use");
             elementContext("NATIVE_APP_NON_INSTRUMENTED");
             if (elementExists(By.xpath(CommonObjects.Done_Button))) {
                 elementClick(By.xpath(CommonObjects.Done_Button), "Done Button");
               }
             }
             elementContext("WEBVIEW_1");
	         } catch (Exception e) {
	                 flag = false;
	                 e.printStackTrace();
	         }
	           return flag;
	 }

	/***
	 * Method to publish score for assignment
	 * @param assignment
	 * @param score
	 * @param feedback
	 * @return
	 * @throws Throwable
	 */
	public boolean publishScore(String assignment, String score, String feedback) throws Throwable {
		boolean flag = true;
		try {
			 elementClick(By.xpath("//*[@text='"+ assignment + "']/../../../*[@class='dcm bsi-button-menu dcm_handle']"), "assignment inverted triangle");
			 selectingOption("Assignment", "View Submissions");
			 //selectingOption("Assignment", "Evaluate");
			 elementClick(By.xpath(AssignmentsObjects.File_image), "File image");
			 elementSwipe(SwipeElementDirection.DOWN, 200, 2000);
			 elementSendText(By.xpath(AssignmentsObjects.Edit_Box_Score), score, "Score");			
			 elementSwipe(SwipeElementDirection.DOWN, 200, 2000);
			 elementClick(By.xpath(AssignmentsObjects.FeedBack_Box), "Feedback Box");
			 elementSendKeys(feedback);
			 elementCloseKeyBoard();			
			 elementClick(By.xpath(AssignmentsObjects.Publish_Button),"Publish Button");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating assignment published
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingAssignmentPublished() throws Throwable {
		boolean flag = true;
		try {	
			 selectingOption("Submissions","Back to Submissions");
			 elementSwipe(SwipeElementDirection.DOWN, 200,2000);
			 elementSwipe(SwipeElementDirection.RIGHT, 200,2000);
			 elementExists(By.xpath(AssignmentsObjects.Published_Text), "Published Text");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	/***
	 * Method to click on assignment published
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public boolean clickOnAssignmentPublished(String name) throws Throwable {
		boolean flag = true;
		try {	
			 elementSwipe(SwipeElementDirection.DOWN, 200,2000);
			 elementSwipe(SwipeElementDirection.RIGHT, 200,2000);
			 elementExists(By.xpath(AssignmentsObjects.Published_Text), "Published Text");
			 elementClick(By.xpath(AssignmentsObjects.Published_Text), "Published Text");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	/***
	 * Method to validating latest sore for evaluated assignment
	 * @param score
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingLatestScoreForEvaluatedAssignment(String score) throws Throwable {
		boolean flag = true;
		try {	
			 String marks = elementGetText(By.xpath(GradeObjects.Score), "Score");
			 if(marks.equals(score)) {
				 SuccessReport("Published Score", "Successfully ELM Primary Faculty verified the latest score assigned to the evaluated assignment");
             } else {
                 failureReport("Published Score", "Failed to verify latest score assigned to the evaluated assignment");
			 }
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to downloading a file
	 * @param assignment
	 * @return
	 * @throws Throwable
	 */
	public boolean fileDownloading(String assignment) throws Throwable {
		boolean flag = true;
		try {
			 selectingOption("Edit Submission Folder", "Files");
			 elementSwipe(SwipeElementDirection.DOWN, 200, 2000);
			 elementClick(By.xpath(AssignmentsObjects.Uploaded_File_Checkbox), "Uploaded File Checkbox");
			 elementClick(By.xpath(AssignmentsObjects.Download_Link), "Download link");
			 elementContext("NATIVE_APP");
			 elementClick(By.xpath("//*[contains(@text,'" + assignment + " Download')]"), "assignment link");			
			 elementClick(By.xpath(AssignmentsObjects.Download_Close_Button), "Download Close Button");
			 elementContext("WEBVIEW_1");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to add restrictions 
	 * @param assignment
	 * @return
	 * @throws Throwable
	 */
	public boolean addRestrictions(String assignment) throws Throwable {
		boolean flag = true;
		try {
			// selectingOption("Assignments", "Restrictions");			
			
			waitForVisibilityOfElement(By.xpath(AssignmentsObjects.HasStartDate_checkbox), "Has Start Checkbox");
			scrollingToElementofAPage(By.xpath(AssignmentsObjects.HasStartDate_checkbox));
			elementClick(By.xpath(AssignmentsObjects.HasStartDate_checkbox), "Has Start Checkbox");
			Thread.sleep(2000);
			waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Now_Button), "Now_Button");
			scrollingToElementofAPage(By.xpath(AssignmentsObjects.Now_Button));
			elementClick(By.xpath(AssignmentsObjects.Now_Button), "Now Button");
			Thread.sleep(2000);
			waitForVisibilityOfElement(By.xpath(AssignmentsObjects.HasEndDate_checkbox), "HasEndDate_checkbox");
			scrollingToElementofAPage(By.xpath(AssignmentsObjects.HasEndDate_checkbox));
			elementClick(By.xpath(AssignmentsObjects.HasEndDate_checkbox), "Has End Date Check box");
			Thread.sleep(2000);
			/*JSClick(By.xpath(AssignmentsObjects.EndDate), "end date fields");
			Thread.sleep(2000);
			elementClear(By.xpath(AssignmentsObjects.EndDate));
			Thread.sleep(1500);*/
			//elementSendText(By.xpath(AssignmentsObjects.EndDate),addDaysToCurrentDate(3), "Adding Days To CurrentDate");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to search a course
	 * @param className
	 * @param classID
	 * @return
	 * @throws Throwable
	 */
	public boolean courseSearch(String className, String classID) throws Throwable {
		boolean flag = true;
		try {
			 if(!configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
				elementSwipe(SwipeElementDirection.UP, 500, 1000);
			 }
			 if(configProps.getProperty("deviceType").equals("iOS")){
					explicitWait(iosDriver, By.xpath(CommonObjects.Select_A_Course_Icon));
				}
				else
				{
					//explicitWait(androidDriver, By.xpath(CommonObjects.Select_A_Course_Icon));
					Thread.sleep(5000);
					explicitWait(androidDriver1, By.xpath(CommonObjects.Select_A_Course_Icon));
					
				}
			 if(elementExists(By.xpath(CommonObjects.Select_A_Course_Icon), "Select A Course Icon")) {
				 elementClick(By.xpath(CommonObjects.Select_A_Course_Icon), "Select A Course Icon");
				 Thread.sleep(4000);
			 } else {
				 if(configProps.getProperty("deviceType").equals("iOS")){
						explicitWait(iosDriver, By.xpath(AssignmentsObjects.Menu_Bar_Icon));
					}
					else
					{
						//explicitWait(androidDriver, By.xpath(AssignmentsObjects.Menu_Bar_Icon));
						//explicitWait(androidDriver1, By.xpath(AssignmentsObjects.Menu_Bar_Icon));
						ExplicitWaitOnElementToBeClickable(By.xpath(AssignmentsObjects.Menu_Bar_Icon));
						
						/*WebElement shadowRoot1 = androidDriver1.findElement(By.cssSelector("d2l-navigation-button"));
						WebElement ele1 = (WebElement) ((JavascriptExecutor) androidDriver1).executeScript("return arguments[0].shadowRoot", shadowRoot1);
						Thread.sleep(4000);
						WebElement inputText = ele1.findElement(By.cssSelector("input"));
						SuccessReport("Type", "Successfully typed on " + text);
						WebElement shadowRoot2= ele1.findElement(By.cssSelector("d2l-button-icon"));
						WebElement ele2 = (WebElement) ((JavascriptExecutor) androidDriver1).executeScript("return arguments[0].shadowRoot", shadowRoot2);
						Thread.sleep(4000);
						//WebElement input = ele2.findElement(By.cssSelector("d2l-icon"));
						WebElement input = ele2.findElement(By.cssSelector(tagName));
						input.click();
						Thread.sleep(4000);*/

						
					} 
				 // Mahender Commented as it is not required at this place
				//elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon), "Menu Bar Icon");
				//elementClick(By.xpath(CommonObjects.Select_A_Course_Icon), "Select A Course Icon");
			 }
			// waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Menu_Bar_Icon), "Menu Bar");
			 elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon), "Menu Bar Icon");
			 Thread.sleep(4000);
			 
			 if(!(isElementDisplayed(By.xpath("//*[text()='"+classID+"']"), "Selected Class"))) {
				 textSearchRubricDoubleShadowDOM(className, "d2l-icon");
			 }
			 
			 
			// elementSendText(By.xpath(AssignmentsObjects.Search_Box), className, "Select Course Search box");
			// elementClick(By.xpath(AssignmentsObjects.Search_Icon), "Search Icon");
			 //elementClick(By.xpath("//*[contains(@text,'"	+ classID + "')]"), "Selected Class");
			 //Mahender
			 Thread.sleep(2000);
			 //elementClick(By.xpath("//a[contains(.,'"	+ classID + "')]"), "Selected Class");
			 elementClick(By.xpath("//*[text()='"+classID+"']"), "Selected Class");
			 Thread.sleep(3000);
			 //waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
			 
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	
	public void textSearchRubricDoubleShadowDOM(String text, String tagName) throws Throwable {
		try {
			//ExplicitWaitOnElementToBeClickable(AssignmentsObjects.txtSearchfor);
			WebElement shadowRoot1 = androidDriver1.findElement(By.xpath("//d2l-input-search[@label='Search']"));
			WebElement ele1 = (WebElement) ((JavascriptExecutor) androidDriver1).executeScript("return arguments[0].shadowRoot", shadowRoot1);
			Thread.sleep(2000);
			WebElement inputText = ele1.findElement(By.cssSelector("input"));
			inputText.clear();
			inputText.sendKeys(text);
			Thread.sleep(3000);
			SuccessReport("Type", "Successfully typed on " + text);
			WebElement shadowRoot2= ele1.findElement(By.cssSelector("d2l-button-icon"));
			WebElement ele2 = (WebElement) ((JavascriptExecutor) androidDriver1).executeScript("return arguments[0].shadowRoot", shadowRoot2);
			Thread.sleep(2000);
			//WebElement input = ele2.findElement(By.cssSelector("d2l-icon"));
			WebElement input = ele2.findElement(By.cssSelector(tagName));
			input.click();
			Thread.sleep(2000);
		}
		catch (Exception e) {
			failureReport("Text Search in Input Box with Search Icon", "Failed");
			e.printStackTrace();
		}
	}
	
	
	public void RubricShadowDOMClick() throws Throwable {
		try {
			//ExplicitWaitOnElementToBeClickable(AssignmentsObjects.txtSearchfor);
			WebElement shadowRoot1 = androidDriver1.findElement(By.xpath("//d2l-input-search[@label='Search']"));
			WebElement ele1 = (WebElement) ((JavascriptExecutor) androidDriver1).executeScript("return arguments[0].shadowRoot", shadowRoot1);
			Thread.sleep(2000);
			WebElement inputText = ele1.findElement(By.xpath("//button[text()='Save and Close']"));
			inputText.click();
			Thread.sleep(2000);
			/*inputText.sendKeys(text);
			Thread.sleep(3000);
			SuccessReport("Type", "Successfully typed on " + text);
			WebElement shadowRoot2= ele1.findElement(By.cssSelector("d2l-button-icon"));
			WebElement ele2 = (WebElement) ((JavascriptExecutor) androidDriver1).executeScript("return arguments[0].shadowRoot", shadowRoot2);
			Thread.sleep(2000);
			//WebElement input = ele2.findElement(By.cssSelector("d2l-icon"));
			WebElement input = ele2.findElement(By.cssSelector(tagName));
			input.click();
			Thread.sleep(2000);*/
		}
		catch (Exception e) {
			failureReport("Text Search in Input Box with Search Icon", "Failed");
			e.printStackTrace();
		}
	}
	
	public void textTypeRubricDoubleShadowDOM(String text, String tagName) throws Throwable {
		try {
			//ExplicitWaitOnElementToBeClickable(AssignmentsObjects.txtSearchfor);
			WebElement shadowRoot1 = androidDriver1.findElement(By.xpath("//d2l-input-search[@label='Search']"));
			WebElement ele1 = (WebElement) ((JavascriptExecutor) androidDriver1).executeScript("return arguments[0].shadowRoot", shadowRoot1);
			Thread.sleep(2000);
			WebElement inputText = ele1.findElement(By.cssSelector("input"));
			inputText.clear();
			inputText.sendKeys(text);Thread.sleep(4000);
			SuccessReport("Type", "Successfully typed on " + text);
			WebElement shadowRoot2= ele1.findElement(By.cssSelector("d2l-button-icon"));
			WebElement ele2 = (WebElement) ((JavascriptExecutor) androidDriver1).executeScript("return arguments[0].shadowRoot", shadowRoot2);
			Thread.sleep(2000);
			//WebElement input = ele2.findElement(By.cssSelector("d2l-icon"));
			WebElement input = ele2.findElement(By.cssSelector(tagName));
			input.click();
			Thread.sleep(2000);
		}
		catch (Exception e) {
			failureReport("textSearchInRubrics", "Failed");
			e.printStackTrace();
		}
	}
	
	/***
     * Method to selecting a menu item
     * @param menuItem
     * @return
     * @throws Throwable
     */
    
     
     public boolean selectMenuItem(String menuItem) throws Throwable {
         boolean flag = true;
         try {
        	 Thread.sleep(3000);
         	if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
                 elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
                 Thread.sleep(2000);
             }
         	
         	/* if(!(isElementPresent(By.xpath(AssignmentsObjects.MobileSideMenuNavigation),""))) {
        		 Thread.sleep(1500);
        		 JSClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
        	 }*/
         	
         	// mahender
//        	 if(!(elementExists(AssignmentsObjects.LinkTextNameInMenu(menuItem), menuItem)==true)) {
//        		 Thread.sleep(1500);
//                 elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
//        	 }
         	
        	
        	/* if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
         	Thread.sleep(1500);
                  elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
                   //elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
              }*/
        	 
               //elementClick(By.xpath("//*[@text='"+menuItem+"']"), ""+menuItem+" ");
               // mahender
         	   //elementClick(By.xpath(" //*[@nodeName='A' and @text='"+menuItem+"']"),""+menuItem+" ");
         	 	waitForVisibilityOfElement(AssignmentsObjects.LinkTextNameInMenu(menuItem), menuItem);
                scrollingToElementofAPage(AssignmentsObjects.LinkTextNameInMenu(menuItem));
         	 	elementClick(AssignmentsObjects.LinkTextNameInMenu(menuItem), menuItem);
                Thread.sleep(5000);
                
         } catch (Exception e) {
                flag = false;
                e.printStackTrace();
         }
         return flag;
  }
     
     public boolean selectMenuIteminELM(String menuItem) throws Throwable {
         boolean flag = true;
         try {
         	  
//         	if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
//                 elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
//             }
         	
         	// mahender
        	 
        	 if(!(isElementDisplayed(By.xpath(AssignmentsObjects.MobileSideMenuNavigation),"Side Navigation MenuBar To Select MenuItems"))) {
        		 Thread.sleep(1500);
        		 JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
        	 }
        	 
        	/* if(!(isElementPresent(By.xpath(AssignmentsObjects.MobileSideMenuNavigation),"Side Navigation MenuBar To Select MenuItems"))) {
        		 Thread.sleep(1500);
        		 JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
        	 }*/
        	 
        	/* 
        	 if(isElementDisplayed((By.xpath(AssignmentsObjects.MobileSideMenuNavigation),"")) {
        		 Thread.sleep(1500);
        		 JSClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
        	 }*/
        	 
         	
        	/* if(!(elementExists(By.xpath("//div[contains(@class,'d2l-navigation-s-mobile-menu-nav')]//d2l-menu[@active and @shown ]"),"Mobile Menu Nav Bar"))==true) {
        		 Thread.sleep(1500);
                 JSClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
        	 }*/
        	 
        	
        	/* if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
         	Thread.sleep(1500);
                  elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
                   //elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
              }*/
        	 
               //elementClick(By.xpath("//*[@text='"+menuItem+"']"), ""+menuItem+" ");
               // mahender
         	   //elementClick(By.xpath(" //*[@nodeName='A' and @text='"+menuItem+"']"),""+menuItem+" ");
                
                elementClick(AssignmentsObjects.d2LmenuItemtextName(menuItem), menuItem);
                
         } catch (Exception e) {
                flag = false;
                e.printStackTrace();
         }
         return flag;
  }
     
     
     public boolean selectMenuItemDoubleClick(String menuItem) throws Throwable {
         boolean flag = true;
         try {
         	  
         	/*if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
                 elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
             }*/
         	
         	// mahender
         	if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
         		Thread.sleep(1500);
                    elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
                    //if (!(isElementPresentNegative(By.xpath("//d2l-menu-item[@text='Help' and not(@class)]"), "Help"))) {
                    elementClickNegative(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
					//}
                }
               //elementClick(By.xpath("//*[@text='"+menuItem+"']"), ""+menuItem+" ");
               // mahender
         	   //elementClick(By.xpath(" //*[@nodeName='A' and @text='"+menuItem+"']"),""+menuItem+" ");
                
                elementClick(By.linkText(menuItem), ""+menuItem+"");
         } catch (Exception e) {
                flag = false;
                e.printStackTrace();
         }
         return flag;
  }
     
     public void verifyUserAbleViewAllTheClassesEnrolled() throws Throwable {
 		try {
 			if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
         		Thread.sleep(1500);
                    elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
                    elementClickNegative(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
 			}
 			 elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon), "Menu Bar Icon");
 			 Thread.sleep(3000);
 			
 			try {

 				List<MobileElement> classes = androidDriver1.findElements(By.xpath("//div[@id='courseSelectorId']//a[@class='d2l-link d2l-datalist-item-actioncontrol']"));
 				int size = classes.size();

 				for (int i = 1; i <= size; i++) {

 					String className = getText(By.xpath(
 							"(//div[@id='courseSelectorId']//a[@class='d2l-link d2l-datalist-item-actioncontrol'])[" + i
 									+ "]"),
 							"Classes").trim();
 					SuccessReport("ClaasesListed", className);
 					if (size == 0)
 						failureReport("Classes", "User not able to access classess enrolled");
 				}
 			} catch (Exception e) {
 				failureReport("Classes Enrolled", "Failed to verify Classes");
 			}
 		//	SuccessReportWithScreenshot("Access Classes", "Classes enrolled");
 			Thread.sleep(4000);
 		} catch (Exception e) {
 			failureReport("Access Classes", "Failed");
 			e.printStackTrace();
 		}

 	}
     public boolean selectingMyTools(String menuItem) throws Throwable {
         boolean flag = true;
         try {
         	  
         	/*if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
                 elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
             }*/
         	
         	// mahender
         	if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon1))){
                    elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
                }
               //elementClick(By.xpath("//*[@text='"+menuItem+"']"), ""+menuItem+" ");
               // mahender
         	   //elementClick(By.xpath(" //*[@nodeName='A' and @text='"+menuItem+"']"),""+menuItem+" ");
         	waitForVisibilityOfElement(AssignmentsObjects.d2LmenuItemtextName("My Tools"), "My Tools");
         	scrollingToElementofAPage(AssignmentsObjects.d2LmenuItemtextName("My Tools"));
         	elementClick(AssignmentsObjects.d2LmenuItemtextName("My Tools"), "My Tools");
         	 Thread.sleep(1000);
         	 //JSClick(By.xpath("//div[@class='d2l-navigation-s-mobile-menu-nav']//span[@class='style-scope d2l-menu-item'and text()='My Tools']"), "My tools");
         	 Thread.sleep(1500);
         	// elementClick(By.linkText(menuItem), ""+menuItem+"");
         	waitForVisibilityOfElement(AssignmentsObjects.LinkTextNameInMyTool(menuItem), menuItem);
         	 elementClick(AssignmentsObjects.LinkTextNameInMyTool(menuItem), "menuItem");
         	 //elementClick(By.xpath("(//d2l-menu-item-link[@text='"+menuItem+"' and not(contains(@class,'d2l-navigation-s-menu-item-root'))])[last()]"), "menuItem Grades");
         	 
         	 Thread.sleep(6000);
         } catch (Exception e) {
                flag = false;
                e.printStackTrace();
         }
         return flag;
  }

	/***
	 * Method to validating announcement present
	 * @param announcement
	 * @return
	 * @throws Throwable
	 */
	public boolean verifyAnnouncementPresent(String announcement) throws Throwable {
		boolean flag = true;
		String finalXpath = AnnouncementObjects.ReplaceString(AnnouncementObjects.List_Of_Annotations, "replaceData",announcement);
		System.out.println("finalXpath :" + finalXpath);
		if (elementExists(By.xpath(finalXpath))) {
			SuccessReport("Announcement", "Successfully found announcement:" + announcement);
		} else {
			failureReport("Announcement", "Failed to find announcement:" + announcement);
			flag = false;
		}
		return flag;
	}

	/***
	 * Method to validating announcement is not present
	 * @param announcement
	 * @return
	 * @throws Throwable
	 */
	public boolean verifyAnnouncementnotPresent(String announcement) throws Throwable {
		boolean flag = true;
		String finalXpath = AnnouncementObjects.ReplaceString(AnnouncementObjects.List_Of_Annotations, "replaceData", announcement);
		if (elementExists(By.xpath(finalXpath))) {
			failureReport("Announcement", "failed: announcement is found" + announcement);
			flag = false;
		} else {
			SuccessReport("Announcement", "Successfully announcement is not found" + announcement);              
		}
		return flag;
	}


	/***
	 * Method to selecting an option from main menu
	 * @param menu
	 * @param subMenu
	 * @return
	 * @throws Throwable
	 */
	public boolean selectingOption(String menu, String subMenu)	throws Throwable {
		boolean flag = true;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				explicitWait(iosDriver, By.xpath("//*[@text='" + subMenu + "']"));
			 }
			 else
			 {
				//explicitWait(androidDriver, By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//label[text()='" + subMenu + "']"));
				 ExplicitWaitOnElementToBeClickable(AssignmentsObjects.LinkTextNameInMyTool(subMenu));
				//explicitWait(androidDriver1,AssignmentsObjects.d2LmenuItemtextName(subMenu));
			 } 
	         
			 //explicitWait(androidDriver1,By.xpath("//label[text()='" + subMenu + "']"));
			 //explicitWait(androidDriver1,AssignmentsObjects.d2LmenuItemtextName(subMenu));
			 elementClick(AssignmentsObjects.LinkTextNameInMyTool(subMenu), subMenu);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	public boolean selectingOptionForLogout(String menu, String subMenu)	throws Throwable {
		boolean flag = true;
		try {
			
			 if(configProps.getProperty("deviceType").equals("iOS")){
				explicitWait(iosDriver, By.xpath("//*[@text='" + subMenu + "']"));
			 }
			 else
			 {
				//explicitWait(androidDriver, By.xpath("//*[@text='" + subMenu + "']"));
				explicitWait(androidDriver1,By.xpath("//*[text()='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//label[text()='" + subMenu + "']"));
			 } 
	         
			 elementClick(By.xpath("//*[text()='"+subMenu+"']"), "Click on Logout Icon");
			 Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	

	public boolean selectingMoreOptions(String menu, String subMenu)	throws Throwable {
		boolean flag = true;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				explicitWait(iosDriver, By.xpath("//*[@text='" + subMenu + "']"));
			 }
			 else
			 {
				//explicitWait(androidDriver, By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//*[@text='" + subMenu + "']"));
				 waitForVisibilityOfElement(By.xpath("//*[text()='"+subMenu+"']"),"");
				 scrollingToElementofAPage(By.xpath("//*[text()='"+subMenu+"']"));
				//explicitWait(androidDriver1,By.xpath("//*[text()='" + subMenu + "']"));
				elementClick(By.xpath("//*[text()='"+subMenu+"']"), "Click on More Actions");
				Thread.sleep(2000);
			 } 
	         
			 //explicitWait(androidDriver1,By.xpath("//*[text()=='" + subMenu + "']"));
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	
	public boolean selectingMoreOptionsasRestore(String menu, String subMenu)	throws Throwable {
		boolean flag = true;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				explicitWait(iosDriver, By.xpath("//*[@text='" + subMenu + "']"));
			 }
			 else
			 {
				//explicitWait(androidDriver, By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//*[@text='" + subMenu + "']"));
//				explicitWait(androidDriver1,By.xpath("//*[text()='" + subMenu + "']"));
//				elementClick(By.xpath("//*[text()='"+subMenu+"']"), "Restore");
				waitForVisibilityOfElement(DiscussionsObjects.d2LmenuItemLinktextName("Restore"), "Restore");
				elementClick(DiscussionsObjects.d2LmenuItemLinktextName("Restore"), "Restore");

			 } 
	         
			 //explicitWait(androidDriver1,By.xpath("//*[text()=='" + subMenu + "']"));
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	
	// Mahender Added Method to Select value which is in span and not matching to above method
	
	public boolean selectingOption1(String menu, String subMenu)	throws Throwable {
		boolean flag = true;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				explicitWait(iosDriver, By.xpath("//div[@id='d2l_1_810_953']//following::a[@class=' vui-dropdown-menu-item-link']//span[text()='"+subMenu+"']']"));
			 }
			 else
			 {
				//explicitWait(androidDriver, By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//label[text()='" + subMenu + "']"));
				
				 //explicitWait(androidDriver1,By.xpath("//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//li//a//span[text()='"+subMenu+"']"));
				 
				 //explicitWait(androidDriver1,By.xpath("(//span[@text='"+subMenu+"'])[1]"));
				 //elementClick(By.xpath("(//span[@text='"+subMenu+"'])[1]"), "Edit Topic");
				 
				 // Working 
				 List<MobileElement> elements=androidDriver1.findElements(By.xpath("//ul[@class='d2l-contextmenu-daylightoff']"));
				 WebElement element=androidDriver1.findElement(By.xpath("(//ul[@class='d2l-contextmenu-daylightoff'])["+(elements.size())+"]/li[3]"));
				 ((JavascriptExecutor) androidDriver1).executeScript("arguments[0].scrollIntoView();", element);
				 element.click();
				
				/* explicitWait(androidDriver1,By.xpath("//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//*[text()='"+subMenu+"']"));
				 elementClick(By.xpath("(//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//*[text()='"+subMenu+"']"), "Edit Topic");
				 */
				 
				System.out.println("Edit Topic Element Found");
				
			 } 
	         
			 //explicitWait(androidDriver1,By.xpath("//div[@id='d2l_1_810_953']//following::a[@class=' vui-dropdown-menu-item-link']//span[text()='"+subMenu+"']']"));
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	public boolean selectingOptionToDelete(String menu, String subMenu)	throws Throwable {
		boolean flag = true;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				explicitWait(iosDriver, By.xpath("//div[@id='d2l_1_810_953']//following::a[@class=' vui-dropdown-menu-item-link']//span[text()='"+subMenu+"']']"));
			 }
			 else
			 {
				//explicitWait(androidDriver, By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//label[text()='" + subMenu + "']"));
				
				 //explicitWait(androidDriver1,By.xpath("//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//li//a//span[text()='"+subMenu+"']"));
				 
				 //explicitWait(androidDriver1,By.xpath("(//span[@text='"+subMenu+"'])[1]"));
				 //elementClick(By.xpath("(//span[@text='"+subMenu+"'])[1]"), "Edit Topic");
				 
				 
				/* List<MobileElement> elements=androidDriver1.findElements(By.xpath("//ul[@class='d2l-contextmenu-daylightoff']"));
				 WebElement element=androidDriver1.findElement(By.xpath("(//ul[@class='d2l-contextmenu-daylightoff'])["+(elements.size())+"]/li[6]"));
				 ((JavascriptExecutor) androidDriver1).executeScript("arguments[0].scrollIntoView();", element);
				 Thread.sleep(2000);*/
				 //element.click();
				 //elementClick(By.xpath("(//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//*[text()='"+subMenu+"']"), "Delete");
				
				/* List<MobileElement> elements=androidDriver1.findElements(By.xpath("//ul[@class='d2l-contextmenu-daylightoff']"));
				 WebElement element = androidDriver1.findElement(By.xpath("(//ul[@class='d2l-contextmenu-daylightoff'])["+(elements.size())+"]/li[6]"));
				 Actions actions = new Actions(androidDriver1);
				 actions.moveToElement(element);
				 actions.build();
				 actions.perform();
				 actions.click();*/
				 //actions.doubleClick();
				 
				/* explicitWait(androidDriver1,By.xpath("//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//*[text()='"+subMenu+"']"));
				 elementClick(By.xpath("(//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//*[text()='"+subMenu+"']"), "Delete");*/
				/* 														// //div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//*[text()='Delete']	
				 WebElement element1=androidDriver1.findElement(By.xpath("//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//*[text()='"+subMenu+"']'"));
				((JavascriptExecutor) androidDriver1).executeScript("arguments[0].scrollIntoView();", element1);
				 element1.click();*/
				 
				 
				/* List<MobileElement> elements=androidDriver1.findElements(By.xpath("//ul[@class='d2l-contextmenu-daylightoff']"));
				 WebElement element=androidDriver1.findElement(By.xpath("(//ul[@class='d2l-contextmenu-daylightoff'])["+(elements.size())+"]/li[6]"));
				 ((JavascriptExecutor) androidDriver1).executeScript("arguments[0].scrollIntoView();", element);
				 element.click();*/
				 
				/* WebElement element1=androidDriver1.findElement(By.xpath("//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//*[text()='"+subMenu+"']//parent::a"));
				((JavascriptExecutor) androidDriver1).executeScript("arguments[0].scrollIntoView();", element1);
				Thread.sleep(5000); 
				element1.click();*/
				
				
				    WebElement element1=androidDriver1.findElement(By.linkText(subMenu));
					((JavascriptExecutor) androidDriver1).executeScript("arguments[0].click();", element1);
					//Thread.sleep(5000); 
					//element1.click();
				 
				 
				 //element.click();
				
				System.out.println("Clicked on Topic Delete Button");
				
			 } 
	         
			 //explicitWait(androidDriver1,By.xpath("//div[@id='d2l_1_810_953']//following::a[@class=' vui-dropdown-menu-item-link']//span[text()='"+subMenu+"']']"));
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	public boolean selectingOptionToEditTopic(String menu, String subMenu)	throws Throwable {
		boolean flag = true;
		try {
			 if(configProps.getProperty("deviceType").equals("iOS")){
				explicitWait(iosDriver, By.xpath("//div[@id='d2l_1_810_953']//following::a[@class=' vui-dropdown-menu-item-link']//span[text()='"+subMenu+"']']"));
			 }
			 else
			 {
				//explicitWait(androidDriver, By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//*[@text='" + subMenu + "']"));
				//explicitWait(androidDriver1,By.xpath("//label[text()='" + subMenu + "']"));
				
				 //explicitWait(androidDriver1,By.xpath("//div[@class='d2l-floating-container vui-dropdown-menu d2l-floating-container-autoclose']//li//a//span[text()='"+subMenu+"']"));
				 
				 explicitWait(androidDriver1,By.xpath("//div[@id='d2l_1_810_953']//following::a[@class=' vui-dropdown-menu-item-link']//span[text()='"+subMenu+"']']"));
				
				System.out.println("Edit Topic Element Found");
				
			 } 
	         
			 explicitWait(androidDriver1,By.xpath("//div[@id='d2l_1_810_953']//following::a[@class=' vui-dropdown-menu-item-link']//span[text()='"+subMenu+"']']"));
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	
	/***
	 * Method to uploading a document
	 * @param fileType
	 * @return
	 * @throws Throwable
	 */
	public boolean uploadingDocument(String fileType) throws Throwable {
		boolean flag = true;
		try {
			switchToFrameByLocator(By.xpath("//iframe[contains(@title,'Add a File')]"), "Iframe for Add a File"); 
			elementClick(By.xpath(AssignmentsObjects.My_Computers), "My Computers");
			switchToDefaultFrame();
			switchToFrameByLocator(By.xpath("//iframe[contains(@title,'Add a File')]"), "Iframe for Add a File");
		     elementClick(By.xpath(AssignmentsObjects.Upload_Button),"Upload Button");
		     switchToDefaultFrame();
			 elementContext("NATIVE_APP");
			 scrollingToElementofAPage(By.xpath("//*[text()='ALLOW']"));
			 elementClick(By.xpath("//*[text()='ALLOW']"), "Allow Chrome To record");
			 scrollingToElementofAPage(By.xpath("//*[text()='ALLOW']"));
			 elementClick(By.xpath("//*[text()='ALLOW']"), "Allow Chrome To record");
			 elementClick(By.cssSelector("#screenshotContainer > div > div > div > div > div > div.Inspector__highlighter-box___Oi319.Inspector__inspected-element-box___3mBB4 > div"), "");
			 //elementClick(By.xpath(AssignmentsObjects.Documents_Option), "Documents Options");
			 elementClick(By.xpath(AssignmentsObjects.File_image),"File Image");
			 elementContext("WEBVIEW_1");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

    /***
    * Method to uploading an image or video 
    * @param fileType
    * @return
    * @throws Throwable
    */
    public boolean uploadingImageOrVideo(String fileType) throws Throwable {
	    boolean flag = true;
	    try {
            elementClick(By.xpath(AssignmentsObjects.My_Computers), "My Computers");
            if (fileType == "Video") {
                            elementClick(By.xpath(AssignmentsObjects.ChooseFile_Button), "Choose file Button");                                                
            } else {
                            elementClick(By.xpath(AssignmentsObjects.Upload_Button),"Upload Button");
            }
            elementContext("NATIVE_APP");
            elementClick(By.xpath(AssignmentsObjects.Documents_Option),"Documents Option");
            if (fileType == "Video") {
                            elementClick(By.xpath(AssignmentsObjects.File_Video), "File video");
            } else {
                            elementClick(By.xpath(AssignmentsObjects.File_image), "File Image");                
                            for (int i = 1; i < 5; i++) {
                                            if(elementExists(By.xpath("xpath=//*[contains(@text,'%')]"))){
                                                            Thread.sleep(5000);
                                            } else{
                                                            break;
                                            }
                            }
            }
            elementContext("WEBVIEW_1");
            if (fileType == "Video") {
                            if (elementExists(By.xpath("//*[@text='Upload']"))) {
                                            selectingOption("Upload Options", "Upload");
                            }
                            for (int i = 1; i < 5; i++) {
                                            if(elementExists(By.xpath("xpath=//*[contains(@text,'Please wait')]"))){
                                                            Thread.sleep(5000);
                                            } else{
                                                            break;
                                            }
                            }
            }
            // Alternative text appears for only while uploading image
            if (fileType == "Image") {
                            if (elementExists(By.xpath("//*[@text='Add']"))) {
                                            if (selectingOption("Upload Options", "Add")) {

                                            } else {

                                            }
                            }
                            if (elementExists(By.xpath(AssignmentsObjects.Alternative_Text))) {
                                            elementSendText(By.xpath(AssignmentsObjects.Alternative_Text),"Alternative text for image", "Alternative text for image");
                                            selectingOption("Alternative text OK", "OK");
                            }
            }
            if (elementExists(By.xpath("//*[@text='Save']"))) {
                            selectingOption("Upload Options", "Save");
            }
            if (fileType == "Video") {              
                            elementClick(By.xpath(AssignmentsObjects.Link_Text), "Link text");
                            elementSendKeys("Link text for video");
            selectingOption("Upload Options", "Insert");
            }
	    } catch (Exception e) {
	                    flag = false;
	                    e.printStackTrace();
	    }
	    return flag;
    }


    /**
    * Method to add a file to iphone
    * @param fileType
    * @return
    * @throws Throwable
    */
    public boolean addAFileIPhone(String fileType) throws Throwable {
       boolean flag = true;
       try {
            if (elementExists(By.xpath(AssignmentsObjects.My_Computers))) {
                            elementClick(By.xpath(AssignmentsObjects.My_Computers), "My Computers");
            }                                              
            elementClick(By.xpath(AssignmentsObjects.Upload_Button), "Upload button");
            elementContext("NATIVE_APP");
            elementClick(By.xpath(AssignmentsObjects.PhotoLibrary_Iphone_Option), "Photo library Option");
            elementClick(By.xpath(AssignmentsObjects.Iphone_CamaraRoll), "Camera Roll");
            if (fileType == "Video") {
                            elementClick(By.xpath(AssignmentsObjects.File_Iphone_Video), "File video");
            } else {
                            elementClick(By.xpath(AssignmentsObjects.File_Iphone_image), "File Image");
            }
            if(elementExists(By.xpath(AssignmentsObjects.Iphone_ChooseVideo_Use))){
                            elementClick(By.xpath(AssignmentsObjects.Iphone_ChooseVideo_Use), "Iphone choose video use");
            }
            elementContext("NATIVE_APP_NON_INSTRUMENTED");
            if (elementExists(By.xpath(CommonObjects.Done_Button))) {
                            elementClick(By.xpath(CommonObjects.Done_Button), "Done Button");
            }
            elementContext("WEBVIEW_1");
            for (int i = 1; i < 5; i++) {
                            if(elementExists(By.xpath("xpath=//*[contains(@text,'%')]"))){
                                            Thread.sleep(5000);
                            } else{
                                            break;
                            }
            }
            selectingOption("Upload Options","Add");
            } catch (Exception e) {
                flag = false;
                e.printStackTrace();
            }
              return flag;
    }

	/***
	 * Method to uploading an image or video in iphone
	 * @param fileType
	 * @return
	 * @throws Throwable
	 */
    public boolean uploadingIphoneImageOrVideo(String fileType) throws Throwable {
        boolean flag = true;
        try {
            elementClick(By.xpath(AssignmentsObjects.My_Computers), "My Computers");
            if (fileType == "Video") {
                            elementClick(By.xpath(AssignmentsObjects.ChooseFile_Button), "Choose File Button");
            } else {
                            elementClick(By.xpath(AssignmentsObjects.Upload_Button), "Upload button");             
            }
            elementContext("NATIVE_APP");
            elementClick(By.xpath(AssignmentsObjects.PhotoLibrary_Iphone_Option), "Photo library option");
            elementClick(By.xpath(AssignmentsObjects.Iphone_CamaraRoll), "Camera roll");
            if (fileType == "Video") {
                            elementClick(By.xpath(AssignmentsObjects.File_Iphone_Video), "File Video");
            } else {
                            elementClick(By.xpath(AssignmentsObjects.File_Iphone_image), "Image");
                            for (int i = 1; i < 5; i++) {
                                            if(elementExists(By.xpath("xpath=//*[contains(@text,'%')]"))){
                                                            Thread.sleep(5000);
                                            } else{
                                                            break;
                                            }
                            }
            }
            if(elementExists(By.xpath(AssignmentsObjects.Iphone_ChooseVideo_Use))){
                            elementClick(By.xpath(AssignmentsObjects.Iphone_ChooseVideo_Use), "Choose video use");
            }
            elementContext("WEBVIEW_1");
            if (fileType == "Video") {
                            if (elementExists(By.xpath("//*[@text='Upload']"))) {
                                            selectingOption("Upload Options", "Upload");
                            }
                            for (int i = 1; i < 5; i++) {
                                            if(elementExists(By.xpath("xpath=//*[contains(@text,'Please wait')]"))){
                                                            Thread.sleep(5000);
                                            } else{
                                                            break;
                                            }
                            }
            }

            // Alternative text appears for only while uploading image
            if (fileType == "Image") {
                            if (elementExists(By.xpath("//*[@text='Add']"))) {
                                            selectingOption("Upload Options", "Add");
                            }
                            if (elementExists(By.xpath(AssignmentsObjects.Alternative_Text))) {
                                            elementSendText(By.xpath(AssignmentsObjects.Alternative_Text),"Alternative text for image","Alternative text for image");
                                            selectingOption("Alternative text OK", "OK");
                            }
            }
            if (elementExists(By.xpath("//*[@text='Save']"))) {
                            selectingOption("Upload Options", "Save");
            }

            if (fileType == "Video") {                               
                            elementClick(By.xpath(AssignmentsObjects.Link_Text), "Link Text");
                            elementSendKeys("Link text for video");
                            elementClick(By.xpath(AssignmentsObjects.Iphone_Alternative_Text), "Alternative Text");
                            elementSendKeys("Alternative text for video");
                            elementCloseKeyBoard();
                            selectingOption("Upload Options", "Insert");
            }
        } catch (Exception e) {
                        flag = false;
                        e.printStackTrace();
        }
        return flag;
}

	/***
	 * Method to calculate end time with start time
	 * @param startTime
	 * @param mins
	 * @return
	 * @throws Throwable
	 */
	public String returnTheStartOrEndTime(String startTime, int mins) throws Throwable {
	   try {
			String hour = startTime.split(" ")[0].split(":")[0];
			String min = startTime.split(" ")[0].split(":")[1].trim();
			String period = startTime.split(" ")[1];
			int mm = Integer.parseInt(min) + mins;
			int tmp = mm;
			int hh = Integer.parseInt(hour);
			/*
			 * if (mm<=9) { m ="0"+mm; mm=Integer.parseInt(m); }
			 */
			if (mm >= 60) {
				hh = Integer.parseInt(hour) + 1;
				mm = mm - 60;
			}

			if (hh == 12) {
				if (tmp >= 60) {
					if (period.equalsIgnoreCase("AM")) {
						period = "PM";
					}
				}
			}
			if (hh == 12) {
				if (tmp >= 60) {
					if (period.equalsIgnoreCase("PM")) {
						period = "AM";
					}
				}
			}

			System.out.println(hh + "   " + mm);
			if (mm <= 9) {
				startTime = hh + ":0" + mm + " " + period;
			} else {
				startTime = hh + ":" + mm + " " + period;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return startTime;
	}

	/***
	 * Method to click on calendar
	 * @throws Throwable
	 */
	public void clickCalendar() throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CommonObjects.Calendar));
		 }
		 else
		 {
			//explicitWait(androidDriver, By.xpath(CommonObjects.Calendar));
			explicitWait(androidDriver1,By.xpath(CommonObjects.Calendar));
		 } 
		
		elementClick(By.xpath(CommonObjects.Calendar), "Calender");		
		Thread.sleep(4000);
	}

	/***
	* Description: Create event,search event and verify that event is able to modify or not as a PI
	* @param EventTitle
	* @param Description
	* @param TestStartDate
	* @param TestEndDate
	* @param Location
	* @throws Throwable
	*/
	public void createEvent(String courseSearch,String EventTitle, String Description,String TestStartDate, String TestEndDate, String Location) throws Throwable {
	 
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CalendarObjects.CreateEvent));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(CalendarObjects.CreateEvent));
			explicitWait(androidDriver1,By.xpath(CalendarObjects.CreateEvent));
		} 
	    
	    elementClick(By.xpath(CalendarObjects.CreateEvent), "Create Event");
	    if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CalendarObjects.EventTitle));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(CalendarObjects.EventTitle));
			explicitWait(androidDriver1,By.xpath(CalendarObjects.EventTitle));
		} 
	    
	    elementSendText(By.xpath(CalendarObjects.EventTitle), EventTitle, "Event Title");
	    if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CalendarObjects.DescriptionText));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(CalendarObjects.DescriptionText));
			//switchToFrameByLocator(By.xpath(CalendarObjects.frameDescription), "Description");
			switchToFrameByIndex(0);
			explicitWait(androidDriver1,By.xpath(CalendarObjects.DescriptionText));
		} 
	    elementClick(By.xpath(CalendarObjects.DescriptionText),"Description Text");
	    elementSendText(By.xpath(CalendarObjects.DescriptionText),Description , "Description field");
	    //elementSendKeys(Description);
	    elementCloseKeyBoard();
	 //   elementClick(By.xpath(CalendarObjects.AttendeeId), "Attendee Id");
	   // elementContext("NATIVE_APP");
	    switchToDefaultFrame();
	    selectByVisibleText(By.xpath(CalendarObjects.AttendeeId), "Everybody in the Course Offering", "Attendees" );
	   // elementClick(By.xpath(CalendarObjects.Select_AttendeeId), "Select AttendeeId");
	   // elementContext("WEBVIEW_1");
	    scrollingToElementofAPage(By.xpath(CalendarObjects.AllDay));
	    JSClick(By.xpath(CalendarObjects.AllDay), "All Day");
	    scrollingToElementofAPage(By.xpath(CalendarObjects.Location));
	    elementSendText(By.xpath(CalendarObjects.Location), Location, "Location");
	    elementCloseKeyBoard();
	    JSClick(By.xpath(CalendarObjects.CreateButton), "Create Button");
	   Thread.sleep(3000);
	    elementExists(By.xpath(CalendarObjects.msgCreatedSuccessfully));
	    elementHardSleep(5000);
	    if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CalendarObjects.SearchEvents));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(CalendarObjects.SearchEvents));
//			explicitWait(androidDriver1,By.xpath(CalendarObjects.SearchEvents));
			textSearchRubricDoubleShadowDOM(EventTitle , "d2l-icon");
		} 
	    
	    elementHardSleep(4000);
	    if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CalendarObjects.ClickEvent));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(CalendarObjects.ClickEvent));
			// 10.8.9 explicitWait(androidDriver1,By.xpath("//a[text()='"+EventTitle+"']"));
			explicitWait(androidDriver1,By.xpath("//div[@class='d2l-page-main-padding']//a[text()='"+courseSearch+"']"));
		} 
	    elementClick(By.xpath("//div[@class='d2l-page-main-padding']//a[text()='"+courseSearch+"']"), "Event");
	    elementHardSleep(4000);
	    //elementContext("WEBVIEW_1");
	    if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath("//*[@text='"+EventTitle+"']/..//a"));
		}
		else
		{
			explicitWait(androidDriver1, By.xpath("//*[text()='"+EventTitle+"']"));
//			explicitWait(androidDriver1,By.xpath("//*[@title='"+EventTitle+"']"));
		} 
	  
	    elementClick(By.xpath("//d2l-button-icon[@text='Actions for: "+EventTitle+"']"),"clicking on Inverted triangle beside event");
	    elementClick(CalendarObjects.d2LmenuItemLinktextName("Edit Event"), "Edit Event");
	    
//	    elementClick(By.xpath("//*[@title='"+EventTitle+"']"),"clicking on Inverted triangle beside event");
	    //selectingOption(EventTitle, "Edit Event");
	    if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CalendarObjects.DescriptionText));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(CalendarObjects.DescriptionText));
			//switchToFrameByLocator(By.xpath(CalendarObjects.frameDescription), "Description");
			switchToFrameByIndex(0);
			explicitWait(androidDriver1,By.xpath(CalendarObjects.DescriptionText));
		} 
	    
	    //elementClick(By.xpath(CalendarObjects.DescriptionText),"Description Text");
	    elementSendText(By.xpath(CalendarObjects.DescriptionText), EventTitle+"Description modified please check", "Description");
	   // elementSendKeys(EventTitle+"Description modified please check");
	   // elementCloseKeyBoard();
	   switchToDefaultFrame();
	    if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CalendarObjects.SaveButton));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(CalendarObjects.SaveButton));
			explicitWait(androidDriver1,By.xpath(CalendarObjects.SaveButton));
		} 
	    
	    JSClick(By.xpath(CalendarObjects.SaveButton), "Save Button");
	    Thread.sleep(3000);
	}

	/***
	* Description: create an Assignment With Restrictions as start date and end date
	* @param AssignmentName
	* @param CatergoryName
	* @param GradeName
	* @param Score
	* @throws Throwable
	*/
	public void createAssignmentWithRestrictions(String assignmentName,String categoryName, String gradeName, String outOfBoxScore,String instructionsDetails) throws Throwable {

	/* selectMenuItem("Assignments");
	 elementWait(By.xpath(AssignmentsObjects.New_Submission_Folder), 60);
	 elementClick(By.xpath(AssignmentsObjects.New_Submission_Folder), "New Submission Folder");
	 elementWait(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), 60);
	 elementClick(By.xpath(AssignmentsObjects.New_Submission_Folder),"New Submission Folder");
	 elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment Name");
	 elementClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
	 String text1 = elementGetText(By.xpath("//*[@text='Name']"), "Category Name"); 
	 System.out.println(text1);
	 elementClick(By.xpath(AssignmentsObjects.Category_Name_TextBox),"Category Name TextBox");
	 elementSendKeys(categoryName);
	 elementCloseKeyBoard();
	 elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
	 elementClick(By.xpath(AssignmentsObjects.Category_Save_Button),"Category Save Button");
	 elementSwipe(SwipeElementDirection.DOWN, 300, 1000);
	 elementClick(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");
	 String text2 = elementGetText(By.xpath("//*[@text='Name']"), "Grade Name"); 
	 System.out.println(text2);
	 elementClick(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"Grade Name TextBox");
	 elementSendKeys(gradeName);
	 elementCloseKeyBoard();
	 elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
	 elementClick(By.xpath(AssignmentsObjects.Grade_Save_Button),"Grade Save Button");
	 elementSwipe(SwipeElementDirection.UP, 500, 1000);
     elementSendText(By.xpath(AssignmentsObjects.Out_Of_Box_Score),outOfBoxScore, "Out of Box Score");
	 elementSwipe(SwipeElementDirection.DOWN, 100, 1000);*/
		/*
		 elementClick(By.xpath(AssignmentsObjects.New_Assignment),"New_Assignment");
		 elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment Name");
		 scrollingToElementofAPage(By.xpath(AssignmentsObjects.New_Category_Link));
		 elementClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
		 Thread.sleep(2000);
		 switchToDefaultFrame();
		 switchToFrameByLocator(By.xpath("//iframe[@title='New Assignment Submission Category']"), "Frame New category");
		Thread.sleep(1500);
		 String text1 = elementGetText(By.xpath("//*[@text='Name']"), "Category Name"); 
		 System.out.println(text1);
		JSClick(By.xpath(AssignmentsObjects.Category_Name_TextBox),"Category Name TextBox");
		 elementSendKeys(categoryName);
		 			
		 elementSendText(By.xpath(AssignmentsObjects.Category_Name_TextBox), categoryName, "Category name");
		 elementCloseKeyBoard();
		 switchToDefaultFrame();
		 JSClick(By.xpath(AssignmentsObjects.Category_Save_Button),"Category Save Button");
		 Thread.sleep(2000);
		 JSClick(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");
		String text2 = elementGetText(By.xpath("//*[@text='Name']"), "Grade Name"); 
		 System.out.println(text2);
		 switchToFrameByIndex(0);
		 		
		 Thread.sleep(2000);
		 switchToFrameByLocator(By.xpath("//iframe[@title='New Grade Item']"), "New grade frame");
		  elementClick(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"Grade Name TextBox");
		 elementSendKeys(gradeName);
		 Thread.sleep(1500);
		 elementSendText(By.xpath(AssignmentsObjects.Grade_Name_TextBox), gradeName, "Grade");
		 elementCloseKeyBoard();
		 switchToDefaultFrame();
		 elementClick(By.xpath(AssignmentsObjects.Grade_Save_Button),"Grade Save Button");
		 Thread.sleep(3000);
         elementSendText(By.xpath(AssignmentsObjects.txtOut_Of_Box_Score),outOfBoxScore, "Out of Box Score");
         Thread.sleep(200);
         elementCloseKeyBoard();
	 
         //selectingOption("Restrictions", "Restrictions");
         elementClick(By.linkText("Restrictions"), "Restrictions tab");
         Thread.sleep(3000);
         elementClick(By.xpath(AssignmentsObjects.chkHiddenUsers), "checkbox Hidden from users");*/
		
		
		
        // elementClick(By.xpath("//*[@name='z_n' and @checked='checked' and @type='checkbox']"), "Hidden from users");
	/* if(elementExists(By.xpath(ELMAssignmentObjects.Created_Successfully_Close))){
         elementClick(By.xpath(ELMAssignmentObjects.Created_Successfully_Close), "Created successfully close");
     }*/
         
		
		waitForVisibilityOfElement(By.xpath(AssignmentsObjects.New_Assignment), "New Assignment");
		 elementClick(By.xpath(AssignmentsObjects.New_Assignment),"New Assignment");
		 Thread.sleep(2000);
		 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), "Assignment Textbox");
		 elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment Name");
		 //textSearchRubricDoubleShadowDOM(assignmentName, tagName);
		 
		 scrollingToElementofAPage(By.xpath(AssignmentsObjects.New_Category_Link));
		 JSClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
		 Thread.sleep(2000);
		 switchToDefaultFrame();
		 switchToFrameByLocator(By.xpath("//iframe[@title='New Assignment Submission Category']"), "Frame New category");
		 Thread.sleep(1500);
		/* String text1 = elementGetText(By.xpath("//*[@text='Name']"), "Category Name"); 
		 System.out.println(text1);
		JSClick(By.xpath(AssignmentsObjects.Category_Name_TextBox),"Category Name TextBox");
		 elementSendKeys(categoryName);
		 */	
		
		 elementSendText(By.xpath(AssignmentsObjects.Category_Name_TextBox), categoryName, "Category name");
		 
		 if(!(isElementPresent(By.xpath(AssignmentsObjects.Category_Name_TextBox),""))) {
			 switchToDefaultFrame();
			 scrollingToElementofAPage(By.xpath(AssignmentsObjects.New_Category_Link));
			 JSClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
			 Thread.sleep(2000);
			 switchToDefaultFrame();
			 switchToFrameByLocator(By.xpath("//iframe[@title='New Assignment Submission Category']"), "Frame New category");
			Thread.sleep(1500);
			waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Category_Name_TextBox), "Category name");
			 elementSendText(By.xpath(AssignmentsObjects.Category_Name_TextBox), categoryName, "Category name");
			}
		 
		 if(elementCloseKeyBoard()==false) {
			 elementCloseKeyBoard(); 
		 }
		 
		 switchToDefaultFrame();
		 JSClick(By.xpath(AssignmentsObjects.Category_Save_Button),"Category Save Button");
		 Thread.sleep(2000);
		 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");
		 JSClick(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");
		/*String text2 = elementGetText(By.xpath("//*[@text='Name']"), "Grade Name"); 
		 System.out.println(text2);
		 switchToFrameByIndex(0);
		 */		
		 Thread.sleep(2000);
		 switchToFrameByLocator(By.xpath("//iframe[@title='New Grade Item']"), "New grade frame");
		 /* elementClick(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"Grade Name TextBox");
		 elementSendKeys(gradeName);*/
		 Thread.sleep(1500);
		 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"gradeName");
		 elementSendText(By.xpath(AssignmentsObjects.Grade_Name_TextBox), gradeName, "Grade");
		// elementCloseKeyBoard();
		 switchToDefaultFrame();
		 JSClick(By.xpath(AssignmentsObjects.Grade_Save_Button),"Grade Save Button");
		 Thread.sleep(3000);
		 waitForVisibilityOfElement(By.xpath(AssignmentsObjects.txtOut_Of_Box_Score),"Out of Box Score");
        elementSendText(By.xpath(AssignmentsObjects.txtOut_Of_Box_Score),outOfBoxScore, "Out of Box Score");
        Thread.sleep(200);
        if(elementCloseKeyBoard()==false) {
			 elementCloseKeyBoard(); 
		 }
       // switchToFrameByLocator(By.xpath(AssignmentsObjects.Instructions_TextBox), "Instructor field description");
        Thread.sleep(2000);
       // elementClick(By.xpath(AssignmentsObjects.txtInstructionsTextBox), "Instruction details");
       // scrollingToElementofAPage(By.xpath(AssignmentsObjects.txtInstructionsTextBox));
       // elementSendText(By.xpath(AssignmentsObjects.txtInstructionsTextBox), instructionsDetails, "Instruction details");
        // elementClick(By.xpath(AssignmentsObjects.Instructions_TextBox),"Instructions TextBox");	 
        //elementSendKeys(instructionsDetails);
        if(elementCloseKeyBoard()==false) {
			 elementCloseKeyBoard(); 
		 }
		 switchToDefaultFrame();
		 Thread.sleep(2000);
		 scrollingToElementofAPage(By.xpath("//*[text()='Restrictions']"));
		 JSClick(By.xpath("//*[text()='Restrictions']"),"Restrictions tab");
		 // selectingOption("Restrictions", "Restrictions");
		// elementClick(By.xpath("//*[@name='z_n' and @checked='checked' and @type='checkbox']"), "Hidden from users");
		 Thread.sleep(3000);
		// elementClick(By.xpath(AssignmentsObjects.chkHiddenUsers), "checkbox Hidden from users");	 
		 /*if(elementExists(By.xpath(ELMAssignmentObjects.Created_Successfully_Close))){
            elementClick(By.xpath(ELMAssignmentObjects.Created_Successfully_Close), "Created successfully close");
        }*/
		
		
		
		
		
		
	waitForVisibilityOfElement(By.xpath(AssignmentsObjects.HasStartDate_checkbox),"HasStartDate_checkbox");
	 //elementWait(By.xpath(AssignmentsObjects.HasStartDate_checkbox), 100);
	scrollingToElementofAPage(By.xpath(AssignmentsObjects.HasStartDate_checkbox)); 
	JSClick(By.xpath(AssignmentsObjects.HasStartDate_checkbox), "Has Start Date Check Box");
	waitForVisibilityOfElement(By.xpath(AssignmentsObjects.Now_Button),"Now_Button");
	scrollingToElementofAPage(By.xpath(AssignmentsObjects.Now_Button));
	Thread.sleep(1000);
	elementClick(By.xpath(AssignmentsObjects.Now_Button), "Now Button");
	 scrollingToElementofAPage(By.xpath(AssignmentsObjects.HasEndDate_checkbox));
	 JSClick(By.xpath(AssignmentsObjects.HasEndDate_checkbox), "Has EndDate CheckBox");
	 scrollingToElementofAPage(By.xpath(AssignmentsObjects.Save_And_Close_Button));
	 elementClick(By.xpath(AssignmentsObjects.Save_And_Close_Button), "Save and Close button");
	 Thread.sleep(3000);
	}

	/***
	* Description: Learner submits an Assignment via Calendar
	* @param AssignmentName
	* @throws Throwable
	*/
	public void learnerSubmitsAssignmentViaCalendar(String AssignmentName1) throws Throwable {
	String AssignmentName="Assignment0704194533";
	/*if(!(isElementPresent(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"),""))){
		elementClick(By.xpath("//a[contains(@aria-label,'Expand Upcoming events')]"), "Upcoming Events");
		Thread.sleep(2000);
		waitForVisibilityOfElement(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"),AssignmentName);
	}*/
	waitForVisibilityOfElement(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"),AssignmentName);
	scrollingToElementofAPage(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"));	
	elementClick(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"), "Assignment Name");
	waitForVisibilityOfElement(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"),AssignmentName);
	scrollingToElementofAPage(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"));	
	elementClick(By.xpath("//*[text()='" + AssignmentName + "']"), "Assignment Name");
	Thread.sleep(1000);
	waitForVisibilityOfElement(By.xpath(AssignmentsObjects.AddFile_button), "Add File Button");
	scrollingToElementofAPage(By.xpath(AssignmentsObjects.AddFile_button));	
	elementClick(By.xpath(AssignmentsObjects.AddFile_button), "Add File Button");
	uploadingDocument("file");
	switchToDefaultFrame();
	elementClick(By.xpath(AssignmentsObjects.Add_Button), "Add Button");
	elementClick(By.xpath(AssignmentsObjects.Submit_button), "Submit Button");
	elementClick(By.xpath(AssignmentsObjects.Done_Button), "Done Button");
 }

	public void ifAssigmentInEventsviaCalendarNotFound(String AssignmentName) throws Throwable {
		if(!(isElementPresent(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"),""))){
			elementClick(By.xpath("//a[contains(@aria-label,'Expand Upcoming events')]"), "Upcoming Events");
			Thread.sleep(2000);
			waitForVisibilityOfElement(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"),AssignmentName);
			scrollingToElementofAPage(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"));
			isElementPresent(By.xpath("//*[text()='" + AssignmentName + " - Availability Ends']"),"AssignmentName is visible to Submit via calendar File upload");
			learnerSubmitsAssignmentViaCalendar(AssignmentName);
		}
	 }
	
	/***
	 * Method to selecting view from settings
	 * @param view
	 * @return
	 * @throws Throwable
	 */
	public boolean selectingViewFromSettings(String view) throws Throwable {
		boolean flag = true;
		try {
			 Thread.sleep(2000);
			if(elementExists(By.xpath(DiscussionsObjects.Three_Dot_Icon), "Three dot Icon")){
				waitForVisibilityOfElement(By.xpath(DiscussionsObjects.Three_Dot_Icon), "Three dot Icon");
				elementClick(By.xpath(DiscussionsObjects.Three_Dot_Icon), "Three dot Icon");
			 }
			waitForVisibilityOfElement(By.xpath(DiscussionsObjects.Three_Dot_Icon), "Three dot Icon");
			 elementClick(AssignmentsObjects.d2LmenuItemtextName("Settings"), "Settings");
         	 Thread.sleep(3000);
			// elementClick(By.xpath(DiscussionsObjects.Settings_Button), "Settings Button");
			 //10.8.9//selectingOption("Default View", view);
         	waitForVisibilityOfElement(By.xpath("//*[text()='"+view+"']//parent::div//input"), view);
         	 elementClick(By.xpath("//*[text()='"+view+"']//parent::div//input"), view);
         	waitForVisibilityOfElement(By.xpath(DiscussionsObjects.Save_Button), "Save Button");
			 elementClick(By.xpath(DiscussionsObjects.Save_Button), "Save Button");
			 Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to creating a forum
	 * @param name
	 * @param desc
	 * @return
	 * @throws Throwable
	 */
	public boolean creatingForum(String name, String desc) throws Throwable {
		boolean flag = true;
		try {
			waitForVisibilityOfElement(By.xpath(DiscussionsObjects.New_Button), "New Button"); 
			elementClick(By.xpath(DiscussionsObjects.New_Button), "New Button");
			 Thread.sleep(1500);
			 //elementClick(By.xpath(DiscussionsObjects.New_Forum), "New Forum");
			 elementClick(AssignmentsObjects.d2LmenuItemtextName("New Forum"), "New Forum");
         	 Thread.sleep(3000);
         	 waitForVisibilityOfElement(By.xpath(DiscussionsObjects.Forum_Title_TextBox), "Forum title");
         	 scrollingToElementofAPage(By.xpath(DiscussionsObjects.Forum_Title_TextBox));
			 elementSendText(By.xpath(DiscussionsObjects.Forum_Title_TextBox), name, "Forum title");
			 //androidDriver.switchTo().frame("forumDescription$html_ifr");
		 	 //elementClick(By.xpath(DiscussionsObjects.Description_TextBox), "Description Textbox");
			 //elementSendKeys(desc);
			 //elementCloseKeyBoard();
			 waitForVisibilityOfElement(By.xpath(DiscussionsObjects.Save_Add_Topic), "Save and Add Topic Button");
			 scrollingToElementofAPage(By.xpath(DiscussionsObjects.Save_Add_Topic));
			 elementClick(By.xpath(DiscussionsObjects.Save_Add_Topic), "Save and Add Topic Button");
			 Thread.sleep(5000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to editing forum
	 * @param name
	 * @param desc
	 * @return
	 * @throws Throwable
	 */
	public boolean editingForum(String name, String desc) throws Throwable {
		boolean flag = true;
		try {
			 elementClear(By.xpath(DiscussionsObjects.Forum_Title_TextBox));
			 elementSendText(By.xpath(DiscussionsObjects.Forum_Title_TextBox), name, "Forum title");
			 
			/* switchToFrameByLocator(By.xpath(DiscussionsObjects.Description_TextBox), "Description Textbox frame");
			 elementClick(By.xpath(DiscussionsObjects.txtDescription), "Description Textbox");
			//elementSendKeys(desc);
			 elementSendText(By.xpath(DiscussionsObjects.txtDescription),desc,"Description Textbox");
			 elementCloseKeyBoard();*/
			 elementClick(By.xpath(DiscussionsObjects.Save_Add_Close), "Save and Close Button");
			 Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to creating or editing topic
	 * @param name
	 * @param desc
	 * @return
	 * @throws Throwable
	 */
		boolean flag = true;
		public boolean creatingOrEditingTopic(String name, String desc) throws Throwable {
		try {
			
			ExplicitWaitOnElementToBeClickable(By.xpath(DiscussionsObjects.Topic_Title_Textbox));
			scrollingToElementofAPage(By.xpath(DiscussionsObjects.Topic_Title_Textbox));
			elementClear(By.xpath(DiscussionsObjects.Topic_Title_Textbox));
			elementSendText(By.xpath(DiscussionsObjects.Topic_Title_Textbox), name, "Topic title");
			
				Thread.sleep(3000);
				waitForVisibilityOfElement(By.cssSelector("#z_a"), "");
	         	scrollingToElementofAPage(By.cssSelector("#z_a"));
				 WebElement element = androidDriver1.findElement(By.cssSelector("#z_a"));
				((JavascriptExecutor) androidDriver1).executeScript("arguments[0].click();", element);
	//			 element.click();
		         Thread.sleep(2000);
			 
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to creating topic with Rate posts
	 * @param name
	 * @param desc
	 * @param rateScheme
	 * @return
	 * @throws Throwable
	 */
	public boolean creatingTopicWithRatePosts(String name, String desc, String rateScheme) throws Throwable {
		boolean flag = true;
		try {
			waitForVisibilityOfElement(By.xpath(DiscussionsObjects.Topic_Title_Textbox), "Topic title");  
			elementSendText(By.xpath(DiscussionsObjects.Topic_Title_Textbox), name, "Topic title");
			// switchToFrameByLocator(By.xpath(DiscussionsObjects.Topic_Description_TextBox), "Topic Description Textbox");
			// Thread.sleep(2000);
			 switchToFrameByIndex(0);
			 scrollingToElementofAPage(By.xpath(DiscussionsObjects.txtMsgArea));
			 elementClick(By.xpath(DiscussionsObjects.txtMsgArea), "Topic Description Textbox");
			 elementSendText(By.xpath(DiscussionsObjects.txtMsgArea), desc, "Description");
			// elementSendKeys(desc);
			 elementCloseKeyBoard();
			 switchToDefaultFrame();
			 Thread.sleep(2000);
			 //elementClick(By.xpath(DiscussionsObjects.Topic_Rate_Posts), "Topic Rate Posts");
			// elementSendKeys(rateScheme);
			 scrollingToElementofAPage(By.xpath(DiscussionsObjects.Topic_Rate_Posts));
			 selectByVisibleText(By.xpath(DiscussionsObjects.Topic_Rate_Posts),rateScheme, "Rating scheme");
			// elementCloseKeyBoard();
			// elementClick(By.xpath(DiscussionsObjects.Topic_Rate_Posts), "Topic Rate Posts");
			 
			 RubricShadowDOMClick();
			 
			/* scrollingToElementofAPage(By.xpath(DiscussionsObjects.Save_Add_Close));
			 JSMousehoverDoubleClick(By.xpath(DiscussionsObjects.Save_Add_Close), "Save and Close Button");*/
			 if(isElementPresentNegative(By.xpath(DiscussionsObjects.Save_Add_Close),"Save and Close Button")) {
				 scrollingToElementofAPage(By.xpath(DiscussionsObjects.Save_Add_Close));
				 JSMousehoverDoubleClick(By.xpath(DiscussionsObjects.Save_Add_Close), "Save and Close Button");
			 }
			 Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to delete a topic
	 * @param topicName
	 * @return
	 * @throws Throwable
	 */
	public boolean deletingTopic(String topicName) throws Throwable {
		boolean flag = true;
		try {		 
			 //elementClick(By.xpath("//*[@text='"+topicName+"']/../../..//*[@class='d2l-contextmenu-ph']"),"Inverted Arrow for Topic created or edited");
			Thread.sleep(2000); 
			waitForVisibilityOfElement((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)), topicName);
	    	scrollingToElementofAPage((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)));
	    	JSMousehoverDoubleClick(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName),"Inverted arrow for topic created");
			Thread.sleep(2000);
			waitForVisibilityOfElement((DiscussionsObjects.d2LmenuItemLinktextName("Delete")), "Delete");
			scrollingToElementofAPage((DiscussionsObjects.d2LmenuItemLinktextName("Delete")));
			JSClick(DiscussionsObjects.d2LmenuItemLinktextName("Delete"), "Delete");
			Thread.sleep(2000);
			explicitWait(androidDriver1,By.xpath(DiscussionsObjects.Yes_Button));
			JSClick(By.xpath(DiscussionsObjects.Yes_Button), "Confirmation Yes Button");
			Thread.sleep(1000);
			System.out.println("Clicked on YES for Topic Delete Confirmation ");
			explicitWait(androidDriver1, By.xpath(DiscussionsObjects.New_Button));
			waitForVisibilityOfElement(By.xpath(DiscussionsObjects.New_Button), "Delete");
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

		public boolean deletingTopic1(String topicName) throws Throwable {
			boolean flag = true;
			try {		 
				 elementClick(By.xpath("//a[text()='"+topicName+"']//following::a[@class='d2l-contextmenu-ph d2l-contextmenu-ph-anchor']"),"Inverted Arrow for Topic created");
				 selectingOptionToDelete("Topic", "Delete");
				 Thread.sleep(6000);
				 
				 if (!elementExists(By.xpath(DiscussionsObjects.Yes_Button), "Confirmation Box")) {
					 elementClick(By.xpath(DiscussionsObjects.Yes_Button), "Confirmation Yes Button");
					 System.out.println("Clicked on YES for Topic Delete Confirmation for first time only");
				 } else {
					 elementClick(By.xpath("//a[text()='"+topicName+"']//following::a[@class='d2l-contextmenu-ph d2l-contextmenu-ph-anchor']"),"Inverted Arrow for Topic created");
					 selectingOptionToDelete("Topic", "Delete");
					 elementClick(By.xpath(DiscussionsObjects.Yes_Button), "Confirmation Yes Button");
				     System.out.println("Clicked on YES for Topic Delete Confirmation ");
				 }
				 elementClick(By.xpath(DiscussionsObjects.Yes_Button), "Confirmation Yes Button");
			     System.out.println("Clicked on YES for Topic Delete Confirmation Directly ");
			}
				
			
			
			catch (Exception e) {
				flag = false;
				e.printStackTrace();
			}
			return flag;
		}
		
		
	/***
	 * Method to validating deleted topic or forum
	 * @param editedTopicName
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingDeletedTopicOrForum(String editedTopicName) throws Throwable {
		boolean flag = true;
		try {		 
			 /*for(int i = 0; i < 1; i++) {
				//elementHardSleep(50000);
				System.out.println("sleep done:" + i);
			*/
				
				//pageRefresh();
			//	System.out.println("refresh done" + i);
			// }
			 
			/* androidDriver1.swipe(584, 1450, 590, 806, 320);
			 androidDriver1.swipe(584, 1450, 590, 806, 320);
			 androidDriver1.swipe(584, 1450, 590, 806, 320);
			 androidDriver1.swipe(584, 1450, 590, 806, 320);
			 androidDriver1.swipe(584, 1450, 590, 806, 320);
			 */
			 
			// elementSwipe(SwipeElementDirection.DOWN, 500, 1000);
			Thread.sleep(4000);
			 if (!elementExists(By.xpath("//*[text()='"+editedTopicName+"']"), "Deleted Topic")) {
			  SuccessReport("Verify Topic or Forum Deleted", "Successfully verified Topic or Forum is deleted");
			
			 } else {
			   failureReport("Verify Topic Deleted", "Failed to verify Topic or Forum is not deleted");
			 }
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to restoring a forum or topic
	 * @param topicName
	 * @return
	 * @throws Throwable
	 */
	public boolean restoringForumOrTopic(String ForumName, String topicName) throws Throwable {
		boolean flag = true;
		try {		 
			selectingMoreOptions("Discussions", "More Actions");
			Thread.sleep(2000);
			selectingMoreOptionsasRestore("More Actions", "Restore");
			Thread.sleep(2000);
			 WebElement element1=androidDriver1.findElement(By.xpath("//*[text()='"+ForumName+" > "+topicName+"']/../..//*[text()='Restore']"));//(By.xpath("//div[contains(text(),'"+topicName+"')]/../..//*[text()='Restore']"));
			((JavascriptExecutor) androidDriver1).executeScript("arguments[0].click();", element1);
			Thread.sleep(2000);
			//elementClick(By.xpath("/div[contains(text(),'"+topicName+"')]/../..//*[text()='Restore']"), "Restore Button");
			 if(isElementDisplayed(By.xpath(DiscussionsObjects.Yes_Button), "Confirmation Yes Button")){
				Thread.sleep(1000);
				 elementClick(By.xpath(DiscussionsObjects.Yes_Button), "Confirmation Yes Button");
				 Thread.sleep(2000);
			 } else if(elementExists(By.xpath(DiscussionsObjects.Restore_Topic_Forum), "Restore Topic and Forum")){
				elementClick(By.xpath(DiscussionsObjects.btn_Restore_Topic_Forum), "Restore Topic and Forum");
				Thread.sleep(3000);
			 }
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}


	/***
	 * Method to deleting forum
	 * @param forumName
	 * @return
	 * @throws Throwable
	 */
	public boolean deletingForum(String forumName) throws Throwable {
		boolean flag = true;
		try {		 
			 //elementClick(By.xpath("//*[@text='"+forumName+"']/..//*[@class='d2l-contextmenu-ph']"),"Inverted Arrow for Forum created");
			 //selectingOption("Forum", "Delete");
			System.out.println("d");
			elementClick(DiscussionsObjects.Inverted_Triangle_Beside_Forum(forumName),"Inverted arrow for forum created");
			JSClick(DiscussionsObjects.d2LmenuItemLinktextName("Delete"), "Delete Forum");
			Thread.sleep(2000);
			elementClick(By.xpath(DiscussionsObjects.Yes_Button), "Confirmation Yes Button");
			
			//elementExists(By.xpath("//*[text()=''"+forumName+"' has been deleted']"));
			explicitWait(androidDriver1, By.xpath(DiscussionsObjects.New_Button));
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to creating a thread
	 * @param name
	 * @param desc
	 * @return
	 * @throws Throwable
	 */
	public boolean creatingThread(String name, String desc) throws Throwable {
		boolean flag = true;
		try {	
			
			switchToDefaultFrame();
				Thread.sleep(2000);
				switchToFrameByIndex(0);
				Thread.sleep(2000);
			// switchToFrameByLocator(By.xpath("//iframe[@title ='Main Content']"), "Main content iframe");
			 switchToFrameByLocator(By.xpath("//frame[@name='FRAME_right']"), "Thread");
			 Thread.sleep(2000);
			 switchToFrameByLocator(By.xpath("//frame[@title='Post List']"), "Second frame");
			 Thread.sleep(2000);
			 elementClick(By.xpath(DiscussionsObjects.Start_A_New_Thread), "Start a New Thread");
			// switchToDefaultFrame();	
			 Thread.sleep(8000);
			 windowHandles();
			 Thread.sleep(3000);
			 switchToFrameByLocator(By.xpath("//frame[@title='Body']"), "Body frame");
			 Thread.sleep(2000);
			 elementSendText(By.xpath(DiscussionsObjects.Thread_Title_Textbox), name, "Thread title");
			 switchToFrameByLocator(By.xpath("//iframe[@id='message$html_ifr']"), "Description frame");
			 elementSendText(By.xpath(DiscussionsObjects.txtMsgArea), desc, "Description text");
			 Thread.sleep(3000);
			 switchToDefaultFrame();
			 switchToFrameByLocator(By.xpath("//frame[@title='Footer']"), "Footer frame");
			 Thread.sleep(2000);
			 //elementClick(By.xpath(DiscussionsObjects.Thread_Description_TextBox), "Thread Description Textbox");
			// elementSendKeys(desc);
			// elementCloseKeyBoard();
			 scrollingToElementofAPage(By.xpath(DiscussionsObjects.Post_Button));
			 elementClick(By.xpath(DiscussionsObjects.Post_Button), "Post Button");
			 Thread.sleep(4000);
			 backToMainWindow();
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating created Topic or Forum
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingCreatedTopicOrForum(String name) throws Throwable {
		boolean flag = true;
		try {	
			Thread.sleep(3000);
			waitForVisibilityOfElement(By.xpath("//*[text()='"+name+"']"), "");
			scrollingToElementofAPage(By.xpath("//*[text()='"+name+"']"));
			Thread.sleep(4000);
			elementExists(By.xpath("//*[text()='"+name+"']"),"Created Topic");
			 
			 
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to click on created topic
	 * @param topicName
	 * @return
	 * @throws Throwable
	 */
	public boolean clickOnCreatedTopic(String topicName) throws Throwable {
		boolean flag = true;
		try {
			waitForVisibilityOfElement(By.xpath("//*[text()='"+topicName+"']"),""); 
			scrollingToElementofAPage(By.xpath("//*[text()='"+topicName+"']"));
			 JSClick(By.xpath("//*[text()='"+topicName+"']"), "Created Topic");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating created thread
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingCreatedThread(String threadName) throws Throwable {
		boolean flag = true;
		try {
		//	10.8.9	//elementExists(By.xpath(DiscussionsObjects.Thread_Created_SuccessFully),"Thread Created Successfully");
			switchToDefaultFrame();
			Thread.sleep(2000);
			switchToFrameByIndex(0);
			switchToFrameByLocator(By.xpath("//frame[@name='FRAME_right']"), "Created Thread frame");
			Thread.sleep(3000);
			switchToFrameByLocator(By.xpath("//frame[@name='FRAME_list']"),"Thread frame" );
			Thread.sleep(3000);
			elementExists(By.xpath("//a[text()='"+threadName+"']"));
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to reply a thread
	 * @param name
	 * @param desc
	 * @return
	 * @throws Throwable
	 */
	public boolean replyToThread(String name, String desc) throws Throwable {
		boolean flag = true;
		try {	
			 scrollingToElementofAPage(By.xpath("//a[text()='"+name+"']"));
			 elementClick(By.xpath("//a[text()='"+name+"']"), "Thread name");
			 Thread.sleep(2000);
			 switchToDefaultFrame();
			 switchToFrameByIndex(0);
			 switchToFrameByLocator(By.xpath("//frame[@name='FRAME_right']"), "right frame");
			 Thread.sleep(2000);
			 switchToFrameByLocator(By.xpath("//frame[@name='FRAME_item']"), "Reply frame");
			 elementClick(By.xpath(DiscussionsObjects.Reply_To_Thread), "Reply to Thread");
			 Thread.sleep(4000);
			 switchToFrameByLocator(By.xpath(DiscussionsObjects.Reply_Description_Box), "Description");
			 elementSendText(By.xpath("//body[@id='tinymce']"), desc, "Description box");
			 Thread.sleep(2000);
			 //elementClick(By.xpath(DiscussionsObjects.Reply_Description_Box), "Reply to Thread description box");
			// elementSendKeys(desc);
			 elementCloseKeyBoard();
			 switchToDefaultFrame();
			 switchToFrameByIndex(0);
			 switchToFrameByLocator(By.xpath("//frame[@name='FRAME_right']"), "right frame");
			 Thread.sleep(2000);
			 switchToFrameByLocator(By.xpath("//frame[@name='FRAME_item']"), "Reply frame");
			 Thread.sleep(2000);
			 scrollingToElementofAPage(By.xpath(DiscussionsObjects.Reply_Post_Button));
			 elementClick(By.xpath(DiscussionsObjects.Reply_Post_Button), "Reply post button");
			 //elementExists(By.xpath(DiscussionsObjects.Replied_SuccessFully),"Replied Successfully");
			 Thread.sleep(8000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to rating a thread by PI
	 * @param name
	 * @param rating
	 * @return
	 * @throws Throwable
	 */
	public boolean ratingToThreadByPI(String name, String rating) throws Throwable {
		boolean flag = true;
		try {	
			/* elementClick(By.xpath("//*[text()='"+name+"']"), "Created Thread");
			 elementClick(By.xpath("//*[text()='"+rating+"']"), "Rating Thread");*/
			 scrollingToElementofAPage(By.xpath("//a[text()='"+name+"']"));
			 elementClick(By.xpath("//a[text()='"+name+"']"), "Thread name");
			 Thread.sleep(4000);
			 switchToDefaultFrame();
			 switchToFrameByIndex(0);
			 switchToFrameByLocator(By.xpath("//frame[@name='FRAME_right']"), "right frame");
			 Thread.sleep(2000);
			 switchToFrameByLocator(By.xpath("//frame[@title='Post List']"), "Second frame");
			 Thread.sleep(3000);
			 elementClick(By.xpath("(//*[text()='"+name+"'])[1]"), "Thread name");
			 Thread.sleep(3000);
			 switchToDefaultFrame();
			 switchToFrameByIndex(0);
			 switchToFrameByLocator(By.xpath("//frame[@name='FRAME_right']"), "right frame");
			 Thread.sleep(2000);
			 switchToFrameByLocator(By.xpath("//frame[@name='FRAME_item']"), "Reply frame");
			 elementClick(By.xpath("//*[@title='Rate "+rating+" out of 5']"), "Rating");
			 Thread.sleep(3000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to validating Success alert
	 * @param message
	 * @throws Throwable
	 */
	public void verifySuccessAlert(String message) throws Throwable{
		explicitWait(androidDriver1, By.xpath("//*[text()='"+message+"']"));
		if(elementExists(By.xpath("//*[text()='"+message+"']"))){
			SuccessReport(""+message+"", ""+message+" is displayed.");				
		} else {
			failureReport(""+message+"", ""+message+" Failed to display");
		}
	}

	/***
	 * Method to send an email through alert icon
	 * @param mailId
	 * @param subject
	 * @param body
	 * @throws Throwable
	 */
	public void sendEmailThroughMessageAlertIcon(String mailId, boolean subject ,boolean body) throws Throwable{
		elementClick(By.xpath(CommonObjects.Message_Alerts), "Message Alert Icon");
		
		waitForVisibilityOfElement(By.xpath(AssignmentsObjects.emailIconInMessageAlerts),"Email Icon in Message Alert at Top of page");
        scrollingToElementofAPage(By.xpath(AssignmentsObjects.emailIconInMessageAlerts));
		Thread.sleep(3000);
		JSMousehoverDoubleClick(By.xpath("(//d2l-button-subtle[@text='Email'])[1]"), "email icon");
		//selectingOption("Email", "Email");
		Thread.sleep(4000);
		waitForVisibilityOfElement(By.xpath(EmailObjects.iframeOfContent), "Frame Of Content");
		switchToFrameByLocator(By.xpath(EmailObjects.iframeOfContent), "Frame Of Content");
		switchToFrameByLocator(By.xpath(EmailObjects.frameOfEmailDetails), "Frame of Email Details");
		switchToFrameByLocator(By.xpath(EmailObjects.frameOfComposeMail), "Frame of Compose Mail");
	//	elementContext("NATIVE_APP_NON_INSTRUMENTED");
		scrollingToElementofAPage(By.xpath(EmailObjects.MessageAlertBox_Email_To));
		elementSendText(By.xpath(EmailObjects.MessageAlertBox_Email_To), mailId, "Mail Id");
		Thread.sleep(3000);
		//elementCloseKeyBoard();
		if(subject == true){
			elementClear(By.xpath(EmailObjects.MessageAlertBox_Email_Subject));
			scrollingToElementofAPage(By.xpath(EmailObjects.MessageAlertBox_Email_Subject));
			elementSendText(By.xpath(EmailObjects.MessageAlertBox_Email_Subject), "subject for email", "subject for email");
			elementCloseKeyBoard();
		}
		Thread.sleep(1000);
		switchToFrameByLocator(By.xpath("//iframe[@title='Body']"), "Body frame");
		scrollingToElementofAPage(By.xpath(EmailObjects.MessageAlertBox_Email_Body));
		elementClear(By.xpath(EmailObjects.MessageAlertBox_Email_Body));
		elementSendText(By.xpath(EmailObjects.MessageAlertBox_Email_Body), "Body for email", "Body for email");
		//	elementCloseKeyBoard();
		//elementSwipe(SwipeElementDirection.UP, 700, 20000);
		Thread.sleep(2000);
		switchToDefaultFrame();
		switchToFrameByLocator(By.xpath(EmailObjects.iframeOfContent), "Frame Of Content");
		switchToFrameByLocator(By.xpath(EmailObjects.frameOfEmailDetails), "Frame of Email Details");
		switchToFrameByLocator(By.xpath(EmailObjects.frameOfComposeMail), "Frame of Compose Mail");
		scrollingToElementofAPage(By.xpath(EmailObjects.MessageAlertBox_Email_Send));
		elementClick(By.xpath(EmailObjects.MessageAlertBox_Email_Send), "Message Alert Box");
		Thread.sleep(4000);
		//elementContext("WEBVIEW_1");
		if(elementExists(By.xpath(EmailObjects.Email_Confirmation_Yes))){
			elementClick(By.xpath("//*[text()='Yes']"), "Yes button");
			//selectingOption("Yes", "Yes");
		}
		ExplicitWaitOnElementToBeClickable(By.xpath(EmailObjects.MessageAlertBox_Email_Send));
		Thread.sleep(3000);
		//verifySuccessAlert("Sent successfully");
	}

	/***
	 * Method to send message through class list instant message
	 * @param userToSendMail
	 * @param message
	 * @throws Throwable
	 */
	public void sendmessageThroughClassListInstantMessage(String userToSendMail, String message) throws Throwable{
		selectMenuItem("Classlist");
		/*String ClassList_CheckBox = EmailObjects.ReplaceString(EmailObjects.ClassList_CheckBox, "oldString", userToSendMail);		
		elementClick(By.xpath(ClassList_CheckBox), "Check Box");*/
		String xpath = "//input[@title='Select "+userToSendMail+"']";
		waitForVisibilityOfElement(By.xpath(xpath),"");
		scrollingToElementofAPage(By.xpath(xpath));
		JSClick(By.xpath("//input[@title='Select "+userToSendMail+"']"), "User");
		if(!(isCheckedNegative(By.xpath("//input[@title='Select "+userToSendMail+"']"), "User"))) {
			scrollingToElementofAPage(By.xpath(xpath));
			JSClick(By.xpath("//input[@title='Select "+userToSendMail+"']"), "User");
			
		}
		Thread.sleep(2000);
		waitForVisibilityOfElement(By.xpath(EmailObjects.lnkInstantMsg), "Instant Message");
		scrollingToElementofAPage((By.xpath(EmailObjects.lnkInstantMsg)));
		JSClick(By.xpath(EmailObjects.lnkInstantMsg), "Instant Message");
		windowHandles();
		Thread.sleep(4000);
		waitForVisibilityOfElement(By.xpath(EmailObjects.txtTextArea),"");
		scrollingToElementofAPage(By.xpath(EmailObjects.txtTextArea));
		elementSendText(By.xpath(EmailObjects.txtTextArea), message, "Message Body");
		elementClick(By.xpath(EmailObjects.MessageAlertBox_Email_Send), "Send button");
		Thread.sleep(1000);
		verifySuccessAlert("Sent successfully");
		Thread.sleep(5000);
		windowHandles();
		/*click(ClassList.lnkInstantMessage, "Instant Message");
		//selectingOption("Instant Message", "Instant Message");
		elementSendText(By.xpath(EmailObjects.Message_Dialog_Box), message, "message dialog box");
		selectingOption("Send", "Send");*/
		
	}

	/***
	 * Method to send email through class list
	 * @param userToSendMail
	 * @param emailSubject
	 * @param emailMessage
	 * @throws Throwable
	 */
	public void sendEmailThroughClassList(String userToSendMail, String emailSubject, String emailMessage) throws Throwable{
		
		selectMenuItem("Classlist");
	/*	String ClassList_CheckBox = EmailObjects.ReplaceString(EmailObjects.ClassList_CheckBox, "oldString", userToSendMail);
		System.out.println("ClassList_CheckBox :"+ClassList_CheckBox);
		elementClick(By.xpath(ClassList_CheckBox), "Check Box");*/
		//selectingOption("Email", "Email");
		String xpath = "//input[@title='Select "+userToSendMail+"']";
		scrollingToElementofAPage(By.xpath(xpath));
		JSClick(By.xpath("//input[@title='Select "+userToSendMail+"']"), "User");
		Thread.sleep(2000);
		scrollingToElementofAPage(By.xpath(EmailObjects.lnkEmail));
		JSMousehoverDoubleClick(By.xpath(EmailObjects.lnkEmail), "Email");
		windowHandles();
		Thread.sleep(4000);
		elementExists(By.xpath(EmailObjects.Auto_Filled_BCC), "Auto filled BCC");
		scrollingToElementofAPage(By.xpath(EmailObjects.MessageAlertBox_Email_Subject));
		elementSendText(By.xpath(EmailObjects.MessageAlertBox_Email_Subject), emailSubject, "Subject");
		Thread.sleep(2000);
		//elementCloseKeyBoard();
		switchToFrameByLocator(By.xpath("//iframe[@id='BodyHtml$html_ifr']"), "Body frame");
		//elementSwipeWhileNotFound(By.xpath(EmailObjects.ClassList_Email_Body), 700, SwipeElementDirection.DOWN, 10000, 1000, 5, true);
		elementSendText(By.xpath(EmailObjects.MessageAlertBox_Email_Body), emailMessage, "Instructions");
		//elementSendKeys(emailMessage);
		//elementCloseKeyBoard();
		//elementSwipe(SwipeElementDirection.UP, 700, 20000);
		switchToDefaultFrame();
		scrollingToElementofAPage(By.xpath(EmailObjects.MessageAlertBox_Email_Send));
		//selectingOption("Send", "Send");
		JSMousehoverDoubleClick(By.xpath(EmailObjects.MessageAlertBox_Email_Send), "Message Alert Box");
		/*if(elementExists(By.xpath(EmailObjects.Email_Confirmation_Yes))){
			elementClick(By.xpath("//*[text()='Yes'"), "Yes button");
			//electingOption("Yes", "Yes");
		}*/
		Thread.sleep(3000);
		backToMainWindow();
		ExplicitWaitOnElementToBeClickable(By.xpath(EmailObjects.lnkEmail));
		//verifySuccessAlert("Sent successfully");
		
		Thread.sleep(4000);
		
	}

	/***
	 * Method to create group
	 * @param groupName
	 * @param descriptionText
	 * @param numberOfGroups
	 * @throws Throwable
	 */
	public void createGroup(String groupName,String descriptionText,String numberOfGroups)throws Throwable {
		/*selectMenuItem("My Tools");
		selectingOption("Groups", "Groups");*/
		selectingMyTools("Groups");
		Thread.sleep(4000);
		elementClick(By.xpath(GroupsObjects.NewCategory),"New Category Button");
		Thread.sleep(2000);
		elementSendText(By.xpath(GroupsObjects.CategoryName),groupName,"Category Name");
		switchToFrameByLocator(By.xpath("//iframe[@id='description$html_ifr']"), "Description frame");
		elementClick(By.xpath(GroupsObjects.DescriptionText),"Description Click");
		elementSendText(By.xpath(GroupsObjects.DescriptionText), descriptionText, "Description text");
		//elementSendKeys(descriptionText);
        elementCloseKeyBoard();
        switchToDefaultFrame();
        scrollingToElementofAPage(By.xpath(GroupsObjects.EnrollmentType));
        selectByVisibleText(By.xpath(GroupsObjects.EnrollmentType),"Groups of # - Self Enrollment", "Group dropdown Enrollment");
		//elementClick(By.xpath(GroupsObjects.EnrollmentType),"Enrollment Type");
		//elementContext("NATIVE_APP");
		//elementClick(By.xpath(GroupsObjects.GroupsOfSelfEnrollment),"GroupsOfSelfEnrollment Option");
		//elementContext("WEBVIEW_1");
		elementSendText(By.xpath(GroupsObjects.NumberOfGroups),numberOfGroups,"NumberOfGroups");
		elementCloseKeyBoard();
		//selectingOption("Save", "Save");
		elementClick(By.xpath(GroupsObjects.btnSave), "Save button");
		verifySuccessAlert("Created successfully");			
	}

	/***
	 * Method to enroll an user to group
	 * @param groupName
	 * @param userToEnroll
	 * @throws Throwable
	 */
	public void enrollAUserToGroup(String groupName , String userToEnroll) throws Throwable {
		//elementClick(By.xpath(GroupsObjects.ViewCategoryDropDown), "View category button");	
		//elementContext("NATIVE_APP");
		//scrollingToElementofAPage(By.xpath("//*[@title='Actions for "+groupName+"']"));
		scrollingToElementofAPage(GroupsObjects.Inverted_Triangle_Beside_Topics(groupName));
		
		Thread.sleep(1500);
		elementClick(GroupsObjects.Inverted_Triangle_Beside_Topics(groupName), "Inverted traingle besdie group");//10.8.9
		Thread.sleep(1500);
		//JSClick(By.xpath("//span[text()='Enroll Users']"), "Enroll users option");
		elementClick(GroupsObjects.d2LmenuItemLinktextName("Enroll Users"), "Enroll users option");
		Thread.sleep(2000);
		ExplicitWaitOnElementToBeClickable(By.xpath("//input[@id='z_r']"));
		scrollingToElementofAPage(By.xpath("//input[@id='z_r']"));
		Thread.sleep(3000);
		//JSClick(By.xpath("//input[@id='z_r']"), "Check box beside user");
		if(!(isCheckedNegative(By.xpath("//input[@id='z_r']"), "User Checkbox")));{
			elementClick(By.xpath("//input[@id='z_r']"), "UserToEnroll");
		}
		
		elementClick(By.xpath(GroupsObjects.btnSave),"Save button");
		Thread.sleep(3000);
		verifySuccessAlert("Saved successfully");
		
		//	elementClick(By.xpath(GroupsObjects.SelectCategoryWithGroup(groupName)), "Group Name");
		//elementContext("WEBVIEW_1");
		/*JSClick(By.xpath(GroupsObjects.selectInvertedTriangleBesideGroup(groupName)), "Inverted Triangle beside group");
		selectingOption("Enroll Users", "Enroll Users");
		elementClick(By.xpath(GroupsObjects.selectUserCheckBox(userToEnroll)), "UserToEnroll");
		selectingOption("Save","Save")*/;
		//verifySuccessAlert("Saved successfully");
	}

	/***
	 * Method to validating user is able to access group
	 * @param groupName
	 * @throws Throwable
	 */
	public void verifyUserAbleToAccessGroup(String groupName) throws Throwable {
		elementExists(By.xpath("//*[text()='"+groupName+"']"), groupName);
	}

	/***
	 * Method for subscription settings
	 * @return
	 * @throws Throwable
	 */
	public boolean subscriptionSettings() throws Throwable {
		boolean flag = true;
		try {	
			if(elementExists(By.xpath(DiscussionsObjects.Three_Dot_Icon), "Three dot Icon")){
				elementClick(By.xpath(DiscussionsObjects.Three_Dot_Icon), "Three dot Icon");
				Thread.sleep(2000);
				//selectingOption("Settings", "Settings");
				 elementClick(AssignmentsObjects.d2LmenuItemtextName("Settings"), "Settings");
	         	 Thread.sleep(3000);
			 } 
			
			 if(elementExists(By.xpath(DiscussionsObjects.Reding_View_Radio_Uncheck), "Reading view radio button")){
				elementClick(By.xpath(DiscussionsObjects.Reding_View_Radio_Uncheck), "Reading view radio button");
			 }
			 
/*
			 if(elementExists(By.xpath(DiscussionsObjects.Subscription_Settings_Uncheck), "Subscription Settings")){
				elementClick(By.xpath(DiscussionsObjects.Subscription_Settings_Uncheck), "Subscription Settings");
			 }*/
			 elementClick(By.xpath(DiscussionsObjects.Save_Button), "Save");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}	

	/***
	 * Method to subscription to newly created topic
	 * @param topicName
	 * @return
	 * @throws Throwable
	 */
	public boolean subscriptionToNewlyCreatedTopic(String topicName) throws Throwable {
		boolean flag = true;
		try {	
			//String Inverted_Triangle_Beside_Topic = DiscussionsObjects.ReplaceString(DiscussionsObjects.Inverted_Triangle_Beside_Topic, "oldString", topicName);
			//		elementSwipeWhileNotFound(DiscussionsObjects.Inverted_Triangle_Beside_Topic(topicName), 100, SwipeElementDirection.DOWN, 20000, 1000, 30, true);
			waitForVisibilityOfElement((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)), "topicName");
			scrollingToElementofAPage(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName));
			elementExists(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName), topicName);
			elementClick((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)), "Inverted triangle beside topic");
			//should write for swipe to element
			
			//elementClick((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)), "Inverted triangle beside topic");
			
			
			scrollingToElementofAPage(AssignmentsObjects.d2LmenuItemtextName("Subscribe"));
			Thread.sleep(1000);
			JSClick(AssignmentsObjects.d2LmenuItemtextName("Subscribe"), "Subscribe");
			Thread.sleep(4000);
			/*scrollingToElementofAPage(By.xpath("//*[@aria-label='Actions for "+topicName+"']//span[text()='Subscribe']"));
			JSClick(By.xpath("//*[@aria-label='Actions for "+topicName+"']//span[text()='Subscribe']"), "Subscribe");*/
			switchToFrameByIndex(0);
			//selectingOption("Subscribe", "Subscribe");
			//selectingOption("Subscribe", "Subscribe");
			waitForVisibilityOfElement(By.xpath(DiscussionsObjects.radioButtonForSubscribe), "radio button");
			scrollingToElementofAPage(By.xpath(DiscussionsObjects.radioButtonForSubscribe));
			elementClick(By.xpath(DiscussionsObjects.radioButtonForSubscribe),"Radio Button for Notification");
			/*switchToDefaultFrame();*/
			Thread.sleep(3000);
			JSClick(By.xpath(DiscussionsObjects.btnSubscribe),"Subscribe button");
			switchToDefaultFrame();
			elementExists(By.xpath(DiscussionsObjects.topicSubscribedSucessfully), "Topic Subscribed Successfully");
			//System.out.println("Test");
			Thread.sleep(3000);
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}	
	
	public boolean unSubscribeTopic(String topicName) throws Throwable {
		boolean flag = true;
		try {
			//elementClick(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName), "Inverted triangle beside topic");
			//Thread.sleep(2000);
		//	elementClick(By.xpath(DiscussionsObjects.unSubscribeFromInvertedTriangle),"Unsubscribe");
			//scrollingToElementofAPage(By.xpath("//*[@aria-label='Actions for "+topicName+"']//span[text()='Unsubscribe']"));
			//JSClick(By.xpath("//*[@aria-label='Actions for "+topicName+"']//span[text()='Unsubscribe']"), "Un subscribe");
			Thread.sleep(2000);
			waitForVisibilityOfElement((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)), "topicName");
			scrollingToElementofAPage(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName));
			elementExists(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName), topicName);
			Thread.sleep(2000);
			JSMousehoverDoubleClick((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)), "Inverted triangle beside topic");
			Thread.sleep(2000);
			//should write for swipe to element
			
			//elementClick((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)), "Inverted triangle beside topic");
			
			//waitForVisibilityOfElement((AssignmentsObjects.d2LmenuItemtextName("Unsubscribe")), "Unsubscribe");
			scrollingToElementofAPage(AssignmentsObjects.d2LmenuItemtextName("Unsubscribe"));
			JSClick(AssignmentsObjects.d2LmenuItemtextName("Unsubscribe"), "Unsubscribe");
			Thread.sleep(3000);
			elementClick(By.xpath(DiscussionsObjects.Yes_Button),"Yes Button");
			elementExists(By.xpath(DiscussionsObjects.topicUnSubscribedSucessfully), "Topic Unsubscribed Successfully");
			Thread.sleep(2000);
				
			
		}
		catch(Exception e) {
			flag = false;
			e.printStackTrace();
		}
		
		
		return flag;
	}

	public void verifyMySurveysExist(String surveyName) throws Throwable{
		
		if(elementExists(By.xpath(SurveyObjects.MySurvey))){
			SuccessReport("My Surveys", "My Surveys displayed");                        
		} else {
			failureReport("My Surveys", "My Surveys not displayed");
		}
	}

	public boolean verifyMySurveysDoesNotExist(String SurveyName) throws Throwable{
		 boolean flag = true;
         try {
       	     pageRefresh();
       	     flag=isElementDisplayed(By.xpath("//*[text()='"+SurveyName+"']"), "Survey Name");
                if(flag==true){
               	 failureReport("Access survey", "Student able to access survey that has expired"); 
                } else {
               	 SuccessReport("Survey Expired", "Student is not able to access survey that has expired");
                }
         } catch (Exception e) {
                flag = false;
              //  e.printStackTrace();
         }
         return flag;
		/*if(!elementExists(By.xpath("//*[text() ='My Surveys']"))){
			SuccessReport("My Surveys", "My Surveys not displayed");                           
		} else {
			failureReport("My Surveys", "My Surveys displayed");
		}*/
	}

	/***
	 * Method to entering quiz details
	 * @param quizName
	 * @param categoryName
	 * @return
	 * @throws Throwable
	 */
	public boolean enteringQuizDetails(String quizName, String categoryName) throws Throwable {
		boolean flag = true;
		try {	
			 elementClick(By.xpath(QuizzesAndExams.New_Quiz), "New Quiz Button");
			 elementSendText(By.xpath(QuizzesAndExams.Quiz_Name), quizName, "Quiz Name");
			 elementClick(By.xpath(QuizzesAndExams.Add_Category), "Add Category Link");	
			 Thread.sleep(2000);
			 switchToFrameByIndex(0);
			 elementClick(By.xpath(QuizzesAndExams.Add_Category_Name), "Category Name textbox");
			 //elementSendKeys(categoryName);
			 elementSendText(By.xpath(QuizzesAndExams.Add_Category_Name),categoryName,"Category Name");
			 elementCloseKeyBoard();
			 switchToDefaultFrame();
			 elementClick(By.xpath(QuizzesAndExams.Category_Save_Button), "Category Save Button");
			 elementClick(By.xpath(QuizzesAndExams.Add_Or_Edit_Questions), "Add/Edit Questions");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}	
	
	/***
	 * Method to editing quiz details
	 * @param originalName
	 * @param quizName
	 * @param categoryName
	 * @return
	 * @throws Throwable
	 */
	public boolean editingQuizDetails(String originalName,String quizName, String categoryName) throws Throwable {
		boolean flag = true;
		try {	
			 elementClick(By.xpath("//*[@text='"+quizName+"']"), "Quiz Name");
			 elementSendText(By.xpath(QuizzesAndExams.Quiz_Name), quizName, "Quiz Name");			
			 elementClick(By.xpath(QuizzesAndExams.Add_Category), "Add Category Link");			
			 elementClick(By.xpath(QuizzesAndExams.Add_Category_Name), "Category Name textbox");
			 elementSendKeys(categoryName);
			 elementCloseKeyBoard();
			 elementClick(By.xpath(QuizzesAndExams.Category_Save_Button), "Category Save Button");
			 elementClick(By.xpath(QuizzesAndExams.Editing_Add_Or_Edit_Questions), "Add/Edit Questions");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}	

	/***
	 * Method to importing quiz questions
	 * @return
	 * @throws Throwable
	 */
	public boolean importingQuizQuestions() throws Throwable {
		boolean flag = true;
		try {	
			 elementContext("NATIVE_APP_NON_INSTRUMENTED");
			 elementClick(By.xpath(QuizzesAndExams.Import_Button), "Import Button");
			 elementClick(By.xpath(QuizzesAndExams.Import_Source), "Import Source");
			 elementSendKeys("From a D2L Text Format File");
			 elementClick(By.xpath(QuizzesAndExams.Choose_File), "Choose File");
			 elementContext("NATIVE_APP");
			 elementClick(By.xpath(AssignmentsObjects.Documents_Option), "Documents option");
			 elementClick(By.xpath(QuizzesAndExams.CSV_File), "CSV File");
			 elementContext("NATIVE_APP_NON_INSTRUMENTED");
			 elementClick(By.xpath(QuizzesAndExams.Quiz_Save_Button), "Quiz Save Button");
			 elementClick(By.xpath(QuizzesAndExams.Done_Editing_Questions), "Done Editing Questions");
			 elementContext("WEBVIEW_1");
			 elementClick(By.xpath(QuizzesAndExams.Save_And_Close), "Save and Close");
			 elementExists(By.xpath(QuizzesAndExams.Saved_Successfully), "Saved Successfully");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}	
	
	/***
	 *  Method to importing quiz from collections
	 * @return
	 * @throws Throwable
	 */
	public boolean importingQuizFromCollection() throws Throwable {
		boolean flag = true;
		try {	
			 elementContext("NATIVE_APP_NON_INSTRUMENTED");
			 elementClick(By.xpath(QuizzesAndExams.Import_Button), "Import Button");
			 elementClick(By.xpath(QuizzesAndExams.Import_Source_Collection), "Import Source");
			 elementSendKeys("Question Library");
			 elementClick(By.xpath(QuizzesAndExams.Choose_Section_To_Import), "Choose Section To Import");
			 elementSendKeys("Collection Root");
			 elementClick(By.xpath(QuizzesAndExams.Save_Button), "Save Button");
			 elementClick(By.xpath(QuizzesAndExams.Done_Editing_Questions), "Done Editing Questions");
			 elementContext("WEBVIEW_1");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}


	/***
	 * Method to validating imported quiz
	 * @param name
	 * @return
	 * @throws Throwable
	 */
	public boolean validatingImportedQuiz(String name) throws Throwable {
		boolean flag = true;
		try {	
			 //elementSwipeWhileNotFound(By.xpath("//*[@text='"+name+"']"), 100, SwipeElementDirection.DOWN, 20000, 1000, 30, false);
			 elementExists(By.xpath("//*[@text='"+name+"']"),"Imported Quiz");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to submitting quiz
	 * @param quizName
	 * @param attempt
	 * @return
	 * @throws Throwable
	 */
	public boolean submittingQuiz(String quizName,String attempt) throws Throwable {
		boolean flag = true;
		try{	
			elementClick(By.xpath("//*[@text='"+quizName+"']"), "Quiz Name");
			//elementClick(By.xpath("//*[@text='"+quizName+"']"), "Quiz Name");
			elementClick(By.xpath(QuizzesAndExams.Start_Quiz_Button), "Start Quiz Button");
			elementClick(By.xpath(QuizzesAndExams.Ok_Button), "Ok Button");
			elementContext("NATIVE_APP_NON_INSTRUMENTED");
			elementClick(By.xpath(QuizzesAndExams.Answer_Description_1), "Answer to Question 1");
			elementClick(By.xpath(QuizzesAndExams.Go_To_Submit_Quiz), "Go To Submit Quiz");
			elementClick(By.xpath(QuizzesAndExams.Submit_Quiz), "Submit Quiz");
			elementContext("WEBVIEW_1");
			elementClick(By.xpath(QuizzesAndExams.Yes_Submit_Quiz), "Yes Submit Quiz");
			elementClick(By.xpath(QuizzesAndExams.Quiz_Done_Button), "Quiz Done Button");
			selectMenuItem("My Tools");
			selectingOption("My Tools", "Quizzes");
			String s1 = elementGetText(By.xpath("//*[@nodeName='TD' and ./*[@text='1'] and ./preceding-sibling::*[./*[@text='"+quizName+"']]]"), "Attempt Text"); 
			if(s1.equals(attempt)){
				SuccessReport("Submit the Quiz", "Successfully Learner is able to access, take and Submitted the Quiz");
			} else {
				failureReport("Submit the Quiz", "Failed to Learner is able to access, take and Submit the Quiz");
			}
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}


	/***
	 * Method to adding quiz questions
	 * @param menuItem
	 * @param questionTitle
	 * @param questionText
	 * @param trueWeight
	 * @param trueFeedBack
	 * @param falseWeight
	 * @param falseFeedBack
	 * @return
	 * @throws Throwable
	 */
	public boolean addingQuizQuestions(String menuItem,String questionTitle,String questionText,String trueWeight,String trueFeedBack,String falseWeight, String falseFeedBack) throws Throwable {
		boolean flag = true;
		try {	
			 elementContext("NATIVE_APP_NON_INSTRUMENTED");
			 elementClick(By.xpath(QuizzesAndExams.New_Button), "New Button");
			 elementClick(By.xpath("//*[contains(@contentDescription,'"+menuItem+"')]"), menuItem);
			 elementSendText(By.xpath(QuizzesAndExams.Question_Title), questionTitle, "Question Title");
			 elementCloseKeyBoard();	
			 elementClick(By.xpath(QuizzesAndExams.Question_Text), "Question Text");
			 elementSendText(By.xpath(QuizzesAndExams.Question_Text), questionText, "Question Text");
			 elementCloseKeyBoard();
			 elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
			 elementClick(By.xpath(QuizzesAndExams.True_Weight), "True Weight");
			 elementSendText(By.xpath(QuizzesAndExams.True_Weight), trueWeight, "True Weight");
			 elementCloseKeyBoard();
			 elementClick(By.xpath(QuizzesAndExams.True_FeedBack), "True FeedBack");
			 elementSendText(By.xpath(QuizzesAndExams.True_FeedBack), trueFeedBack, "True FeedBack");
			 elementCloseKeyBoard();
			 elementClick(By.xpath(QuizzesAndExams.False_Weight), "False Weight");
			 elementSendText(By.xpath(QuizzesAndExams.False_Weight), falseWeight, "False Weight");
			 elementCloseKeyBoard();
			 elementClick(By.xpath(QuizzesAndExams.False_FeedBack), "False FeedBack");
			 elementSendText(By.xpath(QuizzesAndExams.False_FeedBack), falseFeedBack, "False FeedBack");
			 elementCloseKeyBoard();
			 elementClick(By.xpath(QuizzesAndExams.Save_Button), "Save Button");
			 elementClick(By.xpath(QuizzesAndExams.Done_Editing_Questions), "Done Editing Questions");
			 elementContext("WEBVIEW_1");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}	

	/***
	 * Method to adding quiz status
	 * @param minuts
	 * @return
	 * @throws Throwable
	 */
	public boolean addingQuizStatus(int minuts) throws Throwable {
		boolean flag = true;
		try {	
			 selectingOption("Quiz", "Restrictions");
			 elementClick(By.xpath(QuizzesAndExams.Quiz_Status), "Quiz Status");
			 elementSendKeys("Active");
			 elementClick(By.xpath(QuizzesAndExams.HasStartDate_checkbox), "Has Start Date CheckBox");
			 elementClick(By.xpath(QuizzesAndExams.Now_Button), "Now Button");
			 elementClick(By.xpath(QuizzesAndExams.HasEndDate_checkbox), "Has End Date CheckBox");
			 elementSendText(By.xpath(QuizzesAndExams.Has_End_Date),addDaysToCurrentDate(0), "Adding Days To CurrentDate");
			 String endTime = elementGetText(By.xpath("//*[@id='z_v$time']"), "end Date Time");
			 String newEndTime = returnTheStartOrEndTime(endTime ,minuts);
			 By.xpath("//*[@id='z_v$time']");
			 elementSendText(By.xpath("//*[@id='z_v$time']"), newEndTime, "End Time");
			 elementSwipe(SwipeElementDirection.DOWN, 200, 1000);
			 elementClick(By.xpath(QuizzesAndExams.Save_And_Close), "Save and Close");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to add restrictions to quizzes
	 * @param minuts
	 * @return
	 * @throws Throwable
	 */
	public boolean addRestrictionsToQuizzes() throws Throwable {
		boolean flag = true;
		try {
			 selectingOption("Quiz", "Restrictions");
			 elementClick(By.xpath(QuizzesAndExams.Quiz_Status), "Quiz Status");
			 elementSendKeys("Active");
			 elementClick(By.xpath(QuizzesAndExams.HasStartDate_checkbox), "Has Start Date CheckBox");
			 elementClick(By.xpath(QuizzesAndExams.Now_Button), "Now Button");
			 elementClick(By.xpath(QuizzesAndExams.Calender_CheckBox), "Calender CheckBox");			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	/***
	 * Method to add assessment to quizzes
	 * @param gradeName
	 * @param rubricName
	 * @param rubricDescription
	 * @return
	 * @throws Throwable
	 */
	public boolean addAssessmentToQuizzes(String gradeName, String rubricName, String rubricDescription) throws Throwable {
		boolean flag = true;
		try{
			selectingOption("Quiz", "Assessment");
			elementClick(By.xpath(QuizzesAndExams.Allow_Attempt), "Allow Attempt");
			elementClick(By.xpath(QuizzesAndExams.Add_Grade_Item), "Add Grade Item");
			elementClick(By.xpath(QuizzesAndExams.Grade_Name), "Grade Name");			
			elementSendKeys(gradeName);
			elementCloseKeyBoard();
			elementClick(By.xpath(QuizzesAndExams.Grade_Save_Button), "Save Button");			
			elementClick(By.xpath(QuizzesAndExams.Create_New_Rubric), "Create New Rubric Link");			
			elementSendText(By.xpath(QuizzesAndExams.Rubric_Name), rubricName, "Rubric Name");			
			elementClick(By.xpath(QuizzesAndExams.Rubric_Status), "Rubric Status");			
			elementSendKeys("Published");
			elementClick(By.xpath(QuizzesAndExams.Rubric_Description), "Rubric Description");			
			elementSendKeys(rubricDescription);
			elementCloseKeyBoard();
			elementClick(By.xpath(QuizzesAndExams.Save_And_Close), "Save Button");			
			elementSendText(By.xpath(QuizzesAndExams.Save_And_Close), Keys.CONTROL+"w", "Close the Open Tab");
			elementClick(By.xpath(QuizzesAndExams.Add_Rubric), "Add Rubric");
			elementClick(By.xpath("//*[@text='"+rubricName+"']/../..//*[@name='z_a_cb']"),"Selecting Rubric");
			elementClick(By.xpath(QuizzesAndExams.Add_Selected), "Add Selected");
			elementClick(By.xpath(QuizzesAndExams.Save_And_Close), "Save And Close");	
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/***
	 * Method to adding quiz to module
	 * @param moduleName
	 * @return
	 * @throws Throwable
	 */
	public boolean addingQuizToModule(String moduleName) throws Throwable {
		boolean flag = true;
		try {	
			 elementClick(By.xpath(QuizzesAndExams.Add_A_Module), "Add a Module");
			 elementSendText(By.xpath(QuizzesAndExams.txtAdd_A_Module), moduleName, "Module Name"); 
			 elementCloseKeyBoard();
			 androidDriver1.findElement(By.xpath(QuizzesAndExams.txtAdd_A_Module)).sendKeys(Keys.ENTER);
			 //elementSendText(By.xpath(QuizzesAndExams.Add_A_Module), moduleName, "Add a Module");
			// elementClick(By.xpath(QuizzesAndExams.Upload_Or_Create), "Upload or Create");
			// pageRefresh();
			 Thread.sleep(2000);
			 scrollingToElementofAPage(By.xpath("//*[text()='"+moduleName+"']"));
			 elementClick(By.xpath("//*[text()='"+moduleName+"']"), "Module Name");
			 Thread.sleep(2000);
			 elementClick(By.xpath(QuizzesAndExams.btnExistingActivities),"Click on Existing activites");
			 Thread.sleep(1500);
			 elementClick(By.xpath(QuizzesAndExams.optnQuizzes), "Option Quizzes");
			 Thread.sleep(4000);
			 switchToFrameByIndex(0);
			 elementClick(By.xpath(QuizzesAndExams.tpcQuizzes), "Click on quizz");
			 switchToDefaultFrame();
			 elementExists(By.xpath(QuizzesAndExams.Quiz_Name_Validating), "Validating message");
			 Thread.sleep(1000);
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	public boolean deletingModule(String moduleName) throws Throwable {
		boolean flag = true;
		try {	
			//By temp= By.xpath("//a[@title="+"Actions for '"+moduleName+"']");
			//elementClick(temp, "Inverted triangle beside module");
			elementClick(By.xpath("//h1[text()='"+moduleName+"']//ancestor::form/following-sibling::a"), "Inverted triangle");
			Thread.sleep(1500);
			elementClick(By.xpath("//span[text()='Delete Module']"), "Delete module");
			switchToFrameByIndex(0);
			elementClick(By.xpath("(//span[@class='d2l-radio-inline']/input)[1]"),"Radio button");
			JSClick(By.xpath("//button[text()='Delete']"),"Delete button");
			Thread.sleep(10000);
			switchToDefaultFrame();
			
				
			
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
	
	
	
	

	/**
	 * @Descriptin: Create Self Assessement and edit the assesement details and save the details
	 * @param selfAssementName
	 * @param qTitle
	 * @param qText
	 * @return
	 * @throws Throwable
	 * 
	 */
	public void createSelfAssessement(String selfAssementName,String qTitle,String qText)throws Throwable{
	/*	if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SelfAssessementObjects.SelfAssessement));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SelfAssessementObjects.SelfAssessement));
			explicitWait(androidDriver1,By.xpath(SelfAssessementObjects.SelfAssessement));
		}*/
       
        elementClick(By.xpath(SelfAssessementObjects.NewSelfAssessement),"New Self Assessement tab");
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SelfAssessementObjects.NewSelfAssessement));
		}
		/*else
		{
			//explicitWait(androidDriver, By.xpath(SelfAssessementObjects.NewSelfAssessement));
			explicitWait(androidDriver1,By.xpath(SelfAssessementObjects.NewSelfAssessement));
		}*/
        
        //elementClick(By.xpath(SelfAssessementObjects.NewSelfAssessement), "New Self Assessement");
        elementSendText(By.xpath(SelfAssessementObjects.NewSelfAssessement_Name),selfAssementName,"Self AssessementName");
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SelfAssessementObjects.NewSelfAssessement_SaveandCloseButton));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SelfAssessementObjects.NewSelfAssessement_SaveandCloseButton));
			scrollingToElementofAPage(By.xpath(SelfAssessementObjects.NewSelfAssessement_SaveandCloseButton));
			explicitWait(androidDriver1,By.xpath(SelfAssessementObjects.NewSelfAssessement_SaveandCloseButton));
		}
        
        elementClick(By.xpath(SelfAssessementObjects.NewSelfAssessement_SaveandCloseButton), "New Self Assessement save and close button");
        //elementContext("WEBVIEW_1");
        Thread.sleep(1000);
        if(elementExists(By.xpath("//*[text()='"+selfAssementName+"']"))){
        	scrollingToElementofAPage(By.xpath("//*[text()='"+selfAssementName+"']"));
            elementClick(By.xpath("//*[text()='"+selfAssementName+"']"), "Self Assessment");
            Thread.sleep(3000);
        } else {

        }
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SelfAssessementObjects.SelfAssesementEditButton));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SelfAssessementObjects.SelfAssesementEditButton));
			explicitWait(androidDriver1,By.xpath(SelfAssessementObjects.SelfAssesementEditButton));
		}
        
        elementClick(By.xpath(SelfAssessementObjects.SelfAssesementEditButton),"Add/Edit Questions");
        elementHardSleep(8000);
        
    //  elementContext("NATIVE_APP_NON_INSTRUMENTED");
      
        switchToFrameByLocator(By.xpath(SelfAssessementObjects.iframe), "Frame");
		switchToFrameByLocator(By.xpath(SelfAssessementObjects.listFrame), "List Frame");
		
        elementClick(By.linkText("New"),"Click on New Button") ;
        Thread.sleep(3000);
        elementClick(By.xpath(SelfAssessementObjects.NewMultiSelectedvalue),"Written Response Question (WR)");
        Thread.sleep(8000);
        switchToDefaultFrame();
        Thread.sleep(3000);
        switchToFrameByLocator(By.xpath("//iframe[@title='Question Editor']"), "Description box");
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SelfAssessementObjects.Title));
		}
		else
		{

			//explicitWait(androidDriver, By.xpath(SelfAssessementObjects.Title));
			explicitWait(androidDriver1,By.xpath(SelfAssessementObjects.textQstn));
			elementClick(By.xpath(SelfAssessementObjects.textQstn), "Question text");
			Thread.sleep(4000);
		}
        //explicitWait(driver,By.xpath(SelfAssessementObjects.Title));
        elementSendText(By.xpath(SelfAssessementObjects.textQstn), qTitle, "Enter Title ");
        Thread.sleep(1000);
        elementCloseKeyBoard();
      /*  try{
              if (elementExists(By.xpath(SelfAssessementObjects.QuestionText),"Click on Question Tab For Andriod")) {
                    if (elementClick(By.xpath(SelfAssessementObjects.QuestionText),"Click on Question Tab For Andriod")) {
                          SuccessReport("Click on Question Tab For Android", "Click on Question tab Sucessfully");
                    } else {
                          failureReport("Click on Question Tab For Android", "Failed to Click on Question Tab For Andriod");
                    }
              } else {
                    if (elementClick(By.xpath(SelfAssessementObjects.QuestionText1),"Click on Question Tab For Andriod Tab")) {
                          SuccessReport("Click on Question Tab For Android Tab", "Click on Question tab Sucessfully");
                    } else {
                          failureReport("Click on Question Tab For Android Tab", "Failed to Click on Question Tab For Andriod");
                    }
              }
        }catch(Exception e){
        }
*/
       // elementSendKeys(qText);
     //   elementCloseKeyBoard();
        //elementHardSleep(3000);
       // elementSwipe(SwipeElementDirection.DOWN, 500, 10000);
        JSClick(By.xpath(SelfAssessementObjects.SaveButton), "SaveButton");
        Thread.sleep(3000);
        switchToFrameByLocator(By.xpath(SelfAssessementObjects.iframe), "Frame");
		switchToFrameByLocator(By.xpath(SelfAssessementObjects.listFrame), "List Frame");
        
        Thread.sleep(2000);
       
       
        //driver.findElement(By.xpath(SelfAssessementObjects.SaveButton)).click();
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SelfAssessementObjects.DoneEditingQuestion));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SelfAssessementObjects.DoneEditingQuestion));
			explicitWait(androidDriver1,By.xpath(SelfAssessementObjects.DoneEditingQuestion));
		}
        
       elementClick(By.xpath(SelfAssessementObjects.DoneEditingQuestion), "DoneEditingQuestion"); 
       Thread.sleep(3000);
       switchToDefaultFrame();
     
       Thread.sleep(2000);
        //elementContext("WEBVIEW_1");
        elementClick(By.xpath(SelfAssessementObjects.SaveAndClose), "SaveAndClose"); 
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SelfAssessementObjects.SucessMessage));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SelfAssessementObjects.SucessMessage));
			explicitWait(androidDriver1,By.xpath(SelfAssessementObjects.SucessMessage));
		}
        
        elementExists(By.xpath(SelfAssessementObjects.SucessMessage),"Sucess Message");
  }

	public void verifyCreatedSelfAssessmentForLearner(String SelfAssementName)throws Throwable {
       // elementClick(By.xpath(SelfAssessementObjects.lnkManageSelfAssessments),"Self Assessement tab");
        scrollingToElementofAPage(By.xpath("//*[text()='"+SelfAssementName+"']"));
		elementClick(By.xpath("//*[text()='"+SelfAssementName+"']"),"Click on SelfAssesment Link") ;
        elementHardSleep(4000);
        elementClick(By.xpath(SelfAssessementObjects.DoneButton), "DoneButton");
  }

	public void goToSurvey() throws Throwable {
		//selectMenuItem("My Tools");
		//selectingOption("Surveys", "Surveys");
		
		selectingMyTools("Surveys");
	}
	
	public void closeBrowserAndLaunch(String port,String device ) {
		
		try {
			androidDriver1.close();
			System.out.println("Driver Closed");
			androidDriver1 = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));
			System.out.println("Driver launched back again");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
public void closeBrowserAndPort(String port,String device ) {
		
		try {
			androidDriver1.close();
			System.out.println("Driver Closed");
			/*androidDriver1 = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));
			System.out.println("Driver launched back again");*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @Description : Create a Survey by login as a PI
	 * @param SurveyName
	 * @param QuestionTitle
	 * @param QuestionText
	 * @throws Throwable
	 *
	 * @Description: Create Survey
	 */
	public void createSurvey(String surveyName,String questionTitle,String questionText) throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SurveyObjects.NewSurvey));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SurveyObjects.NewSurvey));
			explicitWait(androidDriver1, By.xpath(SurveyObjects.NewSurvey));
		}
        elementClick(By.xpath(SurveyObjects.NewSurvey),"New Survey");
        Thread.sleep(3000);
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SurveyObjects.Surveyname));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SurveyObjects.Surveyname));
			explicitWait(androidDriver1, By.xpath(SurveyObjects.Surveyname));
		}
        elementSendText(By.xpath(SurveyObjects.Surveyname),surveyName,"Survey Name");
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SurveyObjects.AddOrEditQuestions));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SurveyObjects.AddOrEditQuestions));
			explicitWait(androidDriver1,By.xpath(SurveyObjects.AddOrEditQuestions));
			scrollingToElementofAPage(By.xpath(SurveyObjects.AddOrEditQuestions));
		}
        
        JSClick(By.xpath(SurveyObjects.AddOrEditQuestions),"AddOrEdit Questions Button");
        Thread.sleep(6000);
     //   elementContext("NATIVE_APP_NON_INSTRUMENTED");
        switchToFrameByLocator(By.xpath(SelfAssessementObjects.iframe), "Frame");
		switchToFrameByLocator(By.xpath(SelfAssessementObjects.listFrame), "List Frame");
        elementClick(By.xpath(SurveyObjects.NewButton), "New Button");
        Thread.sleep(2000);
        JSClick(SurveyObjects.d2LmenuItemLinktextName("True or False Question (T/F)"),"True or False Question (T/F)");
       /* if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SurveyObjects.QuestionTitle));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SurveyObjects.QuestionTitle));
			explicitWait(androidDriver1,By.xpath(SurveyObjects.QuestionTitle));
		}
        
        elementSendText(By.xpath(SurveyObjects.QuestionTitle),questionTitle,"Question Title");*/
        //elementCloseKeyBoard();
        /*explicitWait(driver,By.xpath(SurveyObjects.QuestionText));
        elementClick(By.xpath(SurveyObjects.QuestionText),"Question Text");*/
        Thread.sleep(30000);
        switchToDefaultFrame();
        Thread.sleep(3000);
        switchToFrameByLocator(By.xpath("//iframe[@title='Question Editor']"), "Question text");
        try{
              if (elementExists(By.xpath(SurveyObjects.QuestionText),"Question Text")) {
                    if (elementClick(By.xpath(SurveyObjects.QuestionText),"Question Text")) {
                          SuccessReport("Click on Question Tab For Android", "Click on Question text field Sucessfully");
                    } else {
                          failureReport("Click on Question Tab For Android", "Failed to Click on Question text field ");
                    }
              } else {
                    if (elementClick(By.xpath("//*[@contentDescription='\n' and @height>0 and ./parent::*[@contentDescription='\n' and ./parent::*[./parent::*[./preceding-sibling::*[@contentDescription='Skip Toolbars for Question Text.']]]]]"),"Click on Question Tab For Andriod Tab")) {
                          SuccessReport("Click on Question Tab For Android Tab", "Click on Question tab Sucessfully");
                    } else {
                          failureReport("Click on Question Tab For Android Tab", "Failed to Click on Question Tab For Andriod");
                    }
              }
        }catch(Exception e){
        	e.printStackTrace();
        }
        elementSendText(By.xpath(SurveyObjects.QuestionText), questionText, "Question text");
       // elementSendKeys(questionText);
        elementCloseKeyBoard();
        Thread.sleep(1000);
    	/*switchToDefaultFrame();
        switchToFrameByLocator(By.xpath(SelfAssessementObjects.iframe), "Frame");
		switchToFrameByLocator(By.xpath(SelfAssessementObjects.listFrame), "List Frame");*/
       // elementSwipe(SwipeElementDirection.DOWN, 500, 200000);
		
		
		 JSClick(By.xpath(SelfAssessementObjects.SaveButton),"Save Button");
		 Thread.sleep(10000);
		switchToDefaultFrame();
		
		switchToFrameByLocator(By.xpath(SelfAssessementObjects.iframe), "Frame");
		switchToFrameByLocator(By.xpath(SelfAssessementObjects.listFrame), "List Frame");
      
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SurveyObjects.DoneEditingQuestions));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SurveyObjects.DoneEditingQuestions));
			explicitWait(androidDriver1, By.xpath(SurveyObjects.btnDoneEditing));
		}
        elementClick(By.xpath(SurveyObjects.btnDoneEditing), "Done Editing Questions");
        Thread.sleep(5000);
        switchToDefaultFrame();
       // elementContext("WEBVIEW_1");	
        //selectingOption("Restrictions", "Restrictions");
        elementClick(By.xpath("//a[text()='Restrictions']"), "Restrictions");
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SurveyObjects.QuestionTitle));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SurveyObjects.SelectStatus));
			explicitWait(androidDriver1, By.xpath(SurveyObjects.SelectStatus));
		}
        selectByVisibleText(By.xpath(SurveyObjects.SelectStatus), "Active", "Status tab");
        Thread.sleep(2000);
        //elementContext("NATIVE_APP");
       // elementClick(By.xpath(SurveyObjects.Active), "Active");
       // elementContext("WEBVIEW_1");
       // elementSwipe(SwipeElementDirection.DOWN, 500, 100000);
        elementClick(By.xpath(SurveyObjects.Save_And_Close_Button), "Save and Close Button");
        Thread.sleep(1000);
        verifySuccessAlert("Saved successfully");
  }
	
	

	/**
	 * @Description: Access Survey login as a learner
	 *
	 * @param SurveyName
	 * @throws Throwable
	 */
	
	public void surveyAddRestrictionsDate(String SurveyName, int minutes) throws Throwable {
		
		scrollingToElementofAPage(By.xpath("//*[text()='"+SurveyName+"']"));
		elementClick(By.xpath("//*[text()='"+SurveyName+"']"), "SurveyName");
		Thread.sleep(3000);
		elementClick(By.xpath("//a[text()='Restrictions']"), "Restrictions tab");
		Thread.sleep(3000);
//		scrollingToElementofAPage(By.xpath("//input[@id='z_l']"));
		scrollingToElementofAPage(By.xpath(SurveyObjects.startDate));
		JSClick(By.xpath(SurveyObjects.startDate), "Start date checkbox");
		JSClick(By.xpath(SurveyObjects.btnNowStartDate), "Now button");
		scrollingToElementofAPage(By.xpath(SurveyObjects.endDate));
		JSClick(By.xpath(SurveyObjects.endDate), "Has End Date");
		JSClick(By.xpath(SurveyObjects.btnNowEndDate), "End Date Now");
		JSClick(By.xpath(SurveyObjects.txtEndDateTime), "end Date Time");
        Thread.sleep(2000);
        scrollingToElementofAPage(By.xpath(SurveyObjects.endDateFuture));
        JSClick(By.xpath(SurveyObjects.endDateFuture), "Select end date");

//        String endTime = getValue(By.xpath(SurveyObjects.txtEndDateTime), "end Date Time");
//        String newEndTime= returnTheStartOrEndTime(endTime,minutes);
//        System.out.println("endTime :"+endTime);
//        System.out.println("endTime :"+newEndTime);
//        Thread.sleep(1500);
//        elementClear(By.xpath(SurveyObjects.txtEndDateTime));
//        Thread.sleep(2000);
//        elementClear(By.xpath(SurveyObjects.txtEndDateTime));
//        elementSendText(By.xpath(SurveyObjects.txtEndDateTime), newEndTime, "End Time");

        Thread.sleep(2000);
        JSClick(By.xpath(SurveyObjects.Save_And_Close_Button), "Save and Close Button");
        Thread.sleep(10000);
	}
	
	public void accessSurvey(String SurveyName) throws Throwable  {
      //  selectingOption(SurveyName, SurveyName);
        //elementContext("NATIVE_APP_NON_INSTRUMENTED");
		//elementClick(By.xpath("//*[text()='SampleSurvey']"), "Survey name"); // this line to verify script
		elementClick(By.xpath("//*[text()='"+SurveyName+"']"), "Survey name");//should uncomment
		Thread.sleep(8000);
		switchToFrameByLocator(By.xpath(SurveyObjects.iframe), "Frame");
		switchToFrameByLocator(By.xpath(SurveyObjects.pageFrame), "Page Frame");
		elementClick(By.xpath(SurveyObjects.radioTrue),"Select True");
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SurveyObjects.SubmitButton));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(SurveyObjects.SubmitButton));
			explicitWait(androidDriver1, By.xpath(SurveyObjects.SubmitButton));
		}
       
        elementClick(By.xpath(SurveyObjects.SubmitButton),"Submit Button");
       // elementContext("WEBVIEW_1");
        switchToDefaultFrame();
        if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(SurveyObjects.YesButton));
		}
		else
			//explicitWait(androidDriver, By.xpath(SurveyObjects.YesButton));
		{
			explicitWait(androidDriver1, By.xpath(SurveyObjects.YesButton));
		}
       
        elementClick(By.xpath(SurveyObjects.YesButton),"Yes Button");
        Thread.sleep(10000);
        switchToFrameByLocator(By.xpath(SurveyObjects.iframe), "Frame");
		Thread.sleep(5000);
        verifySuccessAlert("You have successfully submitted the survey.");
        elementClick(By.xpath(SurveyObjects.ReturnToSurveyList),"ReturnToSurveyList Button");
        Thread.sleep(4000);
  }

	/**
	 * @Description : Method to click on upload file and verify assignment
	 * @throws Throwable
	 */
	public void clickOnUploadedFileAndVerifyAssignmentPresent() throws Throwable {
        elementSwipeWhileNotFound(By.xpath(AssignmentsObjects.Uploaded_File_Link), 100, SwipeElementDirection.DOWN, 500, 10000, 5, true);
		if(elementExists(By.xpath(AssignmentsObjects.Uploaded_File_Link), "uploaded File")){
			elementClick(By.xpath(AssignmentsObjects.Uploaded_File_Link), "uploaded File");
		}
	}

	/**
	 * @Description : Method to click on inverted triangle beside assignment and verify
	 * @param assignmentName
	 * @param verifyPageDisplayed
	 * 
	 * @throws Throwable
	 */
	public boolean clickOnInvertedTriangleBesideAssignmentSelectSubmissionLogAndVerify(String assignmentName, boolean verifyPageDisplayed) throws Throwable {
		boolean flag = true;
		try{
			String InvertedTriangle_Beside_Assignment = AssignmentsObjects.ReplaceString(AssignmentsObjects.InvertedTriangle_Beside_Assignment, "oldString", assignmentName);     
			elementClick(By.xpath(InvertedTriangle_Beside_Assignment), "Inverted triangle Beside assignment");

			selectingOption("Submission Log", "Submission Log");

			if(verifyPageDisplayed == true){
				if(elementExists(By.xpath(AssignmentsObjects.SubmissionLog_Title))){
					SuccessReport("Submission Log", "Submission Log is Displayed");                  
				} else {
					failureReport("Submission Log", "Submission Log is not Displayed");
				}
			}
		}catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * @Description : Method to reorder assignment and verify
	 * @param categoryName
	 * @param verifyReorder
	 * 
	 * @throws Throwable
	 */
	public boolean reorderAssignmentAndVerify(String categoryName, boolean verifyReorder) throws Throwable {
		boolean flag = true;
		try{
			selectingOption("More Actions", "More Actions");
			selectingOption("Reorder", "Reorder");
			String Reorder_Category = AssignmentsObjects.ReplaceString(AssignmentsObjects.Reorder_Category, "oldString", categoryName);
			elementClick(By.xpath(Reorder_Category), "reorder category");
			elementSwipe(SwipeElementDirection.UP, 1000, 500);
			elementContext("NATIVE_APP");
			selectingOption("Done", "Done");
			elementContext("WEBVIEW_1");
			selectingOption("Save", "Save");
			if(verifyReorder == true){
				verifySuccessAlert("Reordered successfully");
			}
		}catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}  

	/**
	 * @Description : Method to creating new rubric through course admin
	 * @param rubric name
	 * 
	 * @throws Throwable
	 */
	public boolean creatingNewRubricThroughCourseAdmin(String rubricName) throws Throwable{ 
		boolean flag = true;
		try{
			selectMenuItem("Course Admin");
			selectingOption("Rubrics", "Rubrics");
			selectingOption("New Rubric", "New Rubric");           
			elementSendText(By.xpath(AssignmentsObjects.Rubric_Name), rubricName, "Rubric Name");
			elementClick(By.xpath(AssignmentsObjects.Rubric_Status), "Rubric Status");
			elementContext("NATIVE_APP");
			if(configProps.getProperty("deviceType").equals("iOS")){
				iosDriver.drag(By.xpath("//*[@class='UIAPicker']"), 0, -70);
			 }
			 else
			 {
				androidDriver.drag(By.xpath("//*[@class='UIAPicker']"), 0, -70);
				 //androidDriver1.drag(By.xpath("//*[@class='UIAPicker']"), 0, -70);
				 
			 } 
			
			selectingOption("Done", "Done");
			elementContext("WEBVIEW_1");
			
			selectingOption("Save", "Save");
		}catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}

	
	
	
	
	/**
	 * @Description : Method to create assignment with selected rubric
	 * @param assignmentName
	 * @param gradeName
	 * @param outOfBoxScore
	 * @param instructionsDetails
	 * @param rubricName
	 * 
	 * @throws Throwable
	 */
	public boolean createAssignmentWithNameAndSelectedRubric(String assignmentName,String gradeName,String outOfBoxScore,String instructionsDetails,String rubricName) throws Throwable{
        boolean flag = true;
        try {
             elementClick(By.xpath(AssignmentsObjects.New_Submission_Folder), "New Submission Folder");
             elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment name");
             elementSwipe(SwipeElementDirection.DOWN, 300, 1000);
			 elementClick(By.xpath(AssignmentsObjects.New_Grade_Link),"New Grade Link");
			 String text2 = elementGetText(By.xpath("//*[@text='Name']"), "Grade Name"); 
			 System.out.println(text2);
			 elementClick(By.xpath(AssignmentsObjects.Grade_Name_TextBox),"Grade Name TextBox");
			 elementSendKeys(gradeName);
			 elementCloseKeyBoard();
			 elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
			 elementClick(By.xpath(AssignmentsObjects.Grade_Save_Button),"Grade Save Button");
			 elementSwipe(SwipeElementDirection.UP, 500, 1000);
             elementSendText(By.xpath(AssignmentsObjects.Out_Of_Box_Score),outOfBoxScore, "Out of Box Score");
             selectingOption("Add Rubric", "Add Rubric");
             Thread.sleep(5000);
             if(configProps.getProperty("deviceType").equals("iOS")){
 				iosDriver.findElement(By.xpath("//*[@nodeName='INPUT' and @name='z_i' and @class='d_edt vui-input' and @onScreen='true']")).clear();
 			 }
 			 else
 			 {
 				//androidDriver.findElement(By.xpath("//*[@nodeName='INPUT' and @name='z_i' and @class='d_edt vui-input' and @onScreen='true']")).clear();
 				androidDriver1.findElement(By.xpath("//*[@nodeName='INPUT' and @name='z_i' and @class='d_edt vui-input' and @onScreen='true']")).clear();
 			 } 
             
             elementSendText(By.xpath("//*[@nodeName='INPUT' and @name='z_i' and @class='d_edt vui-input' and @onScreen='true']"), rubricName, "Rubric search textbox");
             elementClick(By.xpath("//*[@type='button' and @class='vui-input-search-button' and @onScreen='true']"), "Search button");
             String Rubric_checkBox = AssignmentsObjects.ReplaceString(AssignmentsObjects.Rubric_checkBox, "oldString", rubricName);
             elementClick(By.xpath(Rubric_checkBox), "Rubric Check box");
             selectingOption("Add Selected", "Add Selected");             
			 elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
			 elementClick(By.xpath(AssignmentsObjects.Instructions_TextBox),"Instructions TextBox");
			 elementSendKeys(instructionsDetails);
			 elementCloseKeyBoard();
			 selectingOption("Restrictions", "Restrictions");
			 elementClick(By.xpath("//*[@id='z_n' and @checked='checked' and @type='checkbox']"), "Hidden from users");
			 if(elementExists(By.xpath(ELMAssignmentObjects.Created_Successfully_Close))){
                 elementClick(By.xpath(ELMAssignmentObjects.Created_Successfully_Close), "Created successfully close");
             }
			 elementClick(By.xpath(AssignmentsObjects.Save_And_Close_Button),"Save and Close Button");	
        }catch (Exception e) {
               flag = false;
               e.printStackTrace();
        }
        return flag;
 }

	/**
	 * @Description : Method to enter start time
	 * @param minuts
	 * 
	 * @throws Throwable
	 */
	public boolean enterStartTime(int minuts) throws Throwable{
        boolean flag = true;
        try {
             // Change start time
             elementClick(By.xpath(AnnouncementObjects.StartDateNow), "Start Date Now");
             String startTime = elementGetText(By.xpath(AnnouncementObjects.Start_Time), "Start Date Time");
             String newStartTime = returnTheStartOrEndTime(startTime ,3);
             elementClear(By.xpath(AnnouncementObjects.Start_Time));
             elementSendText(By.xpath(AnnouncementObjects.Start_Time), newStartTime, "New Start Time");
        }catch(Exception e) {
               flag = false;
               e.printStackTrace();
        }
        return flag;
   }
	
	/**
	 * @Description : Method to enter end time
	 * @param minuts
	 * 
	 * @throws Throwable
	 */
	 public boolean enterEndTime(int minuts) throws Throwable{
	        boolean flag = true;
	        try {
	             // Change end time
	             elementClick(By.xpath(AnnouncementObjects.Has_End_Date), "Has End Date");
	             elementClick(By.xpath(AnnouncementObjects.EndDateNow), "End Date Now");
	             String endTime = elementGetText(By.xpath(AnnouncementObjects.End_Time), "end Date Time");
	             String newEndTime = returnTheStartOrEndTime(endTime ,minuts);
	             elementClear(By.xpath(AnnouncementObjects.End_Time));
	             elementSendText(By.xpath(AnnouncementObjects.End_Time), newEndTime, "End Time");
	        }catch(Exception e) {
	               flag = false;
	               e.printStackTrace();
	        }
	        return flag;
	 }


	/**
	 * Description :This method is used to Enter Grade value for a particular Learner and 
	 * verifies the Newly created Grade in Event logs and The grade action status should be 
	 * @param GradeName
	 * @param Gradevalue
	 * @param UserName
	 * @throws Throwable
	 */
	public void enterGrades(String GradeName,String Gradevalue,String UserName) throws Throwable {
	//	selectingOption("Enter Grades","Enter Grades");
		elementClick(By.xpath("//a[text()='Enter Grades']"), "Enter grades");
		Thread.sleep(3000);
		//elementExists(By.xpath("//*[contains(text(),'Switch to Spreadsheet View')]"));
		if(isElementDisplayed(By.xpath("//*[contains(text(),'Switch to Spreadsheet View')]"), "SpreadSheetView")) {
			elementClick(By.xpath("//*[contains(text(),'Switch to Spreadsheet View')]"), "Spreadsheet View");}
		//elementContext("NATIVE_APP_NON_INSTRUMENTED");
		
		elementSendText(By.xpath(GradeObjects.txtGradeItemValue(GradeName)), "5", "Grade Item Value");
		scrollingToElementofAPage(By.xpath(GradeObjects.txtAdjustFinalGradeNumerator));
		//elementSendText(By.xpath("//*[text()='"+GradeName+" value for "+UserName+"' and @enabled='true' and @onScreen='true']"),Gradevalue,"Grade value"); 
		//elementCloseKeyBoard();
		elementClear(By.xpath(GradeObjects.txtAdjustFinalGradeNumerator));
		elementSendText(By.xpath(GradeObjects.txtAdjustFinalGradeNumerator), "8", "Adjusted Final Grade Numerator");
		elementCloseKeyBoard();
		elementClear(By.xpath(GradeObjects.txtAdjustFinalGradeDenominator));
		elementSendText(By.xpath(GradeObjects.txtAdjustFinalGradeDenominator), "10", "Adjusted Final Grade Denominator");
		elementCloseKeyBoard();
		//	elementContext("WEBVIEW_1");
		//selectingOption("Save","Save");
		elementClick(By.xpath(GradeObjects.SaveButton), "Save button");
		elementClick(By.xpath(GradeObjects.YesButton),"Yes Button");
		Thread.sleep(1000);
		//verifySuccessAlert("Saved successfully");
		explicitWait(androidDriver1, By.xpath("//*[text()='Saved successfully']"));
		explicitWait(androidDriver1,By.xpath("//a[text()='Enter Grades']"));
		//elementClick(By.xpath(GradeObjects.ManageGrades),"Manage Grades");
		Thread.sleep(1500);
		elementClick(By.xpath(GradeObjects.btnMoreActions),"More Actions");
		elementClick(GradeObjects.d2LmenuItemLinktextName("View Event Log"),"View Event Log");
		textSearchRubricDoubleShadowDOM(GradeName, "d2l-icon");
	
		Thread.sleep(3000);
		elementExists(By.xpath("//*[contains(text(),'"+GradeName+"')]"),"Grade in Event Logs");
	
		Thread.sleep(2000);
		
		if(isElementPresentNegative(By.xpath(GradeObjects.lblCreated(GradeName)), "Grade Item Action"))
			SuccessReport("Event Log", "User Able to access Event Log");
		else
			failureReport("Event Log", "User Not Able to access Event Log");
	}
	
	public boolean creatingAnnouncement(String announcementName, String contentDesc, boolean uploadFile , String uploadFileType, boolean StartTime, int StartTimeValue, boolean EndTime, int EndTimeValue ) throws Throwable {
        boolean flag = true;
        try{
        	if(configProps.getProperty("deviceType").equals("iOS")){
        		explicitWait(iosDriver, By.xpath(AnnouncementObjects.HeadLine_Text));
 			 }
 			 else
 			 {
 				//explicitWait(androidDriver, By.xpath(AnnouncementObjects.HeadLine_Text));
 				explicitWait(androidDriver1, By.xpath(AnnouncementObjects.HeadLine_Text));
 			 } 
               elementSendText(By.xpath(AnnouncementObjects.HeadLine_Text), announcementName, "Announcement Head line");
               
               elementClick(By.xpath(AnnouncementObjects.Content_Text), "Content Text");
               elementSendKeys(contentDesc);
               
               if(uploadFile == true){
                     if(uploadFileType == "Video"){
                            elementClick(By.xpath(AnnouncementObjects.Insert_Sample_Video), "Insert Sample Video");
                     }
                     if(uploadFileType == "Image"){
                            elementClick(By.xpath(AnnouncementObjects.Insert_Sample_Image), "Insert Sample Image");
                     }
                     if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
                            uploadingIphoneImageOrVideo(uploadFileType);
                     } else {
                            uploadingImageOrVideo(uploadFileType);
                     }
               }
               elementCloseKeyBoard();
                if(configProps.getProperty("deviceType").equals("iOS")){
           		    explicitWait(iosDriver, By.xpath("//*[@text='Now']"));
    			 }
    			 else
    			 {
    				//explicitWait(androidDriver, By.xpath("//*[@text='Now']"));
    				explicitWait(androidDriver1, By.xpath("//*[@text='Now']"));
    			 } 
               if(elementExists(By.xpath("//*[@text='Now']"))){
                     selectingOption("Date Now Button","Now");
               }
               
               // Change start time
               if(StartTime && StartTimeValue != 0){
                     enterStartTime(StartTimeValue);
               }
               // Change end time
               if(EndTime && EndTimeValue != 0){
                     enterEndTime(EndTimeValue);
               }
               selectingOption("Publish Button","Publish");
               
        }catch(Exception e) {
               flag = false;
               e.printStackTrace();
        }
        return flag;
   }

	/**
	 * @Description : Method to verify announcement status
	 * @param announcementName
	 * @param status
	 * 
	 * @throws Throwable
	 */
  public void verifyAnnouncementStatus(String announcementName, String status) throws Throwable{
        try{
               // Verifies Announcement published
               if(status == "Published"){
                     String Announcement_Published = CommonObjects.ReplaceString(CommonObjects.Announcement_Published, "oldString", announcementName);
                     if(elementExists(By.xpath(Announcement_Published))){
                           SuccessReport("Announcement Status", "Announcement :" + announcementName+ " published");
                     }else{
                           failureReport("Announcement Status", "Announcement :" + announcementName+ " not published");
                     }
                     
               }
               // Verifies Announcement Scheduled
               if(status == "Scheduled"){
                     String Announcement_Scheduled = CommonObjects.ReplaceString(CommonObjects.Announcement_Scheduled, "oldString", announcementName);
                     if(elementExists(By.xpath(Announcement_Scheduled))){
                            SuccessReport("Announcement Status", "Announcement :" + announcementName+ " Scheduled");
                     }else{
                            failureReport("Announcement Status", "Announcement :" + announcementName+ " not Scheduled");
                     }
               }
               // Verifies Announcement Expired
               if(status == "Expired"){
                     String Announcement_Expired = CommonObjects.ReplaceString(CommonObjects.Announcement_Expired, "oldString", announcementName);
                     if(elementExists(By.xpath(Announcement_Expired))){
                            SuccessReport("Announcement Status", "Announcement :" + announcementName+ " Expired");
                     }else{
                            failureReport("Announcement Status", "Announcement :" + announcementName+ " not Expired");
                     }
               }
        }catch(Exception e) {
               flag = false;
               e.printStackTrace();
        }
   }
  
 //===========================================================================================================================================================================
                                 
                                                              /*ELM ReUsable Business Functions*/
  
  /***
	 * @Description : Method to search for a course for ELM
	 * @param className
	 * @param classID
	 * @return
	 * @throws Throwable
	 */
	public boolean courseSearchForELM(String className, String classID) throws Throwable {
		boolean flag = true;
		try {
			 elementExists(By.xpath(CommonObjects.Select_A_Course_Icon));
			 if(!configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
				elementSwipe(SwipeElementDirection.UP, 500, 1000);
			 }
			 if (elementExists(By.xpath(CommonObjects.Select_A_Course_Icon))) {
				 elementClick(By.xpath(CommonObjects.Select_A_Course_Icon), "Select A Course Icon");
			 } else {
				   if(configProps.getProperty("deviceType").equals("iOS")){
						explicitWait(iosDriver, By.xpath(AssignmentsObjects.Menu_Bar_Icon));
					}
					else
					{
						//explicitWait(androidDriver, By.xpath(AssignmentsObjects.Menu_Bar_Icon));
						explicitWait(androidDriver1, By.xpath(AssignmentsObjects.Menu_Bar_Icon));
					} 
				elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon), "Menu Bar Icon");
				elementClick(By.xpath(CommonObjects.Select_A_Course_Icon), "Select A Course Icon");
			 }
			 if(elementExists(By.xpath(AssignmentsObjects.Search_Box))){
				 elementSendText(By.xpath(AssignmentsObjects.Search_Box), className, "Select Course Search box");
				 elementClick(By.xpath(AssignmentsObjects.Search_Icon), "Search Icon");
			 }
			 elementClick(By.xpath("//*[contains(@text,'"	+ classID + "')]"), "Selected Class");
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		return flag;
	}
  
  /**
   * @Description : Method to create Announcement for ELM
	 * @param announcementName
	 * @param contentDesc
	 * @param StartTime
	 * @param StartTimeValue
	 * @param EndTime
	 * @param EndTimeValue
	 * @return
	 * @throws Throwable
	 */
public boolean creatingAnnouncementInELM(String announcementName, String contentDesc, boolean StartTime, int StartTimeValue, boolean EndTime, int EndTimeValue ) throws Throwable {
      boolean flag = true;
      try{
            // selectingOption("Announcements", "New Announcement");
          elementClick(By.xpath(AnnouncementObjects.btnNewAnnouncement), "New Annoucement button");   
    	  if(configProps.getProperty("deviceType").equals("iOS")){
         		explicitWait(iosDriver, By.xpath(AnnouncementObjects.HeadLine_Text));
  			 }
  			 else
  			 {
  				//explicitWait(androidDriver, By.xpath(AnnouncementObjects.HeadLine_Text));
  				explicitWait(androidDriver1, By.xpath(AnnouncementObjects.HeadLine_Text));
  			 } 
             elementSendText(By.xpath(AnnouncementObjects.HeadLine_Text), announcementName, "Announcement Head line");
             Thread.sleep(1500);
             switchToFrameByIndex(0);
             elementClick(By.xpath(AnnouncementObjects.Content_Text), "Content Text");
            // elementSendKeys(contentDesc);
             elementSendText(By.xpath(AnnouncementObjects.Content_Text), contentDesc, "Contenet description");
             elementCloseKeyBoard();
             switchToDefaultFrame();
             if(isCheckedNegative(By.xpath(AnnouncementObjects.chkShowStartDate), "Show Start Date")==false)
					elementClick(By.xpath(AnnouncementObjects.chkShowStartDate), "Show Start Date");
             if(configProps.getProperty("deviceType").equals("iOS")){
        		    explicitWait(iosDriver, By.xpath("//*[@text='Now']"));
 			 }
 			 else
 			 {
 				//explicitWait(androidDriver, By.xpath("//*[@text='Now']"));
 				explicitWait(androidDriver1, By.xpath(AnnouncementObjects.btnNow));
 			 } 
             if(elementExists(By.xpath(AnnouncementObjects.btnNow))) {
            	 scrollingToElementofAPage(By.xpath(AnnouncementObjects.btnNow));
                 JSClick(By.xpath(AnnouncementObjects.btnNow), "Now button");
            	 //selectingOption("Date Now Button","Now");
             }
             
             // Change start time
            /* if(StartTime && StartTimeValue != 0){
                   enterStartTime(StartTimeValue);
             }
             // Change end time
             if(EndTime && EndTimeValue != 0){
                   enterEndTime(EndTimeValue);
             }*/
           //  selectingOption("Publish Button","Publish");
             elementClick(By.xpath(AnnouncementObjects.btnPublish), "Button publish");
             Thread.sleep(2000);
			 ExplicitWaitOnElementToBeClickable(By.xpath(AnnouncementObjects.btnNewAnnouncement));
             
      }catch(Exception e) {
             flag = false;
             e.printStackTrace();
      }
      return flag;
 }
  
  /**
 * @Description : Method to Edit Announcement in ELM
 * @param previousAnnouncement
 * @param announcementName
 * @param contentDesc
 * @return
 * @throws Throwable
 */
public boolean editingAnnouncementInELM(String previousAnnouncement,String announcementName, String contentDesc) throws Throwable {
      boolean flag = true;
      try{
    	  	 //elementClick(By.xpath("(//*[@text='"+previousAnnouncement+"']/../..//*[@nodeName='A'])[2]"), "Inverted triangle For Announcement");
    	  Thread.sleep(4000);
    	  JSClick(By.xpath("//a[@title='Actions for "+previousAnnouncement+"']"), "Inverted triangle");
            Thread.sleep(1500);
    	  	elementClick(By.xpath(AnnouncementObjects.Edit_Button), "Edit Button");
             if(configProps.getProperty("deviceType").equals("iOS")){
          		explicitWait(iosDriver, By.xpath(AnnouncementObjects.HeadLine_Text));
   			 }
   			 else
   			 {
   				//explicitWait(androidDriver, By.xpath(AnnouncementObjects.HeadLine_Text));
   				explicitWait(androidDriver1, By.xpath(AnnouncementObjects.HeadLine_Text));
   			 } 
             elementClear(By.xpath(AnnouncementObjects.HeadLine_Text));
             Thread.sleep(1500);
             elementSendText(By.xpath(AnnouncementObjects.HeadLine_Text), announcementName, "Announcement Head line");
             switchToFrameByIndex(0);
             elementSendText(By.xpath(AnnouncementObjects.Content_Text), contentDesc,"Content Text");
             //elementCloseKeyBoard();
             switchToDefaultFrame();
           //  elementSendKeys(contentDesc);
             //selectingOption("Update Button","Update");
             elementClick(By.xpath(AnnouncementObjects.btnUpdate), "Update button");
             Thread.sleep(4000);
             
      }catch(Exception e) {
             flag = false;
             e.printStackTrace();
      }
      return flag;
}

  
  
     /***
   * @Description : Method to verify Announcement created
       * @param className
       * @param classID
       * @return
       * @throws Throwable
       */
       public boolean verifyingAnnouncementCreated(String announcementName) throws Throwable {
              boolean flag = true;
              try {
            	  scrollingToElementofAPage(By.xpath("//*[text()='"+announcementName+"']"));
                     elementExists(By.xpath("//*[text()='"+announcementName+"']"), "Announcement Name");
              } catch (Exception e) {
                     flag = false;
                     e.printStackTrace();
              }
              return flag;
       }
       
       /***
   * @Description : Method to verify Announcement Deleted
        * @param className
       * @param classID
       * @return
       * @throws Throwable
       */
       public boolean verifyingAnnouncementDeleted(String announcementName) throws Throwable {
              boolean flag = true;
              try {
            	     pageRefresh();
            	     flag=isElementDisplayed(By.xpath("//*[text()='"+announcementName+"']"), "Announcement Name");
                     if(flag==true){
                    	 failureReport("Deleted Announcement", "Failed to verify announcement is not deleted"); 
                     } else {
                    	 SuccessReport("Deleted Announcement", "Successfully verified announcement is deleted");
                     }
              } catch (Exception e) {
                     flag = false;
                   //  e.printStackTrace();
              }
              return flag;
       }
  
       /**
       * @Description : Method to login into D2l Classic application
       * @return boolean
       * @param userName - PI or Learner user name
       * @param pwd - PI or Learner password
       * @exception throws Throwable exception
       */
       public boolean elmLogIn(String userName, String password) throws Throwable {
        boolean flag = true;
        try {
              elementSendText(By.xpath(ELMCommonObjects.txtAdminUserName), userName, "Username Textbox");
              Thread.sleep(1500);
              elementSendText(By.xpath(ELMCommonObjects.txtAdminPassword), password, "Password textbox");
              elementClick(By.xpath(ELMCommonObjects.btnAdminLogin), "Login Button");           
              Thread.sleep(3000);
        } catch (Exception e) {
               flag = false;
               e.printStackTrace();
        }
        finally{
               if(flag){
                     SuccessReport("Login", "Login successfull");
               } else {
                     failureReport("Login", "Failed to login");
               }
        }
        return flag;
   }

     /**
     * @Description : Method to delete Announcement in ELM
     * @param announcement
     * @return
     * @throws Throwable
     */
    public boolean deletingAnnouncementInELM(String announcement) throws Throwable {
           boolean flag = true;
           try{
                //  elementClick(By.xpath("(//*[@text='"+announcement+"']/../..//*[@nodeName='A'])[2]"), "Inverted triangle For Announcement");
              scrollingToElementofAPage(By.xpath("//a[@title='Actions for "+announcement+"']"));
        	   JSClick(By.xpath("//a[@title='Actions for "+announcement+"']"),"Announcement inverted triangle");
               Thread.sleep(1500);
               elementClick(By.xpath(AnnouncementObjects.ddDelete), "Delete option");
               Thread.sleep(4000);
               elementClick(By.xpath(AnnouncementObjects.Yes_Button), "Yes button");
        	   /*if(elementExists(By.xpath(AnnouncementObjects.Delete_Button))){
                  elementClick(By.xpath(AnnouncementObjects.Delete_Button), "Delete Button");
                  } else {
                	  selectingOption("Delete", "Delete");
                  }*/
               Thread.sleep(5000);
            }catch(Exception e) {
                   flag = false;
                   e.printStackTrace();
            }
            return flag;
      }
       
       /*public void selectingRadioButtonForCompetency(String rubricName) throws Throwable{
           try{
        	     if(elementExists(By.xpath("//*[@text='"+rubricName+"']"), "Rubrics for assignment")){
     	    	    elementClick(By.xpath("//*[@text='"+rubricName+"']"), "Rubrics for assignment"); 
     	    	    elementClick(By.xpath("//*[@nodeName='LABEL' and @text='Criterion 1']/../..//*[@nodeName='INPUT' and @id='z_h']"), "Radio button for Competency");
     	    	    selectingOption("Rubrics", "Save & Record");
        	     }
           }catch(Exception e) {
                  flag = false;
                  e.printStackTrace();
           }
    }
*/
     /**
     * @Description : Method to enter score and feedback
     * @param Score
     * @param feedback
     * @throws Throwable
     */
    public void enterScoreFeedback(String Score, String feedback) throws Throwable{
           try{
        	     
                  elementSendText(By.xpath(GradeObjects.Score), Score, "Score");
                  elementSwipe(SwipeElementDirection.DOWN, 300, 1000);
                  elementClick(By.xpath(GradeObjects.Feedback), "FeedBack");
                  elementSendKeys(feedback);
                  elementCloseKeyBoard();
                  if(elementExists(By.xpath("//*[@text='Publish']"), "Publish")){
                	 elementSwipeWhileNotFound(By.xpath("//*[@text='Publish']"), 100, SwipeElementDirection.DOWN, 20000, 1000, 30, false);
                     selectingOption("publish", "Publish");
                  } else {
                	 selectingOption("update", "Update");
                  }
           }catch(Exception e) {
                  flag = false;
                  e.printStackTrace();
           }
    }

	/**
	 * @Description : Method to verify FeedBack Is Present For Assignment
	 * @param assignmentName
	 * @throws Throwable
	 */
	public void verifyFeedBackIsPresentForAssignment(String assignmentName) throws Throwable{
		try{
			String FeedbackView = AssignmentsObjects.ReplaceString(AssignmentsObjects.FeedbackView, "oldString", assignmentName);
			if(elementExists(By.xpath(FeedbackView))){
                SuccessReport("Feedback", "Feedback is seen for assingment:" + assignmentName);
			} else {
                failureReport("Feedback", "Feedback is not seen for assingment:" + assignmentName);
			}
		}catch(Exception e) {
			flag = false;
			e.printStackTrace();
		}
	}
	
	/**
     * @Description : Method to login into D2l Classic application
     * @return boolean
     * @param userName - PI or Learner user name
     * @param pwd - PI or Learner password
     * @exception throws Throwable exception
     */
     public boolean newChatCreation(String chatTitle, String chatDescription) throws Throwable {
      boolean flag = true;
      try {
            elementClick(By.xpath(ELMChatObjects.New_Chat_Button), "New Chat Button");
            Thread.sleep(1500);
            elementSendText(By.xpath(ELMChatObjects.Chat_Title), chatTitle, "Chat Title Textbox");
            elementClick(By.xpath(ELMChatObjects.General_Chat_Radio_Button), "General Chat Radio Button");
          //  elementSwipe(SwipeElementDirection.DOWN, 300, 1000);
            switchToFrameByIndex(0);
            scrollingToElementofAPage(By.xpath(ELMChatObjects.Chat_Description));
            elementClick(By.xpath(ELMChatObjects.Chat_Description), "Chat Description textbox");
           // elementSendKeys(chatDescription);
            elementSendText(By.xpath(ELMChatObjects.Chat_Description), chatDescription, "Description text");
            elementCloseKeyBoard(); 
            switchToDefaultFrame();
            elementClick(By.xpath(ELMChatObjects.Chat_Create_Button), "Create Button");
            //elementClick(By.xpath(ELMChatObjects.Chat_Create_Button), "Create Button"); 
            Thread.sleep(3000);
      } catch (Exception e) {
             flag = false;
             e.printStackTrace();
      }
      return flag;
 }
     /***
      * @Description : Method for clicking On Chat
       * @param className
      * @return
      * @throws Throwable
      */
      public boolean clickingOnChat(String name) throws Throwable {
             boolean flag = true;
             try {  
                   // elementSwipeWhileNotFound(By.xpath("//*[@text='"+name+"']"), 100, SwipeElementDirection.DOWN, 20000, 1000, 30, false);
                 scrollingToElementofAPage(By.xpath("//*[text()='"+name+"']"));   
            	 elementExists(By.xpath("//*[text()='"+name+"']"),"Created Chat");
                 elementClick(By.xpath("//*[text()='"+name+"']"),"Created Chat");
                 Thread.sleep(3000);
             } catch (Exception e) {
                    flag = false;
                    e.printStackTrace();
             }
             return flag;
      }
      /***
       * @Description : Method for sending message details
        * @param className
       * @return
       * @throws Throwable
       */
       public boolean sendingMessageDetails(String textMessage) throws Throwable {
              boolean flag = true;
              try {    
            	       //elementSwipe(SwipeElementDirection.DOWN, 200, 1000);
                       JSClick(By.xpath(ELMChatObjects.Text_Message_Area), "Text Message Area");
                       Thread.sleep(4000);
                       elementSendText(By.xpath(ELMChatObjects.Text_Message_Area), textMessage, "Text Message Area");
                     //  elementSwipe(SwipeElementDirection.DOWN, 200, 1000);
                   //    elementSwipe(SwipeElementDirection.RIGHT, 200, 1000);
                       Thread.sleep(4000);
                       scrollingToElementofAPage(By.xpath(ELMChatObjects.Send_Button));
                       JSClick(By.xpath(ELMChatObjects.Send_Button), "Send Button");
              } catch (Exception e) {
                     flag = false;
                     e.printStackTrace();
              }
              return flag;
       }
       /***
       * @Description : Method for validating Message In Chat
        * @param className
       * @return
       * @throws Throwable
       */
       public boolean validatingMessageInChat(String textMessage) throws Throwable {
              boolean flag = true;
              try {  
                     //elementExists(By.xpath(ELMChatObjects.Text_Message),"Chat Message");
                    // String chatText = elementGetText(By.xpath(ELMChatObjects.Text_Message), "Chat Message");
            	  String chatText = elementGetText(By.xpath("//span[text()='"+textMessage+"']"), "Chat Message");
            	  Thread.sleep(1500);
            	  if(textMessage.equalsIgnoreCase(chatText)){
                           SuccessReport("Chat Message", "Successfully verified chat message"  +textMessage);
                     } else {
                           failureReport("Chat Message", "Failed to verify chat message");
                     }
              } catch (Exception e) {
                     flag = false;
                     e.printStackTrace();
              }
              return flag;
       }

	/**
	 * @Description : Method for validating Alert in ELM
	 * @param alertMessage
	 * @throws Throwable
	 */
	public void verifyAlertELM(String alertMessage) throws Throwable{
		try{
			// Expanding Alerts
			if(!elementExists(By.xpath(ELMAlertsObjects.ExpandedAlert))){
				elementClick(By.xpath(ELMAlertsObjects.AlertsExpand), "Inverted Triangle beside Alerts");
				selectingOption("Expand this widget", "Expand this widget");
			}
			// Validating Alert message
			String AlertMessageNewAssignmentSubmissions = ELMAlertsObjects.ReplaceString(ELMAlertsObjects.AlertMessageNewAssignmentSubmissions, "oldString", alertMessage);
			if(elementExists(By.xpath(AlertMessageNewAssignmentSubmissions), "Alert message")){
				 SuccessReport("Alert message", "Alert message is seen for assingment submission" + alertMessage);
			} else {
                failureReport("Alert message", "Alert message is not seen for assingment submission" + alertMessage);
			}
		}catch(Exception e) {
			flag = false;
			e.printStackTrace();
		}
	}
	
	/**
	 * @Description : Method for validating FeedBack Present To Assignment
	 * @param assignmentName
	 * @throws Throwable
	 */
	public void verifyFeedBackPresentToAssignment(String assignmentName) throws Throwable{
		try{
			String FeedbackLinkBesideAssignment = ELMAlertsObjects.ReplaceString(ELMAlertsObjects.FeedbackLinkBesideAssignment, "oldString", assignmentName);
			if(elementExists(By.xpath(FeedbackLinkBesideAssignment), "FeedBack beside Assignment")){
				 SuccessReport("Feedback View", "Feedback is seen for assingment submission");
			} else {
                failureReport("Feedback View", "Feedback is not seen for assingment submission");
			}
			
		}catch(Exception e) {
			flag = false;
			e.printStackTrace();
		}
	}
	
	/**
	* @Description:This method is used to verify the Faculty Notes
	* @return
	* @throws Throwable
	*/
	public boolean verifyFacultyNotes() throws Throwable {
	boolean flag = true;
	try {
	      //elementContext("NATIVE_APP_NON_INSTRUMENTED");
		Thread.sleep(2000);
		waitForVisibilityOfElement(By.xpath("//iframe[contains(@class,'d2l-iframe')]"), "Wait until Frame loaded");
		
		switchToFrameByIndex(0);
	      elementExists(By.xpath(ELMFacultyNotesObjects.txtFacultyNotes),"Faculty Notes");
	     // elementContext("WEBVIEW_1");
	} catch (Exception e) {
	flag = false;
	e.printStackTrace();
	}
	return flag;
	}	
	
	
	/**
	 * Used to Wait For Processing in PSFT
	 * 
	 * @throws Throwable
	 */
	public void waitForProcessing() throws Throwable {
		try {
			if(isElementPresentNegative(CommonObjects.processingInPsft, "Processing")==true)
			{
			waitForInVisibilityOfElement(CommonObjects.processingInPsft, "Processing");
			}
		} catch (Exception e) {
			failureReport("Processing", "Processing not Completed");
			e.printStackTrace();
		}
	}
	
	/**
	* @Description:This method is used to select a particular project, particular project step and as well as Topic
	* @param project
	* @param ProjectName
	* @param Step
	* @param StepNumber
	* @param Topic
	* @param TopicName
	* @throws Throwable
	*/
	public void selectProject(boolean project, String ProjectName, boolean Step, String StepNumber, boolean Topic, String TopicName)
	throws Throwable {

	String Project_Name = ELMProjectObjects.ReplaceString(
	ELMProjectObjects.Project_Name, "oldString", ProjectName);
	String Step_Number = ELMProjectObjects.ReplaceString(
	ELMProjectObjects.Step_Number, "oldString", StepNumber);
	String Topic_Name = ELMProjectObjects.ReplaceString(
	ELMProjectObjects.Topic_Name, "oldString", TopicName);

	System.out.println(Project_Name + " " + Step_Number + " " + Topic_Name);

	if (project = true) {
	elementContext("NATIVE_APP_NON_INSTRUMENTED");
	elementClick(By.xpath(Project_Name), "Select Project");
	}
	if (Step = true) {
	elementClick(By.xpath(ELMProjectObjects.Project_Menu),
	"ProjectMenu");
	elementClick(By.xpath(Project_Name), "Select Project");
	elementClick(By.xpath(Step_Number), "Select Project Step");
	}
	if (Topic = true) {
	elementClick(By.xpath(Topic_Name), "Select Topic");
	}
	}


	/**
	* @Description:This method is used to verify discussion link is available or not
	* @param TopicName
	* @throws Throwable
	*/
	public void verifyDiscussionlink(String TopicName) throws Throwable {
	String Topic_Name = ELMProjectObjects.ReplaceString(ELMProjectObjects.Topic_Name, "oldString", TopicName);
	elementContext("NATIVE_APP_NON_INSTRUMENTED");
	elementSwipeWhileNotFound(By.xpath(Topic_Name), 100,
	SwipeElementDirection.DOWN, 2000, 1000, 30, false);
	elementExists(By.xpath(Topic_Name), "Discussion Link");
	}

	
	/**
	* @Description:This method is used to create the particular event and edit the event dates
	* @param EventTitle
	* @param Description
	* @param Location
	* @throws Throwable
	*/
	public void createEventELM(String EventTitle, String Description, String Location) throws Throwable {
	
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.CreateEvent));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.CreateEvent));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.CreateEvent));
	} 
	elementClick(By.xpath(CalendarObjects.CreateEvent), "Create Event");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.EventTitle));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.EventTitle));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.EventTitle));
	} 
	elementSendText(By.xpath(CalendarObjects.EventTitle), EventTitle,"Event Title");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.DescriptionText));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.DescriptionText));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.DescriptionText));
	} 
	elementClick(By.xpath(CalendarObjects.DescriptionText),
	"Description Text");
	elementSendKeys(Description);
	elementCloseKeyBoard();
	elementClick(By.xpath(CalendarObjects.AttendeeId), "Attendee Id");
	elementContext("NATIVE_APP");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.Select_AttendeeId));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.Select_AttendeeId));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.Select_AttendeeId));
	} 
	elementClick(By.xpath(CalendarObjects.Select_AttendeeId),
	"Select AttendeeId");
	elementContext("WEBVIEW_1");
	elementClick(By.xpath(CalendarObjects.AllDay), "");
	elementSendText(By.xpath(CalendarObjects.Location), Location, "Location");
	elementClick(By.xpath(CalendarObjects.CreateButton), "Create Button");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.SearchEvents));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.SearchEvents));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.SearchEvents));
	} 
	elementSendText(By.xpath(CalendarObjects.SearchEvents), EventTitle,	"Event Title");
	elementClick(By.xpath(CalendarObjects.SearchButton), "Search Button");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.ClickEvent));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.ClickEvent));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.ClickEvent));
	} 
	elementClick(By.xpath(CalendarObjects.ClickEvent), "Event");
	elementContext("WEBVIEW_1");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath("//*[@text='" + EventTitle + "']/..//a[1]"));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath("//*[@text='" + EventTitle + "']/..//a[1]"));
		explicitWait(androidDriver1, By.xpath("//*[@text='" + EventTitle + "']/..//a[1]"));
	} 
	elementClick(By.xpath("//*[@text='" + EventTitle + "']/..//a"),
	"clicking on event edit");
	selectingOption(EventTitle, "Edit Event");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.DescriptionText));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.DescriptionText));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.DescriptionText));
	} 
	elementClick(By.xpath(CalendarObjects.DescriptionText),
	"Description Text");
	elementSendKeys(EventTitle + "Description modified please check");
	elementCloseKeyBoard();
	elementSendText(By.xpath(CalendarObjects.TestEndDate),
	addDaysToCurrentDate(1), "Event EndDate");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.SaveButton));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.SaveButton));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.SaveButton));
	} 
	elementClick(By.xpath(CalendarObjects.SaveButton), "Save Button");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.SearchEvents));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.SearchEvents));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.SearchEvents));
	} 
	elementSendText(By.xpath(CalendarObjects.SearchEvents), EventTitle,	"Event Title");
	elementClick(By.xpath(CalendarObjects.SearchButton), "Search Button");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.ClickEvent));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.ClickEvent));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.ClickEvent));
		
	} 
	//explicitWait(driver, By.xpath(CalendarObjects.ClickEvent));
	elementClick(By.xpath(CalendarObjects.ClickEvent), "Event");
	}


	/**
	* @Description:This method is used to delete the particular event in ELM
	* @param EventTitle
	* @throws Throwable
	*/
	public void deleteEventELM(String EventTitle) throws Throwable {
	elementContext("WEBVIEW_1");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath("(//*[@text='" + EventTitle + "']/..//a)[1]"));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath("(//*[@text='" + EventTitle + "']/..//a)[1]"));
		//explicitWait(androidDriver1, By.xpath("(//*[text()='" + EventTitle + "']/..//a)[1]"));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.SearchEvents));
	} 
	//explicitWait(driver, By.xpath("(//*[@text='" + EventTitle + "']/..//a)[1]"));
	//elementClick(By.xpath("//*[@text='" + EventTitle + "']/..//a"),"clicking on event edit");
	//selectingOption(EventTitle, "Delete Event");
	elementSendText(By.xpath(CalendarObjects.SearchEvents), EventTitle, "Event Title");
    elementClick(By.xpath(CalendarObjects.SearchButton), "Search Button");
    elementHardSleep(4000);
    explicitWait(androidDriver1,By.xpath("//div[text()='"+EventTitle+"']"));
    elementClick(By.xpath("//div[text()='"+EventTitle+"']"), "Event Title");
    Thread.sleep(3000);
    elementClick(By.xpath("//a[@title='Actions for: "+EventTitle+"']"), "Inverted traingle beside event");
    Thread.sleep(2000);
    elementClick(By.xpath(CalendarObjects.ddDelete), "Delete Event");
    elementClick(By.xpath(CalendarObjects.YesButton), "Yes button");
    Thread.sleep(1000);
    elementExists(By.xpath(CalendarObjects.msgDeletedSuccessfully));
	}
	
	/**
	* @Description:This method is used to delete the particular event
	* @param EventTitle
	* @throws Throwable
	*/
	public void deleteEvent(String EventTitle) throws Throwable {
		if(configProps.getProperty("deviceType").equals("iOS")){
			explicitWait(iosDriver, By.xpath(CalendarObjects.SearchEvents));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(CalendarObjects.SearchEvents));
			explicitWait(androidDriver1, By.xpath(CalendarObjects.SearchEvents));
		} 
	
	elementSendText(By.xpath(CalendarObjects.SearchEvents), EventTitle,	"Event Title");
	elementClick(By.xpath(CalendarObjects.SearchButton), "Search Button");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath(CalendarObjects.ClickEvent));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath(CalendarObjects.ClickEvent));
		explicitWait(androidDriver1, By.xpath(CalendarObjects.ClickEvent));
	} 
	
	elementClick(By.xpath(CalendarObjects.ClickEvent), "Event");
	elementContext("WEBVIEW_1");
	if(configProps.getProperty("deviceType").equals("iOS")){
		explicitWait(iosDriver, By.xpath("(//*[@text='" + EventTitle + "']/..//a)[1]"));
	}
	else
	{
		//explicitWait(androidDriver, By.xpath("(//*[@text='" + EventTitle + "']/..//a)[1]"));
		explicitWait(androidDriver1, By.xpath("(//*[@text='" + EventTitle + "']/..//a)[1]"));
	} 
	
	elementClick(By.xpath("//*[@text='" + EventTitle + "']/..//a"),
	"clicking on event edit");
	selectingOption(EventTitle, "Delete Event");
	elementClick(By.xpath(CalendarObjects.YesButton), "Yes Button");
	}
	
	/***
     * @Description : Method for verifying Syllabus Details In Syllabus Page
     * @param className
     * @param classID
     * @return
     * @throws Throwable
     */
     public boolean verifyingSyllabusDetailsInSyllabusPage(String courseSearch) throws Throwable {
            boolean flag = true;
            try {
            	
            	switchToFrameByIndex(0);
            	waitForVisibilityOfElement(By.xpath("//div[@id='syllabus']"), "Syllabus Page Load");
            	Thread.sleep(2000);
            	// elementSwipe(SwipeElementDirection.DOWN, 200, 1000);
            	 if(elementExists(By.xpath("//h1[text()='"+courseSearch+"']"), "Syllabus Course title")){
            		 scrollingToElementofAPage(By.xpath("//h1[text()='"+courseSearch+"']"));
            		 SuccessReport("Syllabus Course title", "Successfully verified Syllabus Course Title");
                 } else {
                     failureReport("Syllabus Course title", "Failed to verify Syllabus Course Title");
            	 }
            	 Thread.sleep(1000);
            	 if(elementExists(By.xpath("//div[@class='syl-item syl-item-l1 clearfix']//h2[contains(text(),'Faculty Contact')]"))) {
            		 scrollingToElementofAPage(By.xpath("//div[@class='syl-item syl-item-l1 clearfix']//h2[contains(text(),'Faculty Contact')]"));
            		 SuccessReport("Syllabus Course title", "Successfully verified faculty contact in Syllabus");
            	 }
            	 else
            	 {
            		 failureReport("Syllabus Course title", "Failed to verify faculty contact");
            	 }
            	/* if (isElementPresentNegative(By.xpath(ELMSyllabusObjects.Faculty_Information), "Syllabus Course title")) {
     				scrollingToElementofAPage(By.xpath(ELMSyllabusObjects.Faculty_Information));
     				SuccessReport("Contact Information", "User able view Contact Information");
            	 
            	 }else {
                     failureReport("Syllabus Course title", "Failed to verify Syllabus Course Title");
            	 }
            	 switchToDefaultFrame();*/
            }
            	 catch (Exception e) {
                   flag = false;
                   e.printStackTrace();
            }
            return flag;
     }

     public boolean verifyingMenuOptionsInTheNavigationBar(String Faculty, String Learner) throws Throwable{
    	 boolean flag = true;
    	 	try {
    	 		if(!(isElementPresentNegative(AssignmentsObjects.LinkTextNameInMenu("Course Home"), "Course Home"))) {
    	    		 Thread.sleep(1500);
    	             elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Menu Bar");
    	    	 }
    	 		
    	 		if(Faculty != null && !Faculty.trim().isEmpty()) {
    	 			
    	 		elementExists(AssignmentsObjects.LinkTextNameInMenu("Course Home"), "Course Home");
        	 	elementExists(AssignmentsObjects.LinkTextNameInMenu("Syllabus"), "Syllabus");
        	 	elementExists(AssignmentsObjects.LinkTextNameInMenu("Projects"), "Projects");
        	 	elementExists(AssignmentsObjects.d2LmenuItemtextName("My Tools"), "Projects");
        	 	elementExists(AssignmentsObjects.d2LmenuItemtextName("Resources"), "Resources");
        	 	elementExists(AssignmentsObjects.d2LmenuItemtextName("Help"), "Help");
        	 	elementExists(AssignmentsObjects.d2LmenuItemtextName("Instructor Tools"), "Instructor Tools");
        	 	elementExists(AssignmentsObjects.d2LmenuItemtextName("Admin Tools"), "Admin Tools");
        	 	
    	 	}
    	 	
    	 		
    	 		if(Learner != null && !Learner.trim().isEmpty()) {
        	 		
        	 		elementExists(AssignmentsObjects.LinkTextNameInMenu("Course Home"), "Course Home");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMenu("Syllabus"), "Syllabus");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMenu("Projects"), "Projects");
            	 	elementExists(AssignmentsObjects.d2LmenuItemtextName("My Tools"), "Projects");
            	 	elementExists(AssignmentsObjects.d2LmenuItemtextName("Resources"), "Resources");
            	 	elementExists(AssignmentsObjects.d2LmenuItemtextName("Help"), "Help");
            	 	
        	 	}
    	 		
    	 		if(elementExists(AssignmentsObjects.d2LmenuItemtextName("My Tools"), "My Tools")) {
        	 		elementClick((AssignmentsObjects.d2LmenuItemtextName("My Tools")), "Clicked on My Tools To Validate My Tools Link");
        	 		Thread.sleep(2000);
        	 		
        	 	if(!(isElementPresentNegative(AssignmentsObjects.LinkTextNameInMenu("Discussions"), "Discussions"))) {
    	    		 Thread.sleep(1500);
    	             elementClick(AssignmentsObjects.d2LmenuItemtextName("My Tools"), "Clicked on My Tools to Validate My Tools Link");
    	    	 }
        	 		elementExists(AssignmentsObjects.LinkTextNameInMyTool("Discussions"), "Discussions");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Assignments"), "Assignments");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Groups"), "Groups");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Grades"), "Grades");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Chat"), "Chat");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Locker"), "Locker");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Class Progress"), "Class Progress");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Competencies"), "Competencies");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Classlist"), "Classlist");
            	 	elementExists(AssignmentsObjects.LinkTextNameInMyTool("Course Evaluations"), "Course Evaluations");
        	 		
        	 	}
        	 	    	 		
    	 		elementClick(AssignmentsObjects.LinkTextNameInMyTool("Discussions"), "Discussions");
    	 		Thread.sleep(3000);
    	 		
    	 	}
    	 catch(Exception ex) {
    		 flag = false;
    		 ex.printStackTrace();
    	 }
			return flag;
     }
     
     public void verifyingCourseTitle_Banner_TitleOnBanner(String courseTitle) throws Throwable {
 		try {
 			isElementPresent(By.xpath("(//div[@class='d2l-navigation-s-header-logo-area'])[1]"), "course title");
 			Thread.sleep(3000);
 			isElementPresent(By.xpath(ELMProjects.bannerImg), courseTitle);
 			isElementPresent(By.xpath(ELMProjects.titleOnBanner(courseTitle)), courseTitle);
 		} catch (Exception e) {
 			e.printStackTrace();
 			failureReport("Course Title and Banner", "Failed to verify Course title and Banner on the page");
 		}
 	}
     
     
     
     /***
      * @Description : Method for verifying Faculty In formation In Sy	llabus Page
      * @param className
      * @param classID
      * @return
      * @throws Throwable
      */
      public boolean verifyingFacultyInformationInSyllabusPage() throws Throwable {
             boolean flag = true;
             String facultyInformation = null;
             try {
            	  
            	 
            	 /*if(elementExists(By.xpath(ELMSyllabusObjects.Faculty_Information), "Faculty Information")){
            		  SuccessReport("Faculty Information", "Successfully verified Faculty Information in syllabus");
                  } else {
                      failureReport("Faculty Information", "Failed to verify Faculty Information in syllabus");
            	  }*/
            	 if(isElementPresentNegative(By.xpath(ELMSyllabusObjects.Faculty_Information), "Syllabus Course title")) {
      				
            		scrollingToElementofAPage(By.xpath(ELMSyllabusObjects.Faculty_Information));
            		facultyInformation = getText(By.xpath(ELMSyllabusObjects.Faculty_InformationLikeMailId), "Faculty Information");
      				SuccessReport("Contact Information", "User able view Contact Information and inforamation is :" +  facultyInformation);
             	 
             	 }else {
                      failureReport("Syllabus Course title", "Failed to verify Syllabus Course Title");
             	 }
            	 Thread.sleep(2000);

             } catch (Exception e) {
                    flag = false;
                    e.printStackTrace();
             }
             return flag;
      }
      
      /**
       * @Description : Method for verifying Course Introduction Present
       * Method is used to expand if Course Introduction is not in expand mode and
       * verifies information is present under Course Introduction heading in course home page
       * @throws Throwable
       */
       public void verifyCourseIntroductionPresent() throws Throwable{
              try{
            	  
            	  Thread.sleep(1000);
            	  switchToDefaultFrame();
            	  waitForVisibilityOfElement(By.xpath("//h2[text()='Course Introduction']"), "Course Introduction");
            	  scrollingToElementofAPage(By.xpath(ELMProjects.InvertedTriangleBesideCourseIntroduction));
                  elementClick(By.xpath(ELMProjects.InvertedTriangleBesideCourseIntroduction), "Inverted triangle beside Course Introduction");
                     Thread.sleep(1000);
                     
                     if(isElementDisplayed(By.xpath(ELMProjects.ddCourseIntroductionExpandWidget),"Expand widget dropdown")){
                        JSClick(By.xpath(ELMProjects.ddCourseIntroductionExpandWidget), "Expand Course Introduction");
                     }
                     
                    /* if(isElementPresentNegative(By.xpath(ELMProjects.ddCourseIntroductionExpandWidget),"Expand widget dropdown")){
                         JSClick(By.xpath(ELMProjects.ddCourseIntroductionExpandWidget), "Expand Course Introduction");
                      }*/
                    
                     
                    /* if(isElementPresentNegative(By.xpath(ELMProjects.ddCourseIntroductionExpandWidget),"Expand widget dropdown")){
                    	 JSClick(By.xpath(ELMProjects.ddCourseIntroductionExpandWidget), "Expand Course Introduction");
                     }*/
                     
                     else {
                    	 elementClick(By.xpath(ELMProjects.courseIntroductionText), "click on course Introduction Text to close Arrow button");
                         Thread.sleep(1000);
                     }
                     //switchToFrameByLocator(By.xpath(ELMProjects.iframeCourseIntroduction), "course Introduction iframe");
                    // switchToFrameByIndex(0);
                     if(isElementDisplayed(By.xpath(ELMProjects.infoCourseIntroduction),"")){
                    	 scrollingToElementofAPage(By.xpath(ELMProjects.infoCourseIntroduction));
                    	 SuccessReport("Course Introduction", "Course Introduction information is seen in Course home page");
                     } else {
                         failureReport("Course Introduction", "Course Introduction information is not seen in Course home page");
                     }
                     
                     switchToDefaultFrame();
                    
              }
              catch(Exception e) {
                     //flag = false;
                     e.printStackTrace();
              }
       }
       
       /**
        * @Description : Method is used to expand if Course Introduction is not in expand mode and
        * verifies information is present under Course Introduction heading in course home page
        * @throws Throwable
        */
        public void verifyCourseIntroductionDefaultMode() throws Throwable{
               try{
                    elementClick(By.xpath(ELMProjects.InvertedTriangleBesideCourseIntroduction), "Inverted triangle beside Course Introduction");
                    if(elementExists(By.xpath(ELMProjects.CollapseThisWidget), "Collapse This Widget")){
                    	  SuccessReport("Course Introduction", "Course Introduction is in expanded mode by default in Course home page");
                    } else {
                          failureReport("Course Introduction", "Course Introduction is not in expanded mode by default in Course home page");
                    }
                 
                    elementExists(By.xpath(ELMProjects.CollapseThisWidget), "Collapse This Widget");                    
               }catch(Exception e) {
                      flag = false;
                      e.printStackTrace();
               }
        }
       
      /**
       * @Description : Method is used to expand if Course Introduction is not in expand mode and
       * Verifies projects are present under projects heading in course home page
       * @throws Throwable
       */
       public void verifyProjectsPresent() throws Throwable{
    	   try{
            	  
            	  /*
                     elementClick(By.xpath(ELMProjects.InvertedTriangleBesideProjects), "Inverted triangle beside Projects");
                     if(elementExists(By.xpath(ELMProjects.ExpandThisWidget))){
                           elementClick(By.xpath(ELMProjects.ExpandThisWidget), "Expand Projects");
                     }
                     if(elementExists(By.xpath(ELMProjects.ProjectsInCourseHomePage))){
                           SuccessReport("Projects", "Projects seen in Course home page");
                     } else {
                           failureReport("Projects", "Projects not seen in Course home page");
                     }
              */
    		   	waitForVisibilityOfElement(By.xpath("//div[contains(@class,'homepage-container')]"), "Home Page");
    		   Thread.sleep(1000);
    		   	switchToDefaultFrame();
    		   	waitForVisibilityOfElement(By.xpath("//h2[text()='Projects']"), "Projects");
            	scrollingToElementofAPage(By.xpath(ELMProjects.lblProjects));
            	elementClick(By.xpath(ELMProjects.invertedTriangleBesideProject), "Inverted traingle beside projects");
            	Thread.sleep(3000);
            	if(isElementDisplayed(By.xpath(ELMProjects.ddProjectExpandWidget), "Expand widget"))
            	{
            		elementClick(By.xpath(ELMProjects.ddProjectExpandWidget), "Expand this widget");
            	}
            	
            	else {
            		elementClick(By.xpath(ELMProjects.projectsTextwithArrow), "Click on Projects to close Arrow button");
            	}
            	scrollingToElementofAPage(By.xpath(ELMProjects.iframeProjectsHomePage));
      			switchToFrameByLocator(By.xpath(ELMProjects.iframeProjectsHomePage), "");
            	//switchToFrameByIndex(0);
      			//switchToFrameByLocator(By.xpath(ELMProjects.iframeProjectsHomePage), "");
      			Thread.sleep(2000);
      			scrollingToElementofAPage(By.xpath("//body[@class='course-homepage']//h3[1]"));
      			elementExists(By.xpath("//body[@class='course-homepage']//h3[1]"));
      			switchToDefaultFrame();
      			
              }catch(Exception e) {
                     e.printStackTrace();
              }
       }
       
       /***
        * @Description : Method for validating Newly Updated Scores Feedback For Assignment
        * @param menuItem
        * @return
        * @throws Throwable
        */
        public boolean validatingNewlyUpdatedScoresFeedbackForAssignment(String assignmentName, String score, String feedback) throws Throwable {
               boolean flag = true;
               try {
	            	elementSwipeWhileNotFound(By.xpath("//*[@text='"+assignmentName+"']"), 100, SwipeElementDirection.DOWN, 2000, 1000, 5, false);
	           		elementClick(By.xpath("//*[@nodeName='STRONG' and ./parent::*[./parent::*[./preceding-sibling::*[./*[./*[./*[@text='"+assignmentName+"']]]]]]]"), "Click on View for Assignment");
	           		elementSwipe(SwipeElementDirection.DOWN, 200, 1000);
                     
	           		String updatedScore = elementGetText(By.xpath("//*[@text='Exceeds Project Requirements']"), "Updated Score");
	           		String updatedFeedback = elementGetText(By.xpath("//*[@text='FeedBack Updated']"), "Updated feedback");
	           		
                    if(!score.equals(updatedScore)){
                    	  SuccessReport("Updated Score", "Successfully verified score is updated for an assignment");
                    } else {
                    	  failureReport("Updated Score", "Failed to score is not updated for an assignment");
                    }
                    
                    if(!feedback.equals(updatedFeedback)){
                  	  SuccessReport("Updated feedback", "Successfully verified feedback is updated for an assignment");
                    } else {
                  	  failureReport("Updated feedback", "Failed to feedback is not updated for an assignment");
                    }
                    
                    selectingOption("Assignment", "Done");
               } catch (Exception e) {
                      flag = false;
                      e.printStackTrace();
               }
               return flag;
        }
        
        /***
         * @Description :  Method for clicking On Menu
         * @param menuItem
         * @return
         * @throws Throwable
         */
         public boolean clickingOnMenu() throws Throwable {
                boolean flag = true;
                try { 
                	 if(elementExists(By.xpath(AssignmentsObjects.Menu_Bar_Icon), "Menu Bar")){
                        elementClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon), "Menu Bar");
                     }
                } catch (Exception e) {
                    flag = false;
                    e.printStackTrace();
             }
             return flag;
      } 
                
         /***
          * @Description : Method for clicking On Menu Item
          * @param menuItem
          * @return
          * @throws Throwable
          */
          public boolean clickingOnMenuItem(String courseSearch) throws Throwable {
                 boolean flag = true;
                 try { 
                 	 if(elementExists(By.xpath("//*[@text='"+courseSearch+"' and @nodeName='A']"), "Course title")){
                        elementClick(By.xpath("//*[@text='"+courseSearch+"']"), "Course title");
                      }
                 } catch (Exception e) {
                     flag = false;
                     e.printStackTrace();
              }
              return flag;
       } 

       /*** 
        * @Description : Method for validating Menu Item
        * @param menuItem
        * @return
        * @throws Throwable
        */
        public boolean validatingMenuItem(String menuItem) throws Throwable {
               boolean flag = true;
               try {                      
                      if(elementExists(By.xpath("//*[@text='"+menuItem+"']"), menuItem)){
                    	  SuccessReport("Menu Item", "Successfully verified "+menuItem+" in Course home page");
                      } else {
                    	  failureReport("Menu Item", "Failed to verify "+menuItem+" in Course home page");
                      }
               } catch (Exception e) {
                      flag = false;
                      e.printStackTrace();
               }
               return flag;
        }
        
        /***
    	 * @Description : Method for validating Submenu Option
    	 * @param menu
    	 * @param subMenu
    	 * @return
    	 * @throws Throwable
    	 */
    	public boolean validatingSubmenuOption(String menu, String subMenu)	throws Throwable {
    		boolean flag = true;
    		try {
    			 if(configProps.getProperty("deviceType").equals("iOS")){
             		explicitWait(iosDriver, By.xpath("//*[@text='" + subMenu + "']"));
      			 }
      			 else
      			 {
      				//explicitWait(androidDriver, By.xpath("//*[@text='" + subMenu + "']"));
      				explicitWait(androidDriver1,By.xpath("//*[@text='" + subMenu + "']"));
      			 } 
    	         
    			 if(elementExists(By.xpath("//*[@text='"+subMenu+"']"), subMenu)){
               	  SuccessReport("Menu Item", "Successfully verified "+subMenu+" in Course home page");
                 } else {
               	  failureReport("Menu Item", "Failed to verify "+subMenu+" in Course home page");
                 }
    		} catch (Exception e) {
    			flag = false;
    			e.printStackTrace();
    		}
    		return flag;
    	}

    	/***
    	 * @Description : Method for validating Banner Course Title Course Home
    	 * @param menu
    	 * @param subMenu
    	 * @return
    	 * @throws Throwable
    	 */
    	public boolean validatingBannerCourseTitleCourseHome()	throws Throwable {
    		boolean flag = true;
    		try {
    			 if(elementExists(By.xpath(ELMCommonObjects.Course_Banner), "Banner in Course Home")){
               	  SuccessReport("Banner", "Successfully verified Banner in Course home page");
                 } else {
               	  failureReport("Banner", "Failed to verify Banner in Course home page");
                 }
    			 
    			 if(elementExists(By.xpath(ELMCommonObjects.Course_Title), "Title in Course Home")){
                  	  SuccessReport("Course name and Title", "Successfully verified Course name and Title in Course home page");
                 } else {
                 	  failureReport("Course name and Title", "Failed to verify Course name and Title in Course home page");
                 }
    		} catch (Exception e) {
    			flag = false;
    			e.printStackTrace();
    		}
    		return flag;
    	}
    	
    	
    	/**
    	 * @Description : Method for creating Assignment With Learner Objectives ELM
    	 * @param assignmentName
    	 * @param categoryName
    	 * @param gradeName
    	 * @param score
    	 * @param rubricName
    	 * @param instructionsDetails
    	 * @param competencyName
    	 * @param childCompetencyName
    	 * @param learningObjectiveName
    	 * @throws Throwable
    	 */
    	public void creatingAssignmentWithLearnerObjectivesELM(String assignmentName, String categoryName, String gradeName, String score, String rubricName, String instructionsDetails, String competencyName, String childCompetencyName, String learningObjectiveName) throws Throwable{
    		try{
    			 elementClick(By.xpath(AssignmentsObjects.New_Submission_Folder), "New Submission Folder");
    			 elementSendText(By.xpath(AssignmentsObjects.Assignment_Name_TextBox), assignmentName, "Assignment Name");
    			
    			 elementClick(By.xpath(AssignmentsObjects.New_Category_Link),"New Category Link");
    			 elementClick(By.xpath(AssignmentsObjects.Category_Name_TextBox),"Category Name TextBox");
    			 elementSendKeys(categoryName);
    			 elementCloseKeyBoard();
    			 elementClick(By.xpath(AssignmentsObjects.Category_Save_Button),"Category Save Button");
    			 
    			 elementSwipe(SwipeElementDirection.DOWN, 500, 1000);
    			 elementClick(By.xpath(ELMProjects.Grade_Item_Dropdown), "Grade Item");
    			 elementContext("NATIVE_APP");
    			 if(configProps.getProperty("deviceType").equals("iOS")){
              		iosDriver.drag(By.xpath(ELMCommonObjects.Competency_Status_Options), 0, -140);
       			 }
       			 else
       			 {
       				androidDriver.drag(By.xpath(ELMCommonObjects.Competency_Status_Options), 0, -140);
       				//androidDriver1.drag(By.xpath(ELMCommonObjects.Competency_Status_Options), 0, -140);
       				
       				
       			 } 
    			
    			 selectingOption("Done", "Done");
    			 elementContext("WEBVIEW_1");
    			 
    			 elementSwipe(SwipeElementDirection.DOWN, 500, 1000);
    			 elementSendText(By.xpath(AssignmentsObjects.Out_Of_Box_Score), score, "Out of Box Score");
    			 
    			 selectingOption("Add Rubric", "Add Rubric");
    			 if(configProps.getProperty("deviceType").equals("iOS")){
              		iosDriver.findElement(By.xpath(ELMAssignmentObjects.Rubric_Search_Box)).clear();
       			 }
       			 else
       			 {
       				//androidDriver.findElement(By.xpath(ELMAssignmentObjects.Rubric_Search_Box)).clear();
       				androidDriver1.findElement(By.xpath(ELMAssignmentObjects.Rubric_Search_Box)).clear();
       			 } 
                 
                 System.out.println("rubric name :"+rubricName);
                 elementSendText(By.xpath(ELMAssignmentObjects.Rubric_Search_Box), rubricName, "Rubric search textbox");
                 elementClick(By.xpath(ELMAssignmentObjects.Rubric_Serch_Icon), "Search Icon");
                 String Rubric_checkBox = AssignmentsObjects.ReplaceString(AssignmentsObjects.Rubric_checkBox, "oldString", rubricName);
                 elementClick(By.xpath(Rubric_checkBox), "Rubric Check box");
                 selectingOption("Add Selected", "Add Selected");
                 
                 elementSwipe(SwipeElementDirection.DOWN, 500, 10000);
    			 elementClick(By.xpath(AssignmentsObjects.Instructions_TextBox), "Instructions Textbox");
    			 elementSendKeys(instructionsDetails);
    			 elementCloseKeyBoard();
    			 
//    			 Uncheck Hidden from users in Restrictions tab
    			 selectingOption("Restrictions", "Restrictions");
    			 elementClick(By.xpath(ELMAssignmentObjects.Hidden_From_Users), "Hidden from users");
    			 
//    			 Select Learner objectives in Objectives tab
    			 selectingOption("Objectives", "Objectives");
    			 selectingOption("Associate Learning Objectives", "Associate Learning Objectives");
    			 elementSwipe(SwipeElementDirection.DOWN, 500, 10000);

    			 String Plus_Sign_Beside_Competency = ELMCommonObjects.ReplaceString(ELMCommonObjects.Plus_Sign_Beside_Competency, "oldString", competencyName);
    			 elementClick(By.xpath(Plus_Sign_Beside_Competency), "Plus Sign Beside Competency");
    			 elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
    			 String Plus_Sign_Beside_Child_Competency = ELMCommonObjects.ReplaceString(ELMCommonObjects.Plus_Sign_Beside_Competency, "oldString", childCompetencyName);
    			 elementClick(By.xpath(Plus_Sign_Beside_Child_Competency), "Plus Sign Beside Child Competency");
    			 elementSwipe(SwipeElementDirection.DOWN, 500, 5000);
    			 String CheckBox_Beside_Learner = ELMCommonObjects.ReplaceString(ELMCommonObjects.CheckBox_Beside_Learner, "oldString", learningObjectiveName);
    			 elementClick(By.xpath(CheckBox_Beside_Learner), "CheckBox Beside Learner");
    			 selectingOption("Add Selected", "Add Selected");
    			 
    			 String Inverted_Triangle_Beside_LearnerObjective = ELMCommonObjects.ReplaceString(ELMCommonObjects.Inverted_Triangle_Beside_LearnerObjective, "oldString", learningObjectiveName);
    			 elementClick(By.xpath(Inverted_Triangle_Beside_LearnerObjective), "Inverted Triangle Beside Learner Objective");
    			 selectingOption("Add Assessment", "Add Assessment");

    			 selectingOption("Select Rubric", "Select Rubric");
    			 if(configProps.getProperty("deviceType").equals("iOS")){
               		iosDriver.findElement(By.xpath(ELMAssignmentObjects.Rubric_Search_Box)).clear();
        		 }
        		 else
        		 {
        			//androidDriver.findElement(By.xpath(ELMAssignmentObjects.Rubric_Search_Box)).clear();
        			 androidDriver1.findElement(By.xpath(ELMAssignmentObjects.Rubric_Search_Box)).clear();
        		 } 
    			 
    			 elementSendText(By.xpath(ELMAssignmentObjects.Rubric_Search_Box), rubricName, "Rubric search textbox");
                 System.out.println("rubric name :"+rubricName);
    			 elementClick(By.xpath(ELMAssignmentObjects.Rubric_Serch_Icon), "Search Icon");
    			 String Rubric_Radio_Button = AssignmentsObjects.ReplaceString(ELMCommonObjects.Rubric_Radio_Button, "oldString", rubricName);
    			 elementClick(By.xpath(Rubric_Radio_Button), "Rubric Check box");
    			 selectingOption("Save", "Save");
    			 if(elementExists(By.xpath(ELMCommonObjects.Activity_Required_To_Complete_Learning_Objective))){
    				 elementClick(By.xpath(ELMCommonObjects.Activity_Required_To_Complete_Learning_Objective), "Activity Required To Complete Learning Objective");
    			 }
    			 elementSwipe(SwipeElementDirection.DOWN, 200, 500);
    			 elementClick(By.xpath(ELMAssignmentObjects.Threshold_DropDown), "Threshold");
    			 elementClick(By.xpath(ELMAssignmentObjects.Save_Button), "Save");
    			 
    			 if(elementExists(By.xpath(ELMAssignmentObjects.Created_Successfully_Close))){
    				 elementClick(By.xpath(ELMAssignmentObjects.Created_Successfully_Close), "Created successfully close");
    			 }
    			 selectingOption("Save and Close", "Save and Close");

    		}catch(Exception e) {
    			flag = false;
    			e.printStackTrace();
    		}
    	}
    	
    	/**
    	 * @Description : Method for verifying Class Progress Objectives
    	 * @param status
    	 * @param loginType
    	 * @throws Throwable
    	 */
    	public void verifyClassProgressObjectives(String status, String loginType) throws Throwable{
    		try{
    			String value = "";
    			int count1 = 0;
    			int count2 = 0;
    			
    			switch (status) {
    			case "Not Started":
    				value = elementGetText(By.xpath(ELMProjects.Objectives_Not_Started), "Not Started");
    				count1 = Integer.parseInt(value.replaceAll(" ", "").split("\\(")[1].replace(")", ""));
    				if(count1 > 0){
    					 SuccessReport("Objectives", "Projects that are shown correctly for :" +status +" with Login Type :"+loginType);
    				} else {
    	                 failureReport("Objectives", "Projects that are not shown correctly for :" +status +" with Login Type :"+loginType);
    				}
    			break;
    			
    			case "In progress":
    				value = elementGetText(By.xpath(ELMProjects.Objectives_Completed_Value), status );
    				count1 = Integer.parseInt(value.split("/")[0]);
    				count2 = Integer.parseInt(value.split("/")[1]);
    				if(count1 != 0 && count1 < count2){
    					 SuccessReport("Objectives", "Projects that are shown correctly for :" +status +" with Login Type :"+loginType);
    				} else {
    	                 failureReport("Objectives", "Projects that are not shown correctly for :" +status +" with Login Type :"+loginType);
    				}
    			break;
    			
    			case "Completed":
    				value = elementGetText(By.xpath(ELMProjects.Objectives_Completed_Value), status);
    				count1 = Integer.parseInt(value.split("/")[0]);
    				count2 = Integer.parseInt(value.split("/")[1]);
    				if(count1 == count2){
    					 SuccessReport("Objectives", "Projects that are shown correctly for :" +status +" with Login Type :"+loginType);
    				} else {
    	                 failureReport("Objectives", "Projects that are not shown correctly for :" +status +" with Login Type :"+loginType);
    				}
    			break;
    			}
    		}catch(Exception e) {
    			flag = false;
    			e.printStackTrace();
    		}
    	}
    	
    	/**
    	 * @Description : Method for selecting Radio Button For Competency
    	 * @param rubricName
    	 * @param Score
    	 * @param feedback
    	 * @throws Throwable
    	 */
    	public void selectingRadioButtonForCompetency(String rubricName ,String Score, String feedback) throws Throwable{
            try{
                    elementSendText(By.xpath(GradeObjects.Score), Score, "Score");
                    elementCloseKeyBoard();
                    elementSwipe(SwipeElementDirection.DOWN, 300, 1000);
                    elementClick(By.xpath(GradeObjects.Feedback), "FeedBack");
                    elementSendKeys(feedback);
                    elementCloseKeyBoard();
                    if(elementExists(By.xpath("//*[@text='"+rubricName+"']"), "Rubrics for assignment")){
     	               elementClick(By.xpath("//*[@text='"+rubricName+"']"), "Rubrics for assignment"); 
     	               elementClick(By.xpath("//*[@nodeName='LABEL' and @text='Criterion 1']/../..//*[@nodeName='INPUT' and @id='z_h']"), "Radio button for Competency");
     	               selectingOption("Rubrics", "Save & Record");
                    }
            }catch(Exception e) {
                   flag = false;
                   e.printStackTrace();
            }
     }
}