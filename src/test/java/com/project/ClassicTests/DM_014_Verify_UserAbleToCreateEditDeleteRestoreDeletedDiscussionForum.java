package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.DiscussionsObjects;
import com.project.setup.Data_Provider;

public class DM_014_Verify_UserAbleToCreateEditDeleteRestoreDeletedDiscussionForum extends BusinessFunctions {
	
	@Test
	public void DM_14_Verify_UserAbleTo_CreateEditDeleteRestoreDeletedDiscussionForum() throws Throwable {
		
		tstData = Data_Provider.getTestData("Discussions", "DM_014_Verify_UserAbleToCreateEditDeleteRestoreDeletedDiscussionForum");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String forumName = tstData.get("forumName")+getRandomNumberDate();
		String forumDesc = tstData.get("forumDesc");
		String topicName = tstData.get("topicName")+getRandomNumberDate();
		String topicDesc = tstData.get("topicDesc");
		String editedForumName = tstData.get("editedForumName")+getRandomNumberDate();
		String editedForumDesc = tstData.get("editedForumDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		//commandWrapper getCommand = new commandWrapper();
		
		/*getCommand.launchUrl(url);
		clearBrowserHistory();*/
		launchUrl(url);	
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);	
		selectMenuItem("Discussions");	
		selectingViewFromSettings("Grid View");		
		creatingForum(forumName, forumDesc);	
		creatingOrEditingTopic(topicName, topicDesc);		
		validatingCreatedTopicOrForum(forumName);		
		//elementClick(By.xpath("//*[@text='"+forumName+"']/..//*[@class='d2l-contextmenu-ph']"),"Inverted Arrow for Forum created");
		elementClick(DiscussionsObjects.Inverted_Triangle_Beside_Forum(forumName),"Inverted arrow for forum created");
		Thread.sleep(3000);
		scrollingToElementofAPage((DiscussionsObjects.d2LmenuItemLinktextName("Edit Forum")));
		elementClick(DiscussionsObjects.d2LmenuItemLinktextName("Edit Forum"), "Edit Forum");
		//selectingOption("Forum", "Edit Forum");
		Thread.sleep(3000);
		editingForum(editedForumName, editedForumDesc);		
		validatingCreatedTopicOrForum(editedForumName);
		deletingForum(editedForumName);		
	//	validatingDeletedTopicOrForum(editedForumName);		
		restoringForumOrTopic(editedForumName,topicName);			
		selectMenuItem("Discussions");
		validatingCreatedTopicOrForum(editedForumName);			
		logout();
		closeBrowserAndLaunch(port, device);
	}
}