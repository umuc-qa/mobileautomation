package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ELMObjectRepository.ELMProjects;
import com.project.setup.Data_Provider;

public class DM_029_UserIsAbleToCreateAndAccessAssignmentWithRestrictionsFromCalendar extends BusinessFunctions {
	
	/*  DM-29:Test Case Summary
	  Verify that the user is able to access an assignment created with date restrictions from the calendar and submit it.  
	  Assignment needs to be created and linked to calendar.
	*/
	
	@Test
	public void DM_0029_User_Is_AbleTo_Create_And_AccessAssignmentWithRestrictionsFromCalendar() throws Throwable {
		
		tstData = Data_Provider.getTestData("Calendar", "DM_029_UserIsAbleToCreateAndAccessAssignmentWithRestrictionsFromCalendar");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch=tstData.get("courseSearch");
		String assignmentName=tstData.get("AssignmentName")+getRandomNumberDate();
		String categoryName=tstData.get("CatergoryName")+getRandomNumberDate();
		String gradeName=tstData.get("GradeName")+getRandomNumberDate();
		String score=tstData.get("Score");
		String instructionsDesc = tstData.get("instructionsDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		//String assignmentName="Assignment0704194533";

		//commandWrapper getCommand = new commandWrapper();
		
		//getCommand.launchUrl(url);
		//clearBrowserHistory();
		
		launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Assignments");
		createAssignmentWithRestrictions(assignmentName,categoryName,gradeName,score,instructionsDesc);
		pageRefresh();
		//selectMenuItem("Assignments");
		//assignmentCreation(assignmentName, categoryName, gradeName, score, instructionsDesc);
		
		logout();
		closeBrowserAndLaunch(port, device);
		//clearBrowserHistory();
		//getCommand.
		//launchNewUrl(url);
		launchUrl(url);
		classicLogIn(configProps.getProperty("LearnerUser1"), configProps.getProperty("LearnerPwd")); 
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
		//selectingOption("Calendar", "Expand this widget");
		waitForVisibilityOfElement(By.xpath("//h2[text()='Calendar']//following::d2l-dropdown[1]"), "Action for Calendar");
		scrollingToElementofAPage(By.xpath("//h2[text()='Calendar']//following::d2l-dropdown[1]"));
		
		if(isElementPresent(By.xpath("//*[text()='" + assignmentName + " - Availability Ends']"),"")){
			/*elementClick(By.xpath("//a[contains(@aria-label,'Expand Upcoming events')]"), "Upcoming Events");*/
			Thread.sleep(2000);
			waitForVisibilityOfElement(By.xpath("//*[text()='" + assignmentName + " - Availability Ends']"),assignmentName);
			scrollingToElementofAPage(By.xpath("//*[text()='" + assignmentName + " - Availability Ends']"));
			String assignmentviacalendar=getText(By.xpath("//*[text()='" + assignmentName + " - Availability Ends']"), assignmentName);
			isElementPresent(By.xpath("//*[text()='" + assignmentName + " - Availability Ends']"),"AssignmentName is visible to Submit via calendar File upload");
			SuccessReport("Assignment submit via calendar", assignmentviacalendar + "Needs to be submited due to file upload functionality");
			//learnerSubmitsAssignmentViaCalendar(assignmentName);	
		}
		
		else {
			 elementClick(By.xpath("//d2l-button-icon[@text='Actions for Calendar']"), "Calendar Inverted triangle");
				Thread.sleep(1500);
			 JSClick(By.xpath("//d2l-menu-item[@text='Expand this widget' and contains(@class,'d2l-contextmenu-item d2l-menu-item-last')]"), "Expand this Widget");
			 ifAssigmentInEventsviaCalendarNotFound(assignmentName);
		}
		
		logout();
		closeBrowserAndLaunch(port, device);
		//Deleting Assignment
		launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Assignments");
		deletingAssignment(assignmentName);
		logout();
		closeBrowserAndLaunch(port, device);
		
	}
	

}
