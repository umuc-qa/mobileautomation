package com.project.ClassicTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_044_StudentsAreUnableToAccessSurveyInTheClassThatHasAlreadyEnded extends BusinessFunctions {
	@Test
	public void DM_044_Students_Are_Unable_To_Access_Survey_In_The_Class_That_Has_Already_Ended() throws Throwable {
		
		tstData = Data_Provider.getTestData("Survey", "DM_044_StudentsAreUnableToAccessSurveyInTheClassThatHasAlreadyEnded");
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String SurveyName=tstData.get("Survey")+getRandomNumberDate();
		String QuestionTitle=tstData.get("QuestionTitle")+getRandomNumberDate();
		String QuestionText=tstData.get("QuestionText");
		String port =configProps.getProperty("port");
		String device =configProps.getProperty("device");
		int minutes = 10;
		
		
		/*launchUrl(configProps.getProperty("App_URL"));
		clearBrowserHistory();*/
//		Launch URL
		
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);
//		goToSurvey();
		selectingMyTools("Surveys");
		createSurvey(SurveyName,QuestionTitle,QuestionText);
		surveyAddRestrictionsDate(SurveyName,minutes);
		logout();
		closeBrowserAndLaunch(port, device);
		
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		Thread.sleep(30000);
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);
		goToSurvey();
		verifyMySurveysDoesNotExist(SurveyName);
		logout();
		closeBrowserAndLaunch(port, device);
	}
}
