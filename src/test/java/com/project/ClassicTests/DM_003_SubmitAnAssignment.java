package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_003_SubmitAnAssignment extends BusinessFunctions {

	@Test
	public void DM_003_SubmitAssignment() throws Throwable {

	tstData = Data_Provider.getTestData("Assignments", "DM_003_SubmitAnAssignment");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		//commandWrapper getCommand = new commandWrapper();
		 //String strFilePath=System.getProperty("user.dir");
	//	getCommand.launchUrl(configProps.getProperty("App_URL"));
		//clearBrowserHistory();
		//getCommand.
		/*launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("Assignments");
		assignmentCreationWithoutRestrictions(assignmentName, categoryName, gradeName, score, instructionsDesc);
		logout();*/		
		//clearBrowserHistory();
		//getCommand.

		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(2000);
		selectMenuItem("Assignments");
		validatingCreatedAssignmentOrGrade("TEST");
		//validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment("TEST");
		//clickOnCreatedAssignment(assignmentName);
		Thread.sleep(3000);
		scrollingToElementofAPage(By.xpath("//*[text()='Add a File']"));
		elementClick(By.xpath("//*[text()='Add a File']"), "Add a file button");
		Thread.sleep(3000);
	//	selectingOption("Submit Files","Add a File");
	
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			switchToDefaultFrame();
			Thread.sleep(3000);
			switchToFrameByLocator(By.xpath("//iframe[@class='ddial_c_frame']"), "iframe");
			elementClick(By.xpath("//div[@title='My Computer']//span[text()='My Computer']"), "description");
			elementClick(By.xpath("//button[text()='Upload']"), "upload button");
			
			
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}
		selectingOption("Submit Files","Submit");
        selectingOption("Done Button", "Done");
	    validatingAssignmentSubmission(assignmentName);        
		logout();
	    
		//clearBrowserHistory();
		//	getCommand.
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		deletingAssignment(assignmentName);
		validatingCreatedAssignmentOrGradeIsDeleted(assignmentName);		
		logout();
	}
}