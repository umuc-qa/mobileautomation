package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_037_038_PICanAbleToCreateASurveyandLearnerCanAccess extends BusinessFunctions {
	
	/*  DM-38:Test Case Summary
	  Verify PI Can Able To Create A Survey and Learner Can Access
	*/
	
	@Test
	public void DM_38_PI_CanAbleToCreateASurveyandLearnerCanAccess() throws Throwable {
		
		tstData = Data_Provider.getTestData("Survey", "DM_038_PICanAbleToCreateASurveyandLearnerCanAccess");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch=tstData.get("courseSearch");
		String SurveyName=tstData.get("Survey")+getRandomNumberDate();
		String QuestionTitle=tstData.get("QuestionTitle")+getRandomNumberDate();
		String QuestionText=tstData.get("QuestionText");
		
//		commandWrapper getCommand = new commandWrapper();
	//	getCommand.launchUrl(url);
		//clearBrowserHistory();		
		//getCommand.
		launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
		goToSurvey();
		createSurvey(SurveyName,QuestionTitle,QuestionText); 
		//createSurvey("TestSurvey0726171341",QuestionTitle,QuestionText);
		logout();
		
		//getCommand.launchUrl(url);s
		//clearBrowserHistory();
		//getCommand.
		launchNewUrl(url);
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd")); 
		//Thread.sleep(10000);
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
		//elementHardSleep(3000);
		goToSurvey();
		accessSurvey(SurveyName); 
		logout();
	}	

}
