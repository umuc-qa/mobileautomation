package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_035_AddQuizToContentModule extends BusinessFunctions {
	
	@Test
	public void DM_035_AddQuizTo_ContentModule() throws Throwable {
		
		tstData = Data_Provider.getTestData("Quizzes&Exams", "DM_035_AddQuizToContentModule");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String moduleName = tstData.get("moduleName")+getRandomNumberDate();
		//commandWrapper getCommand = new commandWrapper();
		//launchUrl(configProps.getProperty("App_URL"));
	//	clearBrowserHistory();
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);		
		selectMenuItem("Content");
		addingQuizToModule(moduleName);	
		deletingModule(moduleName);
		logout();
	}
}