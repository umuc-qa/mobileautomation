package com.project.ClassicTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_008_GoViewSubmissionsDownloadIndividualSubmissions extends BusinessFunctions {
	
	@Test
	public void DM_008_GoView_SubmissionsDownloadIndividualSubmissions() throws Throwable {
		
    tstData = Data_Provider.getTestData("Assignments", "DM_008_GoViewSubmissionsDownloadIndividualSubmissions");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		String feedBackDesc = tstData.get("feedBackDesc");
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(url);		
		clearBrowserHistory();	
		getCommand.launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));		
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);		
		selectMenuItem("Assignments");
		assignmentCreation(assignmentName, categoryName, gradeName, score, instructionsDesc);
//        Logout();
		
        clearBrowserHistory();        
        getCommand.launchUrl(url);		
        classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));		
        courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);		
		selectMenuItem("Assignments");		
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}
		enterFeedBackDetailsInAssignmentSubmission(feedBackDesc);
        selectingOption("Done Button", "Done");
	    validatingAssignmentSubmission(assignmentName);   
//        Logout();
	    
        clearBrowserHistory();
        getCommand.launchUrl(url);
        classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
        courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		fileDownloading(assignmentName);
		validatingDownloadedAssignment(assignmentName);
//		Logout();
	}	
}