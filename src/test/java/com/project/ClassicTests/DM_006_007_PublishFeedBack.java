package com.project.ClassicTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_006_007_PublishFeedBack extends BusinessFunctions {
	
	@Test
	public void DM_006_007_Publish_FeedBack() throws Throwable {
		
    tstData = Data_Provider.getTestData("Assignments", "DM_006_007_PublishFeedBack");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(url);		
		clearBrowserHistory();	
		getCommand.launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));	
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Assignments");
		assignmentCreation(assignmentName, categoryName, gradeName, score, instructionsDesc);
        //Logout();        
		
        clearBrowserHistory();
        getCommand.launchUrl(url);
        classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
        courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}		
		selectingOption("Submit Files","Submit");
        selectingOption("Done Button", "Done");
	    validatingAssignmentSubmission(assignmentName);
        //Logout();
             
        clearBrowserHistory();
	    getCommand.launchUrl(url);
        classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
        courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Assignments");
		validatingCreatedAssignmentOrGrade(assignmentName);
		clickOnCreatedAssignment(assignmentName);
		clickOnUploadedFileAndVerifyAssignmentPresent();
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
			
		}
		enterScoreFeedback("89", "FeedBack");
		if(configProps.getProperty("deviceType").equals("iOS")){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		validatingAssignmentPublished();			
		//Logout();
	}
}