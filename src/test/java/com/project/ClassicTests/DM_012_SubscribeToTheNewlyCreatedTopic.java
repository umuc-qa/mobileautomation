package com.project.ClassicTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_012_SubscribeToTheNewlyCreatedTopic extends BusinessFunctions{
	@Test
	public void DM_12_SubscribeToTheNewlyCreatedTopic() throws Throwable {
		
    tstData = Data_Provider.getTestData("Discussions", "DM_012_SubscribeToTheNewlyCreatedTopic");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String forumName = tstData.get("forumName")+getRandomNumberDate();
		String forumDesc = tstData.get("forumDesc");
		String topicName = tstData.get("topicName")+getRandomNumberDate();
		String topicDesc = tstData.get("topicDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
				
		launchUrl(configProps.getProperty("App_URL"));
		Thread.sleep(4000);
		//clearBrowserHistory();
		//launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(20000);
		selectMenuItem("Discussions");
		selectingViewFromSettings("Reading View");
		creatingForum(forumName, forumDesc);
		creatingOrEditingTopic(topicName, topicDesc);
		Thread.sleep(3000);
		logout();
		Thread.sleep(1000);
		closeBrowserAndLaunch(port, device);
		Thread.sleep(1500);
		//student login
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		Thread.sleep(4000);
		selectMenuItem("Discussions");
		selectingViewFromSettings("Grid View");
		subscriptionToNewlyCreatedTopic(topicName);
		Thread.sleep(2000);
		//unSubscribeTopic(topicName);
		logout();	
		closeBrowserAndLaunch(port, device);
	}
}