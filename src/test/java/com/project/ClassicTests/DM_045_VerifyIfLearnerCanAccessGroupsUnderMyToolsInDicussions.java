package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.ClassicObjectRepository.DiscussionsObjects;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_045_VerifyIfLearnerCanAccessGroupsUnderMyToolsInDicussions extends BusinessFunctions{
	
	boolean flag =  false;
	@Test
	public void DM_045_VerifyIfLearner_CanAccessGroupsUnderMyToolsInDicussions() throws Throwable {
		
     tstData = Data_Provider.getTestData("Discussions", "DM_045_VerifyIfLearnerCanAccessGroupsUnderMyToolsInDicussions");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		String noOfEnroleUsers = null;
		
		//commandWrapper getCommand = new commandWrapper();
		
	//	getCommand.launchUrl(configProps.getProperty("App_URL"));
		//clearBrowserHistory();
		//getCommand.launchUrl(configProps.getProperty("App_URL"));	
		//selectMenuItem("My Tools");
		//selectingOption("Groups", "Groups");
		launchUrl(configProps.getProperty("App_URL"));	
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);	
		selectingMyTools("Groups");
		elementExists(By.xpath(DiscussionsObjects.List_Of_Groups), "List Of Groups");		
		Thread.sleep(2000);
		elementClick(By.xpath(DiscussionsObjects.Group_Link), "Group");
//		switchToFrameByIndex(0);
		elementExists(By.xpath(DiscussionsObjects.Members_Enrolled_In_Group), "Members enrolled in a group");	
		noOfEnroleUsers = elementGetText(By.xpath(DiscussionsObjects.Members_Enrolled_In_Group), "Members Enrolled In Group");
		System.out.println("noOfEnroleUsers :"+noOfEnroleUsers);
//		switchToDefaultFrame();
		JSClick(By.xpath("//button[text()='Cancel']"),"Cancel button");
		Thread.sleep(2000);
		logout();
		closeBrowserAndLaunch(port, device);
	}
}
