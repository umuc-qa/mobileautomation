package com.project.ClassicTests;

import io.appium.java_client.SwipeElementDirection;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AnnouncementObjects;
import com.project.ClassicObjectRepository.CommonObjects;
import com.project.setup.*;

public class DM_026_27_VerifyPIandLearnerAbleToViewEnrolledCourses extends BusinessFunctions {

	@Test
	public void DM_026_27VerifyPIandLearnerAbleToViewEnrolledCourses() throws Throwable {
		
		tstData = Data_Provider.getTestData("SSO", "DM_026_27_VerifyPIandLearnerAbleToViewEnrolledCourses");
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		String InstrutorUsername = tstData.get("InstrutorUsername");
		String LearnerUsername=tstData.get("LearnerUsername");


		launchUrl(configProps.getProperty("App_URL"));
		
//	    Login as PI
		//classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		classicLogIn(InstrutorUsername, configProps.getProperty("PIPwd"));
//		Course Search			
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		
//		Verify My Role Primary instructor
//		
		switchToFrameByLocator(By.xpath("//iframe[@title='Role Switch']"), "My Role");
		waitForVisibilityOfElement(By.xpath("//*[text()='Current Role']"), "My Role");
		scrollingToElementofAPage(By.xpath("//*[text()='Current Role']"));
		//elementClick(By.xpath(CommonObjects.MyRole_InvertedTriangle), "Inverted Triangle");
		waitForVisibilityOfElement(By.xpath(CommonObjects.MyRole_PrimaryInstructor),"Primary Instrutor");
		elementExists(By.xpath(CommonObjects.MyRole_PrimaryInstructor),"Primary Instrutor");
		String primaryInstrutor=getText(By.xpath(CommonObjects.MyRole_PrimaryInstructor),"Primary Instrutor");
		SuccessReport("Current Role is", primaryInstrutor);
		logout();
		closeBrowserAndLaunch(port, device);	
		
		
		// Learner Course validation as a Role 
		
		launchUrl(configProps.getProperty("App_URL"));
		
//	  
		
		classicLogIn(LearnerUsername, configProps.getProperty("LearnerPwd"));
//		Course Search			
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		SuccessReport("Enrolled course for learner is", subjectArea+" "+catalog_no+" "+classSection+" "+term);
//		
//		
	/*	switchToFrameByLocator(By.xpath("//iframe[@title='Role Switch']"), "My Role");
		waitForVisibilityOfElement(By.xpath("//*[text()='Current Role']"), "My Role");
		scrollingToElementofAPage(By.xpath("//*[text()='Current Role']"));
		elementExists(By.xpath("//*[text()='Current Role']"),"My Role");
		waitForVisibilityOfElement(By.xpath(CommonObjects.MyRole_Learner),"Learner");*/
		//elementExists(By.xpath(CommonObjects.MyRole_Learner),"Learner");
		logout();
		closeBrowserAndLaunch(port, device);
	}
}
