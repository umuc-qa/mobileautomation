package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_013_017_StartNewThreadTopicVerifyUserAbleReplyToExistingDiscussionTopic extends BusinessFunctions {
	
	@Test
	public void DM_13_17_StartNewThreadTopic_VerifyUserAbleReplyToExistingDiscussionTopic() throws Throwable {
		
		tstData = Data_Provider.getTestData("Discussions", "DM_013_017_StartNewThreadTopicVerifyUserAbleReplyToExistingDiscussionTopic");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String forumName = tstData.get("forumName")+getRandomNumberDate();
		String forumDesc = tstData.get("forumDesc");
		String topicName = tstData.get("topicName")+getRandomNumberDate();
		String topicDesc = tstData.get("topicDesc");
		String threadName = tstData.get("threadName")+getRandomNumberDate();
		String threadDesc = tstData.get("threadDesc");
		String replyThreadDesc = tstData.get("replyThreadDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Discussions");	
		creatingForum(forumName, forumDesc);
		creatingOrEditingTopic(topicName, topicDesc);
		validatingCreatedTopicOrForum(topicName);
		logout();
		Thread.sleep(2000);
		//clearBrowserHistory();
		closeBrowserAndLaunch(port, device);
		
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Discussions");	
		validatingCreatedTopicOrForum(topicName);
		clickOnCreatedTopic(topicName);	
		creatingThread(threadName, threadDesc);
		validatingCreatedThread(threadName);
		replyToThread(threadName, replyThreadDesc);	
		logout();
		closeBrowserAndLaunch(port, device);
	}
}