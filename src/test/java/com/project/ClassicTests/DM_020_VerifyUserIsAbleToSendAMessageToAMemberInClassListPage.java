package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_020_VerifyUserIsAbleToSendAMessageToAMemberInClassListPage extends BusinessFunctions {
	
	@Test
	public void DM_20_VerifyUserIsAbleToSendAMessageToAMemberInClassListPage() throws Throwable {
		
		tstData = Data_Provider.getTestData("EmailAndChat", "DM_20_VerifyUserIsAbleToSendAMessageToAMemberInClassListPage");
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String umucUser = tstData.get("umucUser");
		String bodyText = tstData.get("bodyText");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
				
		/*launchUrl(configProps.getProperty("App_URL"));
		clearBrowserHistory();
		Launch URL
		Thread.sleep(5000);*/
	    //Login as Learner
		
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		/*if(elementExists(By.xpath(EmailObjects.Continue_Button))){
			elementClick(By.xpath(EmailObjects.Continue_Button), "Continue");
		}*/
		Thread.sleep(3000);
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		sendmessageThroughClassListInstantMessage(umucUser, bodyText);
		logout();
		closeBrowserAndLaunch(port, device);
	}
}
