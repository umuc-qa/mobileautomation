package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.DiscussionsObjects;
import com.project.setup.Data_Provider;

public class DM_016_ConfirmAbilityToEditDeleteRestoreDeletedDiscussionForum extends BusinessFunctions {
	
	@Test
	public void DM_16_ConfirmAbilityTo_EditDeleteRestoreDeletedDiscussionForum() throws Throwable {
		
    tstData = Data_Provider.getTestData("Discussions", "DM_016_ConfirmAbilityToEditDeleteRestoreDeletedDiscussionForum");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String forumName = tstData.get("forumName")+getRandomNumberDate();
		String forumDesc = tstData.get("forumDesc");
		String topicName = tstData.get("topicName")+getRandomNumberDate();
		String topicDesc = tstData.get("topicDesc");
		String editedTopicName = tstData.get("editedTopicName")+getRandomNumberDate();
		String editedTopicDesc = tstData.get("editedTopicDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
	//	commandWrapper getCommand = new commandWrapper();		
	//	getCommand.launchUrl(url);
		//clearBrowserHistory();
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Discussions");
		selectingViewFromSettings("Grid View");
		creatingForum(forumName, forumDesc);
		creatingOrEditingTopic(topicName, topicDesc);
		//validatingCreatedTopicOrForum(topicName);
		//elementClick(By.xpath("//*[@text='"+topicName+"']/../../..//*[@class='d2l-contextmenu-ph']"),"Inverted Arrow for Topic created");
		//selectingOption("Topic", "Edit Topic");
		waitForVisibilityOfElement(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName),"Inverted arrow for topic created");
		scrollingToElementofAPage(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName));
		elementClick(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName),"Inverted arrow for topic created");
		Thread.sleep(2000);
//		scrollingToElementofAPage(DiscussionsObjects.actionsForTopic(topicName, "Edit Topic"));
		JSClick(DiscussionsObjects.d2LmenuItemLinktextName("Edit Topic"), "Edit Topic");
		creatingOrEditingTopic(editedTopicName, editedTopicDesc);
		//validatingCreatedTopicOrForum(editedTopicName);		
		deletingTopic(editedTopicName);
		//validatingDeletedTopicOrForum(editedTopicName);
		restoringForumOrTopic(forumName, editedTopicName);
		selectMenuItem("Discussions");
		validatingCreatedTopicOrForum(editedTopicName);
		logout();
		closeBrowserAndLaunch(port, device);
	}
}