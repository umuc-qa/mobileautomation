package com.project.ClassicTests;

import io.appium.java_client.SwipeElementDirection;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_022_GradeAssignmentAndProvideFeedback extends BusinessFunctions{
	@Test
	public void DM_22_GradeAssignmentAndProvideFeedback() throws Throwable {

		tstData = Data_Provider.getTestData("Assignments", "DM_022_GradeAssignmentAndProvideFeedback");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
//		String assignmentName = "Assignment0829171847";
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();

		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		String feedBackDesc = tstData.get("feedBackDesc");		
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(configProps.getProperty("App_URL"));
		
		clearBrowserHistory();
		
//		Launch URL
		getCommand.launchUrl(configProps.getProperty("App_URL"));
//	    Login as PI
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
//		Course Search	
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		
		selectMenuItem("Assignments");
		
		// Creating assignment with Rubrics
		assignmentCreationWithRubric(assignmentName, categoryName, gradeName, score, instructionsDesc);
		elementClick(By.xpath(AssignmentsObjects.Save_And_Close_Button),"Save and Close Button");
			
//		Logout();

		clearBrowserHistory();

		getCommand.launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd")); //("ACOOK15", "TestPw1");
		
//		Course Search			
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		
		selectMenuItem("Assignments");
		clickOnCreatedAssignment(assignmentName);
		selectingOption("Submit Files","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Upload Options","Add");
		}
		enterFeedBackDetailsInAssignmentSubmission(feedBackDesc);
		selectingOption("Done Button", "Done");
		
//		Logout();
		
		clearBrowserHistory();
		
		launchUrl(configProps.getProperty("App_URL"));
//		Login as PI
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		
//		Course Search	
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		Thread.sleep(5000);
		
		selectMenuItem("Assignments");
		
		clickOnCreatedAssignment(assignmentName);
		
		clickOnUploadedFileAndVerifyAssignmentPresent();
		if(configProps.getProperty("deviceType") == "iOS"){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		
		enterScoreFeedback("100", "FeedBack");
		if(configProps.getProperty("deviceType") == "iOS"){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
			
		}
		//driver.deviceAction("Change Orientation");

//		Logout();
		clearBrowserHistory();

		getCommand.launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd")); //("ACOOK15", "TestPw1");
		
//		Course Search			
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		selectMenuItem("Assignments");
		
		verifyFeedBackIsPresentForAssignment(assignmentName);
		
//		Logout();
//		Deleting Created Assignments
		elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
		clearBrowserHistory();
		
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		selectMenuItem("Assignments");
		deletingAssignment(assignmentName);
		// Deleting Created Grade
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Grades");
		selectingOption("Grades", "Manage Grades	");
		deletingGrade(gradeName);
//		
//		Logout();
		
	}
}
