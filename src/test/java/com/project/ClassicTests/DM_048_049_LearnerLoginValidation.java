package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_048_049_LearnerLoginValidation extends BusinessFunctions{
	@Test
	public void DM_48_49_LearnerLoginValidation()throws Throwable {
		 tstData = Data_Provider.getTestData("SSO", "DM_048_049_LearnerLoginValidation");
			
			String subjectArea = tstData.get("subjectArea");
			String catalog_no = tstData.get("catalog_no");
			String classSection = tstData.get("classSection");
			String term = tstData.get("term");
			String courseSearch=tstData.get("courseSearch");
			String port = configProps.getProperty("port");
			String device = configProps.getProperty("device");
			
			//commandWrapper getCommand = new commandWrapper();
			//getCommand.launchUrl(url);
			//clearBrowserHistory();
			launchUrl(url);
			classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
			courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
			logout();
			closeBrowserAndLaunch(port, device);
	}
}
