package com.project.ClassicTests;

import java.net.URL;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.report.Utilities.Data_Provider;

import io.appium.java_client.android.AndroidDriver;

public class DM_041_042_VerifyUserIsAbleToSetUpGroups extends BusinessFunctions {
	
	/*  DM-42:Test Case Summary
	  Verify that the user is able to set up Groups
	*/
	
	@Test
	public void DM_041_042_Verify_User_IsAbleToSetUpGroups() throws Throwable {
		
		tstData = Data_Provider.getTestData("Groups","DM_041_042_VerifyUserIsAbleToSetUpGroups");
		String subjectArea = tstData.get("subjectArea");
        String catalog_no = tstData.get("catalog_no");
        String classSection = tstData.get("classSection");
        String term = tstData.get("term");
        String courseSearch = tstData.get("courseSearch");
        String GroupName = tstData.get("GroupName")+getRandomNumberDate();
        String DescriptionText = tstData.get("DescriptionText");
        String NumberOfGroups = tstData.get("NumberOfGroups");
        String umucUser = tstData.get("umucUser");
        String port = configProps.getProperty("port");
    	String device = configProps.getProperty("device");
	/*	commandWrapper getCommand = new commandWrapper();
		getCommand.launchUrl(url);
		clearBrowserHistory();
		getCommand.*/
        
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd")); 
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		createGroup(GroupName, DescriptionText, NumberOfGroups);		
		enrollAUserToGroup(GroupName , umucUser);				
		logout();
	/*	launchUrl(configProps.getProperty("App_URL"));
		clearBrowserHistory();*/
		//androidDriver1.close();
		//System.out.println("safasdfa");
	/*	deleteCookies();
		Thread.sleep(1500);*/
		//androidDriver1 = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));
		closeBrowserAndLaunch(port, device);
		
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd")); 
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);	
		/*courseSearch("ACCT 301 1950 2175" , "ACCT  301 1950 Accounting for Nonaccounting Managers (2175)");		
		selectMenuItem("My Tools");
		selectingOption("Groups", "Groups");*/
		selectingMyTools("Groups");
		verifyUserAbleToAccessGroup(GroupName);
		logout();
		closeBrowserAndLaunch(port, device);
	}
}
