package com.project.ClassicTests;

//import io.appium.java_client.SwipeElementDirection;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.EmailObjects;
//import com.project.ClassicObjectRepository.GradeObjects;
import com.project.setup.Data_Provider;
//import com.project.setup.commandWrapper;

public class DM_023_EmailStudentsFromGradebook extends BusinessFunctions{
	@Test
	public void DM_23_EmailStudentsFromGradebook() throws Throwable {
		tstData = Data_Provider.getTestData("Grades", "DM_023_EmailStudentsFromGradebook");

		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String emailSubject = tstData.get("emailSubject");
		String emailMessage = tstData.get("emailMessage");
		String userName = tstData.get("userName");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
	//	commandWrapper getCommand = new commandWrapper();
	//	getCommand.launchUrl(configProps.getProperty("App_URL"));
	//	clearBrowserHistory();
		//Launch URL
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		//selectMenuItem("My Tools");
		//selectingOption("Grades", "Grades");
		selectingMyTools("Grades");
		//selectingOption("Enter Grades", "Enter Grades");
		Thread.sleep(2000);
		waitForVisibilityOfElement(By.linkText("Enter Grades"), "Enter Grades link");
		scrollingToElementofAPage(By.linkText("Enter Grades"));
		elementClick(By.linkText("Enter Grades"),"Enter Grades link");
		//elementSwipe(SwipeElementDirection.DOWN, 700, 1000);
//		String CheckBox_Beside_User=GradeObjects.ReplaceString(GradeObjects.CheckBox_Beside_User, "oldString", userName);
		//scrollingToElementofAPage(By.xpath("//input[@title='Select "+userName+"']"));
		Thread.sleep(3000);
		waitForVisibilityOfElement(By.xpath("//input[@title='Select "+userName+"']"), "Checkbox beside user");
		scrollingToElementofAPage(By.xpath("//input[@title='Select "+userName+"']"));
		JSClick(By.xpath("//input[@title='Select "+userName+"']"), "Checkbox beside user");
//		selectingOption("Email", "Email");
		scrollingToElementofAPage(By.xpath(EmailObjects.lnkEmail));
		elementClick(By.xpath(EmailObjects.lnkEmail), "Email icon");
		windowHandles();
		Thread.sleep(4000);
		scrollingToElementofAPage(By.xpath("EmailObjects.Auto_Filled_BCC)"));
		elementExists(By.xpath(EmailObjects.Auto_Filled_BCC), "Auto filled BCC");
		elementSendText(By.xpath(EmailObjects.MessageAlertBox_Email_Subject), emailSubject, "Subject");
		switchToFrameByLocator(By.xpath("//iframe[@id='BodyHtml$html_ifr']"), "Body frame");
		elementSendText(By.xpath(EmailObjects.MessageAlertBox_Email_Body), emailMessage, "Instructions");
		//elementCloseKeyBoard();
		//elementSwipe(SwipeElementDirection.DOWN, 700, 1000);
		//elementSwipeWhileNotFound(By.xpath(EmailObjects.ClassList_Email_Body), 700, SwipeElementDirection.DOWN, 10000, 1000, 5, true);
		switchToDefaultFrame();
		//elementSendKeys(emailMessage);
		scrollingToElementofAPage(By.xpath(EmailObjects.MessageAlertBox_Email_Send));
		elementClick(By.xpath(EmailObjects.MessageAlertBox_Email_Send), "Message Alert Box");
		//elementCloseKeyBoard();
		//elementSwipe(SwipeElementDirection.UP, 700, 20000);
	//	selectingOption("Send", "Send");
		verifySuccessAlert("Sent successfully");
		backToMainWindow();
		ExplicitWaitOnElementToBeClickable(By.xpath(EmailObjects.lnkEmail));
		logout();
		closeBrowserAndLaunch(port, device);
	}
}
