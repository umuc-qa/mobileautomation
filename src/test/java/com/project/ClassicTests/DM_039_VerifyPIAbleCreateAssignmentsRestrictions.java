package com.project.ClassicTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_039_VerifyPIAbleCreateAssignmentsRestrictions extends BusinessFunctions {
	
	@Test
	public void DM_039_VerifyPI_AbleCreateAssignmentsRestrictions() throws Throwable {
		
    tstData = Data_Provider.getTestData("Assignments", "DM_039_VerifyPIAbleCreateAssignmentsRestrictions");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		//commandWrapper getCommand = new commandWrapper();
		
		//getCommand.launchUrl(url);
		//clearBrowserHistory();		
		launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		//swipeBottumToTop(By.xpath("//label[text()='Current Role']"), "Test");
		selectMenuItem("Assignments");		
		assignmentCreationWithRubric(assignmentName, categoryName, gradeName, score, instructionsDesc);
		addRestrictions(assignmentName);
		validatingCreatedAssignmentWithRestrictions();
		logout();
		closeBrowserAndLaunch(port, device);
	}
}