package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_032_033_VerifyPIAbletoCreatesSelfAssessments extends BusinessFunctions{
	@Test
	public void DM_32_DM_33_VerifyPIisabletocreatesSelfAssessments()throws Throwable {
		 tstData = Data_Provider.getTestData("SelfAssessement", "DM_032_VerifyPIisabletocreatesSelfAssessments");
			
			String subjectArea = tstData.get("subjectArea");
			String catalog_no = tstData.get("catalog_no");
			String classSection = tstData.get("classSection");
			String term = tstData.get("term");
			String courseSearch=tstData.get("courseSearch");
			String SelfAssessementName=tstData.get("SelfAssessementName")+getRandomNumberDate();
			String QuestionTitle=tstData.get("Title")+getRandomNumberDate();
			String QuestionText=tstData.get("QuestionText")+getRandomNumberDate();
			//commandWrapper getCommand = new commandWrapper();
			
			//getCommand.launchUrl(url);
			//clearBrowserHistory();
			//getCommand.
			launchUrl(url);
			classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
			courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);			
			//selectMenuItem("My Tools");
			selectingMyTools("Self Assessments");
			createSelfAssessement(SelfAssessementName,QuestionTitle,QuestionText);
			logout();
			//clearBrowserHistory();
			//getCommand.
			
			launchUrl(url);
			classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
			Thread.sleep(8000);
			courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
			selectingMyTools("Self Assessments");
			//selectMenuItem("My Tools");
			verifyCreatedSelfAssessmentForLearner(SelfAssessementName);
			logout();		
	}
}