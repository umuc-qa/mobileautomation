package com.project.ClassicTests;

import io.appium.java_client.SwipeElementDirection;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_021_VerifyUserViewAssignmentReorderAndGradeAssignmentWithRubricsAndGradebookLink extends BusinessFunctions{
	@Test
	public void DM_21_VerifyUserIsAbleToViewAssignmentSubmissionlogRerderAssignmentAndGradeAssignmentWithRubricsAndLinkToGradebook() throws Throwable {

		tstData = Data_Provider.getTestData("Assignments", "DM_021_VerifyUserIsAbleToViewAssignmentSubmissionlogRerderAssignmentAndGradeAssignmentWithRubricsAndLinkToGradebook");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		String feedBackDesc = tstData.get("feedBackDesc");
		
		String RubricName = tstData.get("RubricName")+getRandomNumberDate();
		
		String assignmentNameTwo = tstData.get("assignmentNameTwo")+getRandomNumberDate();
		
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(configProps.getProperty("App_URL"));
		clearBrowserHistory();
//		
////		Launch URL
		getCommand.launchUrl(configProps.getProperty("App_URL"));
//	    Login as PI
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
//		Course Search	
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		
		selectMenuItem("Assignments");
		// Creating assignment with Rubrics
		assignmentCreationWithRubric(assignmentName, categoryName, gradeName, score, instructionsDesc);
		if (getCommand.elementClick(By.xpath(AssignmentsObjects.Save_And_Close_Button),"Save and Close Button")) {
			SuccessReport("Save and Close", "Save and close button clicked successfully");
		} else {
			failureReport("Save and Close", "Failed to click save and close button");
		}
//		Logout();

		clearBrowserHistory();

		getCommand.launchUrl(configProps.getProperty("App_URL"));
		
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd")); //("ACOOK15", "TestPw1");
//		Course Search			
		courseSearch("ACCT 301 1950 2175" , courseSearch);		
		selectMenuItem("Assignments");
		clickOnCreatedAssignment(assignmentName);
		selectingOption("Add a File","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Add","Add");
		}
		enterFeedBackDetailsInAssignmentSubmission(feedBackDesc);
		selectingOption("Done Button", "Done");
		
//		Logout();
		clearBrowserHistory();
		
		getCommand.launchUrl(configProps.getProperty("App_URL"));
//		Login as PI
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
//		Course Search	
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		
		selectMenuItem("Assignments");
		clickOnCreatedAssignment(assignmentName);
		clickOnUploadedFileAndVerifyAssignmentPresent();
		selectingOption("Back to Submissions", "Back to Submissions");
 		selectingOption("Submission Folders", "Submission Folders");
		
		clickOnInvertedTriangleBesideAssignmentSelectSubmissionLogAndVerify(assignmentName, true);
		selectingOption("Folder List", "Folder List");
		reorderAssignmentAndVerify(categoryName, true);
		creatingNewRubricThroughCourseAdmin(RubricName);
		selectMenuItem("Assignments");
		createAssignmentWithNameAndSelectedRubric(assignmentNameTwo, gradeName, score, instructionsDesc, RubricName);
		
//		Logout();
		clearBrowserHistory();
		
		getCommand.launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd")); //("ACOOK15", "TestPw1");
		
//		Course Search			
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		selectMenuItem("Assignments");
		selectingOption(assignmentName, assignmentName);
		selectingOption("Add a File","Add a File");
		if(configProps.getProperty("deviceType").equalsIgnoreCase("iOS")){
			addAFileIPhone("Image");
		} else {
			addAFile("Image");
			selectingOption("Add","Add");
		}
		selectingOption("Submit", "Submit");
		selectingOption("Done", "Done");
		
//		Logout();
		clearBrowserHistory();
		
		getCommand.launchUrl(configProps.getProperty("App_URL"));
		
//		Login as PI
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
//		Course Search	
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		selectMenuItem("Assignments");
		clickOnInvertedTriangleBesideAssignmentSelectSubmissionLogAndVerify(assignmentName, false);
		selectingOption("Folder List", "Folder List");
		selectingOption(assignmentName, assignmentName);
		clickOnUploadedFileAndVerifyAssignmentPresent();
		
		if(configProps.getProperty("deviceType") == "iOS"){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		enterScoreFeedback("100", "FeedBack");
		if(configProps.getProperty("deviceType") == "iOS"){
			iosDriver.deviceAction("Change Orientation");
		}
		else
		{
			//androidDriver.deviceAction("Change Orientation");
		}
		//driver.deviceAction("Change Orientation");
		
//		Logout();
		clearBrowserHistory();

		getCommand.launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd")); //("ACOOK15", "TestPw1");
		
//		Course Search			
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		selectMenuItem("Assignments");
		
		verifyFeedBackIsPresentForAssignment(assignmentName);
//		Logout();
		
//		Deleting Created Assignments
		elementSwipe(SwipeElementDirection.DOWN, 100, 1000);
		clearBrowserHistory();
		
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		selectMenuItem("Assignments");
		deletingAssignment(assignmentName);
		// Deleting Created Grade
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Grades");
		selectingOption("Grades", "Manage Grades	");
		deletingGrade(gradeName);
//		
//		Logout();

	}
}
