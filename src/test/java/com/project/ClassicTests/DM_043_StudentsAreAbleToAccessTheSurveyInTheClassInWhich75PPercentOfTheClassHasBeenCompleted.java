package com.project.ClassicTests;

import java.net.URL;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

import io.appium.java_client.android.AndroidDriver;

public class DM_043_StudentsAreAbleToAccessTheSurveyInTheClassInWhich75PPercentOfTheClassHasBeenCompleted extends BusinessFunctions{
	@Test
	public void DM_043_Students_Are_Able_ToAccess_Survey_In_Class_In_Which_75_Persent_ClassHasBeenCompleted() throws Throwable {

		tstData = Data_Provider.getTestData("Survey", "DM_043_StudentsAreAbleToAccessTheSurveyInTheClassInWhich75PPercentOfTheClassHasBeenCompleted");
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String SurveyName=tstData.get("Survey")+getRandomNumberDate();
		String QuestionTitle=tstData.get("QuestionTitle")+getRandomNumberDate();
		String QuestionText=tstData.get("QuestionText");
		String port =configProps.getProperty("port");
		String device =configProps.getProperty("device");
		
		
	//	clearBrowserHistory();
//		Launch URL
		launchUrl(configProps.getProperty("App_URL"));
//	    Login as PI
		//launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
		goToSurvey();
		createSurvey(SurveyName,QuestionTitle,QuestionText); 
		//createSurvey("TestSurvey0726171341",QuestionTitle,QuestionText);
		logout();
		androidDriver1.close();
		System.out.println("test");
		androidDriver1 = new AndroidDriver<>(new URL("http://localhost:"+ port + "/wd/hub"), createDesiredCapabilities(device));
		
		
		launchNewUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
//		Course Search	
		Thread.sleep(2000);
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		goToSurvey();
		verifyMySurveysExist(SurveyName);
		logout();
	}
}
