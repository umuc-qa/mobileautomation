package com.project.ClassicTests;
import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_018_019_VerifyUserIsAbleToSendAnEmailViaEmailIconFromMiniBarAndTheRosterPage extends BusinessFunctions {

	@Test
	public void DM_18_19_VerifyUserIsAbleToSendAnEmailViaEmailIconFromMiniBarAndTheRosterPage() throws Throwable {
		
		tstData = Data_Provider.getTestData("EmailAndChat", "DM_018_019_VerifyUserIsAbleToSendAnEmailViaEmailIconFromMiniBarAndTheRosterPage");
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String otherEmailId = tstData.get("otherEmailId");
		String umucUser = tstData.get("umucUser");
		String subjectText = tstData.get("subjectText");
		String bodyText = tstData.get("bodyText");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		

		launchUrl(configProps.getProperty("App_URL"));
		//Thread.sleep(5000);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		sendEmailThroughMessageAlertIcon(otherEmailId, true ,false);
		logout();
		closeBrowserAndLaunch(port, device);
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		sendEmailThroughClassList(umucUser, subjectText, bodyText);		
		logout();
		closeBrowserAndLaunch(port, device);
	}
}
