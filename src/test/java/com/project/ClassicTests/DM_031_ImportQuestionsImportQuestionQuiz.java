package com.project.ClassicTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_031_ImportQuestionsImportQuestionQuiz extends BusinessFunctions {
	
	@Test
	public void DM_031_ImportQuestions_ImportQuestionQuiz() throws Throwable {
		
		tstData = Data_Provider.getTestData("Quizzes&Exams", "DM_031_ImportQuestionsImportQuestionQuiz");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String quizName = tstData.get("quizName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		//commandWrapper getCommand = new commandWrapper();
		
	//	launchUrl(configProps.getProperty("App_URL"));
//	clearBrowserHistory();
		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);	
		//selectMenuItem("My Tools");
		//selectingOption("My Tools", "Quizzes");
		selectingMyTools("Quizzes");
		enteringQuizDetails(quizName, categoryName);
		importingQuizQuestions();
		validatingImportedQuiz(quizName);
		logout();
	}
}