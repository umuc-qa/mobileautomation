package com.project.ClassicTests;

import io.appium.java_client.SwipeElementDirection;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AnnouncementObjects;
import com.project.ClassicObjectRepository.CommonObjects;
import com.project.setup.*;

public class DM_001_026_CreateAndManageNewsItems extends BusinessFunctions {

	@Test
	public void DM_001_026_CreateAnd_Manage_News_Items() throws Throwable {
		
		tstData = Data_Provider.getTestData("Announcements", "DM_001_026_CreateAndManageNewsItems");
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String announcementName = tstData.get("announcementName")+getRandomNumberDate();
		String contentDesc = tstData.get("contentDesc");
		String announcementNameTwo = tstData.get("announcementNameTwo")+getRandomNumberDate();
		String contentDescTwo = tstData.get("contentDescTwo");

//		launchUrl(configProps.getProperty("App_URL"));
// 
//		clearBrowserHistory();
		
//		Launch URL
		launchUrl(configProps.getProperty("App_URL"));
		
//	    Login as PI
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		
//		Course Search			
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		
//		Verify My Role Primary instructor
//		elementSwipeWhileNotFound(By.xpath(CommonObjects.MyRole_InvertedTriangle), 700, SwipeElementDirection.DOWN, 20000, 10000, 5, false);
		elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
		elementClick(By.xpath(CommonObjects.MyRole_InvertedTriangle), "Inverted Triangle");
		elementExists(By.xpath(CommonObjects.MyRole_PrimaryInstructor));
			
		
// 		Creating new Announcement with present start time
		if(configProps.getProperty("deviceType") == "iOS"){
			explicitWait(iosDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
			explicitWait(androidDriver1, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
		}
		
		elementClick(By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle), "Announcements Inverted Triangle");
		elementClick(By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle_Value), "Announcements Inverted Triangle Value");
		creatingAnnouncement(announcementName, contentDesc, true, "Video", false, 0 , false, 0);
		
		Thread.sleep(5000);
//      Creating new Announcement with future start and end time
		selectingOption("New Announcement","New Announcement");
		creatingAnnouncement(announcementNameTwo, contentDescTwo, true, "Image", true, 3, true, 5);
		elementHardSleep(10000);
		
//      Verifying weather Announcement is published with present time
		verifyAnnouncementStatus(announcementName, "Published");
		
//		Wait till second Announcement publish
		elementHardSleep(150000);
		pageRefresh();
		verifyAnnouncementStatus(announcementNameTwo, "Published");
		
//		Wait till second Announcement Expire		
		elementHardSleep(120000);
		pageRefresh();
		verifyAnnouncementStatus(announcementNameTwo, "Expired");
//		Logout();
	}
}
