package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.setup.Data_Provider;

public class DM_004_CreateAddRestrictionsToFolder extends BusinessFunctions {
	
	@Test
	public void DM_004_CreateAndAddRestrictionsToFolder() throws Throwable {
		
    tstData = Data_Provider.getTestData("Assignments", "DM_004_CreateAddRestrictionsToFolder");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		launchUrl(url);
		Thread.sleep(2000);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		//JSMousehoverDoubleClick(By.xpath(AssignmentsObjects.Menu_Bar_Icon1), "Side Nav Menu Bar");
		selectMenuItem("Assignments");
		assignmentCreation(assignmentName, categoryName, gradeName, score, instructionsDesc);
		
		enteringToRestrictionsFromAssignment(assignmentName);
		
		waitForVisibilityOfElement(By.xpath("//*[text()='Create and Attach']"), "Create and Attach button");
		scrollingToElementofAPage(By.xpath("//*[text()='Create and Attach']"));
		JSClick(By.xpath("//*[text()='Create and Attach']"), "Create and Attach button");
		Thread.sleep(3000);
		createNewReleaseCondition(" Grade value on a grade item", gradeName, "100");
		deletingAssignment(assignmentName);
		//validatingCreatedAssignmentOrGradeIsDeleted(assignmentName);		
		selectingMyTools("Grades");
		deletingGrade(gradeName);
		Thread.sleep(1000);
		logout();
		Thread.sleep(1000);
		closeBrowserAndLaunch(port, device);
	}
}