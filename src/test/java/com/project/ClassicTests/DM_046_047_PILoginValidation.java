package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_046_047_PILoginValidation extends BusinessFunctions{
	@Test
	public void DM_46_DM_47_PILoginValidation()throws Throwable {
		 tstData = Data_Provider.getTestData("SSO", "DM_046_047_PILoginValidation");
			
			String subjectArea = tstData.get("subjectArea");
			String catalog_no = tstData.get("catalog_no");
			String classSection = tstData.get("classSection");
			String term = tstData.get("term");
			String courseSearch=tstData.get("courseSearch");
			String port = configProps.getProperty("port");
			String device = configProps.getProperty("device");
			
		//	commandWrapper getCommand = new commandWrapper();
			//getCommand.launchUrl(url);
			//clearBrowserHistory();			
			launchUrl(url);
			classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
			courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
			logout();
			closeBrowserAndLaunch(port, device);
	}
}
