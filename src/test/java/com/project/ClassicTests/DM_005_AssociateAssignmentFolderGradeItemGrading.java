package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_005_AssociateAssignmentFolderGradeItemGrading extends BusinessFunctions {
	
	@Test
	public void DM_005_AssociateAnAssignmentFolderWithAGradeItemAndGrading() throws Throwable {
		
    tstData = Data_Provider.getTestData("Assignments", "DM_005_AssociateAssignmentFolderGradeItemGrading");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
	//	clearBrowserHistory();
		launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Assignments");
		assignmentCreation(assignmentName, categoryName, gradeName, score, instructionsDesc);
		
		selectingMyTools("Grades");
		//selectingOption("My Tools", "Grades");
		//selectingOption("Grades", "Manage Grades	");
		waitForVisibilityOfElement(By.linkText("Manage Grades"), "manage grades link");
		elementClick(By.linkText("Manage Grades"), "manage grades link");
		validatingCreatedAssignmentOrGrade(gradeName);
		
//		Deleting created assignment
		selectMenuItem("Assignments");
		deletingAssignment(assignmentName);
		logout();
		closeBrowserAndLaunch(port, device);
	}		
}