package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.DiscussionsObjects;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_015_VerifyUserAbleToCreateEditDeleteTopic extends BusinessFunctions {
	
	@Test
	public void DM_15_VerifyUserAbleTo_CreateEditDeleteTopic() throws Throwable {
		
    tstData = Data_Provider.getTestData("Discussions", "DM_015_VerifyUserAbleToCreateEditDeleteTopic");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String forumName = tstData.get("forumName")+getRandomNumberDate();
		String forumDesc = tstData.get("forumDesc");
		String topicName = tstData.get("topicName")+getRandomNumberDate();
		String topicDesc = tstData.get("topicDesc");
		String editedTopicName = tstData.get("editedTopicName")+getRandomNumberDate();
		String editedTopicDesc = tstData.get("editedTopicDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		//clearBrowserHistory();
		launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));	
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);
		selectMenuItem("Discussions");
		selectingViewFromSettings("Grid View");
		creatingForum(forumName, forumDesc);
		creatingOrEditingTopic(topicName, topicDesc);	
		Thread.sleep(3000);
		waitForVisibilityOfElement(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName),"Inverted arrow for topic created");
		scrollingToElementofAPage(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName));
		elementClick(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName),"Inverted arrow for topic created");
		Thread.sleep(2000);
		JSClick(DiscussionsObjects.d2LmenuItemLinktextName("Edit Topic"), "Edit Topic");
//		JSClick(DiscussionsObjects.actionsForTopic(topicName, "Edit Topic"), "Edit Topic");
		creatingOrEditingTopic(editedTopicName, editedTopicDesc);
		//validatingCreatedTopicOrForum(editedTopicName);
		deletingTopic(editedTopicName);
		//validatingDeletedTopicOrForum(editedTopicName);		
		logout();
		closeBrowserAndLaunch(port, device);
	}

}
