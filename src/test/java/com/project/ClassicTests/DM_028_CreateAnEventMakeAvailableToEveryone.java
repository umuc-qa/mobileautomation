package com.project.ClassicTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_028_CreateAnEventMakeAvailableToEveryone extends BusinessFunctions {
	
	/*DM-28:Test case Summary
	Create Event  - Make available to everyone  - Make available all day  
	Go to Calendar and Search for the created Event.    
	Update and restrict dates for event.
	*/
	
	@Test
	public void DM_0028_Create_An_EventMakeAvailableToEveryone() throws Throwable {
	
		tstData = Data_Provider.getTestData("Calendar","DM_028_CreateAnEventMakeAvailableToEveryone");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch=tstData.get("courseSearch");
		String EventTitle=tstData.get("EventTitle")+getRandomNumberDate();
		String Description=tstData.get("Description");
		String TestStartDate="";
		String TestEndDate="";
	    String Location=tstData.get("Location");
	    String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
	
		//commandWrapper getCommand = new commandWrapper();
		//getCommand.launchUrl(url);
		//clearBrowserHistory();
		launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
		clickCalendar();
		createEvent(courseSearch,EventTitle,Description,TestStartDate,TestEndDate,Location);
		logout();	
		closeBrowserAndLaunch(port, device);
	}
}