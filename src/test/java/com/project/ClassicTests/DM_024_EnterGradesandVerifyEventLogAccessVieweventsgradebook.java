package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_024_EnterGradesandVerifyEventLogAccessVieweventsgradebook extends BusinessFunctions {
	
	/*  DM-24:Test Case Summary
	 Enter Grades/ More Actions/ Event Log Access and View events log of grade book.
	*/
	
	@Test
	public void DM_024_Enter_GradesandVerifyEventLogAccessVieweventsgradebook() throws Throwable {
		
      	tstData = Data_Provider.getTestData("Grades","DM_024_EnterGradesandVerifyEventLogAccessVieweventsgradebook");
	
		//Common objects
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch=tstData.get("courseSearch");
		String assignmentName = tstData.get("assignmentName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String score = tstData.get("score");
		String instructionsDesc = tstData.get("instructionsDesc");
		String UserName = tstData.get("UserName");
		String GradeValue = tstData.get("GradeValue");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		launchUrl(url);
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term,courseSearch);
		selectMenuItem("Assignments");
		assignmentCreation(assignmentName,categoryName,gradeName,score,instructionsDesc);
		
		selectingMyTools("Grades");
		enterGrades(gradeName,GradeValue,UserName);		
		logout();
		closeBrowserAndLaunch(port, device);
	}	
}
