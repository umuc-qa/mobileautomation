package com.project.ClassicTests;

import org.testng.annotations.Test;
import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_034_PropertiesCreateQuizImportQuestionsRestrictionsSetQuizActiveDisplayCalendarAssessment extends BusinessFunctions {
	
	@Test
	public void DM_034_PropertiesCreateQuizImportQuestionsRestrictions_SetQuizActiveDisplayCalendarAssessment() throws Throwable {
		
		tstData = Data_Provider.getTestData("Quizzes&Exams", "DM_034_PropertiesCreateQuizImportQuestionsRestrictionsSetQuizActiveDisplayCalendarAssessment");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String quizName = tstData.get("quizName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String menuItem = tstData.get("menuItem");
		String questionTitle = tstData.get("questionTitle");
		String questionText = tstData.get("questionText");
		String trueWeight = tstData.get("trueWeight");
		String trueFeedBack = tstData.get("trueFeedBack");
		String falseWeight = tstData.get("falseWeight");
		String falseFeedBack = tstData.get("falseFeedBack");
		String editQuizName = tstData.get("editQuizName")+getRandomNumberDate();
		String editCategoryName = tstData.get("editCategoryName")+getRandomNumberDate();
		String gradeName = tstData.get("gradeName")+getRandomNumberDate();
		String rubricName = tstData.get("rubricName")+getRandomNumberDate();
		String rubricDescription = tstData.get("rubricDescription");
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(configProps.getProperty("App_URL"));
		clearBrowserHistory();
		getCommand.launchUrl(configProps.getProperty("App_URL"));	
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);		
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Quizzes");
		enteringQuizDetails(quizName, categoryName);
		addingQuizQuestions(menuItem, questionTitle, questionText, trueWeight, trueFeedBack, falseWeight, falseFeedBack);
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Quizzes");
		editingQuizDetails(quizName, editQuizName, editCategoryName);
		importingQuizFromCollection();
		addRestrictionsToQuizzes();
		addAssessmentToQuizzes(gradeName, rubricName, rubricDescription);
		//Logout();
	}
}