package com.project.ClassicTests;

import org.openqa.selenium.By;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.DiscussionsObjects;
import com.project.setup.Data_Provider;

public class DM_010_ReadingView_VerifyUserAbleToCreateEditDeleteTopic extends BusinessFunctions {
	
	@Test
	public void DM_010_ReadingView_VerifyUserAbleTo_CreateEditDeleteTopic() throws Throwable {
		
		tstData = Data_Provider.getTestData("Discussions", "DM_010_ReadingView_VerifyUserAbleToCreateEditDeleteTopic");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String forumName = tstData.get("forumName")+getRandomNumberDate();
		String forumDesc = tstData.get("forumDesc");
		String topicName = tstData.get("topicName")+getRandomNumberDate();
		String topicDesc = tstData.get("topicDesc");
		String editedTopicName = tstData.get("editedTopicName")+getRandomNumberDate();
		String editedTopicDesc = tstData.get("editedTopicDesc");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		
		launchUrl(configProps.getProperty("App_URL"));
		//clearBrowserHistory();
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);		
		Thread.sleep(20000);
		selectMenuItem("Discussions");
		selectingViewFromSettings("Reading View");
		creatingForum(forumName, forumDesc);
		creatingOrEditingTopic(topicName, topicDesc);		
		waitForVisibilityOfElement((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)), topicName);
    	scrollingToElementofAPage((DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName)));
    	JSMousehoverDoubleClick(DiscussionsObjects.Inverted_Triangle_Beside_Topics(topicName),"Inverted arrow for topic created");
		Thread.sleep(2000);
		scrollingToElementofAPage((DiscussionsObjects.d2LmenuItemLinktextName("Edit Topic")));
		JSClick(DiscussionsObjects.d2LmenuItemLinktextNameandIDName("Edit Topic"), "Edit Topic");
		Thread.sleep(3000);
		creatingOrEditingTopic(editedTopicName, editedTopicDesc);
		//validatingCreatedTopicOrForum(editedTopicName);
		deletingTopic(editedTopicName);	
		Thread.sleep(3000);
		//validatingDeletedTopicOrForum(editedTopicName);
		restoringForumOrTopic(forumName,editedTopicName);
		selectMenuItem("Discussions");
		validatingCreatedTopicOrForum(editedTopicName);
		logout();
		closeBrowserAndLaunch(port, device);
	}
}