package com.project.ClassicTests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AssignmentsObjects;
import com.project.report.Utilities.Data_Provider;

public class DM_27_LearnerLoginAndAbleToSeeCoursesEnrolled extends BusinessFunctions{
	@Test
	public void DM_27_learnerLoginAndAbleToSeeCoursesEnrolled()throws Throwable {
		 tstData = Data_Provider.getTestData("SSO", "DM_048_049_LearnerLoginValidation");
			
			String subjectArea = tstData.get("subjectArea");
			String catalog_no = tstData.get("catalog_no");
			String classSection = tstData.get("classSection");
			String term = tstData.get("term");
			String courseSearch=tstData.get("courseSearch");
			String port = configProps.getProperty("port");
			String device = configProps.getProperty("device");
			String className =subjectArea+" "+catalog_no+" "+classSection+" "+term ;
			
			
			launchUrl(configProps.getProperty("App_URL"));
			classicLogIn(configProps.getProperty("Learner1"), configProps.getProperty("LearnerPwd"));
			verifyUserAbleViewAllTheClassesEnrolled();
			textSearchRubricDoubleShadowDOM(className, "d2l-icon");
			Thread.sleep(3000);
			elementClick(By.xpath("//*[text()='"+courseSearch+"']"), "Selected Class");
			Thread.sleep(2000);
			logout();
			closeBrowserAndLaunch(port, device);
			
	}
}
