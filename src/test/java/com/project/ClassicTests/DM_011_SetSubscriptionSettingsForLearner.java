package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;

public class DM_011_SetSubscriptionSettingsForLearner extends BusinessFunctions{
	
	@Test
	public void DM_011_SetSubscriptionSettings_ForLearner() throws Throwable {
   
		tstData = Data_Provider.getTestData("Discussions", "DM_011_SetSubscriptionSettingsForLearner");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String port = configProps.getProperty("port");
		String device = configProps.getProperty("device");
		launchUrl(configProps.getProperty("App_URL"));
		//clearBrowserHistory();		
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);		
		selectMenuItem("Discussions");
		subscriptionSettings();
		logout();
		closeBrowserAndLaunch(port, device);
	}
}
