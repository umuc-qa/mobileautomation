package com.project.ClassicTests;

//import io.appium.java_client.SwipeElementDirection;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.ClassicObjectRepository.AnnouncementObjects;
import com.project.ClassicObjectRepository.CommonObjects;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_002_027_ViewNewsItems extends BusinessFunctions{

	@Test
	public void DM_002_027ViewNewsItems() throws Throwable {
		CommandWrapper getCommand = new CommandWrapper();
			
		tstData = Data_Provider.getTestData("Announcements", "DM_002_027_ViewNewsItems");

		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		
		String announcementName = tstData.get("announcementName")+getRandomNumberDate();
		String announcementNameTwo = tstData.get("announcementNameTwo")+getRandomNumberDate();
		String contentDesc = tstData.get("contentDesc");
		String contentDescTwo = tstData.get("contentDescTwo");
		
		launchUrl(configProps.getProperty("App_URL"));

		clearBrowserHistory();
				
//		Launch URL
		launchUrl(configProps.getProperty("App_URL"));
		
//	    Login as PI
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		
//		Course Search			
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
			
		//elementSwipe(SwipeElementDirection.DOWN, 100, 10000);
// 		Creating new Announcement with present start time
		if(configProps.getProperty("deviceType") == "iOS"){
			explicitWait(iosDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
		}
		else
		{
			//explicitWait(androidDriver, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));
			explicitWait(androidDriver1, By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle));

		}
		elementClick(By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle), "Announcements Inverted Triangle");
		elementClick(By.xpath(AnnouncementObjects.Announcements_Inverted_Triangle_Value), "Announcements Inverted Triangle Value");
		creatingAnnouncement(announcementName, contentDesc, true, "Video", false, 0 , false, 0);
		
		Thread.sleep(5000);
//      Creating new Announcement with future start and end time
		selectingOption("New Announcement","New Announcement");
		creatingAnnouncement(announcementNameTwo, contentDescTwo, true, "Image", true, 8, true, 15);
		elementHardSleep(10000);
		
//		Logout();
		clearBrowserHistory();

		launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term , courseSearch);		
		Thread.sleep(5000);
		
		if(!getCommand.elementExists(By.xpath(CommonObjects.MyRole_InvertedTriangle))){
			SuccessReport("My Role", "Successfully verified my role");
		}else{
			failureReport("My Role", "Failed to verify my role");
		}
		
		selectingOption("Announcements", "Announcements");
		Thread.sleep(10000);
		
		verifyAnnouncementPresent(announcementName);
		verifyAnnouncementnotPresent(announcementNameTwo);
		Thread.sleep(120000);
		pageRefresh();
		verifyAnnouncementPresent(announcementNameTwo);
		
		//logout();
	}
}
