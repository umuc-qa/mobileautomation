package com.project.ClassicTests;

import org.testng.annotations.Test;

import com.project.BusinessFunctions.BusinessFunctions;
import com.project.setup.Data_Provider;
import com.project.setup.CommandWrapper;

public class DM_030_036_AddQuestionsToQuizAsLearnerQuizTimeExpireAccessQuizTakeSubmitQuiz extends BusinessFunctions {
	
	@Test
	public void DM_030_AddQuestions_ToQuiz() throws Throwable {
		
		tstData = Data_Provider.getTestData("Quizzes&Exams", "DM_030_Add_Questions_To_Quiz");
		
		String subjectArea = tstData.get("subjectArea");
		String catalog_no = tstData.get("catalog_no");
		String classSection = tstData.get("classSection");
		String term = tstData.get("term");
		String courseSearch = tstData.get("courseSearch");
		String quizName = tstData.get("quizName")+getRandomNumberDate();
		String categoryName = tstData.get("categoryName")+getRandomNumberDate();
		String menuItem = tstData.get("menuItem");
		String questionTitle = tstData.get("questionTitle");
		String questionText = tstData.get("questionText");
		String trueWeight = tstData.get("trueWeight");
		String trueFeedBack = tstData.get("trueFeedBack");
		String falseWeight = tstData.get("falseWeight");
		String falseFeedBack = tstData.get("falseFeedBack");
		String attempt = tstData.get("attempt");
		CommandWrapper getCommand = new CommandWrapper();
		
		getCommand.launchUrl(configProps.getProperty("App_URL"));
		clearBrowserHistory();
		getCommand.launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("PIUser"), configProps.getProperty("PIPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);		
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Quizzes");
		enteringQuizDetails(quizName, categoryName);
		addingQuizQuestions(menuItem, questionTitle, questionText, trueWeight, trueFeedBack, falseWeight, falseFeedBack);
		addingQuizStatus(5);
		//Logout();
		
		clearBrowserHistory();
		getCommand.launchUrl(configProps.getProperty("App_URL"));
		classicLogIn(configProps.getProperty("LearnerUser"), configProps.getProperty("LearnerPwd"));
		courseSearch(subjectArea+" "+catalog_no+" "+classSection+" "+term, courseSearch);		
		selectMenuItem("My Tools");
		selectingOption("My Tools", "Quizzes");
		submittingQuiz(quizName, attempt);
		//Logout();
	}
}