package com.project.report.Accelerators;
import java.awt.Color;
import java.awt.Font;
import java.io.File;import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.text.DefaultCaret;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.android.library.AndroidWebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.project.report.Support.*;
import com.project.report.Utilities.*;
import com.project.setup.TestSetup;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;


	

public class TestEngine extends SendMail {
	public static Logger logger = Logger.getLogger(TestSetup.class.getName());

	public static ConfiguratorSupport configProps = new ConfiguratorSupport("config.properties");

	public static ConfiguratorSupport counterProp = new ConfiguratorSupport(configProps.getProperty("counterPath"));
 
	public static HashMap<String, String> tstData = null;
	//public static HashMap<String,String> ExecutionData = new HashMap<String, String>();
 
	public String currentSuite = "";
	public String method = "";
	
	public boolean flag = false;
	
	public static WebDriver webDriver = null;
	//public static WebDriver appiumDriver = null;
	public static WebDriver d = null;
	public static EventFiringWebDriver driver = null;
	public static int stepHeaderNum = 0;
	public static int stepNum = 0;
	public static int PassNum = 0;
	public static int FailNum = 0;
 
	public static int passCounter = 0;
	public static int failCounter = 0;
	public static int skipCounter = 0;
	// public failCounter =0;
	public String testName = "";
	public String testCaseExecutionTime = "";
	public StringBuffer x = new StringBuffer();
	public String finalTime = "";
	public boolean isSuiteRunning = false;
	public static String testCaseDescription="";
	public static Map<String, String> testDescription = new LinkedHashMap<String, String>();
	public static Map<String, String> testResults = new LinkedHashMap<String, String>();
	public String url = null;
	/*static ExcelReader xlsrdr = new ExcelReader(configProps.getProperty("TestData"),
			configProps.getProperty("sheetName0"));
	*/
	public int countcompare = 0;
	public static String projectName="d2l";
	public static String browser1;
	static int len = 0;
	static int i = 0;
	public static ITestContext itc;
	public static String groupNames = null;
	public static String rpSumDt = null;
	public static File file=null;
	public static String  application=null;
	public static String rStartDate = null;
	public static String suiteName = "";
	public static String folderName = null;
	public static String chartName = null;

	/**
	 * Initializing browser requirements, Test Results file path and Database
	 * requirements from the configuration file
	 * 
	 * 
	 * @Revision History
	 * 
	 */
	 //Before Suite
	@BeforeSuite(alwaysRun = true)
	public void first(ITestContext context) throws Throwable {
	try{
		SimpleDateFormat rpSumDtSDT = new SimpleDateFormat("hhmmss");
		rpSumDt = rpSumDtSDT.format(new Date());
		rStartDate=rpSumDtSDT.format(new Date());
		ReportStampSupport.calculateSuiteStartTime(); 
		Map<String, String> suiteParameters = context.getCurrentXmlTest().getSuite().getParameters();
		//Map<String, String> testParameters = context.getCurrentXmlTest().getSuite().getTests().toString();
		String testParameters = context.getCurrentXmlTest().getSuite().getTests().toString();
		System.out.println(testParameters);
		//System.out.println(testParameters.get("name"));
		// Setting the configuration Path
		//Configuration.PutProperty("ConfigFilePath", suiteParameters.get("ConfigFilePath"));
		// 1.Read the configuration
		//Configuration.loadConfiguration();
		System.out.println("~~~~~~~~Before Suite~~~~~~~~");
		//System.out.println("Before Test: " +stepHeaderNum);
		//System.out.println(suiteParameters.get("CurrentApplication"));
		Configuration.PutProperty("CurrentApplication", suiteParameters.get("CurrentApplication"));
		Configuration.PutProperty("browser", suiteParameters.get("browser"));
		browser1=suiteParameters.get("browser");
		application=suiteParameters.get("CurrentApplication");
		suiteName=context.getCurrentXmlTest().getSuite().getName().replace(" ", "_").trim();
		
		
		
		}catch(Exception ex){
	 
	}
	}

	//Before Class
	@Parameters({"browser"})
	@BeforeClass(alwaysRun = true)
	public void first(ITestContext ctx, String browser) throws Throwable {
		System.out.println("~~~~~~~~Before Class~~~~~~~~");
		/*itc = ctx;
		//browser="chrome";
		if (browser.equalsIgnoreCase("firefox")) {
			ProfilesIni profiles=new ProfilesIni();
	        FirefoxProfile p=profiles.getProfile("default");
			webDriver = new FirefoxDriver(p);
			i = i + 1;
			

		} else if (browser.equalsIgnoreCase("ie")) { 
			File file = new File("Drivers/IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "about:blank");
			caps.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			caps.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
			caps.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true); 
			caps.setCapability("IE.binary", "C:/Program Files/Internet Explorer/iexplore.exe");
			caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			caps.setCapability("ignoreProtectedModeSettings", true);
			caps.setJavascriptEnabled(true);
			webDriver = new InternetExplorerDriver(caps);
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			webDriver = new InternetExplorerDriver();
			i = i + 1;

		} 
		else if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
			webDriver = new ChromeDriver();
			i = i + 1;

		}
		else if (browser.equalsIgnoreCase("safari")) {
			SafariOptions cap = new SafariOptions();
			System.setProperty("webdriver.safari.noinstall", "true");
			File file = new File("Drivers/SafariDriver.safariextz");
			System.setProperty("webdriver.safari.driver", file.getAbsolutePath());
			// cap.setCapability("safari.dataDir","/Users/me/Library/Safari");
			cap.setUseCleanSession(true); //if you wish safari to forget session everytime
			webDriver = new SafariDriver(cap);
		}
		driver = new EventFiringWebDriver(webDriver);
		//d=driver;

		MyListener myListener = new MyListener();
		driver.register(myListener);
		
		try {
			//driver.manage().deleteAllCookies();
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			reportCreater();
			currentSuit = ctx.getCurrentXmlTest().getSuite().getName();
		} catch (Exception e1) {
			System.out.println(e1);
		}*/

	}

	/**
	 * De-Initializing and closing all the connections
	 * 
	 * @throws Throwable
	 */
	//After Suite
	@Parameters({"browser"})
	@AfterSuite(alwaysRun = true)
	public void tearDownFirefox(ITestContext ctx,String browser) throws Throwable {
		System.out.println("~~~~~~~~After Suite~~~~~~~~");
		ReportStampSupport.calculateSuiteExecutionTime();
		//HtmlReportSupport.createHtmlSummaryReport(browser);
		/*if(driver!=null)
			driver.quit();*/
		if (configProps.getProperty("MailTransfer").equalsIgnoreCase("True"))
            SendMail.attachReportsToEmail();

	}
	/*//@Parameters({"browser"})
	//@AfterSuite(alwaysRun = true)
	public void tearDownChrome(ITestContext ctx) throws Throwable {

		browser = "Chrome";
		ReportStampSupport.calculateSuiteExecutionTime();

		HtmlReportSupport.createHtmlSummaryReport(browser);

		driver.quit();
		closeSummaryReport(browser);

	}*/

	/**
	 * Write results to Browser specific path
	 * @Revision History
	 * 
	 */
	// @Parameters({"browserType"})
	public static String filePath() {
		String strDirectoy = "";

		//strDirectoy = TestEngine.folderName+"_"+configProps.getProperty("ReleaseVersion")+"_"+rStartDate;

		strDirectoy = TestSetup.folderName.trim()+"_"+rStartDate;
		if (strDirectoy != "") {
			new File(configProps.getProperty("screenShotPath") + strDirectoy).mkdirs();
		}

		File results = new File(configProps.getProperty("screenShotPath") + strDirectoy + "/" + "Screenshots");
		if (!results.exists()) {
			results.mkdir();
			HtmlReportSupport.copyLogos();
		}

		return configProps.getProperty("screenShotPath") + strDirectoy;

	}

	/**
	 * Browser type prefix for Run ID
	 * @throws InterruptedException 
	 * 
	 * @Revision History
	 * 
	 */
	public String result_browser() throws InterruptedException {
		if (groupNames.equals("")) {
			if (configProps.getProperty("browserType").equals("ie")) {
				return "IE";
			} else if (configProps.getProperty("browserType").equals("firefox")) {
				return "Firefox";
			} else {
				return "Chrome";
			}
		} else {
			if (browser.equalsIgnoreCase("ie")) {
				return "IE";

			} else if (browser.equalsIgnoreCase("firefox")) {
				return "Firefox";

			} else {
				killBrowserInstance();
				return "Chrome";

			}
		}
	}

	/**
	 * Related to Xpath
	 * 
	 * @Revision History
	 * 
	 */
	public String methodName() {
		if (groupNames.equals("")) {
			if (configProps.getProperty("browserType").equals("ie")) {
				return "post";
			} else {
				return "POST";
			}
		} else {
			if (browser.equals("ie")) {
				return "post";
			} else {
				return "POST";
			}
		}
	}

	//Before Method
	@Parameters({ "browser" })
	@BeforeMethod(alwaysRun = true)
	public void reportHeader(Method method, ITestContext ctx, String browser) throws Throwable {
		System.out.println("~~~~~~~~Before Method~~~~~~~~");
		/*SimpleDateFormat rpDt = new SimpleDateFormat("hhmmss");
		rpTime = rpDt.format(new Date());*/
		
		//===============================
		itc = ctx;
		//browser="chrome";
		if (browser.equalsIgnoreCase("firefox")) {
			ProfilesIni profiles=new ProfilesIni();
	        FirefoxProfile p=profiles.getProfile("default");
			webDriver = new FirefoxDriver(p);
			i = i + 1;
			

		} else if (browser.equalsIgnoreCase("ie")) { 
			File file = new File("Drivers/IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "about:blank");
			caps.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			caps.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
			caps.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true); 
			caps.setCapability("IE.binary", "C:/Program Files/Internet Explorer/iexplore.exe");
			caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			caps.setCapability("ignoreProtectedModeSettings", true);
			caps.setJavascriptEnabled(true);
			webDriver = new InternetExplorerDriver(caps);
			/*System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			webDriver = new InternetExplorerDriver();*/
			i = i + 1;

		} 
		else if (browser.equalsIgnoreCase("chrome")) {
			
			killBrowserInstance();
			Thread.sleep(2000);
			System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
			webDriver = new ChromeDriver();
			i = i + 1;

		}
		else if (browser.equalsIgnoreCase("safari")) {
			SafariOptions cap = new SafariOptions();
			System.setProperty("webdriver.safari.noinstall", "true");
			/*File file = new File("Drivers/SafariDriver.safariextz");
			System.setProperty("webdriver.safari.driver", file.getAbsolutePath());*/
			// cap.setCapability("safari.dataDir","/Users/me/Library/Safari");
			cap.setUseCleanSession(true); //if you wish safari to forget session everytime
			webDriver = new SafariDriver(cap);
		}
		driver = new EventFiringWebDriver(webDriver);
		//d=driver;
		Thread.sleep(2000);
		MyListener myListener = new MyListener();
		driver.register(myListener);
		
		try {
			//driver.manage().deleteAllCookies();
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			
			//FolderCreation
			reportCreater();
			currentSuit = ctx.getCurrentXmlTest().getSuite().getName();
		} catch (Exception e1) {
			System.out.println(e1);
		}
		//===============================
		
		itc = ctx;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");
		sdf.format(date);
		calculateTestCaseStartTime();

		flag = false;

		tc_name = method.getName().toString() + "-" + browser;
		String[] ts_Name = this.getClass().getName().toString().split("\\.");
		packageName = ts_Name[0] + "." + ts_Name[1] + "." + ts_Name[2];

		testHeader(tc_name.replace("-"+browser, ""));

		stepNum = 0;
		PassNum = 0;
		FailNum = 0;
		testName = method.getName();
		
		String[] tmp = testName.split("_");
		String desc ="";
        if((this.testCaseDescription!=null) && (!this.testCaseDescription.isEmpty())){
        	desc=this.testCaseDescription;
        }else{		 
        	for(int i=0;i<tmp.length;i++){
        		desc = desc + " " + tmp[i];
        	}
        } 
		testDescription.put(testName + "-" + browser, desc);

	}

	
	/*// Before Method from Get Swift

    //@SuppressWarnings("rawtypes")

    //@Parameters({ "browser" })

    @BeforeMethod(alwaysRun = true)

    public void reportHeader(Method method, ITestContext ctx) throws Throwable {

           System.out.println("~~~~~~~~Before Method~~~~~~~~");



           itc = ctx;

           if (browser.equalsIgnoreCase("firefox")) {

                  ProfilesIni profiles = new ProfilesIni();

                  FirefoxProfile p = profiles.getProfile("default");

                  webDriver = new FirefoxDriver(p);

                  i = i + 1;

           } else if (browser.equalsIgnoreCase("ie")) {

                  browser=browser.toUpperCase()+" 11";

                  File file = new File("Drivers/IEDriverServer.exe");

                  System.setProperty("webdriver.ie.driver", file.getAbsolutePath());

                  DesiredCapabilities caps = DesiredCapabilities.internetExplorer();

                  caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);

                  caps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "about:blank");

                  caps.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);

                  caps.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);

                  caps.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);

                  caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);

                  caps.setCapability("IE.binary", "C:/Program Files/Internet Explorer/iexplore.exe");

                  caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

                  caps.setCapability("ignoreProtectedModeSettings", true);

                  caps.setJavascriptEnabled(true);

                  webDriver = new InternetExplorerDriver(caps);

                  i = i + 1;



           } else if (browser.equalsIgnoreCase("chrome")) {

                  // killBrowserInstance();

                  Thread.sleep(2000);

                  System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");

                  webDriver = new ChromeDriver();

                  i = i + 1;



           } else if (browser.equalsIgnoreCase("safari")) {

                  SafariOptions cap = new SafariOptions();

                  System.setProperty("webdriver.safari.noinstall", "true");

                  cap.setUseCleanSession(true); // if you wish safari to forget session everytime

                  webDriver = new SafariDriver(cap);

           }

           driver = new EventFiringWebDriver(webDriver);

           // d=driver;

           Thread.sleep(2000);

           MyListener myListener = new MyListener();

           driver.register(myListener);

          



           try {

                  // driver.manage().deleteAllCookies();



                  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

                  driver.manage().window().maximize();



                  // FolderCreation

                  reportCreater();

                  currentSuit = ctx.getCurrentXmlTest().getSuite().getName();

           } catch (Exception e1) {

                  System.out.println(e1);

           }

          

          

          

           //Create Mobile object

           try {

                  int a=1;

                  if(configProps.getProperty("isNative").equalsIgnoreCase("yes")){

                        if(configProps.getProperty("browserType").equalsIgnoreCase("Android") && !app.equalsIgnoreCase("NA")) {

                               DesiredCapabilities capabilities = new DesiredCapabilities();

                               capabilities.setCapability("BROWSER_NAME", configProps.getProperty("browserType"));

                               //capabilities.setCapability("VERSION", platformVersion);

                               //capabilities.setCapability("deviceName",udid);

                               //capabilities.setCapability("platformName",platformName);

                               capabilities.setCapability("newCommandTimeout", 300000);

                               capabilities.setCapability("appPackage", configProps.getProperty("ANDROID_APP_PACKAGE"));//"co.getswift.kfg.qa2"

                               capabilities.setCapability("appActivity",configProps.getProperty("ANDROID_APP_ACTIVITY")); //"co.getswift.swift.UI.MainActivity"

                               //capabilities.setCapability("session-override", true);

                              

                               appiumDriver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);      

                              

                              

                              

                               //appiumDriver.findElement(By.xpath("//*[@class='android.widget.EditText' and @text='Email or username']")).sendKeys("abdfh");;

                        }

                        if(configProps.getProperty("browserType").equalsIgnoreCase("iOS") && !app.equalsIgnoreCase("NA")) {

                               DesiredCapabilities capabilities = new DesiredCapabilities();

                          capabilities.setCapability("platformName", "iOS");

                          capabilities.setCapability("deviceName", "iPhone 6");

                          capabilities.setCapability("platformVersion", "8.4");

                          //capabilities.setCapability("app", "https://s3.amazonaws.com/appium/TestApp8.4.app.zip");

                          capabilities.setCapability("browserName", "");

                          //capabilities.setCapability("deviceOrientation", "portrait");

                 

                          //appiumDriver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

                                            

                        }

                       

                  }

           } catch (Exception e) {

                  System.out.println("Issue with appium server or mobile set up");

                  e.printStackTrace();

           }



          

           // ===============================



           itc = ctx;

           Date date = new Date();

           SimpleDateFormat sdf = new SimpleDateFormat("dd_MMM_yyyy hh mm ss SSS");

           sdf.format(date);

           calculateTestCaseStartTime();



           flag = false;



           tc_name = method.getName().toString();

           // + "-" + browser;

           if (tc_name.length() >= 62)

                  tc_name = tc_name.substring(0, 62);

           String[] ts_Name = this.getClass().getName().toString().split("\\.");

           packageName = ts_Name[0] + "." + ts_Name[1] + "." + ts_Name[2];



           testHeader(tc_name);



           stepNum = 0;

           PassNum = 0;

           FailNum = 0;

           testName = method.getName();



           String[] tmp = testName.split("_");

           String desc = "";

           if ((this.testCaseDescription != null) && (!this.testCaseDescription.isEmpty())) {

                  desc = this.testCaseDescription;

           } else {

                  for (int i = 0; i < tmp.length; i++) {

                        desc = desc + " " + tmp[i];

                  }

           }

           testDescription.put(testName + "-" + browser, desc);



    }

*/
	
	
	
//	/~~~~~~~~After Method~~~~~~~~
	@Parameters({ "browser" })
	@AfterMethod(alwaysRun = true)
	public void tearDown(String browser) throws Exception {
		System.out.println("~~~~~~~~After Method~~~~~~~~");
		calculateTestCaseExecutionTime();
		closeDetailedReport(browser);
		
		try {
			
			if (FailNum != 0) {

				testResults.put(tc_name, "FAIL");
				failCounter = failCounter + 1;
			} else {
				testResults.put(tc_name, "PASS");
				passCounter = passCounter + 1;
			}
			
			
			
			/*if (FailNum != 0 && tstData!=null ) {
				
				testResults.put(tc_name, "FAIL");
				failCounter = failCounter + 1;
			} else if(FailNum == 0 && tstData!=null){
				testResults.put(tc_name, "PASS");
				passCounter = passCounter + 1;
			}else {// if(tstData.get("ExecuteStatus").trim().equalsIgnoreCase("No")){
				testResults.put(tc_name, "SKIP");
				skipCounter=skipCounter+1;
			}*/
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println(tc_name);
		HtmlReportSupport.createHtmlSummaryReport(browser);
		try {
			driver.switchTo().alert();
			//return true;
		} // try
		catch (NoAlertPresentException Ex) {
			//return false;
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
			Date date = new Date();
			File scrFile = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(TestSetup.filePath() + "/Screenshots/"
					+testName+dateFormat.format(date) + ".png"));
		}
		driver.close();
		/*if(driver!=null)
			driver.quit();*/

	}

	//~~~~~~~~After Class~~~~~~~~
	@Parameters({ "browser" })
	@AfterClass(alwaysRun = true)
	public void close(String browser) {
		System.out.println("~~~~~~~~After Class~~~~~~~~");
		if(driver!=null)
			driver.quit();
		/*driver.close();*/
		/*if(driver!=null)
			driver.quit();
		
		driver.close();
		if(driver!=null)
			driver.quit();
		if (FailNum != 0) {

			testResults.put(tc_name, "FAIL");
			failCounter = failCounter + 1;
		} else {
			testResults.put(tc_name, "PASS");
			passCounter = passCounter + 1;
		}*/
	}

	public void reportCreater() throws Throwable {
		int intReporterType = Integer.parseInt(configProps.getProperty("reportsType"));

		switch (intReporterType) {
		case 1:

			break;
		case 2:

			//htmlCreateReport();
			// HtmlReportSupport.createDetailedReport();

			break;
		default:

			htmlCreateReport();
			break;
		}
	}

	public void calculateTestCaseStartTime() {
		iStartTime = System.currentTimeMillis();
	}

	/***
	 * This method is supposed to be used in the @AfterMethod to calculate the
	 * total test case execution time to show in Reports by taking the start
	 * time from the calculateTestCaseStartTime method.
	 */
	public void calculateTestCaseExecutionTime() {
		iEndTime = System.currentTimeMillis();
		iExecutionTime = (iEndTime - iStartTime);
		TimeUnit.MILLISECONDS.toSeconds(iExecutionTime);
		HtmlReportSupport.executionTime.put(tc_name, String.valueOf(TimeUnit.MILLISECONDS.toSeconds(iExecutionTime)));
		// System.out.println(tc_name+";Time
		// :"+String.valueOf(TimeUnit.MILLISECONDS.toSeconds(iExecutionTime)));
	}

 
//	public void onSuccess(String strStepName, String strStepDes) { 
//		onSuccess( strStepName, strStepDes, "");
//	}
// 
	public void onSuccess(String strStepName, String strStepDes, String stepTime) {   
		
 
		file = new File(TestSetup.filePath() + "/" + strTestName.split("-")[0] + "_"+rpTime+ ".html");// "SummaryReport.html"
		Writer writer = null;
		stepNum = stepNum + 1;
		
		try {
			// testdescrption.put(TestTitleDetails.x.toString(),
			// TestEngine.testDescription.get(TestTitleDetails.x));
			if (!map.get(packageName + ":" + tc_name).equals("FAIL")) {
				map.put(packageName + ":" + tc_name, "PASS");
				// map.put(TestTitleDetails.x.toString(),
				// TestEngine.testDescription.get(TestTitleDetails.x.toString()));
			}
			writer = new FileWriter(file, true);
			writer.write("<tr class='content2' >");
			writer.write("<td>" + stepNum + "</td> ");
			writer.write("<td class='justified'>" + strStepName + "</td>");
			writer.write("<td class='justified'>" + strStepDes.replace("[", "<b>").replace("]",  "</b>") + "</td> ");
			writer.write(
					"<td class='Pass' align='center'><font size='2' color='green'><B>Pass</B></font><img  src='./Screenshots/passed.ico' width='18' height='18'/></td> ");
			PassNum = PassNum + 1;
			String strPassTime = ReportStampSupport.getTime();
			writer.write("<td><small>" + strPassTime + "</small></td> ");
			writer.write("</tr> ");
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void closeStep(){
 
		
		if(stepHeaderNum>0){
			
		 
		try {
			File file = new File(TestSetup.filePath() + "/" + strTestName.split("-")[0] + "_"+rpTime+ ".html");// "SummaryReport.html"
			Writer writer = null; 
			writer = new FileWriter(file, true); 
			writer.write("</tbody>");
			writer.close();
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	}
//	public void addStep(String strStepName, String strStepDes) {
	public void addStep(String strStepDes) {
		closeStep();
		File file = new File(TestSetup.filePath() + "/" + strTestName.split("-")[0] + "_"+rpTime+ ".html");// "SummaryReport.html"
		Writer writer = null;
		 
		stepHeaderNum=stepNum+1; 
		String sheaderId="Header_"+stepHeaderNum;
		try {
		 
			writer = new FileWriter(file, true); 
			writer.write("<tbody><tr class='section'>");
			writer.write("<td colspan='5' onclick=toggleMenu('"+sheaderId+"')>+ ");
			writer.write( strStepDes.replace("[", "<b>").replace("]",  "</b>")
					+ "</td></tr></tbody>");
			writer.write("<tbody id='"+sheaderId+"' style='display:table-row-group'>");
			
			/*writer.write("<tr class='content2' >");
			writer.write("<td>" + stepNum + "</td> ");
			writer.write("<td class='justified'>" + strStepName + "</td>");
			writer.write("<td class='justified'><span style='text-align: left;'>" + strStepDes.replace("[", "<b>").replace("]",  "</b>") + "</span></td> ");
			writer.write("<td class='Pass' align='center'><img  src='./Screenshots/passed.ico' width='18' height='18'/></td> ");
			PassNum = PassNum + 1;
			String strPassTime = ReportStampSupport.getTime();
			writer.write("<td><small>" + strPassTime + "</small></td> ");
			writer.write("</tr> ");*/
			writer.close();
 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	 
	

	public void onWarning(String strStepName, String strStepDes) {
		onWarning( strStepName,  strStepDes,"");
	}
 
	public void onWarning(String strStepName, String strStepDes, String stepTime) {
 
		Writer writer = null;
		try {
			File file = new File(TestSetup.filePath() + "/" + strTestName.split("-")[0] + "_"+rpTime+ ".html");// "SummaryReport.html"

			writer = new FileWriter(file, true);
			stepNum = stepNum + 1;

			writer.write("<tr class='content2' >");
			writer.write("<td>" + stepNum + "</td> ");
			writer.write("<td class='justified'>" + strStepName + "</td>");
			writer.write("<td class='justified'>" + strStepDes + "</td> ");
			FailNum = FailNum + 1;

			writer.write("<td class='Fail'  align='center'><a  href='" + "./Screenshots" + "/"
					+ strStepDes.replace(" ", "_").replace("[", "").replace("]", "") +stepTime+ ".jpeg'"
					+ " alt= Screenshot  width= 15 height=15 style='text-decoration:none;'><img src='./Screenshots/warning.ico' width='18' height='18'/></a></td>");

			String strFailTime = ReportStampSupport.getTime();
			writer.write("<td><small>" + strFailTime + "</small></td> ");
			writer.write("</tr> ");
			writer.close();

		} catch (Exception e) {

		}

	}

	 
	public void onFailure(String strStepName, String strStepDes, String stepTime) {
		Writer writer = null;
		try {
			File file = new File(TestSetup.filePath() + "/" + strTestName.split("-")[0] + "_"+rpTime+ ".html");// "SummaryReport.html"

			writer = new FileWriter(file, true);
			stepNum = stepNum + 1;

			writer.write("<tr class='content2' >");
			writer.write("<td>" + stepNum + "</td> ");
			writer.write("<td class='justified'>" + strStepName + "</td>");
 
			writer.write("<td class='justified'>" + strStepDes.replace("[", "<b>").replace("]",  "</b>") + "</td> ");
 
			FailNum = FailNum + 1;

			writer.write("<td class='Fail' align='center'><a  href='" + "./Screenshots" + "/"
					+ strStepDes.replace(" ", "_").replace("[", "").replace("]", "") +stepTime+ ".jpeg'"
					+ " alt= Screenshot  width= 15 height=15 style='text-decoration:none;'><font size='2' color='red'><B>Fail</B></font><img  src='./Screenshots/failed.ico' width='18' height='18'/></a></td>");

			String strFailTime = ReportStampSupport.getTime();
			writer.write("<td><small>" + strFailTime + "</small></td> ");
			writer.write("</tr> ");
			writer.close();
			if (!map.get(packageName + ":" + tc_name).equals("PASS")) {
				map.put(packageName + ":" + tc_name + ":", "FAIL");
				// map.put(TestTitleDetails.x.toString(),
				// TestEngine.testDescription.get(TestTitleDetails.x.toString()));
			}
		} catch (Exception e) {

		}

	}

	public void closeDetailedReport(String browser) {
		

		try {
			closeStep();
			
			File file = new File(TestSetup.filePath() + "/" + strTestName.split("-")[0] + "_"+rpTime+ ".html");// "SummaryReport.html"
			Writer writer = null;
			writer = new FileWriter(file, true);
			writer.write("</table>");
			writer.write("<table id='footer'>");
			writer.write("<colgroup>");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("</colgroup>");
			writer.write("<tfoot>");
			writer.write("<tr class='heading'> ");
			writer.write("<th colspan='4'>Execution Time In Seconds (Includes Report Creation Time) : "
					+ executionTime.get(tc_name) + "&nbsp;</th> ");
			writer.write("</tr> ");
			writer.write("<tr class='content'>");
			writer.write("<td class='pass'>&nbsp;Steps Passed&nbsp;:</td>");
			writer.write("<td class='pass'> " + PassNum + "</td>");
			writer.write("<td class='fail'>&nbsp;Steps Failed&nbsp;: </td>");
			writer.write("<td class='fail'>" + FailNum + "</td>");
			writer.write("</tr>");
			writer.close();
		} catch (Exception e) {

		}
	}

	public void closeSummaryReport(String browser) {
		file = new File(TestSetup.filePath() + "/" + "Summary_Results_"+TestSetup.rpSumDt+ ".html");// "SummaryReport.html"
		Writer writer = null;
		try {
			writer = new FileWriter(file, true);

			writer.write("<table id='footer'>");
			writer.write("<colgroup>");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' />");
			writer.write("<col style='width: 25%' /> ");
			writer.write("</colgroup> ");
			writer.write("<tfoot>");
			writer.write("<tr class='heading'>");
			writer.write("<th colspan='4'>Total Duration  In Seconds (Including Report Creation) : "
					+ ((int) iTestExecutionTime) + "</th>");
			writer.write("</tr>");
			writer.write("<tr class='content'>");
			writer.write("<td class='pass'>&nbsp;Tests Passed&nbsp;:</td>");
			writer.write("<td class='pass'> " + passCounter + "</td> ");
			writer.write("<td class='fail'>&nbsp;Tests Failed&nbsp;:</td>");
			writer.write("<td class='fail'> " + failCounter + "</td> ");
			writer.write("</tr>");
			writer.write("</tfoot>");
			writer.write("</table> ");

			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reportStep(String StepDesc) {
		StepDesc = StepDesc.replaceAll(" ", "_");
		File file = new File(TestSetup.filePath() + "/" + strTestName.split("-")[0] + "_"+rpTime+ ".html");// "SummaryReport.html"
		Writer writer = null;

		try {
			writer = new FileWriter(file, true);
			if (BFunctionNo > 0) {
				writer.write("</tbody>");
			}
			writer.write("<tbody>");
			writer.write("<tr class='section'> ");
			writer.write("<td colspan='5' onclick=toggleMenu('" + StepDesc + stepNum + "')>+ " + StepDesc + "</td>");
			writer.write("</tr> ");
			writer.write("</tbody>");
			writer.write("<tbody id='" + StepDesc + stepNum + "' style='display:table-row-group'>");
			writer.close();
			BFunctionNo = BFunctionNo + 1;
		} catch (Exception e) {

		}
	}
	
	/**
	 * killBrowserInstance :: It kills the browser processes running if any.
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public static void killBrowserInstance() throws InterruptedException {
		try {
			System.out.println("..");
			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
			//Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
			Thread.sleep(10000);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used for Results Folder creation with TestName froM XML
	 * 
	 * @param testContext
	 */
	
	@BeforeTest
	public void startTest(final ITestContext testContext) {
	
		iTestStartTime = System.currentTimeMillis();
		application=testContext.getCurrentXmlTest().getParameter("CurrentApplication").trim();
		chartName=configProps.getProperty("ReleaseVersion")+"ExecutionStatus";
		failCounter=0;
		passCounter=0;
		serialNo=0;
		folderName=testContext.getName().trim();
		System.out.println(folderName); // it prints "Check name test"
	}
	
	/**
	 * Closing the SummaryReport
	 * @throws IOException
	 */
	
	@AfterTest
	public void closeTest() throws IOException {

		ReportStampSupport.calculateTestExecutionTime();
		
		if(configProps.getProperty("ChartType").trim().equalsIgnoreCase("Pie")||configProps.getProperty("ChartType").trim().contains("Pie")||configProps.getProperty("ChartType").trim().contains("pie"))
			createPieChartForReport(chartName,passCounter,failCounter, skipCounter);
		else
			System.out.println("Currently Supports only Pie Graph");
			//createBarChartForReport(chartName, passCounter, failCounter, 0);		
		
		closeSummaryReport(browser);
		writer.close();
		
	}

	
	/**
	 * Used to Create Pie Chart for Report
	 * @param suiteName
	 * @param passCount
	 * @param failCount
	 * @param skipCount
	 */
	 public void createPieChartForReport(String chartName,int passCounter,int failCounter, int skipCounter) throws IOException
	   {
		   
	      DefaultPieDataset dataset = new DefaultPieDataset( );
	      
	      //Data Set Values
	      dataset.setValue("Pass "+passCounter, new Double( passCounter ) );
		  dataset.setValue("Fail "+failCounter, new Double( failCounter ) );
	      dataset.setValue("Skip "+skipCounter, new Double( skipCounter ) );
	     
	      JFreeChart chart = ChartFactory.createPieChart(chartName ,dataset,true,true,true);
	  
	     // LegendTitle legend = chart.getLegend();
	      //legend.setFrame(new BlockBorder(Color.white));
	      
	      PiePlot ColorConfigurator = (PiePlot) chart.getPlot();
	      ColorConfigurator.setSimpleLabels(true);
          ColorConfigurator.setLabelLinksVisible(true);
          ColorConfigurator.setSectionOutlinesVisible(false);
          ColorConfigurator.setShadowPaint(null);
          ColorConfigurator.setLabelGenerator(null);
       
          
          //Percentage Values
          ColorConfigurator.setLabelGenerator(new StandardPieSectionLabelGenerator("{2}"));

          //Customized Colors
	      Color pass_color = new Color(200,57,35);
	      Color fail_color = new Color(231,222,21); 
	      Color skip_color = new Color(83,180,29); 

	      ColorConfigurator.setSectionPaint("Skip "+skipCounter, fail_color);
          ColorConfigurator.setSectionPaint("Fail "+failCounter, pass_color);
          ColorConfigurator.setSectionPaint("Pass "+passCounter, skip_color);
	      
	      int width = 400;
	      int height =400; 
	      try {
			ChartUtilities.saveChartAsJPEG( new File(TestSetup.filePath()+"\\Screenshots\\"+chartName+".jpg") , chart , width , height );
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	
	   }	
	
}
