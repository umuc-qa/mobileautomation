package com.project.report.Accelerators;

import org.openqa.selenium.By;

/**
 * UserCredentialBase :: It acts as UserCredentialBase for both SFDC and PEGA. 
 * 
 * @author 
 * 
 *   		Date 			Author			ChangesIncorporated
 * -----------------------------------------------------------------------------------
 * 		03/25/2015			os58325			Provided comments for all the methods.  
 *
 */
public class UserCredentialBase {

	public  By UserName;
	public  By Password;
	public  By LoginButton;
	public  By LogoutButton;
	
}
