package com.project.report.Utilities;

import java.io.File;
import java.util.HashMap;

import org.apache.bcel.classfile.Method;

import com.project.report.Accelerators.Configuration;
import com.project.setup.TestSetup;

import jxl.LabelCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Data_Provider {
	

	//public static Property configProps=new Property("config.properties");
	/**
	 * Read data from excel sheet using data provider
	 * 
	 * @param sheetName
	 * 
	 * @throws Exception
	 * @Date  19/02/2013
	 * @Revision History
	 * 
	 */
	
	

	public static String[][] getTableArray(String sheetName) throws Exception{
		try{

			Workbook workbook = Workbook.getWorkbook(new File("Data\\"+Configuration.GetProperty("CurrentApplication")+"\\TestData.xls"));
 
			Sheet sheet = workbook.getSheet(sheetName);
			int rows = sheet.getRows();
		//	System.out.println("---["+rows);
			int cols = sheet.getColumns();
			
			
			
			String[][] tabArray=new String[rows-1][cols];
			for (int i=1;i<rows;i++){
				for (int j=0;j<cols;j++){
					tabArray[i-1][j]=sheet.getCell(j,i).getContents();
				}
			}
			System.out.println("");

			workbook.close();
			return(tabArray);
		}
		catch (Exception e) {
			System.out.println(e+Thread.currentThread().getStackTrace()[1].getClassName()+" dataprovider");
			return null;
		}

	}
	public static HashMap<String,String> getDataFromExcel(String sheetName,String key) {
		try{
			HashMap<String,String> data=new HashMap<String,String>();
			Workbook workbook = Workbook.getWorkbook(new File("Data\\dvtr\\TestData.xls"));
			String[]  tdata=null;
			Sheet sheet = workbook.getSheet(sheetName);
			int rows = sheet.getRows();
			int cols = sheet.getColumns(); 
			String[]  tabHeader=new String[cols];
			for (int i=0;i<cols;i++){
				  tabHeader[i]=sheet.getCell(i,0).getContents();
			}
			
			for (int i=1;i<rows;i++){ 
				if(sheet.getCell(0,i).getContents().equalsIgnoreCase(key)){ //matching key;
					tdata=new String[cols];
				     for (int j=0;j<cols;j++){ 
				    	 	if(sheet.getCell(j,i).getContents().equalsIgnoreCase("n/a")){
				    	 		tdata[j]="";
				    	 	}else{
				    	 		tdata[j]=sheet.getCell(j,i).getContents();
					 	}
				    	 	Method method =new Method();
					    	System.out.println(method.getName());
					}
				    break; 
					}
				}
			 for(int i=0;i<tabHeader.length;i++){
				 data.put(tabHeader[i], tdata[i]);
			  }

			workbook.close();
			return data;
		}
		catch (Exception e) {
			System.out.println("-----------");
			System.out.println(e+Thread.currentThread().getStackTrace()[1].getClassName()+" dataprovider");
			return null;
		}

	}
	
	public static void setTableData_dummy(String sheetName,String uname,String text,int offset) throws Exception
	{
		Workbook work = Workbook.getWorkbook(new File("Data\\TestData.xls"));
		WritableWorkbook writer = Workbook.createWorkbook(new File("TestData\\TestData.xls"),work);
		//Workbook workbook = Workbook.getWorkbook(new File("TestData\\TestData.xls"));
		WritableSheet sheet = writer.getSheet(sheetName);
		LabelCell cell = sheet.findLabelCell(uname);
		//Cell cell = sheet.findCell(uname, 0, 0, 1000, 1000, false);
		
		sheet.addCell(new jxl.write.Label(cell.getColumn()+offset,cell.getRow() ,text));
		
		writer.write();
		writer.close();
		work.close();
	}
	
	public static void setTableData(String sheetName,String assClass,String clsNumber,int offset) throws Exception
	{
		String filePath=System.getProperty("user.dir")+ "\\Data\\"+Configuration.GetProperty("CurrentApplication")+"\\TestData.xls";
		Workbook work = Workbook.getWorkbook(new File(filePath));
		WritableWorkbook writer = Workbook.createWorkbook(new File(filePath),work);
		//Workbook workbook = Workbook.getWorkbook(new File("TestData\\TestData.xls"));
		WritableSheet sheet = writer.getSheet(sheetName);
		//LabelCell cell = sheet.findLabelCell(uname);
		//Cell cell = sheet.findCell(uname, 0, 0, 1000, 1000, false);
		Label label1 = new Label(6, offset, assClass);
		Label label2 = new Label(7, offset, clsNumber);
		
		//sheet.addCell(new jxl.write.Label(cell.getColumn()+offset,cell.getRow() ,text));
		sheet.addCell(label1);
		sheet.addCell(label2);
		
		writer.write();
		writer.close();
		work.close();
	}
	
	public static HashMap<String,String> getTestData(String sheetName, String executeTCName) throws Exception{//, String executeTCName, String executeStatus
		try{
			System.out.println(TestSetup.application);
			HashMap<String,String> data=new HashMap<String,String>();
			//String filePath=System.getProperty("user.dir")+ "\\Data\\"+TestEngine.application+"\\TestData.xls";
			String filePath=System.getProperty("user.dir")+File.separator+"Data"+File.separator+TestSetup.application+File.separator+"TestData.xls";
			Workbook workbook = Workbook.getWorkbook(new File(filePath));
			String[]  tdata=null;
			Sheet sheet = workbook.getSheet(sheetName);
			int rows = sheet.getRows();
			int cols = sheet.getColumns(); 
			String[]  tabHeader=new String[cols];
			for (int i=0;i<cols;i++){
				  tabHeader[i]=sheet.getCell(i,0).getContents();
			}
			
			for (int i=1;i<rows;i++){ 
				//if(sheet.getCell(0,i).getContents().equalsIgnoreCase("Yes") && sheet.getCell(1,i).getContents().equalsIgnoreCase(executeTCName)){ //matching key;
					tdata=new String[cols];
					
				     for (int j=0;j<cols;j++){
				    	 
				    	 	if(sheet.getCell(j,i).getContents().equalsIgnoreCase("n/a")){
				    	 		tdata[j]="";
				    	 	}else{
				    	 		tdata[j]=sheet.getCell(j,i).getContents();
				    	 	}
				    	 	
				    	
					}
				     
				    break; 
					//}
				/*else{
						TestEngine te=new TestEngine();
						te.onWarning(executeTCName ," -- Skipped test case.");

					}*/
				}
			 for(int i=0;i<tabHeader.length;i++){
				 data.put(tabHeader[i], tdata[i]);
			  }

			workbook.close();
			return data;
			
			
			
			/*HashMap<String,String> data=new HashMap<String,String>();
			Workbook workbook = Workbook.getWorkbook(new File("Data\\"+Configuration.GetProperty("CurrentApplication")+"\\TestData.xls"));
 
			Sheet sheet = workbook.getSheet(sheetName);
			int rows = sheet.getRows();
		//	System.out.println("---["+rows);
			int cols = sheet.getColumns();
			
			
			
			String[][] tabArray=new String[rows-1][cols];
			for (int i=1;i<rows;i++){
				for (int j=0;j<cols;j++){
					tabArray[i-1][j]=sheet.getCell(j,i).getContents();
				}
			}
			System.out.println("");

			workbook.close();
			return(tabArray);*/
		}
		catch (Exception e) {
			System.out.println(e+Thread.currentThread().getStackTrace()[1].getClassName()+" dataprovider");
			return null;
		}

	}
	


}




/*if(i==rows){
TestEngine te=new TestEngine();
te.onWarning(executeTCName ," -- Skipped test case.");					
}*/

/*for(int i=0;i<tabHeader.length;i++){
data.put(tabHeader[i], tdata[i]);
}*/
