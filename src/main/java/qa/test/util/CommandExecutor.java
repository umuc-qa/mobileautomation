package qa.test.util;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import static sun.font.FontUtilities.isWindows;

/**
 * Created by e001237 on 27/04/17.
 */
public class CommandExecutor implements Runnable {
    private InputStream inputStream;
    private Consumer<String> consumer;

    public CommandExecutor(InputStream inputStream, Consumer<String> consumer) {
        this.inputStream = inputStream;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        new BufferedReader(new InputStreamReader(inputStream)).lines()
                .forEach(consumer);
    }

    /**
     *Execute Shell Command.
     */
    public static String execCommand(String setCommand) {

            StringBuffer output = new StringBuffer();
            ProcessBuilder builder = new ProcessBuilder();
            if (isWindows) {
                builder.command("cmd.exe", "/c", setCommand);
            } else {
                builder.command("sh", "-c", setCommand);
            }
            try {
                builder.directory(new File(System.getProperty("user.home")));
                Process process = builder.start();

                CommandExecutor commandGobbler = new CommandExecutor(process.getInputStream(), System.out::println);
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

                String line = "";
                while ((line = reader.readLine()) != null) {
                    output.append(line + "\n");
                }

                List<String> lines = Arrays.asList(output.toString().split(System.getProperty("line.separator")));
                Executors.newSingleThreadExecutor().submit(commandGobbler);

                int exitCode = process.waitFor();
//                assert exitCode == 0;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return output.toString();
    }

    public static String executeCommand2(String command) {

//        String getRemoteStatus = client.getDeviceProperty("device.remote");
//        if (getRemoteStatus.equalsIgnoreCase("false")) {
            StringBuffer output = new StringBuffer();

            Process proc;
            try {
                proc = Runtime.getRuntime().exec(command);
                proc.waitFor();
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(proc.getInputStream()));

                String line = "";
                while ((line = reader.readLine()) != null) {
                    output.append(line + "\n");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return output.toString();

//        } else {
//            return client.run(command);
//        }
    }

    public static void appiumRun() {
//        String AppiumNodeFilePath = "/usr/local/lib/node_modules/npm/bin/npm-cli.js";
        String AppiumNodeFilePath = "";
                String AppiumJavaScriptServerFile = "/usr/local/lib/node_modules/appium/lib/Appium.js";
        String AppiumServerConfigurations = " -a 127.0.0.1 -p 4723 --bootstrap-port 4728 -U emulator-5554 --nodeconfig \"run_node1.json\"";
        executeCommand("/usr/local/bin/node \" /Applications/Appium.app/Contents/Resources/node/bin/node\" /usr/local/bin/appium \"/Applications/Appium.app/Contents/Resources/node_modules/appium/lib/appium.js\" -a 127.0.0.1 -p 4723 --bootstrap-port 4728 -U emulator-5554 --nodeconfig \"run_node1.json\"");

    }

    public static void executeCommand(String command) {
        String s = null;

        try {
            Process p = Runtime.getRuntime().exec(command);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // read the output from the command
            System.out.println("Standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }

            // read any errors from the attempted command
            System.out.println("Standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        } catch (IOException e) {
            System.out.println("exception happened: ");
            e.printStackTrace();
        }
    }
}
