package qa.test.util;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import qa.test.util.TestNGParallelTestBase;

import java.lang.reflect.Method;

/**
 * Test base for TestNG tests.
 */
public abstract class TestNGParallelClassesTestBase extends TestNGParallelTestBase
{
    @BeforeMethod
    public void setupBeforeMethod(Method method)
    {
        setTestName(generateTestNameByClass(method));
        setTestDescription(method.getAnnotation(Test.class).description());
        extentTest = ExtentTestManager.startTest(getTestName(), getTestDescription());
        extentTest.log(LogStatus.UNKNOWN, "Initialized test class <i>" + canonicalClassName + "</i>");
        extentTest.log(LogStatus.INFO, "Starting test with name <b>" + getTestName() + "</b>");
    }

    @AfterMethod
    public void teardownAfterMethod(ITestResult testResult)
    {
        logTestResult(extentTest, testResult);
        ExtentTestManager.endTest();
    }



}
