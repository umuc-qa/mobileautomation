package qa.test.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.*;

public class DebugTestNGListenerOrder implements ITestListener, ISuiteListener, IExecutionListener, IInvokedMethodListener
{
    private static final Logger logger = LogManager.getLogger(DebugTestNGListenerOrder.class.getName());

    @Override
    public void onExecutionStart()
    {
        logger.info("IExecutionListener.onExecutionStart: Invoked before the TestNG run starts.");
    }

    @Override
    public void onStart(ISuite suite)
    {
        logger.info("ISuiteListener.onStart: This method is invoked before the SuiteRunner starts.");
    }

    @Override
    public void onStart(ITestContext context)
    {
        logger.info("ITestListener.onStart: Invoked after the test class is instantiated and before any configuration method is called.");
    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult)
    {
        logger.info("IInvokedMethodListener.beforeInvocation: Invoked right before test method.");
    }

    @Override
    public void onTestStart(ITestResult result)
    {
        logger.info("ITestListener.onTestStart: Invoked each time before a test will be invoked.");
    }

    @Override
    public void onTestSuccess(ITestResult result)
    {
        logger.info("ITestListener.onTestSuccess: Invoked each time a test succeeds.");
    }

    @Override
    public void onTestFailure(ITestResult result)
    {
        logger.info("ITestListener.onTestFailure: Invoked each time a test fails.");
    }

    @Override
    public void onTestSkipped(ITestResult result)
    {
        logger.info("ITestListener.onTestSkipped: Invoked each time a test is skipped.");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result)
    {
        logger.info("ITestListener.onTestFailedButWithinSuccessPercentage : Invoked each time a method fails but has been annotated with successPercentage and this failure still keeps it within the success percentage requested.");
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult)
    {
        logger.info("IInvokedMethodListener.afterInvocation: Invoked right after test method.");
    }

    @Override
    public void onFinish(ITestContext context)
    {
         logger.info("ITestListener.onFinish: Invoked after all the tests have run and all their Configuration methods have been called.");
    }

    @Override
    public void onFinish(ISuite suite)
    {
        logger.info("ISuiteListener.onFinish: This method is invoked after the SuiteRunner has run all the test suites.");
    }

    @Override
    public void onExecutionFinish()
    {
        logger.info("IExecutionListener.onExecutionFinish: Invoked once all the suites have been run.");
    }

}
